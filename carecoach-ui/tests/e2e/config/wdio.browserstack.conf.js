const { config } = require("./wdio.shared.conf")

exports.config = {
  ...config,
  ...{
    user: process.env.BROWSERSTACK_USERNAME,
    key: process.env.BROWSERSTACK_ACCESS_KEY,
    services: ["browserstack"],

    capabilities: [
      {
        maxInstances: 1,
        "bstack:options": {
          os: "OS X",
          osVersion: "Big Sur",
          local: "false",
          networkLogs: "true",
        },
        browserName: "Chrome",
        browserVersion: "latest",
      },

      // {
      //     maxInstances: 1,
      //     'bstack:options': {
      //         "os": "OS X",
      //         "osVersion": "Big Sur",
      //         "local": "false",
      //         "networkLogs": "true",
      //     },
      //     browserName: "Firefox",
      //     browserVersion: "latest",
      // },

      {
        maxInstances: 1,
        "bstack:options": {
          os: "OS X",
          osVersion: "Big Sur",
          local: "false",
          networkLogs: "true",
        },
        browserName: "Safari",
        browserVersion: "14.0",
      },
    ],
  },
}
