/* eslint-disable func-names, no-unused-expressions */
import { When } from "cucumber"
import util from "../utilities/util"
import loginPage from "../pageobjects/loginPage"
import carecoachDashboardPage from "../pageobjects/carecoachDashboardPage"
import changePasswordPage from "../pageobjects/changePasswordPage"
import profilePage from "../pageobjects/profilePage"
import myCaregiversPage from "../pageobjects/myCaregiversPage"
import carePlanPage from "../pageobjects/carePlanPage"
import notificationsPage from "../pageobjects/notificationsPage"
import existingCarePlanPage from "../pageobjects/existingCarePlanPage"
import progressCheckPage from "../pageobjects/progressCheckPage"

When(/^I enter username as "([^"]*)"$/, function (uname) {
  const username = util.decryptedText(util.getUserDetails().username)
  if (uname === "actualusername") {
    loginPage.enterUsername(username)
    loginPage.usernameInput.getValue().should.equal(username)
  } else {
    loginPage.enterUsername(uname)
    loginPage.usernameInput.getValue().should.equal(uname)
  }
})

When(/^I enter password as "([^"]*)"$/, function (pwd) {
  const password = util.decryptedText(util.getUserDetails().password)
  if (pwd === "actualpassword") {
    loginPage.enterPassword(password)
  } else {
    loginPage.enterPassword(pwd)
  }
})

When(/^I attempt to login$/, function () {
  loginPage.clickOnSignButton()
})

When(
  /^I click on "([^"]*)" link on the care coach home page$/,
  function (link) {
    carecoachDashboardPage.clickOnMenuLink()
    if (link === "Profile") {
      carecoachDashboardPage.clickOnProfileLink()
    } else if (link === "Sign Out") {
      carecoachDashboardPage.clickOnSignOutLink()
    }
  }
)

When(/^I click on "([^"]*)" button on my profile page$/, function (buttonText) {
  if (buttonText === "Change my password") {
    profilePage.clickOnChangePasswordButton()
  } else if (buttonText === "Edit") {
    profilePage.clickOnEditButton()
  } else if (buttonText === "profile headshot edit") {
    profilePage.clickOnEditProfileheadshotButton()
  }
})

When(/^I click on cancel button on change my password page$/, function () {
  changePasswordPage.clickOnCancelButton()
})

When(/^I enter "([^"]*)" on change my password page$/, function (text) {
  const password = util.decryptedText(util.getUserDetails().password)
  const tempPassword = util.decryptedText(util.getUserDetails().tempPassword)
  if (text === "Current Password") {
    changePasswordPage.enterCurrentPassword(password)
  } else if (text === "New Password") {
    changePasswordPage.enterNewPassword(tempPassword)
  }
})

When(
  /^I click on "([^"]*)" button on "([^"]*)" page$/,
  function (buttonText, pageText) {
    if (pageText === "Change my password") {
      changePasswordPage.clickOnChangePasswordButton()
    } else if (pageText === "Edit my profile") {
      profilePage.clickOnSaveChangesButtonOnEditMyProfile()
    }
  }
)

When(
  /^I enter incorrect "([^"]*)" on change my password page$/,
  function (text) {
    // here, we providing username as current password,to test incorrect current password testing
    const password = util.decryptedText(util.getUserDetails().username)
    if (text === "Current Password") {
      changePasswordPage.enterCurrentPassword(password)
    }
  }
)

When(/^I update care coach info on my profile page$/, function () {
  profilePage.updateDataOnEditMyProfilePage()
})

When(
  /^I update "([^"]*)" care coach headshot on my profile page$/,
  function (headshotType) {
    profilePage.updateHeadshotWith(headshotType)
  }
)

When(
  /^I should click on "([^"]*)" menu from left sidebar$/,
  function (leftMenu) {
    carecoachDashboardPage.clickOnCareCoachesLeftSideBarMenu(leftMenu)
  }
)

When(/^I click on any random caregivers from the list$/, function () {
  myCaregiversPage.clickOnRandomCaregiverFromList()
})

When(
  /^I enter any random character in search box on my caregivers$/,
  function () {
    myCaregiversPage.enterRandomCharacterOnSearchField()
  }
)

When(/^I click on "([^"]*)" button$/, function (buttonTxt) {
  carePlanPage.clickOnSpecifiedButtonOnCarePlan(buttonTxt)
})

When(/^I select random template and apply$/, function () {
  carePlanPage.selectRandomTemplateAndApply().should.be.true
})

When(/^I select random milestone and perform undo$/, function () {
  carePlanPage.selectRandomMilestoneAndUndo().should.be.true
})

When(
  /^I click on caregiver from the list who does not have created care plan$/,
  function () {
    myCaregiversPage.clickOnSpecificCaregiverFromList(1)
  }
)

When(
  /^I click on caregiver from the list who have already created care plan$/,
  function () {
    myCaregiversPage.clickOnSpecificCaregiverFromList(0)
  }
)

When(/^I select random milestone from the list$/, function () {
  carePlanPage.clickOnRandomMSFromRightSideList().should.be.true
})

When(
  /^I click on back stepper and should see warning drawer with nevermind and continue button$/,
  function () {
    carePlanPage.clickOnStepperAndVerifyWarningDrawerWithButtons().should.be
      .true
  }
)

When(/^I click on "([^"]*)" button on warning drawer$/, function (buttonText) {
  carePlanPage.clickOnWarningDrawerButton(buttonText)
})

When(/^I click on notification icon$/, function () {
  notificationsPage.clickOnNotificationsIcon()
})

When(/^I close the notification channel$/, function () {
  notificationsPage.closeNotificationChannel().should.be.true
})
When(/^I should remove all the added milestones from right side$/, function () {
  carePlanPage.removeAllMSFromRightSideList().should.be.true
})

When(
  /^I select random milestone from the upcoming milestone list$/,
  function () {
    existingCarePlanPage.clickOnRandomMSFromUpcomingMilestoneList()
  }
)
When(/^I select random milestones from the list$/, function () {
  existingCarePlanPage.selectRandomMSFromSearchMilestones().should.be.true
})
When(
  /^I enter any character in search milestones field and select random milestone from the list and click on add milestone$/,
  function () {
    existingCarePlanPage.searchAndSelectRandomMSFromSearchMilestones().should.be
      .true
  }
)

When(
  /^I click on caregiver from the list who have already submitted the mini survey$/,
  function () {
    myCaregiversPage.clickOnSpecificCaregiverFromList(1)
  }
)
When(/^I click on recently submitted survey$/, function () {
  progressCheckPage.clickOnRecentlySubmittedSurvey()
})
When(/^I click on "([^"]*)" link from left sidebar$/, function (linkText) {
  progressCheckPage.clickOnProgressLink()
})
When(
  /^I click on caregiver from the list who has not submitted the mini survey$/,
  function () {
    myCaregiversPage.clickOnSpecificCaregiverFromList(0)
  }
)
When(/^I click on stepper two button on care plan$/, function () {
  carePlanPage.clickOnStepperTwoOnCarePlan()
})
