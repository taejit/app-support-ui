/* eslint-disable func-names, no-unused-expressions */
import { Then } from "cucumber"
import util from "../utilities/util"
import loginPage from "../pageobjects/loginPage"
import loginErrorPage from "../pageobjects/loginErrorPage"
import forbiddenPage from "../pageobjects/forbiddenPage"
import carecoachDashboardPage from "../pageobjects/carecoachDashboardPage"
import changePasswordPage from "../pageobjects/changePasswordPage"
import profilePage from "../pageobjects/profilePage"
import myCaregiversPage from "../pageobjects/myCaregiversPage"
import carePlanPage from "../pageobjects/carePlanPage"
import notificationsPage from "../pageobjects/notificationsPage"
import existingCarePlanPage from "../pageobjects/existingCarePlanPage"
import progressCheckPage from "../pageobjects/progressCheckPage"
import quoteOfTheDayPage from "../pageobjects/quoteOfTheDayPage"

Then(/^I should see cognito login form fields$/, function () {
  loginPage.validateUIFields().should.be.true
})

Then(/^I should see login error message as "([^"]*)"$/, function (message) {
  loginErrorPage.getErrorMessage().should.equal(message)
  browser.pause(3000)
})

Then(/^I should see care coach home page$/, function () {
  carecoachDashboardPage.getCareCoachDashboard().should.be.true
})

Then(/^I should see "([^"]*)" page$/, function (text) {
  if (text === "Forbidden") {
    forbiddenPage.getForbiddenLabel().should.equal(text)
  } else if (text === "My Profile") {
    profilePage.getMyProfileHeadeing().should.equal(text)
  } else if (text === "Edit my profile") {
    profilePage.getEditMyProfileHeadeing().should.equal(text)
    profilePage.verifyViewProfileAndEditProfileData().should.be.true
  } else if (text === "Change my password") {
    changePasswordPage.getHeading().should.equal(text)
  }
})

Then(/^I should see forbidden message as "([^"]*)"$/, function (message) {
  forbiddenPage.getForbiddenMessage().should.equal(message)
})

Then(
  /^I should be successfully logged out and land on the login page$/,
  function () {
    loginPage.validateUIFields().should.be.true
  }
)

Then(
  /^I should see "([^"]*)" message at the bottom on "([^"]*)" page$/,
  function (message, pageName) {
    if (pageName === "My Profile") {
      profilePage.verifyEditProfileDataAndViewProfile().should.be.true
    } else if (pageName === "Change my password") {
      changePasswordPage.getSuccessMessgae().should.equal(message)
    }
    browser.pause(3000)
  }
)

Then(/^swap passwords in config file$/, function () {
  const { username } = util.getUserDetails()
  const { password } = util.getUserDetails()
  const { tempPassword } = util.getUserDetails()
  changePasswordPage.swapPasswords(password, tempPassword, username)
})

Then(
  /^I should see error message "([^"]*)" for incorrect current password$/,
  function (msg) {
    changePasswordPage.getErrorMessage().should.equal(msg)
    browser.pause(3000)
  }
)

Then(/^I should see care coach info on my Profile Page$/, function () {
  profilePage.validateUIFieldsOnProfilePage().should.be.true
})

Then(
  /^I should see "([^"]*)" message at the bottm on "([^"]*)" page$/,
  function (message, PageText) {
    if (PageText === "My Profile") {
      profilePage.getMessageOnMyProfile().should.equal(message)
    } else if (PageText === "Edit my profile picture") {
      profilePage.getMessageOnEditMyProfilePicture().should.equal(message)
    }
  }
)

Then(/^I should see My Caregivers page and related information$/, function () {
  myCaregiversPage.getMyCareGiversPageToLoad().should.equal("My Caregivers")
  myCaregiversPage.validateMyCaregiversInformation().should.be.true
})

Then(
  /^I should see Caregivers Care Plan page and related information$/,
  function () {
    myCaregiversPage.validateCaregiversCarePlanInformation().should.be.true
  }
)

Then(/^I should see empty state page$/, function () {
  myCaregiversPage.validateEmptyStatePage().should.be.true
})

Then(
  /^I should see matched caregivers or nothing found page if no matched caregivers or conditions$/,
  function () {
    myCaregiversPage.validateMatchedCaregiversOrNothingFound().should.be.true
  }
)

Then(/^I should see Caregivers profile information$/, function () {
  myCaregiversPage.validateCaregiversProfileInformation().should.be.true
})

Then(
  /^I should able to add notes for caregiver and able to see the added notes$/,
  function () {
    myCaregiversPage.addAndViewNotesForCaregivers().should.be.true
  }
)

Then(
  /^I should able to edit random note for caregiver and able to see the updated notes$/,
  function () {
    myCaregiversPage.editRandomNoteAndViewNoteForCaregivers().should.be.true
  }
)

Then(
  /^I should able to remove random note for caregiver and verify the note is deleted for the caregiver$/,
  function () {
    myCaregiversPage.removeRandomNoteAndVerifyNoteDeletedForCaregiver().should
      .be.true
  }
)

Then(
  /^I enter any random character in search templates field and verify the result$/,
  function () {
    carePlanPage.enterRandomCharacterOnSearchTemplateAndVerifyResult().should.be
      .true
  }
)

Then(
  /^I select multiple milestones and verify milestones added on milestones listing$/,
  function () {
    carePlanPage.selectMultipleMilestonesAndVerifyResult().should.be.true
  }
)

Then(
  /^I enter any random character in search milestones field and verify the result$/,
  function () {
    carePlanPage.enterRandomCharacterOnSearchMilestoneAndVerifyResult().should
      .be.true
  }
)

Then(/^I click on cancel button on edit MS Drawer$/, function () {
  carePlanPage.clickOnCancelButtonOnEditMSDrawer().should.be.true
})

Then(
  /^I edit overview section and perform delete and add tasks and save changes$/,
  function () {
    carePlanPage.editOverviewSectionAndPerformDeleteAddTasksAndSaveChanges()
      .should.be.true
  }
)

Then(/^I should verify the changes on milestone listing$/, function () {
  carePlanPage.verifySavedChangesOnMilestoneList().should.be.true
})

Then(/^I click on remove button to delete milestone$/, function () {
  carePlanPage.clickOnRemoveButtonToDeleteMS().should.be.true
})

Then(
  /^I should verify the deleted milestone is removed from milestone listing$/,
  function () {
    carePlanPage.verifyDeletedMSRemovedOnMilestoneList().should.be.true
  }
)

Then(/^I should be on same milestone list page$/, function () {
  carePlanPage.verifyCustomizeMSLeftAndRightSideMSList().should.be.true
})

Then(/^I should be on start with a template page$/, function () {
  carePlanPage.verifyStartingTemplateDrawerAndEmptyStateForMS().should.be.true
})

Then(
  /^I entered details on overview section and perform add delete and add tasks and save milestone$/,
  function () {
    carePlanPage.enterOverviewSectionAndPerformAddDeleteAddTasksAndSaveChanges()
      .should.be.true
  }
)

Then(
  /^I should see either empty state for notifcation or list of receievd notifications$/,
  function () {
    notificationsPage.verifyResultOfNotificationChannel().should.be.true
  }
)

Then(
  /^I should see badge on notifcation icon if any notification there$/,
  function () {
    notificationsPage.verifyBadgeOnNotificationIcon().should.be.true
  }
)

Then(/^I should not see any badge on notification icon$/, function () {
  notificationsPage.verifyNoBadgeOnNotificationIconIfVisitedNC().should.be.true
})

Then(/^I should see continue button is disable$/, function () {
  carePlanPage.verifyContinueButtonDisabled().should.be.true
})

Then(/^I should see drawer for options to add milestones$/, function () {
  existingCarePlanPage.verifyOptionsToAddMilestonesDrawer().should.be.true
})

Then(
  /^I should verify the changes on "([^"]*)" section of existing care plan page$/,
  function (section) {
    existingCarePlanPage.verifySavedChangesOnSpecifiedSectionOfExistingCarePlan(
      section
    ).should.be.true
  }
)
Then(
  /^I should verify the deleted milestone is removed from upcoming section of existing care plan$/,
  function () {
    existingCarePlanPage.verifyDeletedMSRemovedFromUpcomingSectionOfExistingCarePlan()
      .should.be.true
  }
)
Then(/^I remove milestones from the selected bottom list$/, function () {
  existingCarePlanPage.verifyRemovedMilestonesFromBottomSection().should.be.true
})

Then(/^I should see landing page$/, function () {
  myCaregiversPage.validateIndividualCaregiverInfoWithCaregiversListingData()
})

Then(/^I should see and verify caregivers progress$/, function () {
  progressCheckPage.verifyCaregiversProgress().should.be.true
})

Then(
  /^I should see right drawer and verify submitted date and score$/,
  function () {
    progressCheckPage.verifySubmittedDateAndScoreOnDrawer().should.be.true
  }
)
Then(/^I should verify empty state of mini survey$/, function () {
  progressCheckPage.verifyEmptyStateOfMiniSurvey().should.be.true
})
Then(/^I should verify review and finish page of care plan$/, function () {
  carePlanPage.verifyReviewAndFinishPage().should.be.true
})
Then(/^I should verify customize milestones page of care plan$/, function () {
  carePlanPage.verifyCustomizeMilestonePage().should.be.true
})
Then(/^I should see daily quote and its related imformation$/, function () {
  quoteOfTheDayPage.verifyDailyQuoteAndItsRelatedInfo().should.be.true
})
