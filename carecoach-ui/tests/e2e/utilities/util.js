const CryptoJS = require("crypto-js")

const encryptedBase64Key = "bXVzdGJlMTZieXRlc2tleQ=="
const parsedBase64Key = CryptoJS.enc.Base64.parse(encryptedBase64Key)
const filePath = "tests/e2e/config/user.conf.json"
const fs = require("fs")

const phoneArray = [
  5042010052,
  2029674478,
  2029674530,
  4047772394,
  2027953213,
  2014222730,
  2014222656,
  2243238312,
]

class Util {
  generateName(length) {
    let result = ""
    const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    const charactersLength = characters.length
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength))
    }
    return result
  }

  generateNumber(length) {
    return Math.floor(
      10 ** (length - 1) + Math.random() * 9 * 10 ** (length - 1)
    )
  }

  checkValueExistInColumn(columnList, textToBeVarified) {
    let result = false
    for (let i = 0; i < columnList.length; i++) {
      if (columnList[i].getText().equal(textToBeVarified)) {
        result = true
        break
      }
    }
    return result
  }

  clearValue(inputElement) {
    var CTRL_CMD = process.platform == "darwin" ? "Command" : "Control"
    inputElement.click()
    inputElement.keys([CTRL_CMD, "a"])
    inputElement.keys("\uE003")
    browser.pause(500) // backspace
  }

  getUserDetails() {
    const data = fs.readFileSync(filePath, { encoding: "utf8", flag: "r" })
    return JSON.parse(data)
  }

  getAnyPhoneFromList() {
    return phoneArray[Math.floor(Math.random() * phoneArray.length)]
  }

  getAnyPhoneFromListExceptExistingPhone(phone) {
    const phn = phoneArray[Math.floor(Math.random() * phoneArray.length)]
    if (phn === phone) {
      return this.getAnyPhoneFromListExceptExistingPhone(phone)
    }
    return phn
  }

  checkDataNotEquals(fields, value) {
    if (fields.getText().equal(value)) {
      return false
    }
    return true
  }

  selectValueFromDropDown(field, value) {
    try {
      field.selectByIndex(value)
    } catch (err) {
      field.selectByIndex(0)
    }
  }

  encryptedData(plaintText) {
    return CryptoJS.AES.encrypt(plaintText, parsedBase64Key, {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7,
    })
  }

  decryptedText(encryptedCipherText) {
    const decryptedData = CryptoJS.AES.decrypt(
      encryptedCipherText,
      parsedBase64Key,
      {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7,
      }
    )
    return decryptedData.toString(CryptoJS.enc.Utf8)
  }

  checkNamesInAlphaOrder(elements) {
    let arr = []
    let sorted = true
    elements.forEach((el) => {
      arr.push(el.getText())
    })

    for (let i = 0; i < arr.length - 1; i++) {
      if (arr[i] > arr[i + 1]) {
        sorted = false
        break
      }
    }
    return sorted
  }

  checkMSInAlphaOrder(arr) {
    let sorted = true
    for (let i = 0; i < arr.length - 1; i++) {
      if (arr[i] > arr[i + 1]) {
        sorted = false
        break
      }
    }
    return sorted
  }

  checkValidNames(elements) {
    let arr = []
    let isValidName = true
    var regEx = /^([a-zA-Z]*(\s)*[\.]*)*$/
    elements.forEach((el) => {
      arr.push(el.getText())
    })

    for (let i = 0; i < arr.length - 1; i++) {
      if (arr[i].match(regEx)) {
      } else {
        isValidName = false
        break
      }
    }
    return isValidName
  }

  checkValidContactStatus(elements) {
    let conStatus = true
    let contactStatus = []
    elements.forEach((el) => {
      contactStatus.push(el.getText())
    })
    for (let i = 0; i < contactStatus.length; i++) {
      if (
        !(
          contactStatus[i].startsWith("Call today at") ||
          contactStatus[i].startsWith("Last contacted") ||
          contactStatus[i] === "Not contacted yet!"
        )
      ) {
        conStatus = false
        break
      }
    }
    return conStatus
  }

  generateTitle() {
    let result = this.generateName(6) + " " + this.generateName(4)
    return result
  }

  generateBody() {
    let result = this.generateName(30) + " " + this.generateName(20)
    return result
  }
}

export default new Util()
