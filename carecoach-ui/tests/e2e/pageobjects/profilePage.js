/* eslint-disable no-unused-expressions */
import { LocalStorage } from "node-localstorage"
import Page from "./page"
import util from "../utilities/util"

const fs = require("fs")
const path = require("path")

const parentDir = path.dirname(__dirname)

class ProfilePage extends Page {
  /**
   * define elements for my profile Page
   */

  get myProfileHeader() {
    return $("//div[text()='My Profile']")
  }

  get editMyProfileHeader() {
    return $("//div[text()='Edit my profile']")
  }

  get aboutYouLabel() {
    return $("//div[@id='aboutyoutitle']")
  }

  get whatYouDoLabel() {
    return $("//div[@id='whatyoudotitle']")
  }

  get nameOnAboutYou() {
    return $("//td[@id='name']")
  }

  get pronounsOnAboutYou() {
    return $("//td[@id='pronouns']")
  }

  get emailOnAboutYou() {
    return $("//td[@id='email']")
  }

  get mobilePhoneOnAboutYou() {
    return $("//td[@id='phone']")
  }

  get locationOnAboutYou() {
    return $("//td[@id='location']")
  }

  get aboutStatementOnAboutYou() {
    return $("//td[@id='aboutstatement']")
  }

  get credentials() {
    return $("//td[@id='specialities']")
  }

  get startDateOnAboutYou() {
    return $("//td[@id='jobstartdate']")
  }

  get profilePicIcon() {
    return $(
      "//button/following-sibling::div//*[name()='svg']//div[@id='ccProfilePic']/img[not(@id) and contains(@src,'lantern')]"
    )
  }

  get profilePicOnAboutYou() {
    return $(
      "//div[contains(@class,'MuiGrid-root')]//*[name()='svg']//div[@id='ccProfilePic']/img[not(@id) and contains(@src,'lantern')]"
    )
  }

  get changePasswordButton() {
    return $("//div[@id='changemypassword']/following-sibling::*[name()='svg']")
  }

  get editButton() {
    return $("//button[@id='editprofilebutton']/span[.='Edit']")
  }

  /**
   * define elements for edit my profile Page
   */

  get firstNameInput() {
    return $("//input[@id='firstName']")
  }

  get lastNameInput() {
    return $("//input[@id='lastName']")
  }

  get pronounsDropDown() {
    return $("//div[@id='pronouns']")
  }

  get emailInput() {
    return $("//input[@id='email']")
  }

  get phoneInput() {
    return $("//input[@id='phone']")
  }

  get locationInput() {
    return $("//input[@id='google-map-demo']")
  }

  get aboutStatementInput() {
    return $("//textarea[@id='aboutstatement']")
  }

  get editHeadshotButton() {
    return $(
      "//div[contains(@class,'MuiBox-root')]/*[name()='svg' and @height]//*[name()='path'][2]"
    )
  }

  get credentialsInput() {
    return $("//input[@id='credentials']")
  }

  get jobStartDateInput() {
    return $("//input[@id='jobstartdate']")
  }

  get calendarIcon() {
    return $(
      "//button[@tabindex='0' and not(@aria-label)]/span[@class='MuiIconButton-label']/*[name()='svg']"
    )
  }

  get availableStartDateList() {
    return $$(
      "//div[@role='presentation']/button[not(contains(@class,'hidden') or contains(@class,'daySelected'))]/span/p"
    )
  }

  get cancelButtonOnEditMyProfile() {
    return $("//button[@id='editprofilecancel']/span")
  }

  get saveChangesButtonOnEditMyProfile() {
    return $("//button[@id='editprofilesavechanges']")
  }

  get noChangeText() {
    return $("//div[text()='No changes have been made.']")
  }

  /**
   * define elements for edit my profile picture Page
   */
  get editMyProfilePictureHeader() {
    return $("//div[text()='Edit my profile picture']")
  }

  get fileUpload() {
    return $(
      "//div[text()='Edit my profile picture']/following-sibling::div//div[@id='ccProfilePic']/img"
    )
  }

  get changeButtonOnEditMyProfilePicture() {
    return $("//span[@id='ccUploadButton']//span[text()='Change']")
  }

  waitForMyProfilePageToLoad() {
    if (!this.myProfileHeader.isDisplayed()) {
      this.myProfileHeader.waitForDisplayed(3000)
    }
  }

  getMyProfileHeadeing() {
    this.waitForMyProfilePageToLoad()
    const profileName = this.myProfileHeader.getText()
    return profileName.substring(0, profileName.indexOf("\n"))
  }

  waitForEditMyProfilePageToLoad() {
    if (!this.editMyProfileHeader.isDisplayed()) {
      this.editMyProfileHeader.waitForDisplayed(3000)
    }
  }

  getEditMyProfileHeadeing() {
    this.waitForEditMyProfilePageToLoad()
    return this.editMyProfileHeader.getText()
  }

  getStateOfAboutTab() {
    this.aboutTab.getAttribute("class")
  }

  clickOnEditButton() {
    global.localStorage = new LocalStorage("./scratch")
    localStorage.setItem("name", this.nameOnAboutYou.getText())
    localStorage.setItem("pronouns", this.pronounsOnAboutYou.getText())
    localStorage.setItem("email", this.emailOnAboutYou.getText())
    localStorage.setItem("phone", this.mobilePhoneOnAboutYou.getText())
    localStorage.setItem("location", this.locationOnAboutYou.getText())
    localStorage.setItem("statement", this.aboutStatementOnAboutYou.getText())
    localStorage.setItem("credentials", this.credentials.getText())
    localStorage.setItem("startDate", this.startDateOnAboutYou.getText())
    this.editButton.click()
    browser.pause(6000)
  }

  verifyViewProfileAndEditProfileData() {
    try {
      const name = localStorage.getItem("name")
      const fName = name.substring(0, name.indexOf(" "))
      const lName = name.substring(name.indexOf(" ") + 1)
      this.firstNameInput.getValue().should.equal(fName)
      this.lastNameInput.getValue().should.equal(lName)
      this.pronounsDropDown
        .getText()
        .should.equal(localStorage.getItem("pronouns"))
      this.emailInput.getValue().should.equal(localStorage.getItem("email"))
      this.phoneInput.getValue().should.equal(localStorage.getItem("phone"))
      this.locationInput
        .getValue()
        .should.equal(localStorage.getItem("location"))
      this.aboutStatementInput
        .getValue()
        .should.equal(localStorage.getItem("statement"))
      this.credentialsInput
        .getValue()
        .should.equal(localStorage.getItem("credentials"))
      this.jobStartDateInput
        .getValue()
        .should.equal(localStorage.getItem("startDate"))
      this.noChangeText.isDisplayed().should.be.true
      this.cancelButtonOnEditMyProfile.isDisplayed().should.be.true
      expect(this.saveChangesButtonOnEditMyProfile.isEnabled()).to.equal(false)
      localStorage._deleteLocation()
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  verifyEditProfileDataAndViewProfile() {
    try {
      this.nameOnAboutYou.getText().should.equal(localStorage.getItem("name"))
      this.pronounsOnAboutYou
        .getText()
        .should.equal(localStorage.getItem("pronouns"))
      this.emailOnAboutYou.getText().should.equal(localStorage.getItem("email"))
      this.mobilePhoneOnAboutYou
        .getText()
        .should.equal(localStorage.getItem("phone"))
      this.locationOnAboutYou
        .getText()
        .should.equal(localStorage.getItem("location"))
      this.aboutStatementOnAboutYou
        .getText()
        .should.equal(localStorage.getItem("statement"))
      this.credentials
        .getText()
        .should.equal(localStorage.getItem("credentials"))
      this.startDateOnAboutYou
        .getText()
        .should.equal(localStorage.getItem("startDate"))
      localStorage._deleteLocation()
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  getMessageOnEditMyProfilePicture() {
    return this.messageBannerOnEditMyProfilePicture.getText()
  }

  clickOnChangePasswordButton() {
    this.changePasswordButton.click()
    browser.pause(3000)
  }

  clickOnSaveChangesButtonOnEditMyProfile() {
    global.localStorage = new LocalStorage("./scratch")
    localStorage.setItem(
      "name",
      `${this.firstNameInput.getValue()} ${this.lastNameInput.getValue()}`
    )
    localStorage.setItem("pronouns", this.pronounsDropDown.getText())
    localStorage.setItem("email", this.emailInput.getValue())
    localStorage.setItem("phone", this.phoneInput.getValue())
    localStorage.setItem("location", this.locationInput.getValue())
    localStorage.setItem("statement", this.aboutStatementInput.getValue())
    localStorage.setItem("credentials", this.credentialsInput.getValue())
    localStorage.setItem("startDate", this.jobStartDateInput.getValue())
    this.saveChangesButtonOnEditMyProfile.click()
    browser.pause(6000)
  }

  updateDataOnEditMyProfilePage() {
    const name = util.generateName(6)
    util.clearValue(this.firstNameInput)
    this.firstNameInput.setValue(`fn_${name}`)
    browser.pause(1000)
    util.clearValue(this.lastNameInput)
    this.lastNameInput.setValue(`ln_${name}`)
    browser.pause(1000)
    util.clearValue(this.credentialsInput)
    this.credentialsInput.setValue(name)
    browser.pause(1000)
    const currentPhone = this.phoneInput.getValue()
    const phone = util.getAnyPhoneFromListExceptExistingPhone(currentPhone)
    util.clearValue(this.phoneInput)
    this.phoneInput.setValue(phone)
    browser.pause(1000)
    util.clearValue(this.aboutStatementInput)
    this.aboutStatementInput.setValue(`${name}_about my statements`)
    browser.pause(1000)

    this.calendarIcon.click()
    browser.pause(1000)

    let x = 0
    const no = Math.floor(Math.random() * this.availableStartDateList.length)
    this.availableStartDateList.forEach((el) => {
      if (x === no) {
        el.click()
      }
      x++
    })
    browser.pause(2000)
  }

  validateUIFieldsOnProfilePage() {
    browser.pause(12000)
    try {
      if (
        this.aboutYouLabel.isDisplayed() &&
        this.whatYouDoLabel.isDisplayed() &&
        this.nameOnAboutYou.getText() !== "" &&
        this.pronounsOnAboutYou.getText() !== "" &&
        this.emailOnAboutYou.getText() !== "" &&
        this.mobilePhoneOnAboutYou.getText() !== "" &&
        this.locationOnAboutYou.getText() !== "" &&
        this.aboutStatementOnAboutYou.getText() !== "" &&
        this.credentials.getText() !== "" &&
        this.startDateOnAboutYou.getText() !== "" &&
        this.changePasswordButton.isDisplayed() &&
        this.editButton.isDisplayed() &&
        this.profilePicOnAboutYou.isDisplayed() &&
        this.profilePicIcon.isDisplayed()
      ) {
        return true
      } else {
        return false
      }
    } catch (err) {
      console.error(err)
      return false
    }
  }

  updateHeadshotWith(headshotType) {
    this.editMyProfilePictureHeader.isDisplayed().should.be.true
    browser.pause(1000)
    this.changeButtonOnEditMyProfilePicture.click()
    browser.pause(1000)
    if (headshotType === "invalid size") {
      this.uploadProfilePicture(headshotType)
    } else if (headshotType === "valid") {
      this.uploadProfilePicture(headshotType)
    }
  }

  clickOnEditProfileheadshotButton() {
    this.profilePicOnAboutYou.click()
    browser.pause(10000)
  }

  uploadProfilePicture(headshotType) {
    browser.pause(1000)
    let filePath, fileName
    if (headshotType === "invalid size") {
      filePath = path.join(
        `${parentDir}/utilities/invalidfiles`,
        "Vineyard-Wing-View.jpg"
      )
    } else if (headshotType === "valid") {
      const length = fs.readdirSync(`${parentDir}/utilities/images`).length - 1
      fileName = Math.floor(Math.random() * length) + 1

      fs.readdirSync(`${parentDir}/utilities/images`).forEach((file) => {
        if (path.parse(file).name === fileName) fileName = file
      })

      filePath = path.join(`${parentDir}/utilities/images`, fileName)
    }

    this.fileUpload.waitForDisplayed()
    this.fileUpload.setValue(filePath)
    browser.pause(12000)
  }
}

export default new ProfilePage()
