import Page from "./page"

class LoginErrorPage extends Page {
  /**
   * define elements
   */

  get errorLabel() {
    return $("//div[contains(@class,'visible-lg')]//p[@id='loginErrorMessage']")
  }

  waitForLandingPageToLoad() {
    if (!this.errorLabel.isDisplayed()) {
      this.errorLabel.waitForDisplayed(3000)
    }
  }

  getErrorMessage() {
    return this.errorLabel.getText()
  }
}

export default new LoginErrorPage()
