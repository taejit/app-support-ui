import Page from "./page"

const fs = require("fs")

const filePath = "tests/e2e/config/user.conf.json"

class ChangePasswordPage extends Page {
  /**
   * define elements
   */

  get changeMyPasswordLabel() {
    return $("//div[text()='Change my password' and not(@id)]")
  }

  get currentPasswordInput() {
    return $("//input[@name='currentPassword']")
  }

  get newPasswordInput() {
    return $("//input[@name='newPassword']")
  }

  get changePasswordButton() {
    return $("//button/span[text()='Change password']")
  }

  get passwordCombinationMessage() {
    return $("//p[@id='newPassword-helper-text']")
  }

  get cancelButton() {
    return $("//span[text()='Cancel']")
  }

  get errorMessageBanner() {
    return $("//div/h4")
  }

  get successMessageBanner() {
    return $("//div/h4")
  }

  /**
   * page specific methods
   */
  waitForChangeMyPassowordToLoad() {
    if (!this.changeMyPasswordLabel.isDisplayed()) {
      this.changeMyPasswordLabel.waitForDisplayed(5000)
    }
  }

  getHeading() {
    this.waitForChangeMyPassowordToLoad()
    return this.changeMyPasswordLabel.getText()
  }

  enterCurrentPassword(currentPassword) {
    this.currentPasswordInput.setValue(currentPassword)
    browser.pause(1000)
  }

  enterNewPassword(newPassword) {
    this.newPasswordInput.setValue(newPassword)
    browser.pause(1000)
  }

  clickOnCancelButton() {
    this.cancelButton.click()
    browser.pause(1000)
  }

  clickOnChangePasswordButton() {
    this.passwordCombinationMessage.click()
    browser.pause(1000)
    this.changePasswordButton.click()
    browser.pause(1000)
  }

  getErrorMessage() {
    return this.errorMessageBanner.getText()
  }

  getSuccessMessgae() {
    return this.successMessageBanner.getText()
  }

  swapPasswords(pwd, tmp_pwd, username) {
    const usrObj = { username, password: tmp_pwd, tempPassword: pwd }

    fs.writeFileSync(filePath, JSON.stringify(usrObj), "utf-8", (err) => {
      if (err) {
        console.error("Error in writing data in config file", err)
        return
      }
      console.log("swapping :", usrObj)
      console.log("wrote to file successfully")
    })
  }
}

export default new ChangePasswordPage()
