import Page from "./page"

class CareCoachDashboardPage extends Page {
  get getStartedButton() {
    return $("//button[@id='getstarted']")
  }

  get nextButton() {
    return $("//span[text()='Next']")
  }

  get finishButton() {
    return $("//span[text()='Finish']")
  }
  /**
   * define elements
   */

  get welcomePage() {
    return $("//span[@id='welcometext']")
  }

  get homePage() {
    return $("//p[.='Daily quote']")
  }

  get menuLink() {
    return $("//div/h6/following-sibling::*[name()='svg']")
  }

  get signOutLink() {
    return $("//div/following-sibling::span[text()='Sign Out']")
  }

  get profileLink() {
    return $("//div/following-sibling::span[text()='Profile']")
  }

  get ChangesSaved() {
    return $(
      "//div[contains(@class,'alert-success') and text()='Changes Saved']"
    )
  }

  get editIconOfFirstRecord() {
    return $(
      "//table[@class='table']//tr[not(@class)][1]//td/*[name()='svg'][1]"
    )
  }

  get employerNameOnTable() {
    return $(
      "//table[@class='table']//tr[not(@class)][1]/td[@class='emp-name'][1]"
    )
  }

  get contactNameOnTable() {
    return $(
      "//table[@class='table']//tr[not(@class)][1]/td[@class='emp-contact'][1]"
    )
  }

  get statusOnTable() {
    return $("//table[@class='table']//tr[not(@class)][1]/td[3]//span")
  }

  get employerTable() {
    return $("//h1[text()='Employers']/following-sibling::table")
  }

  waitForMyCareGiversPageToLoad() {
    if (!this.homePage.isDisplayed()) {
      this.homePage.waitForDisplayed(5000)
    }
  }

  getCareCoachDashboard() {
    this.waitForMyCareGiversPageToLoad()
    return this.homePage.isDisplayed()
  }

  employersNameOnTable() {
    return this.employerNameOnTable.getText()
  }

  contactsNameOnTable() {
    return this.contactNameOnTable.getText()
  }

  changesSavedMessage() {
    return this.ChangesSaved.getText()
  }

  clickOnMenuLink() {
    this.menuLink.click()
    browser.pause(2000)
  }

  clickOnProfileLink() {
    this.profileLink.click()
    browser.pause(3000)
  }

  clickOnSignOutLink() {
    this.signOutLink.click()
    browser.pause(5000)
  }

  clickOnAddNewButton() {
    this.addNewButton.click()
    browser.pause(3000)
  }

  clickOnEditIconOfFirstRecord() {
    this.editIconOfFirstRecord.click()
    browser.pause(3000)
  }

  getEmployerTable() {
    browser.pause(5000)
    return this.employerTable.isDisplayed()
  }

  getStatusOnEmployerList() {
    return this.statusOnTable.getText()
  }

  clickOnCareCoachesLeftSideBarMenu(menu) {
    $(`//span[text()='${menu}']`).click()
    browser.pause(1000)
  }
}

export default new CareCoachDashboardPage()
