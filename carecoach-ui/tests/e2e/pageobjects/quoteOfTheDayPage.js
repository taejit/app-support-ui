/* eslint-disable no-unused-expressions */
import Page from "./page"

class QuoteOfTheDayPage extends Page {
  get image() {
    return $("//p[.='Daily quote']/../following-sibling::div")
  }
  get doubleQuote() {
    return $("//p[.='Daily quote']/../following-sibling::div//*[name()='svg']")
  }

  get actualQuote() {
    return $(
      "//p[.='Daily quote']/../following-sibling::div//div[not(@class)]/following-sibling::div[1]"
    )
  }

  get authorName() {
    return $(
      "//p[.='Daily quote']/../following-sibling::div//div[not(@class)]/following-sibling::div[2]"
    )
  }

  get authorLabel() {
    return $(
      "//p[.='Daily quote']/../following-sibling::div//div[not(@class)]/following-sibling::div[3]"
    )
  }

  verifyDailyQuoteAndItsRelatedInfo() {
    try {
      this.doubleQuote.isDisplayed().should.be.true
      this.actualQuote.isDisplayed().should.be.true
      this.authorName.isDisplayed().should.be.true
      this.authorLabel.isDisplayed().should.be.true
      return true
    } catch (err) {
      console.error("Error in validating daily quotes", err)
      return false
    }
  }
}
export default new QuoteOfTheDayPage()
