/* eslint-disable no-unused-expressions */
import Page from "./page"
import carePlanPage from "../pageobjects/carePlanPage"
import myCaregiversPage from "../pageobjects/myCaregiversPage"
import { LocalStorage } from "node-localstorage"

var selectedMS
class ExistingCarePlanPage extends Page {
  /**
   * define elements for add milestones from two options drawer
   */
  get addMilestoneHeadingOnDrawer() {
    return $("//div[@id='title']")
  }
  get searchExistinMSOnAddMilestoneDrawer() {
    return $("//p[.='Search existing Milestones']")
  }
  get addFromScratchSectionOnAddMilestoneDrawer() {
    return $("//p[.='Add from scratch']")
  }
  get cancelButtonOnAddMilestoneDrawer() {
    return $("//span[.='Cancel']")
  }
  get goBackButtonOnDrawer() {
    return $("//button[@id='backBtn']/span[.='Go back']")
  }
  get listOfMilestonesLeftSide() {
    return $$("//div[@id='itemBox']/div")
  }

  get addMilestonesButtonOnSearchMilestonesDrawer() {
    return $("//span[.='Add Milestones']/ancestor::button[@id='submitform']")
  }
  get listOfMilestonesSelectedOnBottom() {
    return $$(
      "//span[.='Add Milestones']/ancestor::div[contains(@class,'MuiGrid-container')]/preceding-sibling::div//span[@class='MuiButton-label']"
    )
  }
  get noMilestoneSelectedLabel() {
    return $("//p[text()='No Milestones selected.']")
  }
  /**
   * define elements list of  care plan
   */
  get listOfUpcomingMSOnCarePlan() {
    return $$(
      "//p[.='Upcoming']/../../../following-sibling::div//p/../following-sibling::div/div[1]/p[1]"
    )
  }

  get listOfCompleteMSOnCarePlan() {
    return $$(
      "//p[.='Complete']/../../../following-sibling::div//p/../following-sibling::div/div[1]/p[1]"
    )
  }

  waitForAddMilestoneDrawerToLoad() {
    if (!this.addMilestoneHeadingOnDrawer.isDisplayed()) {
      this.addMilestoneHeadingOnDrawer.waitForDisplayed(5000)
    }
  }

  verifyOptionsToAddMilestonesDrawer() {
    try {
      this.waitForAddMilestoneDrawerToLoad()
      this.searchExistinMSOnAddMilestoneDrawer.isDisplayed().should.be.true
      this.addFromScratchSectionOnAddMilestoneDrawer.isDisplayed().should.be
        .true
      this.cancelButtonOnAddMilestoneDrawer.click()
      browser.pause(2000)
      this.addMilestoneHeadingOnDrawer.isDisplayed().should.be.false
      myCaregiversPage.addMilestonesButtonOnCarePlan.click()
      this.waitForAddMilestoneDrawerToLoad()
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  verifySavedChangesOnSpecifiedSectionOfExistingCarePlan(section) {
    try {
      browser.pause(2000)
      var MSArray = []
      if (section === "Complete") {
        this.listOfCompleteMSOnCarePlan.forEach((el) => {
          MSArray.push(el.getText())
        })
        console.log(
          "Saved MS showing in Completed MS List, No. of MS on Completed: " +
            this.listOfCompleteMSOnCarePlan.length
        )
      } else {
        this.listOfUpcomingMSOnCarePlan.forEach((el) => {
          MSArray.push(el.getText())
        })
        console.log(
          "Updated MS showing in Upcoming MS List, No. of MS on Upcoming: " +
            this.listOfUpcomingMSOnCarePlan.length
        )
      }

      MSArray.includes(localStorage.getItem("editedMSName")).should.be.true
      localStorage._deleteLocation()
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  clickOnRandomMSFromUpcomingMilestoneList() {
    console.log(
      "No. of MS available on Upcoming Care Plan : " +
        this.listOfUpcomingMSOnCarePlan.length
    )
    let randomNo = Math.floor(
      Math.random() * this.listOfUpcomingMSOnCarePlan.length
    )
    console.log(
      "Selected Milestone is:" +
        this.listOfUpcomingMSOnCarePlan[randomNo].getText()
    )
    selectedMS = this.listOfUpcomingMSOnCarePlan[randomNo].getText()
    this.listOfUpcomingMSOnCarePlan[randomNo].click()
    browser.pause(2000)
  }

  verifyDeletedMSRemovedFromUpcomingSectionOfExistingCarePlan() {
    try {
      browser.pause(3000)
      let msList = []
      this.listOfUpcomingMSOnCarePlan.forEach((el) => {
        msList.push(el.getText())
      })
      msList.includes(selectedMS).should.be.false
      console.log(
        "No. of MS available on Upcoming Care Plan : " +
          this.listOfUpcomingMSOnCarePlan.length
      )
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  selectRandomMSFromSearchMilestones() {
    try {
      this.addMilestonesButtonOnSearchMilestonesDrawer
        .getAttribute("class")
        .includes("disabled").should.be.true
      this.noMilestoneSelectedLabel.isDisplayed().should.be.true

      this.listOfMilestonesLeftSide.forEach((el) => {
        el.click()
        browser.pause(1000)
        el.getAttribute("class").includes("Mui-selected").should.be.true
      })
      this.addMilestonesButtonOnSearchMilestonesDrawer
        .getAttribute("class")
        .includes("disabled").should.be.false

      this.noMilestoneSelectedLabel.isDisplayed().should.be.false

      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  verifyRemovedMilestonesFromBottomSection() {
    try {
      let x = 0
      this.listOfMilestonesSelectedOnBottom.forEach((el) => {
        el.click()
        browser.pause(1000)
        this.listOfMilestonesLeftSide[x]
          .getAttribute("class")
          .includes("Mui-selected").should.be.false
        x++
      })
      this.addMilestonesButtonOnSearchMilestonesDrawer
        .getAttribute("class")
        .includes("disabled").should.be.true

      this.noMilestoneSelectedLabel.isDisplayed().should.be.true
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }
  searchAndSelectRandomMSFromSearchMilestones() {
    try {
      carePlanPage.searchTemplateInputField.setValue("a")
      browser.pause(1000)
      carePlanPage.commonLabel.isDisplayed().should.be.false
      console.log(
        `No. of Searched Milestones available:${this.listOfMilestonesLeftSide.length}`
      )
      let randomNo = Math.floor(
        Math.random() * this.listOfMilestonesLeftSide.length
      )
      this.listOfMilestonesLeftSide[randomNo].scrollIntoView()
      browser.pause(500)

      this.listOfMilestonesLeftSide[randomNo].click()
      browser.pause(1000)
      console.log(
        "Selected Milestone is:" +
          this.listOfMilestonesLeftSide[randomNo].getText()
      )
      global.localStorage = new LocalStorage("./scratch")
      localStorage.setItem(
        "editedMSName",
        this.listOfMilestonesLeftSide[randomNo].getText()
      )
      this.addMilestonesButtonOnSearchMilestonesDrawer.click()
      browser.pause(5000)
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }
}
export default new ExistingCarePlanPage()
