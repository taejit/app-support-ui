/* eslint-disable no-unused-expressions */
import Page from "./page"
import myCaregiversPage from "./myCaregiversPage"

var submittedDateOnRecentlySubmittedSurvey,
  submittedScoreOnRecentlySubmittedSurvey
class ProgressCheckPage extends Page {
  get progressLink() {
    return $("//div[text()='Progress']")
  }

  get recentSurveySubmittedDate() {
    return $("//p[text() ='Checks']/following-sibling::div[1]/p[2]")
  }

  get recentSurveySubmittedScore() {
    return $("//p[text() ='Checks']/following-sibling::div[1]/p[3]")
  }

  get checkProgressTitleOnDrawer() {
    return $("//div[@id='title']")
  }

  get surveySubmittedDateOnDrawer() {
    return $("//div[@id='subscript']")
  }

  get burdonScoreOnDrawer() {
    return $("//div[@id='burdenScore']/span")
  }

  get burdonScoreTextOnDrawer() {
    return $("//div[@id='burdenScore']")
  }

  get questionListOnDrawer() {
    return $$("//div[@id='progressList']/li/div[1]")
  }

  get goBackButton() {
    return $("//span[text() ='Go back']")
  }

  get lastScoreOnImage() {
    return $("//div/p/span/span")
  }

  get burdenCategoryOnImage() {
    return $("//div[contains(@class, 'MuiCardMedia-root')]/div[2]/div[1]")
  }

  get caregiversProgressText() {
    return $("//p[.='Checks']/../../preceding-sibling::div//p")
  }

  get emptyStateLabelOnProgress() {
    return $("//p[text()='Step by step, one ventures far.']")
  }
  get progressLink() {
    return $("//div[text()='Progress']")
  }
  get emptyStateTextOnProgress() {
    return $(
      "//p[text()='As your Caregiver completes their progress checks, you’ll be able to track their score and responses here.']"
    )
  }

  clickOnProgressLink() {
    this.progressLink.click()
    browser.pause(2000)
  }

  verifyCaregiversProgress() {
    try {
      let cgName = myCaregiversPage.getCareGiverName()
      this.caregiversProgressText
        .getText()
        .should.equals(`${cgName.substring(0, cgName.indexOf(" "))}'s Progress`)

      if (this.burdenCategoryOnImage.getText() === "Indeterminate") {
      } else {
        this.recentSurveySubmittedScore
          .getText()
          .should.equal(this.lastScoreOnImage.getText())
        let lastScore = parseInt(this.lastScoreOnImage.getText())
        this.verifyBurdonCategoryWithBurdonScore(
          lastScore,
          this.burdenCategoryOnImage.getText()
        )
      }
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  clickOnRecentlySubmittedSurvey() {
    submittedDateOnRecentlySubmittedSurvey = this.recentSurveySubmittedDate.getText()
    submittedScoreOnRecentlySubmittedSurvey = this.recentSurveySubmittedScore.getText()
    this.recentSurveySubmittedDate.click()
    browser.pause(2000)
  }

  verifySubmittedDateAndScoreOnDrawer() {
    try {
      this.checkProgressTitleOnDrawer
        .getText()
        .should.equal("View Progress Check")
      let subDateOnDrawer = this.surveySubmittedDateOnDrawer.getText()
      subDateOnDrawer = subDateOnDrawer.substring(
        0,
        subDateOnDrawer.indexOf("at") - 1
      )
      submittedDateOnRecentlySubmittedSurvey.should.equal(subDateOnDrawer)
      submittedScoreOnRecentlySubmittedSurvey.should.equal(
        this.burdonScoreOnDrawer.getText()
      )
      if (this.questionListOnDrawer.length !== 10) {
        return false
      }

      let lastScore = parseInt(this.burdonScoreOnDrawer.getText())
      let burdonCategoryOnDrawer = this.burdonScoreTextOnDrawer.getText()
      burdonCategoryOnDrawer = burdonCategoryOnDrawer.substring(
        burdonCategoryOnDrawer.indexOf("•") + 2
      )
      this.verifyBurdonCategoryWithBurdonScore(
        lastScore,
        burdonCategoryOnDrawer
      )

      browser.pause(2000)

      this.goBackButton.click()
      browser.pause(2000)
      this.checkProgressTitleOnDrawer.isDisplayed().should.be.false
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  verifyBurdonCategoryWithBurdonScore(lastScore, burdenCategory) {
    if (lastScore >= 0 && lastScore <= 4) {
      burdenCategory.should.equal("Light Burden")
      return true
    } else if (lastScore >= 5 && lastScore <= 14) {
      burdenCategory.should.equal("Mild Burden")
      return true
    } else if (lastScore >= 15 && lastScore <= 30) {
      burdenCategory.should.equal("Heavy Burden")
      return true
    } else {
      return false
    }
  }

  clickOnProgressLink() {
    this.progressLink.click()
    browser.pause(2000)
  }

  verifyEmptyStateOfMiniSurvey() {
    try {
      this.emptyStateLabelOnProgress.isDisplayed().should.be.true
      this.emptyStateTextOnProgress.isDisplayed().should.be.true
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }
}

export default new ProgressCheckPage()
