/* eslint-disable no-unused-expressions */
import Page from "./page"
import util from "../utilities/util"
import myCaregiversPage from "./myCaregiversPage"
import existingCarePlanPage from "../pageobjects/existingCarePlanPage"
import { LocalStorage } from "node-localstorage"

var rightSideMSList = [],
  selectedMS
class CarePlanPage extends Page {
  /**
   * define elements for templates
   */

  get startWithATemplateHeadingOnDrawer() {
    return $("//h6[.='Start with a template']")
  }

  get searchTemplateInputField() {
    return $("//input[@id='ccSearchTemplate']")
  }

  get commonLabel() {
    return $("//div[text()='COMMON']")
  }

  get nothingFoundLabel() {
    return $("//p[text()='Nothing found!']")
  }

  get selectedTemplate() {
    return $(
      "//button[contains(@class,'MuiButton-outlined')]/span[@class='MuiButton-label' and not(text()='Apply template')]"
    )
  }

  get applyTemplateButton() {
    return $(
      "//button[contains(@class,'MuiButtonBase-root')]/span[text()='Apply templates']"
    )
  }

  get clearSearchIcon() {
    return $("//div[@id='ccClearSearchIcon']")
  }

  get noTemplateSelectedLabel() {
    return $("//p[text()='No templates selected.']")
  }

  get continueFromScratchButton() {
    return $("//button/span[text()='Continue from scratch']")
  }

  get rightSideSectionOnStepOne() {
    return $("//div[@id='backStepLayer']/following-sibling::div")
  }

  /**
   * define elements for milestone listing for both left and right side
   */
  get reviewAndFinishHeadingOnDrawer() {
    return $("//h6[.='Review & Finish']")
  }
  get finishAndSendButtonOnDrawer() {
    return $("//button[@id='finish-btn']/span[.='Finish & send']")
  }
  get stepperIcon() {
    return $(
      "//div[@id='step_0']//span[contains(@class,'MuiStepLabel-iconContainer')]//*[name()='svg']"
    )
  }
  get stepperTwoIcon() {
    return $(
      "//div[@id='step_0']/following-sibling::div[@id='step_1']//span//*[name()='svg']"
    )
  }

  get warningTextOnDrawer() {
    return $("//div[.='Return to the template stage?']")
  }
  get nevermindButtonOnDrawer() {
    return $("//button[@id='nevermindBtn']/span[.='Nevermind']")
  }
  get continueButtonOnDrawer() {
    return $("//button[@id='stepbackCtnbtn']/span[.='Continue']")
  }
  get removeButtonOnDrawer() {
    return $("//button[@id='stepbackCtnbtn']/span[.='Remove']")
  }

  get customizeMilestonesHeadingOnDrawer() {
    return $("//h6[.='Customize Milestones']")
  }

  get undoButton() {
    return $("//div[text()='UNDO']")
  }

  get addNewButton() {
    return $("//span[text()='Add new']")
  }

  get listOfTemplatesLeftSide() {
    return $$("//div[@id='itemBox']/div")
  }

  get listOfUncheckedMilestonesLeftSide() {
    return $$("//div[@id='itemBox']/div[not(contains(@class,'Mui-selected'))]")
  }

  get listOfMilestonesLeftSide() {
    return $$("//div[@id='itemBox']/div")
  }

  get listOfMilestonesRightSide() {
    return $$(
      "//div[@id='backStepLayer']/following-sibling::div//div[1]//p[contains(@class,'MuiTypography-root')][1]"
    )
  }

  get listOfDueDateOnMilestonesRightSide() {
    return $$(
      "//div[@id='backStepLayer']/following-sibling::div//div[2]//p[contains(@class,'MuiTypography-root')][1]"
    )
  }

  /**
   * define elements for customize existing milestone
   */
  get milestoneHeadingOnMSDrawer() {
    return $("//div[@id='editmilestonetitle']")
  }

  get overviewNameInputOnMSDrawer() {
    return $("//textarea[@id='milestonename']")
  }

  get overviewRationaleInputOnMSDrawer() {
    return $("//textarea[@id='milestonerationale']")
  }

  get overviewDescriptionInputOnMSDrawer() {
    return $("//textarea[@id='milestonedescription']")
  }

  get dueDateInputOnMSDrawer() {
    return $("//input[@id='milestoneduedate']")
  }
  get clearDueDateIconOnMSDrawer() {
    return $("//button[@id='clearduedate']/span")
  }
  get overviewSectionOnMSDrawer() {
    return $("//div[@id='overviewsection']")
  }
  get calendarIcon() {
    return $(
      "//input[@id='milestoneduedate']/following-sibling::div//button[@tabindex='0' and not(@aria-label)]/span[@class='MuiIconButton-label']/*[name()='svg']"
    )
  }

  get availableDueDateList() {
    return $$(
      "//div[@role='presentation']/button[not(contains(@class,'hidden') or contains(@class,'MuiPickersDay-dayDisabled'))]/span/p"
    )
  }

  get tasksSectionOnMSDrawer() {
    return $("//div[contains(@class,'MuiTypography-body1') and .='Tasks']")
  }

  get tasksCountOnMSDrawer() {
    return $(
      "//div[contains(@class,'MuiTypography-body1') and .='Tasks']/../../div/following-sibling::div/div"
    )
  }

  get tasksNameListOnMSDrawer() {
    return $$(
      "//div[contains(@class,'MuiTypography-body1') and .='Tasks']/../../following-sibling::div//div[@id='task-checkbox']/span[2]"
    )
  }

  get tasksEditIconListOnMSDrawer() {
    return $$(
      "//div[contains(@class,'MuiTypography-body1') and .='Tasks']/../../following-sibling::div//div[@id='task-checkbox']/following-sibling::div/*[name()='svg']"
    )
  }

  get externalLinkIconListOnMSDrawer() {
    return $$(
      "//div[contains(@class,'MuiTypography-body1') and .='Tasks']/../../following-sibling::div//div[@id='task-checkbox']/following-sibling::div/a"
    )
  }

  get addNewplusTaskButtonOnMSDrawer() {
    return $("//button[@id='addnewtask']/span[.='ADD NEW']")
  }

  get noTasksYetTextOnMSDrawer() {
    return $("//p[.='No tasks yet. Click to add!']")
  }

  get addAtLeastOneTaskErrorTextOnMSDrawer() {
    return $("//p[.='You must add at least one task.']")
  }
  get noChangesTextOnEditMSDrawer() {
    return $("//h4")
  }

  get cancelButtonOnMSDrawer() {
    return $("//button[@id='cancel']/span[.='Cancel']")
  }

  get continueButtonOnStepTwoOnCarePlan() {
    return $("//span[.='Continue']/ancestor::button[@id='continue-btn']")
  }

  get removeButtonOnEditMSDrawer() {
    return $("//button[@id='remove']/span[.='Remove']")
  }

  get saveChangesButtonOnEditMSDrawer() {
    return $("//span[.='Save changes']/ancestor::button[@id='submitform']")
  }

  get addButtonOnAddMSDrawer() {
    return $("//span[.='Add']/ancestor::button[@id='submitform']")
  }
  /**
   * define elements for Add new task from scratch / Edit Task
   */
  get headingOnTaskDrawer() {
    return $("//div[@id='edittasktitle']")
  }

  get tasksNameInputOnMSDrawer() {
    return $("//textarea[@id='taskname']")
  }

  get tasksLinkInputOnMSDrawer() {
    return $("//textarea[@id='tasklink']")
  }

  get goBackButtonOnAddTaskFromScratchDrawer() {
    return $("//button[@id='backBtn']/span[.='Go back']")
  }

  get addButtonOnAddTaskFromScratchDrawer() {
    return $("//span[.='Add']/ancestor::button[@id='submitform']")
  }
  get removeTaskLabelOnWarningDrawer() {
    return $(
      "//div[contains(text(),'This cannot be undone')]/preceding-sibling::div[contains(text(),'?')]"
    )
  }

  clickOnSpecifiedButtonOnCarePlan(buttonTxt) {
    if (buttonTxt === "Create Care Plan") {
      myCaregiversPage.createCarePlanButtonOnCarePlan.click()
    } else if (buttonTxt === "Continue from scratch") {
      this.continueFromScratchButton.click()
    } else if (buttonTxt === "Add new") {
      this.addNewButton.click()
    } else if (buttonTxt === "Cancel") {
      this.cancelButtonOnMSDrawer.click()
    } else if (buttonTxt === "Add Milestones") {
      myCaregiversPage.addMilestonesButtonOnCarePlan.click()
    } else if (buttonTxt === "Add from scratch") {
      existingCarePlanPage.addFromScratchSectionOnAddMilestoneDrawer.click()
    } else if (buttonTxt === "Search existing Milestones") {
      existingCarePlanPage.searchExistinMSOnAddMilestoneDrawer.click()
    } else if (buttonTxt === "Go back") {
      existingCarePlanPage.goBackButtonOnDrawer.click()
    } else if (buttonTxt === "Continue") {
      this.continueButtonOnStepTwoOnCarePlan.click()
    }
    browser.pause(2000)
  }

  enterRandomCharacterOnSearchTemplateAndVerifyResult() {
    try {
      this.commonLabel.isDisplayed().should.be.true
      console.log(
        `No. of Common Templates available : ${this.listOfTemplatesLeftSide.length}`
      )
      this.searchTemplateInputField.setValue(util.generateName(1).toLowerCase())
      browser.pause(1000)

      this.commonLabel.isDisplayed().should.be.false
      console.log(
        `No. of Searched Templates available:${this.listOfTemplatesLeftSide.length}`
      )
      this.clearSearchIcon.isDisplayed().should.be.true
      try {
        this.nothingFoundLabel.isDisplayed().should.be.true
        console.log("Searched Result Not Found")
      } catch (err) {
        console.log("Searched Result Found")
      }
      this.noTemplateSelectedLabel.isDisplayed().should.be.true
      this.continueFromScratchButton.isDisplayed().should.be.true
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  enterRandomCharacterOnSearchMilestoneAndVerifyResult() {
    try {
      this.commonLabel.isDisplayed().should.be.true
      console.log(
        `No. of Common Milestones available : ${this.listOfMilestonesLeftSide.length}`
      )
      this.searchTemplateInputField.setValue(util.generateName(1).toLowerCase())
      browser.pause(1000)

      try {
        this.commonLabel.isDisplayed().should.be.false
        console.log(
          `No. of Searched Milestones available:${this.listOfMilestonesLeftSide.length}`
        )
      } catch (err) {
        this.nothingFoundLabel.isDisplayed().should.be.true
      }
      this.clearSearchIcon.isDisplayed().should.be.true
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  selectRandomTemplateAndApply() {
    try {
      browser.pause(2000)
      this.continueFromScratchButton.isDisplayed().should.be.true
      //performing template selection and deselection,view of apply template and continue from scratch button
      let randomNo = 0
      this.listOfTemplatesLeftSide[randomNo].click()
      browser.pause(2000)
      this.continueFromScratchButton.isDisplayed().should.be.false
      var selected = this.listOfTemplatesLeftSide[randomNo].getAttribute(
        "class"
      )
      selected.includes("Mui-selected").should.be.true
      this.listOfTemplatesLeftSide[randomNo]
        .getText()
        .should.equal(this.selectedTemplate.getText())
      this.applyTemplateButton.isDisplayed().should.be.true
      this.selectedTemplate.click()
      browser.pause(2000)
      this.noTemplateSelectedLabel.isDisplayed().should.be.true
      this.continueFromScratchButton.isDisplayed().should.be.true
      selected = this.listOfTemplatesLeftSide[randomNo].getAttribute("class")
      selected.includes("Mui-selected").should.be.false
      //performing template selection and apply template button

      this.listOfTemplatesLeftSide[1].click()
      browser.pause(1000)
      this.applyTemplateButton.click()
      browser.pause(2000)
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  randomIndexNumberForUnSelectedMSFromLeftSideList() {
    let index = Math.floor(Math.random() * this.listOfMilestonesLeftSide.length)
    let selected = this.listOfMilestonesLeftSide[index].getAttribute("class")
    if (selected.includes("Mui-selected")) {
      return this.randomIndexNumberForUnSelectedMSFromLeftSideList()
    }
    return index
  }

  selectRandomMilestoneAndUndo() {
    try {
      browser.pause(2000)
      this.addNewButton.isDisplayed().should.be.true
      try {
        this.continueButtonOnStepTwoOnCarePlan.isDisplayed().should.be.true
      } catch (err) {}
      //performing one milestone selection and undo
      let randomNo = this.randomIndexNumberForUnSelectedMSFromLeftSideList()
      this.listOfMilestonesLeftSide[randomNo].click()
      browser.pause(1000)
      var selectedMS,
        selected,
        rightSideMSList = []

      selected = this.listOfMilestonesLeftSide[randomNo].getAttribute("class")
      selected.includes("Mui-selected").should.be.true
      selectedMS = this.listOfMilestonesLeftSide[randomNo].getText()

      this.listOfMilestonesRightSide.forEach((el) => {
        rightSideMSList.push(el.getText())
      })

      rightSideMSList.includes(selectedMS).should.be.true

      rightSideMSList = []
      this.undoButton.click()
      browser.pause(2000)
      this.listOfMilestonesRightSide.forEach((el) => {
        rightSideMSList.push(el.getText())
      })

      rightSideMSList.includes(selectedMS).should.be.false
      return true
    } catch (err) {
      console.log("Error in selecting random MS and Undo: ", err)
      return false
    }
  }

  selectMultipleMilestonesAndVerifyResult() {
    try {
      browser.pause(1000)
      var rightSideMSList = []
      let x = 0
      var selected, selectedMS
      this.listOfUncheckedMilestonesLeftSide.forEach((el) => {
        if (x % 2 === 0) {
          el.click()
          browser.pause(1000)

          try {
            selected = this.listOfMilestonesLeftSide[x + 1].getAttribute(
              "class"
            )
            selected.includes("Mui-selected").should.be.true
            selectedMS = this.listOfMilestonesLeftSide[x + 1].getText()
          } catch (err) {
            selected = this.listOfMilestonesLeftSide[x].getAttribute("class")
            selected.includes("Mui-selected").should.be.true
            selectedMS = this.listOfMilestonesLeftSide[x].getText()
          }

          this.listOfMilestonesRightSide.forEach((el) => {
            rightSideMSList.push(el.getText())
          })
          rightSideMSList.includes(selectedMS).should.be.true
          browser.pause(2000)
          rightSideMSList = []
        }
        x++
      })
      //performing list should in ascending order
      util.checkNamesInAlphaOrder(this.listOfMilestonesRightSide).should.be.true
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  clickOnRandomMSFromRightSideList() {
    try {
      console.log(
        "No. of MS available: " + this.listOfMilestonesRightSide.length
      )
      let randomNo = Math.floor(
        Math.random() * this.listOfMilestonesRightSide.length
      )
      console.log(
        "Selected Milestone is:" +
          this.listOfMilestonesRightSide[randomNo].getText()
      )
      selectedMS = this.listOfMilestonesRightSide[randomNo].getText()
      this.listOfMilestonesRightSide.forEach((el) => {
        rightSideMSList.push(el.getText())
      })
      this.listOfMilestonesRightSide[randomNo].click()
      browser.pause(2000)
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  clickOnCancelButtonOnEditMSDrawer() {
    try {
      this.removeButtonOnEditMSDrawer.scrollIntoView()
      browser.pause(1000)
      this.noChangesTextOnEditMSDrawer.isDisplayed().should.be.true
      var isDisabled = this.saveChangesButtonOnEditMSDrawer.getAttribute(
        "disabled"
      )
      isDisabled.should.equal("true")
      this.removeButtonOnEditMSDrawer.isDisplayed().should.be.true

      this.cancelButtonOnMSDrawer.click()
      browser.pause(1000)
      console.log(
        "No. of MS available after cancel:" +
          this.listOfMilestonesRightSide.length
      )
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  editOverviewSectionAndPerformDeleteAddTasksAndSaveChanges() {
    try {
      this.milestoneHeadingOnMSDrawer.getText().should.equal("Edit Milestone")
      //performing toggling of overview and tasks section
      this.verifyOverviewAndTaskSectionToggling()

      //Edit the Overview section with name,rationale,description
      this.clearAndEnteredDataForOverviewSection()

      //select random date from calendar,clear and verify and reselect
      this.selectDueDateAndClearAndVerifyAndReselectDueDate()

      //Delete All the Tasks ,verify placeholder and add two new tasks
      this.tasksEditIconListOnMSDrawer.length.should.equal(
        parseInt(this.tasksCountOnMSDrawer.getText())
      )
      this.tasksEditIconListOnMSDrawer.forEach((el) => {
        el.click()
        this.removeTaskOnEditMSDrawer()
        browser.pause(1000)
      })
      this.tasksCountOnMSDrawer.isDisplayed().should.be.false
      this.noTasksYetTextOnMSDrawer.isDisplayed().should.be.true

      this.noTasksYetTextOnMSDrawer.click()
      browser.pause(2000)
      this.milestoneHeadingOnMSDrawer.isDisplayed().should.be.false
      this.headingOnTaskDrawer.getText().should.equal("Add task from scratch")
      this.goBackButtonOnAddTaskFromScratchDrawer.click()
      browser.pause(1000)
      this.milestoneHeadingOnMSDrawer.isDisplayed().should.be.true
      this.saveChangesButtonOnEditMSDrawer.click()
      browser.pause(1000)
      this.addAtLeastOneTaskErrorTextOnMSDrawer.isDisplayed().should.be.true

      this.addTaskFromScratch("mailinator").should.be.true
      /*var parentGUID = browser.getWindowHandle()
      this.validateLinkAndComeBackToApp(parentGUID, 0).should.be.true*/

      this.saveChangesButtonOnEditMSDrawer.click()
      browser.pause(2000)
      return true
    } catch (err) {
      console.log("Error in Editing Milestone: ", err)
      return false
    }
  }

  verifySavedChangesOnMilestoneList() {
    try {
      var rightSideMSList = [],
        dueDateList = []
      this.listOfMilestonesRightSide.forEach((el) => {
        rightSideMSList.push(el.getText())
      })
      this.listOfDueDateOnMilestonesRightSide.forEach((el) => {
        dueDateList.push(el.getText())
      })
      rightSideMSList.includes(localStorage.getItem("editedMSName")).should.be
        .true
      dueDateList[0].should.equal(localStorage.getItem("dueDate"))
      //performing list should in ascending order
      rightSideMSList.shift()
      util.checkMSInAlphaOrder(rightSideMSList).should.be.true
      localStorage._deleteLocation()
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  clickOnRemoveButtonToDeleteMS() {
    try {
      let msName = this.overviewNameInputOnMSDrawer.getText()
      this.removeButtonOnEditMSDrawer.scrollIntoView()
      browser.pause(1000)
      this.removeButtonOnEditMSDrawer.click()
      browser.pause(1000)
      //calling warner delete
      this.removeTaskLabelOnWarningDrawer
        .getText()
        .should.equal("Remove " + '"' + msName + '?"')
      this.removeButtonOnDrawer.click()
      browser.pause(500)
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  verifyDeletedMSRemovedOnMilestoneList() {
    try {
      console.log("Deleted Milestone is: ", selectedMS)
      rightSideMSList = []
      this.listOfMilestonesRightSide.forEach((el) => {
        rightSideMSList.push(el.getText())
      })

      rightSideMSList.includes(selectedMS).should.be.false
      console.log(
        "No. of MS after deletion: " + this.listOfMilestonesRightSide.length
      )
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  addTaskFromScratch(taskName) {
    try {
      browser.pause(1000)
      this.addNewplusTaskButtonOnMSDrawer.click()
      browser.pause(2000)
      this.milestoneHeadingOnMSDrawer.isDisplayed().should.be.false
      this.headingOnTaskDrawer.getText().should.equal("Add task from scratch")
      this.tasksNameInputOnMSDrawer.setValue(
        taskName + " " + util.generateTitle()
      )
      browser.pause(1000)
      this.tasksLinkInputOnMSDrawer.setValue("http://" + taskName + ".com")
      browser.pause(1000)
      this.addButtonOnAddTaskFromScratchDrawer.click()
      browser.pause(2000)
      return true
    } catch (err) {
      console.log("Error in adding task: ", err)
      return false
    }
  }

  validateLinkAndComeBackToApp(parentGUID, index) {
    browser.pause(2000)
    try {
      this.externalLinkIconListOnMSDrawer[index].click()
      browser.pause(3000)
      // get the All the session id of the browsers
      var allGUID = browser.getWindowHandles()

      for (var i = 0; i < allGUID.length; i++) {
        if (allGUID[i] === parentGUID) {
          browser.switchToWindow(parentGUID)
          browser.pause(3000)
        }
      }
      return true
    } catch (err) {
      console.log("Error in verifying link: ", err)
      return false
    }
  }

  verifyOverviewAndTaskSectionToggling() {
    this.overviewNameInputOnMSDrawer.isDisplayed().should.be.true
    this.overviewSectionOnMSDrawer.click()
    browser.pause(2000)
    this.overviewNameInputOnMSDrawer.isDisplayed().should.be.false

    this.addNewplusTaskButtonOnMSDrawer.isDisplayed().should.be.true
    this.tasksSectionOnMSDrawer.click()
    browser.pause(2000)
    this.addNewplusTaskButtonOnMSDrawer.isDisplayed().should.be.false
    //perform to visible Overview and tasks section
    this.tasksSectionOnMSDrawer.click()
    browser.pause(2000)
    this.overviewSectionOnMSDrawer.click()
    browser.pause(2000)
  }

  clearAndEnteredDataForOverviewSection() {
    let randomName = util.generateTitle()
    global.localStorage = new LocalStorage("./scratch")
    localStorage.setItem("editedMSName", randomName)

    util.clearValue(this.overviewNameInputOnMSDrawer)
    this.overviewNameInputOnMSDrawer.setValue(randomName)

    util.clearValue(this.overviewRationaleInputOnMSDrawer)
    this.overviewRationaleInputOnMSDrawer.setValue(util.generateBody())

    util.clearValue(this.overviewDescriptionInputOnMSDrawer)
    this.overviewDescriptionInputOnMSDrawer.setValue(util.generateBody())
  }

  selectDueDateAndClearAndVerifyAndReselectDueDate() {
    this.calendarIcon.click()
    browser.pause(1000)

    let x = 0
    let no = Math.floor(Math.random() * this.availableDueDateList.length)
    this.availableDueDateList.forEach((el) => {
      if (x === no) {
        el.click()
      }
      x++
    })
    browser.pause(1000)
    //clear the selected due date
    this.clearDueDateIconOnMSDrawer.click()
    browser.pause(1000)
    //verifying no due date is present on input field
    this.dueDateInputOnMSDrawer.getValue().should.equal("")
    //again selecting new due date
    this.calendarIcon.click()
    browser.pause(1000)
    x = 0
    no = Math.floor(Math.random() * this.availableDueDateList.length)
    this.availableDueDateList.forEach((el) => {
      if (x === no) {
        el.click()
      }
      x++
    })
    let dueDate = this.dueDateInputOnMSDrawer.getValue()
    dueDate = dueDate.substring(0, dueDate.indexOf(","))
    dueDate =
      dueDate.substring(0, 3) +
      " " +
      dueDate.substring(dueDate.indexOf(" ") + 1, dueDate.length - 2)
    localStorage.setItem("dueDate", dueDate)
    browser.pause(2000)
  }

  clickOnStepperAndVerifyWarningDrawerWithButtons() {
    try {
      this.stepperIcon.click()
      browser.pause(1000)
      this.warningTextOnDrawer.isDisplayed().should.be.true
      this.nevermindButtonOnDrawer.isDisplayed().should.be.true
      this.continueButtonOnDrawer.isDisplayed().should.be.true
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  clickOnWarningDrawerButton(buttonText) {
    if (buttonText === "Nevermind") {
      this.nevermindButtonOnDrawer.click()
    } else if (buttonText === "Continue") {
      this.continueButtonOnDrawer.click()
    }
    browser.pause(1000)
  }

  verifyCustomizeMSLeftAndRightSideMSList() {
    try {
      this.customizeMilestonesHeadingOnDrawer.isDisplayed().should.be.true
      this.addNewButton.isDisplayed().should.be.true
      this.continueButtonOnStepTwoOnCarePlan.isDisplayed().should.be.true
      this.listOfMilestonesRightSide[0].isDisplayed().should.be.true
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  verifyStartingTemplateDrawerAndEmptyStateForMS() {
    try {
      this.startWithATemplateHeadingOnDrawer.isDisplayed().should.be.true
      this.noTemplateSelectedLabel.isDisplayed().should.be.true
      this.continueFromScratchButton.isDisplayed().should.be.true
      this.rightSideSectionOnStepOne.isDisplayed().should.be.true
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  enterOverviewSectionAndPerformAddDeleteAddTasksAndSaveChanges() {
    try {
      try {
        this.milestoneHeadingOnMSDrawer
          .getText()
          .should.equal("Add new Milestone")
      } catch (err) {
        this.milestoneHeadingOnMSDrawer
          .getText()
          .should.equal("Add from scratch")
      }
      //performing toggling of overview and tasks section
      this.verifyOverviewAndTaskSectionToggling()

      //Edit the Overview section with name,rationale,description
      this.clearAndEnteredDataForOverviewSection()

      //select random date from calendar,clear and verify and reselect
      this.selectDueDateAndClearAndVerifyAndReselectDueDate()

      // verify placeholder and add two new tasks,Delete All the Tasks, and again add two new tasks
      this.tasksCountOnMSDrawer.isDisplayed().should.be.false
      this.noTasksYetTextOnMSDrawer.isDisplayed().should.be.true

      this.noTasksYetTextOnMSDrawer.click()
      browser.pause(2000)
      this.milestoneHeadingOnMSDrawer.isDisplayed().should.be.false
      this.headingOnTaskDrawer.getText().should.equal("Add task from scratch")
      this.goBackButtonOnAddTaskFromScratchDrawer.click()
      browser.pause(1000)
      this.milestoneHeadingOnMSDrawer.isDisplayed().should.be.true
      this.addButtonOnAddMSDrawer.click()
      browser.pause(1000)
      this.addAtLeastOneTaskErrorTextOnMSDrawer.isDisplayed().should.be.true

      this.addTaskFromScratch("task1").should.be.true
      this.addTaskFromScratch("task2").should.be.true

      this.tasksEditIconListOnMSDrawer.length.should.equal(
        parseInt(this.tasksCountOnMSDrawer.getText())
      )
      this.tasksEditIconListOnMSDrawer.forEach((el) => {
        el.click()
        this.removeTaskOnEditMSDrawer()
        browser.pause(1000)
      })
      this.addAtLeastOneTaskErrorTextOnMSDrawer.isDisplayed().should.be.true

      this.addTaskFromScratch("mailinator").should.be.true

      this.checksAllTaskOnMSDrawer().should.be.true
      this.addButtonOnAddMSDrawer.click()
      browser.pause(2000)
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  verifyContinueButtonDisabled() {
    try {
      browser.pause(1000)
      var isDisabled = this.continueButtonOnStepTwoOnCarePlan.getAttribute(
        "disabled"
      )
      isDisabled.should.equal("true")
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  removeAllMSFromRightSideList() {
    try {
      console.log(
        "No. of MS available Be: " + this.listOfMilestonesRightSide.length
      )
      this.listOfMilestonesRightSide.forEach((el) => {
        el.click()
        browser.pause(1000)
        this.removeButtonOnEditMSDrawer.scrollIntoView()
        browser.pause(500)
        this.removeButtonOnEditMSDrawer.click()
        browser.pause(1000)
      })
      console.log(
        "No. of MS available: " + this.listOfMilestonesRightSide.length
      )

      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  checksAllTaskOnMSDrawer() {
    try {
      this.tasksNameListOnMSDrawer.forEach((el) => {
        el.click()
        browser.pause(500)
      })
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  verifyReviewAndFinishPage() {
    try {
      this.reviewAndFinishHeadingOnDrawer.isDisplayed().should.be.true
      this.finishAndSendButtonOnDrawer.isDisplayed().should.be.true
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  verifyCustomizeMilestonePage() {
    try {
      this.customizeMilestonesHeadingOnDrawer.isDisplayed().should.be.true
      this.continueButtonOnStepTwoOnCarePlan.isDisplayed().should.be.true
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  clickOnStepperTwoOnCarePlan() {
    try {
      browser.pause(1000)
      this.stepperTwoIcon.click()
      browser.pause(1000)
    } catch (err) {
      console.error("getting error while clicking on stepper-2", err)
    }
  }

  removeTaskOnEditMSDrawer() {
    try {
      browser.pause(500)
      this.headingOnTaskDrawer.getText().should.equal("Edit Task")
      let taskName = this.tasksNameInputOnMSDrawer.getText()
      this.removeButtonOnEditMSDrawer.click()
      browser.pause(500)
      this.removeTaskLabelOnWarningDrawer
        .getText()
        .should.equal("Remove " + '"' + taskName + '?"')
      this.removeButtonOnDrawer.click()
    } catch (err) {
      console.error("Error in deleting Task", err)
    }
  }
}

export default new CarePlanPage()
