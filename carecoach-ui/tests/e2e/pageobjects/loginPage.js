import Page from "./page"

class LoginPage extends Page {
  /**
   * define elements
   */
  get headerImage() {
    return $("//div[contains(@class,'visible-lg')]//img")
  }

  get usernameInput() {
    return $(
      "//div[contains(@class,'visible-lg')]//input[@id='signInFormUsername']"
    )
  }

  get passwordInput() {
    return $(
      "//div[contains(@class,'visible-lg')]//input[@id='signInFormPassword']"
    )
  }

  get loginButton() {
    return $(
      "//div[contains(@class,'visible-lg')]//input[@name='signInSubmitButton']"
    )
  }

  get usernameLabel() {
    return $(
      "//div[contains(@class,'visible-lg')]//label[text()='Email or Phone number']"
    )
  }

  get passwordLabel() {
    return $("//div[contains(@class,'visible-lg')]//label[text()='Password']")
  }

  get forgotYourPasswordLabel() {
    return $(
      "//div[contains(@class,'visible-lg')]//a[text()='Forgot your password?']"
    )
  }

  get signUpLink() {
    return $("//div[contains(@class,'visible-lg')]//a[text()='Sign up']")
  }

  /**
   * define or overwrite page methods
   */
  open() {
    browser.maximizeWindow()
    super.open("")
    browser.pause(3000)
  }
  /**
   * page specific methods
   */

  waitForloginPageToLoad() {
    if (!this.headerImage.isDisplayed()) {
      this.headerImage.waitForDisplayed(5000)
    }
  }

  validateUIFields() {
    this.waitForloginPageToLoad()
    try {
      this.usernameLabel.isDisplayed() &&
        this.passwordLabel.isDisplayed() &&
        this.forgotYourPasswordLabel.isDisplayed() &&
        this.signUpLink.isDisplayed() &&
        this.usernameInput.isDisplayed() &&
        this.passwordInput.isDisplayed() &&
        this.loginButton.isDisplayed()
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  enterUsername(username) {
    this.usernameInput.clearValue()
    this.usernameInput.setValue(username)
    browser.pause(1000)
  }

  enterPassword(pswd) {
    this.passwordInput.clearValue()
    this.passwordInput.setValue(pswd)
    browser.pause(1000)
  }

  clickOnSignButton() {
    this.loginButton.click()
  }
}

export default new LoginPage()
