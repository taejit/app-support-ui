/* eslint-disable no-unused-expressions */
import myCaregiversPage from "./myCaregiversPage"
import Page from "./page"

class NotificationsPage extends Page {
  /**
   * define elements
   */

  get notificationIcon() {
    return $("//button[@id='NotificationIcon']")
  }

  get badgeOnNotificationIcon() {
    return $(
      "//button[@id='NotificationIcon']//span[contains(@class,'MuiBadge-badge')]"
    )
  }

  get notificationLabel() {
    return $("//p[.='Notifications']")
  }

  get crossButtonOnNC() {
    return $("//p[.='Notifications']/following-sibling::button[2]/span[1]")
  }

  get emptyStateLabelOnNC() {
    return $("//h1[@class='alertscreen_headingText']")
  }

  get earlierLabelOnNC() {
    return $("//p[.='EARLIER']")
  }

  get listOfNotifications() {
    return $$("//div[@id='NotificationList-main']//div[@aria-hidden]")
  }

  waitForNotificationChannelToLoad() {
    if (!this.notificationLabel.isDisplayed()) {
      this.notificationLabel.waitForDisplayed(5000)
    }
  }

  clickOnNotificationsIcon() {
    myCaregiversPage.waitForMyCareGiversPageToLoad()
    this.notificationIcon.click()
    this.waitForNotificationChannelToLoad()
    browser.pause(3000)
  }

  verifyResultOfNotificationChannel() {
    try {
      try {
        this.listOfNotifications[0].isDisplayed().should.be.true
      } catch (err) {
        this.emptyStateLabelOnNC.isDisplayed().should.be.true
      }
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  verifyBadgeOnNotificationIcon() {
    browser.pause(5000)
    console.log(
      "Badge On Notification Icon Available :",
      this.badgeOnNotificationIcon.isDisplayed()
    )
    return true
  }

  verifyNoBadgeOnNotificationIconIfVisitedNC() {
    try {
      this.badgeOnNotificationIcon.isDisplayed().should.be.false
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  closeNotificationChannel() {
    try {
      browser.pause(1200)
      this.crossButtonOnNC.click()
      browser.pause(1000)
      this.notificationLabel.isDisplayed().should.be.false
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }
}

export default new NotificationsPage()
