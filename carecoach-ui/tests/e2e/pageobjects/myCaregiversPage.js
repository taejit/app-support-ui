/* eslint-disable no-unused-expressions,no-unused-vars */
import Page from "./page"
import util from "../utilities/util"

var selectedName, selectedContactStatus, searchedChar
class MyCaregiversPage extends Page {
  /**
   * define elements
   */

  get myCaregiversHeading() {
    return $("//div[contains(@class,'primaryText')]")
  }

  get goodThingsLabel() {
    return $("//div[text()='Good things take time!']")
  }

  get goodThingsImage() {
    return $("//div//img[@id='caregiverImg']")
  }

  get careGiversFullNameList() {
    return $$("//div[contains(@class,'primaryText capitalize')]")
  }

  get conditionsList() {
    return $$("//img[@alt='recipient-conditions']/following-sibling::div")
  }

  get careGiversContactStatusList() {
    return $$(
      "//div[contains(@class,'lastContacted') or contains(@class,'notContactedAtAll')]"
    )
  }

  get messageBadgeList() {
    return $$("//div[contains(@class,'messageIcon')]//img")
  }

  get myCaregiverGridList() {
    return $$(
      "//div[@id='ccCaregiverCard']//div[contains(@class,'caregiverData')]"
    )
  }

  get careGiversInitials() {
    return $("//div[contains(@class,'caregiverAvatar')]/div")
  }

  get careGiversCarePlan() {
    return $(
      "//span[.='Create' or .='Add Milestones']/ancestor::div[contains(@class,'MuiGrid-item')][1]/div/div[1]"
    )
  }

  get UpcomingToggleTextOnCarePlan() {
    return $("//p[.='Upcoming']")
  }

  get CompleteToggleTextOnCarePlan() {
    return $("//p[.='Complete']")
  }

  get addMilestonesButtonOnCarePlan() {
    return $(
      "//button[@id='editcareplanaddmilestone']/span[text()='Add Milestones']"
    )
  }

  get NoOfUpcomingMSOnCarePlan() {
    return $("//p[.='Upcoming']/../../following-sibling::div//div")
  }

  get NoOfCompleteMSOnCarePlan() {
    return $("//p[.='Complete']/../../following-sibling::div//div")
  }

  get listOfUpcomingMSOnCarePlan() {
    return $$(
      "//p[.='Upcoming']/../../../following-sibling::div//p/../following-sibling::div/div[1]/p[1]"
    )
  }

  get listOfCompleteMSOnCarePlan() {
    return $$(
      "//p[.='Complete']/../../../following-sibling::div//p/../following-sibling::div/div[1]/p[1]"
    )
  }

  get createButtonOnCarePlan() {
    return $("//button[@id='createBtn']/span[text()='Create']")
  }

  get createCarePlanButtonOnCarePlan() {
    return $("//button[@id='createCarePlan']/span[text()='Create Care Plan']")
  }

  get blankSlateLabelOnCarePlan() {
    return $("//div[contains(text(),'a blank slate!')]")
  }

  get blankSlateTextOnCarePlan() {
    return $(
      "//div[text()='To get started, click the Create button and search for relevant medical conditions or tasks.']"
    )
  }

  get searchBoxOnMyCaregiversPage() {
    return $("//input[@id='ccSearchCaregiver']")
  }

  get nothingFoundOnMyCaregiversPage() {
    return $("//div[text(='Nothing found!']")
  }

  get nothingFoundImageOnMyCaregiversPage() {
    return $("//div//img[@id='caregiverImg']")
  }

  get profileLinkOnCaregivers() {
    return $("//div[text()='Profile']")
  }

  get notesLinkOnCaregivers() {
    return $("//div[text()='Notes']")
  }

  /**Xpaths for profile of caregivers */

  get careGiversProfile() {
    return $("//div[@id='profiletitle']")
  }

  get aboutTheCaregiverLabelOnCaregiversProfilePage() {
    return $("//div[@id='profilecaregivertabletitle']")
  }

  get cgFullName() {
    return $(
      "//div[text()='ABOUT THE CAREGIVER']/following-sibling::div//th[text()='Full name']/following-sibling::td"
    )
  }
  get cgGoesBy() {
    return $(
      "//div[text()='ABOUT THE CAREGIVER']/following-sibling::div//th[text()='Goes by']/following-sibling::td"
    )
  }
  get cgPronouns() {
    return $(
      "//div[text()='ABOUT THE CAREGIVER']/following-sibling::div//th[text()='Pronouns']/following-sibling::td"
    )
  }
  get cgEmployer() {
    return $(
      "//div[text()='ABOUT THE CAREGIVER']/following-sibling::div//th[text()='Employer']/following-sibling::td"
    )
  }

  get cgEmail() {
    return $(
      "//div[text()='ABOUT THE CAREGIVER']/following-sibling::div//th[text()='Email']/following-sibling::td"
    )
  }
  get cgMobilePhone() {
    return $(
      "//div[text()='ABOUT THE CAREGIVER']/following-sibling::div//th[text()='Mobile phone']/following-sibling::td"
    )
  }
  get cgLocation() {
    return $(
      "//div[text()='ABOUT THE CAREGIVER']/following-sibling::div//th[text()='Location']/following-sibling::td"
    )
  }
  get cgCaregivingRole() {
    return $(
      "//div[text()='ABOUT THE CAREGIVER']/following-sibling::div//th[text()='Caregiving role']/following-sibling::td"
    )
  }
  get cgTimeSpentInRole() {
    return $(
      "//div[text()='ABOUT THE CAREGIVER']/following-sibling::div//th[text()='Time spent in role']/following-sibling::td"
    )
  }
  get cgCommPreference() {
    return $(
      "//div[text()='ABOUT THE CAREGIVER']/following-sibling::div//th[text()='Communication preference']/following-sibling::td"
    )
  }

  get aboutTheLovedOneOnCaregiversProfilePage() {
    return $("//div[@id='profilecarereceipienttabletitle']")
  }

  get crFullName() {
    return $(
      "//div[text()='ABOUT THE LOVED ONE']/following-sibling::div//th[text()='Full name']/following-sibling::td"
    )
  }
  get crGoesBy() {
    return $(
      "//div[text()='ABOUT THE LOVED ONE']/following-sibling::div//th[text()='Goes by']/following-sibling::td"
    )
  }
  get crPronouns() {
    return $(
      "//div[text()='ABOUT THE LOVED ONE']/following-sibling::div//th[text()='Pronouns']/following-sibling::td"
    )
  }
  get dob() {
    return $(
      "//div[text()='ABOUT THE LOVED ONE']/following-sibling::div//th[text()='Date of birth']/following-sibling::td"
    )
  }
  get relation() {
    return $(
      "//div[text()='ABOUT THE LOVED ONE']/following-sibling::div//th[text()='Relation']/following-sibling::td"
    )
  }
  get livingArrangement() {
    return $(
      "//div[text()='ABOUT THE LOVED ONE']/following-sibling::div//th[text()='Living arrangement']/following-sibling::td"
    )
  }
  get crLocation() {
    return $(
      "//div[text()='ABOUT THE LOVED ONE']/following-sibling::div//th[text()='Location']/following-sibling::td"
    )
  }
  get crConditions() {
    return $(
      "//div[text()='ABOUT THE LOVED ONE']/following-sibling::div//th[text()='Conditions']/following-sibling::td"
    )
  }
  /**Notes xpaths */
  get notesHeading() {
    return $("//div[@id='notestitle']")
  }

  get addButtonOnNotes() {
    return $("//span[text()='Add']")
  }

  get addANoteButtonOnNotes() {
    return $("//span[text()='Add a note']")
  }

  get jotThatDownLabelOnNotes() {
    return $("//div[@id='jotthatdown']")
  }

  get notesPageImageOnNotes() {
    return $("//img[@alt='No notes']")
  }

  get cancelButtonOnNotes() {
    return $("//span[text()='Cancel']")
  }

  get addButtonOnAddNotes() {
    return $("//button[@id='submitform']/span[text()='Add']")
  }

  get addOrEditHeadingOnNotes() {
    return $("//div[@id='rightdrawertitle']")
  }

  get titleInputOnNotes() {
    return $("//textarea[@id='noteTitle']")
  }
  get bodyInputOnNotes() {
    return $("//textarea[@id='noteBody']")
  }

  get nextStepsInputOnNotes() {
    return $("//textarea[@id='noteNextSteps']")
  }
  get noteTitleList() {
    return $$("//div[@id='notetitle']")
  }
  get noteBodyList() {
    return $$("//div[@id='notebody']")
  }

  get saveChangesButton() {
    return $("//button[@id='submitform']/span[text()='Save changes']")
  }

  get removeButton() {
    return $("//button[@id='remove']/span[text()='Remove']")
  }

  get noChangesMadeText() {
    return $("//h4")
  }
  waitForMyCareGiversPageToLoad() {
    if (!this.myCaregiversHeading.isDisplayed()) {
      this.myCaregiversHeading.waitForDisplayed(5000)
    }
  }

  getMyCareGiversPageToLoad() {
    this.waitForMyCareGiversPageToLoad()
    return this.myCaregiversHeading.getText()
  }

  validateMyCaregiversInformation() {
    this.waitForMyCareGiversPageToLoad()
    try {
      browser.pause(2000)
      try {
        console.log(`${this.myCaregiverGridList.length} Caregivers available`)
        console.log(
          `${this.messageBadgeList.length} unread message(s) from Caregivers`
        )
        util.checkValidNames(this.careGiversFullNameList).should.be.true
        util.checkValidContactStatus(this.careGiversContactStatusList).should.be
          .true
        // util.checkNamesInAlphaOrder(this.careGiversFullNameList).should.be.true
      } catch (err) {
        console.log("No Caregivers allotted to this Care Coach")
        this.goodThingsLabel.isDisplayed().should.be.true
        this.goodThingsImage.isDisplayed().should.be.true
      }
      return true
    } catch (err) {
      console.error("Error in Caregiver Listing page", err)
      return false
    }
  }

  clickOnRandomCaregiverFromList() {
    browser.pause(6000)
    let x = 0,
      no = 5
    this.careGiversFullNameList.forEach((el) => {
      if (x === no) {
        selectedName = el.getText()
      }
      x++
    })
    x = 0
    this.careGiversContactStatusList.forEach((el) => {
      if (x === no) {
        selectedContactStatus = el.getText()
      }
      x++
    })
    x = 0
    this.careGiversFullNameList.forEach((el) => {
      if (x === no) {
        el.click()
      }
      x++
    })
  }

  clickOnSpecificCaregiverFromList(no) {
    browser.pause(6000)
    let x = 0
    this.careGiversFullNameList.forEach((el) => {
      if (x === no) {
        selectedName = el.getText()
      }
      x++
    })
    x = 0
    this.careGiversContactStatusList.forEach((el) => {
      if (x === no) {
        selectedContactStatus = el.getText()
      }
      x++
    })
    x = 0
    this.careGiversFullNameList.forEach((el) => {
      if (x === no) {
        el.click()
      }
      x++
    })
  }

  waitForCareGiversCarePlanPageToLoad() {
    if (!this.careGiversCarePlan.isDisplayed()) {
      this.careGiversCarePlan.waitForDisplayed(5000)
    }
  }

  validateIndividualCaregiverInfoWithCaregiversListingData() {
    browser.pause(6000)
    let cgContactStatus
    let cgName = this.getCareGiverName()
    this.careGiversContactStatusList.forEach((el) => {
      cgContactStatus = el.getText()
    })
    const cgInitial =
      cgName.substring(0, 1) +
      cgName.substring(cgName.indexOf(" ") + 1, cgName.indexOf(" ") + 2)
    selectedName.should.equals(cgName)
    selectedContactStatus.should.equals(cgContactStatus)
    this.careGiversInitials.getText().should.equals(cgInitial)
  }

  validateEmptyStatePage() {
    this.waitForCareGiversCarePlanPageToLoad()
    try {
      this.validateIndividualCaregiverInfoWithCaregiversListingData()
      this.createButtonOnCarePlan.isDisplayed().should.be.true
      this.createCarePlanButtonOnCarePlan.isDisplayed().should.be.true
      this.blankSlateLabelOnCarePlan.isDisplayed().should.be.true
      this.blankSlateTextOnCarePlan.isDisplayed().should.be.true
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  validateCaregiversCarePlanInformation() {
    this.waitForCareGiversCarePlanPageToLoad()
    try {
      this.validateIndividualCaregiverInfoWithCaregiversListingData()
      this.addMilestonesButtonOnCarePlan.isDisplayed().should.be.true
      this.UpcomingToggleTextOnCarePlan.isDisplayed().should.be.true
      this.CompleteToggleTextOnCarePlan.isDisplayed().should.be.true
      this.listOfUpcomingMSOnCarePlan.length.should.equal(
        parseInt(this.NoOfUpcomingMSOnCarePlan.getText())
      )
      this.listOfCompleteMSOnCarePlan.length.should.equal(
        parseInt(this.NoOfCompleteMSOnCarePlan.getText())
      )
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  enterRandomCharacterOnSearchField() {
    this.searchBoxOnMyCaregiversPage.setValue(
      util.generateName(1).toLowerCase()
    )
    browser.pause(2000)
    searchedChar = this.searchBoxOnMyCaregiversPage.getValue()
  }

  validateMatchedCaregiversOrNothingFound() {
    try {
      var isMatched = true
      var cgNames = [],
        conditionNames = []
      try {
        this.careGiversFullNameList.forEach((el) => {
          cgNames.push(el.getText())
        })
        this.conditionsList.forEach((el) => {
          conditionNames.push(el.getText())
        })

        for (let i = 0; i < cgNames.length - 1; i++) {
          if (
            cgNames[i].toLowerCase().includes(searchedChar) ||
            conditionNames[i].toLowerCase().includes(searchedChar)
          ) {
          } else {
            isMatched = false
            break
          }
        }
      } catch (err) {
        console.log("No Matched Caregivers or Conditions found")
        this.nothingFoundOnMyCaregiversPage.isDisplayed().should.be.true
        this.nothingFoundImageOnMyCaregiversPage.isDisplayed().should.be.true
      }
      return isMatched
    } catch (err) {
      console.error(err)
      return false
    }
  }

  waitForCareGiversProfilePageToLoad() {
    if (!this.aboutTheCaregiverLabelOnCaregiversProfilePage.isDisplayed()) {
      this.aboutTheCaregiverLabelOnCaregiversProfilePage.waitForDisplayed(5000)
    }
  }

  validateCaregiversProfileInformation() {
    try {
      this.waitForCareGiversCarePlanPageToLoad()
      let cgName = this.getCareGiverName()
      this.profileLinkOnCaregivers.click()
      this.waitForCareGiversProfilePageToLoad()

      this.careGiversProfile
        .getText()
        .should.equals(`${cgName.substring(0, cgName.indexOf(" "))}'s Profile`)
      browser.pause(1000)
      this.aboutTheLovedOneOnCaregiversProfilePage.scrollIntoView()
      browser.pause(1000)
      this.crConditions.scrollIntoView()
      browser.pause(1000)

      this.cgGoesBy.isDisplayed().should.be.true
      this.cgPronouns.isDisplayed().should.be.true
      this.crGoesBy.isDisplayed().should.be.true
      this.crPronouns.isDisplayed().should.be.true

      if (
        this.cgFullName.getText() !== "" &&
        this.cgEmployer.getText() !== "" &&
        this.cgEmail.getText() !== "" &&
        this.cgMobilePhone.getText() !== "" &&
        this.cgLocation.getText() !== "" &&
        this.cgCaregivingRole.getText() !== "" &&
        this.cgTimeSpentInRole.getText() !== "" &&
        this.cgCommPreference.getText() !== "" &&
        this.crFullName.getText() !== "" &&
        this.dob.getText() !== "" &&
        this.relation.getText() !== "" &&
        this.livingArrangement.getText() !== "" &&
        this.crLocation.getText() !== "" &&
        this.crConditions.getText() !== ""
      ) {
        return true
      } else {
        return false
      }
    } catch (err) {
      console.error(err)
      return false
    }
  }

  getCareGiverName() {
    let cgName
    this.careGiversFullNameList.forEach((el) => {
      cgName = el.getText()
    })
    return cgName
  }

  waitForNotesPageToLoad() {
    if (!this.notesHeading.isDisplayed()) {
      this.notesHeading.waitForDisplayed(5000)
    }
  }

  addAndViewNotesForCaregivers() {
    try {
      this.waitForCareGiversCarePlanPageToLoad()
      let cgName = this.getCareGiverName()
      this.notesLinkOnCaregivers.click()
      this.waitForNotesPageToLoad()

      this.notesHeading
        .getText()
        .should.equals(
          "Your Notes on " + cgName.substring(0, cgName.indexOf(" "))
        )
      browser.pause(1000)
      this.addButtonOnNotes.isDisplayed().should.be.true

      try {
        this.addANoteButtonOnNotes.isDisplayed().should.be.true
        this.jotThatDownLabelOnNotes.isDisplayed().should.be.true
        this.notesPageImageOnNotes.isDisplayed().should.be.true
        this.addANoteButtonOnNotes.click()
        browser.pause(2000)
        this.addOrEditHeadingOnNotes.getText().should.equal("Add Note")
        this.cancelButtonOnNotes.click()
        browser.pause(1000)
        this.addANoteButtonOnNotes.isDisplayed().should.be.true
        this.jotThatDownLabelOnNotes.isDisplayed().should.be.true
        this.notesPageImageOnNotes.isDisplayed().should.be.true
      } catch (err) {
        console.log(`No. of Notes available:${this.noteTitleList.length}`)
        this.addButtonOnNotes.click()
        browser.pause(2000)
        this.addOrEditHeadingOnNotes.getText().should.equal("Add Note")
        this.cancelButtonOnNotes.click()
        browser.pause(1000)
      }
      this.addButtonOnNotes.click()
      browser.pause(2000)
      this.addOrEditHeadingOnNotes.getText().should.equal("Add Note")
      var title = util.generateTitle()
      this.titleInputOnNotes.setValue(title)
      browser.pause(1000)
      var body = util.generateBody()
      this.bodyInputOnNotes.setValue(body)
      browser.pause(1000)
      this.nextStepsInputOnNotes.setValue(body)
      browser.pause(1000)
      this.addButtonOnAddNotes.click()
      browser.pause(6000)

      let recentlyAddedTitle = this.noteTitleList[0].getText()
      let recentlyAddedBody = this.noteBodyList[0].getText()

      recentlyAddedTitle.should.equal(title)
      recentlyAddedBody.should.equal(body)
      console.log(
        `After Adding new Notes, No. of Notes available:${this.noteTitleList.length}`
      )
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  editRandomNoteAndViewNoteForCaregivers() {
    try {
      this.waitForCareGiversCarePlanPageToLoad()
      this.notesLinkOnCaregivers.click()
      this.waitForNotesPageToLoad()
      browser.pause(1000)
      this.addButtonOnNotes.isDisplayed().should.be.true

      this.noteTitleList[
        Math.floor(Math.random() * this.noteTitleList.length)
      ].click()
      browser.pause(2000)
      this.noChangesMadeText.isDisplayed().should.be.true
      this.cancelButtonOnNotes.click()
      browser.pause(2000)
      console.log(
        `Before Editing No. of Notes available:${this.noteTitleList.length}`
      )
      this.noteTitleList[
        Math.floor(Math.random() * this.noteTitleList.length)
      ].click()
      browser.pause(2000)

      var title = util.generateTitle()
      util.clearValue(this.titleInputOnNotes)
      this.titleInputOnNotes.setValue(title)
      browser.pause(1000)
      var body = util.generateBody()
      util.clearValue(this.bodyInputOnNotes)
      this.bodyInputOnNotes.setValue(body)
      browser.pause(1000)
      util.clearValue(this.nextStepsInputOnNotes)
      this.nextStepsInputOnNotes.setValue(body)
      browser.pause(1000)
      this.saveChangesButton.click()
      browser.pause(6000)
      let recentlyAddedTitle = this.noteTitleList[0].getText()
      let recentlyAddedBody = this.noteBodyList[0].getText()

      recentlyAddedTitle.should.equal(title)
      recentlyAddedBody.should.equal(body)
      console.log(
        `After Editing No. of Notes available:${this.noteTitleList.length}`
      )
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  removeRandomNoteAndVerifyNoteDeletedForCaregiver() {
    try {
      let noteArray = []
      let isDeleted = true
      this.waitForCareGiversCarePlanPageToLoad()
      this.notesLinkOnCaregivers.click()
      this.waitForNotesPageToLoad()
      browser.pause(1000)
      console.log(
        `Before Removing, No. of Notes available:${this.noteTitleList.length}`
      )
      this.noteTitleList[
        Math.floor(Math.random() * this.noteTitleList.length)
      ].click()
      browser.pause(2000)
      let note = this.titleInputOnNotes.getText()
      this.removeButton.click()
      browser.pause(6000)
      this.noteTitleList.forEach((el) => {
        noteArray.push(el.getText())
      })
      for (let i = 0; i < noteArray.length - 1; i++) {
        if (noteArray[i] === note) {
          isDeleted = false
          break
        }
      }
      console.log(
        `After Removing, No. of Notes available:${this.noteTitleList.length}`
      )
      return isDeleted
    } catch (err) {
      console.error(err)
      return false
    }
  }
}

export default new MyCaregiversPage()
