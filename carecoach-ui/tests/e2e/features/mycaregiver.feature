Feature: Memento Care Coach My Caregivers

    As a Care Coach
    I want to test all the functionality of my Caregivers Page

    Background: User is logged in and is on Care Coach Home Page
        Given I am on the cognito login page
        When I enter username as "actualusername"
        And I enter password as "actualpassword"
        And I attempt to login
        Then I should see care coach home page
        When I should click on "My Caregivers" menu from left sidebar
        Then I should see My Caregivers page and related information


    Scenario: Care Coach is able to search and see the matched records for caregivers and conditions
        When I enter any random character in search box on my caregivers
        Then I should see matched caregivers or nothing found page if no matched caregivers or conditions

    Scenario: Care Coach is able to see random caregivers profile
        When I click on any random caregivers from the list
        Then I should see Caregivers profile information

    Scenario: Care Coach is able to add and view notes for caregivers
        When I click on any random caregivers from the list
        Then I should able to add notes for caregiver and able to see the added notes

    Scenario: Care Coach is able to edit any random note and view note for caregivers
        When I click on any random caregivers from the list
        Then I should able to edit random note for caregiver and able to see the updated notes

    Scenario: Care Coach is able to delete any random note and verify the note is deleted for the caregiver
        When I click on any random caregivers from the list
        Then I should able to remove random note for caregiver and verify the note is deleted for the caregiver



