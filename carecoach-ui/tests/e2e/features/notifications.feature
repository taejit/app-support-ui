Feature: Memento Care Coach Notification Channel

    As a Care Coach
    I want to see all the received notifications with read unread functionality and show badge icon for the new received notification

    Background: User is logged in and is on Care Coach Home Page
        Given I am on the cognito login page
        When I enter username as "actualusername"
        And I enter password as "actualpassword"
        And I attempt to login
        Then I should see care coach home page

    Scenario: Care Coach is able to see receievd notifcation in the notification channel
        When I click on notification icon
        Then I should see either empty state for notifcation or list of receievd notifications

    Scenario: Care Coach is able to see badge on notifcation icon if any new notification is there
        Then I should see badge on notifcation icon if any notification there
        When I click on notification icon
        And I close the notification channel
        Then I should not see any badge on notification icon