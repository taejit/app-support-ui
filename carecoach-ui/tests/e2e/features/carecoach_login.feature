Feature: Memento Care Coach Portal Login
    As a Care Coach
    I want to visit the login page of cognito for Memento Application
    So that I can Login into my account

    Background:

        Given I am on the cognito login page

    Scenario: Visit the Login page
        Then I should see cognito login form fields

    Scenario: Attempt to login with wrong credentials
        When I enter username as "wrong@email.com"
        And I enter password as "wrongPassword"
        And I attempt to login
        Then I should see login error message as "Incorrect username or password."

    Scenario: User is not part of care coach group and try to attempt to login with his credentials
        When I enter username as "harshal@mailinator.com"
        And I enter password as "actualpassword"
        And I attempt to login
        Then I should see "Forbidden" page
        And  I should see forbidden message as "You do not have permissions to access this portal."

    Scenario: Attempt to login with correct credentials
        When I enter username as "actualusername"
        And I enter password as "actualpassword"
        And I attempt to login
        Then I should see care coach home page

