Feature: View/Edit Care Coach Profile
    As a Care Coach User of the application
    I want to view and update my profile

    Background: Care Coach is logged in and is on Home Page
        Given I am on the cognito login page
        When I enter username as "actualusername"
        And I enter password as "actualpassword"
        And I attempt to login
        Then I should see care coach home page

    Scenario: Successful View Care Coach Profile
        When I click on "Profile" link on the care coach home page
        Then I should see "My Profile" page
        And I should see care coach info on my Profile Page

    Scenario: Successful Edit Care Coach information on Profile
        When I click on "Profile" link on the care coach home page
        Then I should see "My Profile" page
        When I click on "Edit" button on my profile page
        Then I should see "Edit my profile" page
        When I update care coach info on my profile page
        And I click on "Save changes" button on "Edit my profile" page
        Then I should see "Profile updated." message at the bottom on "My Profile" page

