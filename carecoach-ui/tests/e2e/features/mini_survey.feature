Feature: Memento Care Coach My CareGivers - Mini Survey

    As a Care Coach user of the application
    I want to test the functionality of Mini Survey submitted by Caregivers

    Background: User is logged in and is on Care Coach Home Page
        Given I am on the cognito login page
        When I enter username as "actualusername"
        And I enter password as "actualpassword"
        And I attempt to login
        Then I should see care coach home page
        When I should click on "My Caregivers" menu from left sidebar
        Then I should see My Caregivers page and related information

    Scenario: Show empty state if no survey filled yet
        When I click on caregiver from the list who has not submitted the mini survey
        Then I should see landing page
        When I click on "Progress" link from left sidebar
        Then I should verify empty state of mini survey

    Scenario: Check answers given by Caregiver
        When I click on caregiver from the list who have already submitted the mini survey
        Then I should see landing page
        When I click on "Progress" link from left sidebar
        Then I should see and verify caregivers progress
        When I click on recently submitted survey
        Then I should see right drawer and verify submitted date and score
