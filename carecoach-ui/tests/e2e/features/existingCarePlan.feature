Feature: Memento Care Coach My Caregivers - Existing Care Plan Details

    As a Care Coach
    I want to test add new milestone,remove milestone,search from cms for the existing care plan of caregiver

    Background: User is logged in and is on Care Coach Home Page
        Given I am on the cognito login page
        When I enter username as "actualusername"
        And I enter password as "actualpassword"
        And I attempt to login
        Then I should see care coach home page
        When I should click on "My Caregivers" menu from left sidebar
        Then I should see My Caregivers page and related information
        When I click on caregiver from the list who have already created care plan
        Then I should see Caregivers Care Plan page and related information

    ##This is failing
    Scenario: Care Coach is able to add new milestone from scratch with add remove and add tasks and save the milestone for the existing care plan
        When I click on "Add Milestones" button
        Then I should see drawer for options to add milestones
        When I click on "Add from scratch" button
        Then I click on "Go back" button
        When I click on "Add from scratch" button
        Then I entered details on overview section and perform add delete and add tasks and save milestone
        And I should verify the changes on "Complete" section of existing care plan page

    Scenario: Care Coach is able to edit existing milestone for the existing care plan
        When I select random milestone from the upcoming milestone list
        Then I edit overview section and perform delete and add tasks and save changes
        And I should verify the changes on "Upcoming" section of existing care plan page

    Scenario: Care Coach is able to remove existing milestone for the existing care plan
        When I select random milestone from the upcoming milestone list
        Then I click on remove button to delete milestone
        And I should verify the deleted milestone is removed from upcoming section of existing care plan

    Scenario: Care Coach is able to search existing milestones from contentful and able to view the searched milestone list for the existing care plan
        When I click on "Add Milestones" button
        Then I should see drawer for options to add milestones
        When I click on "Search existing Milestones" button
        Then I click on "Go back" button
        When I click on "Search existing Milestones" button
        Then I enter any random character in search milestones field and verify the result

    Scenario: Care Coach is able to add new milestones from search existing milestone and save the milestone for the existing care plan
        When I click on "Add Milestones" button
        And I click on "Search existing Milestones" button
        And I select random milestones from the list
        Then I remove milestones from the selected bottom list
        When I enter any character in search milestones field and select random milestone from the list and click on add milestone
        Then I should verify the changes on "Upcoming" section of existing care plan page
