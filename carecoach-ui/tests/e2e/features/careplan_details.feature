Feature: Memento Care Coach My Caregivers - Care Plan Details

    As a Care Coach
    I want to test all the functionality of Caregivers - Care Plan Details

    Background: User is logged in and is on Care Coach Home Page
        Given I am on the cognito login page
        When I enter username as "actualusername"
        And I enter password as "actualpassword"
        And I attempt to login
        Then I should see care coach home page
        When I should click on "My Caregivers" menu from left sidebar
        Then I should see My Caregivers page and related information

    Scenario: Care Coach is able to see Empty State of Care Plan,search care plan template and able to view the searched templates list
        When I click on caregiver from the list who does not have created care plan
        Then I should see empty state page
        When I click on "Create Care Plan" button
        Then I enter any random character in search templates field and verify the result

    Scenario: Care Coach is able to View Care Plan
        When I click on caregiver from the list who have already created care plan
        Then I should see Caregivers Care Plan page and related information

    Scenario: Care Coach is able to search existing milestones from contentful and able to view the searched milestone list
        When I click on caregiver from the list who does not have created care plan
        Then I should see empty state page
        When I click on "Create Care Plan" button
        And I select random template and apply
        Then I enter any random character in search milestones field and verify the result

    Scenario: Care Coach is able to add care plan template & multiple milestones to the right side milestone list & review care plan
        When I click on caregiver from the list who does not have created care plan
        Then I should see empty state page
        When I click on "Create Care Plan" button
        And I select random template and apply
        And I select random milestone and perform undo
        Then I select multiple milestones and verify milestones added on milestones listing
        When I click on "Continue" button
        Then I should verify review and finish page of care plan
        And I click on back stepper and should see warning drawer with nevermind and continue button
        And I click on "Nevermind" button on warning drawer
        Then I should verify review and finish page of care plan
        When I click on back stepper and should see warning drawer with nevermind and continue button
        And I click on "Continue" button on warning drawer
        Then I should be on start with a template page

    Scenario: Care Coach is able to add template & customize the existing milestones with remove and add tasks and save the milestone
        When I click on caregiver from the list who does not have created care plan
        Then I should see empty state page
        When I click on "Create Care Plan" button
        And I select random template and apply
        And I select random milestone from the list
        Then I click on cancel button on edit MS Drawer
        When I select random milestone from the list
        Then I edit overview section and perform delete and add tasks and save changes
        And I should verify the changes on milestone listing

    Scenario: Care Coach is able to Delete any existing Milestone from list during creation of care plan
        When I click on caregiver from the list who does not have created care plan
        Then I should see empty state page
        When I click on "Create Care Plan" button
        And I select random template and apply
        And I select random milestone from the list
        Then I click on remove button to delete milestone
        And I should verify the deleted milestone is removed from milestone listing

    Scenario: Care Coach is able to return to template stage by clicking on step 1 from step 2
        When I click on caregiver from the list who does not have created care plan
        Then I should see empty state page
        When I click on "Create Care Plan" button
        And I select random template and apply
        When I click on back stepper and should see warning drawer with nevermind and continue button
        And I click on "Nevermind" button on warning drawer
        Then I should be on same milestone list page
        When I click on back stepper and should see warning drawer with nevermind and continue button
        And I click on "Continue" button on warning drawer
        Then I should be on start with a template page

    ##This is failing
    Scenario: Care Coach is able to add milestone from scratch with add and remove tasks and save the milestone
        When I click on caregiver from the list who does not have created care plan
        Then I should see empty state page
        When I click on "Create Care Plan" button
        And I click on "Continue from scratch" button
        Then I should see continue button is disable
        When I select random milestone and perform undo
        Then I should see continue button is disable
        Then I select multiple milestones and verify milestones added on milestones listing
        When I click on "Add new" button
        And I click on "Cancel" button
        And I click on "Add new" button
        Then I entered details on overview section and perform add delete and add tasks and save milestone
        And I should verify the changes on milestone listing
        When I should remove all the added milestones from right side
        Then I should see continue button is disable
