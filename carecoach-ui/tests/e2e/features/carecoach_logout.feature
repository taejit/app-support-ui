Feature: Memento Care Coach Portal Logout
    As a Logged in Care Coach User of the Memento Application
    I want to Logout successfully

    Background: User is logged in and is on Care Coach Home Page
        Given I am on the cognito login page
        When I enter username as "actualusername"
        And I enter password as "actualpassword"
        And I attempt to login
        Then I should see care coach home page

    Scenario: Successful Logout
        When I click on "Sign Out" link on the care coach home page
        Then I should be successfully logged out and land on the login page