
Feature: Memento Care Coach Change My Password

    As a Care Coach
    I want an option to reset password
    So that I can change password anytime at wish

    Background: User is logged in and is on Care Coach Home Page
        Given I am on the cognito login page
        When I enter username as "actualusername"
        And I enter password as "actualpassword"
        And I attempt to login

    #Scenario 1.1
    Scenario: Scenario 1.1: Care Coach is able to reset password successfully
        Then I should see care coach home page
        When I click on "Profile" link on the care coach home page
        Then I should see "My Profile" page
        When I click on "Change my password" button on my profile page
        Then I should see "Change my password" page
        When I click on cancel button on change my password page
        Then I should see "My Profile" page
        When I click on "Change my password" button on my profile page
        When I enter "Current Password" on change my password page
        And I enter "New Password" on change my password page
        And I click on "Change password" button on "Change my password" page
        Then I should see "Change password successful." message at the bottom on "Change my password" page

    #Scenario 1.2
    Scenario: Scenario 1.2: Care Coach is able to swap current password to check incorrect username & password
        Then I should see login error message as "Incorrect username or password."
        And swap passwords in config file

    #Scenario 1.3
    Scenario: Scenario 1.3: Care Coach is able to again reset password successfully
        Then I should see care coach home page
        When I click on "Profile" link on the care coach home page
        Then I should see "My Profile" page
        When I click on "Change my password" button on my profile page
        Then I should see "Change my password" page
        When I enter "Current Password" on change my password page
        And I enter "New Password" on change my password page
        And I click on "Change password" button on "Change my password" page
        Then I should see "Change password successful." message at the bottom on "Change my password" page
        And swap passwords in config file

    #Scenario 2.0
    Scenario: care coach failed to reset password due to wrong current password
        Then I should see care coach home page
        When I click on "Profile" link on the care coach home page
        Then I should see "My Profile" page
        When I click on "Change my password" button on my profile page
        Then I should see "Change my password" page
        When I enter incorrect "Current Password" on change my password page
        And I enter "New Password" on change my password page
        And I click on "Change password" button on "Change my password" page
        Then I should see error message "Could not change password. Try again!" for incorrect current password
