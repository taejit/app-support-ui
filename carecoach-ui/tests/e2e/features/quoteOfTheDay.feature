Feature: Memento Carecoach Portal - Quote Of The Day
        As a Care Coach,
        I need the ability to see the quote of the day

        Background: User is logged in and is on Carecoach Dashboard Page
                Given I am on the cognito login page
                When I enter username as "actualusername"
                And I enter password as "actualpassword"
                And I attempt to login
                Then I should see care coach home page

        Scenario: Check quote of the day
                Then I should see daily quote and its related imformation
