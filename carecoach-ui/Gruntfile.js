module.exports = function (grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),
    webdriver: {
      "test-sauce": {
        configFile: "./tests/e2e/config/wdio.sauce.conf.js",
      },
      "test-browserstack": {
        configFile: "./tests/e2e/config/wdio.browserstack.conf.js",
      },
      "test-local": {
        configFile: "./tests/e2e/config/wdio.local.conf.js",
      },
    },
  })

  grunt.loadNpmTasks("grunt-cucumberjs")
  grunt.loadNpmTasks("grunt-webdriver")
  grunt.registerTask("default", ["webdriver:test-local"])
  // grunt.registerTask('default', ['webdriver:test-sauce']);
  grunt.registerTask("default", ["test-browserstack"])
}
