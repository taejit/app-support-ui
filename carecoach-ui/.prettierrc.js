module.exports = {
  jsxBracketSameLine: false,
  singleQuote: false,
  trailingComma: "es5",
  semi: false,
}
