# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### Local Run

#### Integration Environment

Starts the local admin UI pointing to integration environment.

```bash

# dev server with hot reload at http://localhost:3000
npm start
```

#### Feature Environment

Starts the local admin UI pointing to feature environment.

```bash

# dev server with hot reload at http://localhost:3000
# Pick the appropriate feature
npm run start:feature-1
npm run start:feature-2
npm run start:feature-3
npm run start:feature-4
```

Navigate to [http://localhost:3000](http://localhost:3000). The app will automatically reload if you change any of the source files.

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)

# How to Run End to End Automation Tests

## NOTE: We are using WebDriverIO with Cucumber JS and BrowserStack Integration, POM Framework and the tests can be found in the `/tests/e2e` directory.

The End to End Automation Tests Framework contains folder structure as below:-

- config
  - This folder containing configuration file for BrowserStack , local and shared config data with test user details.
- features
  - This folder containing all the feature files which having business rule (acceptance scenarios) for web application.
- pageobjects
  - As we are using Page Object Model Framework , all the xpaths and rendoring methods are defining in their respective Page files.
- stepDefinitions
  - This folder contains given.js, when.js and then.js ,basically the statements we are defining in feature files,are creating in these 3 files based on Gherkin Keywords.
- utilities
  - This folder contains utility file and data used in web application.

## Pre-requisites

### Install NodeJS

```bash
brew install node #MacOS
```

Or use [nodejs.org](https://nodejs.org/en/download/)

### Install dependencies

```bash
npm install
```

## Configuration

We have 5 environment for testing:- feature-1, feature-2, feature-3, feature-4, dev(integration), Staging. Below are Urls :-

- feature1: "https://feature-1.dev.lanternapp.care/carecoach",
- feature2: "https://feature-2.dev.lanternapp.care/carecoach",
- feature3: "https://feature-3.dev.lanternapp.care/carecoach",
- feature4: "https://feature-4.dev.lanternapp.care/carecoach",
- dev: "https://dev.lanternapp.care/carecoach",
- staging: "https://staging.lanternapp.care/carecoach"

If you want to execute Test Scenarios on specific env, you can provide url like below :-

### 'export url=https://staging.lanternapp.care/carecoach]`

if no url is provide, then default url is Integration/dev
Similarly,if you want your test scenarios run on browserstack provide BS username and key like below :-

### 'export BROWSERSTACK_USERNAME=<USERNAME> && export BROWSERSTACK_ACCESS_KEY=<ACCESS_KEY>'

## Running tests

- If you want to run test scenarios on local
  -use 'npm run test-local' command

- If you want to run test scenarios on Browser-Stack :-
  -use 'npm run test-browserstack' command

Note:- The browser preferences and number of instances should configure in 'wdio.browserstack.conf.js' file
