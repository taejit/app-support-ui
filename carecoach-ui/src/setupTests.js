// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)

import "@testing-library/jest-dom"

window._env_ = {
  Aws_Region: "us-east-1",
  Cognito_Pool_Id: "us-east-1_n4MkEto1c",
  Cognito_Base_Url: "lantern-feature-4.auth.us-east-1.amazoncognito.com",
  Cognito_Carecoach_Client_Id: "67vc1jpanagtid51bj8hmjor5e",
  Carecoach_Home: "http://localhost:3001/",
  REACT_APP_CARECOACH_API_URL: "http://localhost:3000",
  BUILD_ID: "local",
  Cognito_Identity_Pool_Id: "us-east-1:e1dc8398-106c-4c31-b591-c92e5505c472",
}

jest.mock("react-i18next", () => ({
  // this mock makes sure any components using the translate hook can use it without a warning being shown
  I18nextProvider: `<div></div>`,
  useTranslation: () => {
    return {
      t: (str) => str,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    }
  },
}))
