import React from "react"
import { HashRouter, Route, Switch } from "react-router-dom"
import Loading from "./components/Loading"

const AuthRedirect = React.lazy(() => import("./components/AuthRedirect"))

const App = () => {
  return (
    <HashRouter>
      <React.Suspense fallback={<Loading />}>
        <Switch>
          <Route path="/" name="AuthRedirect" render={() => <AuthRedirect />} />
        </Switch>
      </React.Suspense>
    </HashRouter>
  )
}

export default App
