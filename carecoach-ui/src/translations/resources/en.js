import commonen from "./common-en"

const translationObj = {
  en: {
    translations: {
      ...commonen,
      welcometext: "Welcome!",
      welcomemsg1:
        "Thank you for joining the Memento team! We’d like to get to",
      welcomemsg2: "know you better before we match you with caregivers.",
      getstarted: "Get Started",
      aboutyouintro: "Introduce yourself!",
      aboutyou: "About you",
      aboutyousecondarytext:
        "Please begin by entering your contact information",
      name: "Name",
      firstname: "First name",
      lastname: "Last name",
      pronouns: "Pronouns",
      employername: "Employer",
      pronountextbox: "Custom Pronoun",
      email: "Email",
      mobilephone: "Mobile phone",
      location: "Location",
      next: "Next",
      locationPopupImageAlt: "No options Image",
      locationPopupTextOne: "Try something simple like a city",
      locationPopupTextTwo: "For example: Baltimore, Maryland.",
      forbiddentitle: "Forbidden",
      forbiddennopermission:
        "You do not have permissions to access this portal.",
      forbiddencontactadmin:
        "Please contact your admin to be assigned to a carecoach group, in order to get access to this portal.",
      forbiddengotologin: "Go to Login",
      profileImgDefaultText1: "Please make sure your new",
      profileImgDefaultText2: "picture is a crisp headshot",
      profile_Img_DefaultText_3: "of yourself.",
      profileImgUploadLoading: "Upload in progress...",
      profileImgUploadError1: "Unable to upload profile image.",
      profileImgUploadError2: "Please try again.",
      profileImgGetError1: "An error occurred.",
      profileImgGetError2: "Please try again.",
      profileImgUploadComplete1: "Upload complete.",
      profileImgUploadComplete2: "Looking good!",
      profileImgSizeError1: "File size too large.",
      profileImgSizeError2: "Please try again.",
      profileImgInvalidError1: "File is not a valid image.",
      profileImgInvalidError2: "Please upload JPG, GIF, or PNG.",
      profileImgUploadBtn: "Upload",
      profileImgChangeBtn: "Change",
      profileImgExtText: "1MB JPG, GIF, or PNG",
      dashboardwelcometext: "Welcome to Dashboard",
      whataboutyou: "What you do",
      whataboutyousubheading1: "Please describe your professional experience.",
      whataboutyousubheading2: "What makes you a good Care Coach?",
      finish: "Finish",
      introduceyourself: "Introduce yourself!",
      required: "Required",
      invalidemail: "Invalid Email Address",
      phonenumberisnotvalid: "Phone number is not valid.",
      editmyprofilepicture: "Edit my profile picture",
      editmyprofilepicturetext:
        "Please make sure your new picture is a crisp headshot of yourself.",
      aboutbelow:
        "Tell us about yourself. Don't add a salutation - we'll handle that.",
      jobstartdate: "Start Date",
      specialities: "Credentials",
      aboutstatement: "About Statement",
      onboarding: "Onboarding",
      tick: "Tick Appears Here",
      credentials: "For example: RN, MSW",
      datepicker: "When you started in your field",
      maxlength: "300 characters.",
      "edit-carecoach-profile-start-date":
        "When you got started in your field.",
      "care-coach-profile-first-name": "First name",
      "care-coach-profile-last-name": "Last name",
      "care-coach-profile-credentials": "Credentials",
      "edit-profile-invalid-email": "Invalid email address.",
      "edit-profile-max-300-characters-allowed":
        "Max 300 characters are allowed.",
      "edit-profile-first-name-required": "First name is a required field.",
      "edit-profile-last-name-required": "Last name is a required field.",
      "edit-profile-email-required": "Email is a required field.",
      "edit-profile-pronouns-required": "Pronouns is a required field.",
      "edit-profile-phone-required": "Mobile phone is a required field.",
      "edit-profile-location-required": "Location is a required field.",
      "edit-profile-about-required": "About statement is a required field.",
      "edit-profile-credential-required": "Credential is a required field.",
      "custompronoun-required": "Custom Pronoun is a required field.",
      "edit-profile-start-date-required": "Start date is a required field.",
      "edit-profile-job-start-date": "Start date",
      "view-profile-change-my-password": "Change my password",
      "edit-profile-no-changes-have-been-made": "No changes have been made.",
      "edit-profile-picture-done": "Done",
      "specialities-required": "Credentials is a required field",
      "job-start-date": "Start date is a required field",
      "about-required": "About statement is a required field",
      "change-password-title": "Change my password",
      "change-password-current-password": "Current password",
      "change-password-new-password": "New password",
      "change-password-new-password-criteria":
        "8 mixed-case letters, numbers, and symbols",
      "change-password-change-password": "Change password",
      "change-password-cancel": "Cancel",
      "change-password-error-message": "Could not change password. Try again!",
      "change-password-current-password-required":
        "Current password is required.",
      "password-shouldnt-same-error-message":
        "Must be different than your current Password",
      "current-password-wrong-error-message":
        "Your current password is incorrect. Try again!",
      "change-password-new-password-required": "New password is required.",
      "change-password-success-message": "Change password successful.",
      "change-password-min-characters": "Minimum 8 characters.",
      "change-password-max-characters": "Maximum 99 characters allowed.",
      "change-password-lowercase-character":
        "Should contain lowercase character.",
      "change-password-uppercase-characters":
        "Should contain uppercase character.",
      "change-password-special-characters": "Should contain special character.",
      "change-password-number": "Should contain number.",
      "caregivers-sidebar-menu-care-plan": "Care Plan",
      "caregivers-sidebar-menu-progress": "Progress",
      "caregivers-sidebar-menu-notes": "Notes",
      "caregivers-sidebar-menu-files": "Files",
      "caregivers-sidebar-menu-profile": "Profile",
      "caregivers-sidebar-menu-send-a-message": "Send a message",
      "caregiver-profile-full-name": "Full name",
      "caregivers-title-profile": "Profile",
      "caregivers-about-the-caregiver": "ABOUT THE CAREGIVER",
      "caregivers-about-the-loved-one": "ABOUT THE LOVED ONE",
      "caregiver-profile-goes-by": "Goes by",
      "caregiver-profile-caregiving-role": "Caregiving role",
      "caregiver-profile-time-spent-in-role": "Time spent in role",
      "caregiver-profile-communication-preference": "Communication preference",
      "caregiver-profile-carereceipient-dob": "Date of birth",
      "caregiver-profile-carereceipient-relation": "Relation",
      "caregiver-profile-carereceipient-living-arrangement":
        "Living arrangement",
      "caregiver-profile-carereceipient-conditions": "Conditions",
      "caregiver-profile-communicatore-prefer": "Prefers",
      "caregivers-notes-your-notes-on-caregiver": "Your Notes on",
      "caregivers-notes-add-button-text": "Add",
      "caregivers-notes-jot-that-down": "Jot that down!",
      "caregivers-notes-no-notes-line1":
        "This is a place for you to add notes about this Caregiver,",
      "caregivers-notes-no-notes-line2":
        "their loved one, or their situation. Don't worry, the",
      "caregivers-notes-no-notes-line3": "Caregiver will not see these notes.",
      "caregivers-notes-add-a-note": "Add a note",
      "caregivers-notes-edit-note": "Edit Note",
      "caregivers-notes-no-notes-image": "No notes",
      "caregivers-add-note": "Add Note",
      "caregivers-add-note-title-is-requied": "Title is a required field.",
      "caregivers-add-note-body-is-requied": "Body is a required field.",
      "caregivers-add-note-title-label": "Title",
      "caregivers-add-note-body-label": "Body",
      "caregivers-add-note-nextstep-label": "Next steps (Optional)",
      "caregivers-add-note-added-success": "added",
      "caregivers-add-note-removed-success": "removed",
      "caregivers-add-note-updated-success": "updated",
      "caregivers-delete-undo": "UNDO",
      "search-caregiver-text": "Search Caregivers...",
      "my-caregivers-text": "My Caregivers",
      "no-caregivers-text": "Good things take time!",
      "no-caregivers-text-subscript":
        "You haven't been assigned any Caregivers yet. We'll alert you as soon as we assign you one so that you can say hi.",
      "no-caregivers-search-text": "Nothing found!",
      "no-caregivers-search-text-subscript":
        "Make sure you spelled everything correctly. You can search for names and conditions.",
      "care-plan-header": "'s Care Plan",
      "no-careplan-sub-text":
        "’s Care Plan will appear here once you apply a template and add some Milestones.",
      "no-careplans-text": "It's a blank slate!",
      "no-careplans-text-subscript":
        "To get started, click the Create button and search for relevant medical conditions or tasks.",
      "create-care-plan": "Create Care Plan",
      "search-placeholder-on-step-1": "Search templates...",
      "search-placeholder-on-step-2": "Search milestones...",
      "common-text": "COMMON",
      "careplan-step-1-title": "Start with a template",
      "careplan-step-1-description":
        "Templates are groups of Milestones common for medical conditions or caregiving responsibilities.",
      "careplan-step-2-title": "Customize Milestones",
      "careplan-step-2-description":
        "Milestones are small groups of related tasks that, when completed, mark important progress in caregiving.",
      "careplan-step-2-continue-btn": "Continue",
      "careplan-step-2-milestone-message":
        "You can click on the milestones to add tasks, set due dates, and more.",
      "careplan-step-3-milestone-message":
        "To view and edit individual Milestones, return to step 2.",
      "careplan-step-2-add-new-milestone": "Add new",
      "continue-from-scratch": "Continue from scratch",
      "no-search-text": "Nothing found!",
      "no-search-text-subscript":
        "Try simplifying your search or starting with a medical condition.",
      "no-template-selected": "No templates selected.",
      "apply-template": "Apply templates",
      "upcoming-title": "Upcoming",
      "completed-title": "Complete",
      "add-milestones": "Add Milestones",
      "AllQuiet-labels-mainTitle": "All quiet for now.",
      "AllQuiet-labels-secondaryTitle":
        "Important notifications will appear here. Don’t worry — It won't take too long!",
      "SomethingWrong-labels-mainTitle": "Something’s not right.",
      "SomethingWrong-labels-secondaryTitle":
        "We’re having trouble fetching your notifications right now. Try again shortly.",
      "care-plan-sent": "Care Plan Sent",
      "care-plan-sent-description":
        "Success! Nikola will now be able to view and interact with the Care Plan you’ve just created. You’ll be able to add more Milestones as Nikola’s caregiving needs evolve.",
      "view-care-plan": "View Care Plan",
      "edit-milestone-drawer-title-add-mode": "Add new Milestone",
      "edit-milestone-drawer-title-edit-mode": "Edit Milestone",
      "edit-milestone-name": "Name",
      "edit-milestone-overview": "Overview",
      "edit-milestone-tasks": "Tasks",
      "edit-milestone-description": "Description",
      "edit-milestone-rationale": "Rationale",
      "edit-milestone-duedate": "Due date (Optional)",
      "edit-milestone-validation-message": "is required.",
      "general-form-cancel-text": "Cancel",
      "general-form-save-changes-text": "Save changes",
      "general-form-remove-text": "Remove",
      "general-form-add-text": "Add",
      "general-form-no-changes-have-been-made": "No changes have been made.",
      "general-form-add-new": "ADD NEW",
      "no-tasks-list-text": "No tasks yet. Click to add!",
      "general-form-updated": "updated",
      "add-new-task-title": "Add new task",
      "add-new-task-subscript": "How do you want to do this?",
      "add-new-task-description":
        "Tasks are the small, direct actions that make up Milestones in a Care Plan. Each task is named and may be linked to a resource that embodies it.",
      "search-lantern-resources": "Search Memento Resources",
      "search-existing-milestones": "Search existing Milestones",
      "add-from-scratch": "Add from scratch",
      "general-form-go-back": "Go back",
      "or-text": "OR",
      "edit-task-name": "Name",
      "edit-task-link": "Link (Optional)",
      "edit-task-link-description": "For example an online form to complete",
      "edit-task-link-invalid-msg": "Please enter a valid url",
      "edit-task-drawer-title-add-scratch-mode": "Add task from scratch",
      "edit-task-drawer-title-edit-mode": "Edit Task",
      "milestone-due": "Due",
      "no-tasks-list-error-text": "You must add at least one task.",
      "careplan-backstep-title": "Return to the template stage?",
      "careplan-backstep-description-1": "Milestones you’ve added and changes",
      "careplan-backstep-description-2": "you’ve made will be discarded.",
      "careplan-backstep-description-3": "This cannot be undone.",
      "video-call-end-text": "End this call?",
      "video-call-end-text-subscript": "No one will be able to re-join.",
      "nevermind-button-text": "NEVERMIND",
      "nevermind-button-text-error-drawer": "Nevermind",
      "end-call-button-text": "End call",
      "video-call-wait-text": "will join soon",
      "search-milestones-withoutdots": "Search Milestones",
      "dashboard-hey": "Hey",
      "dashboard-no-data-text": "You’re all caught up!",
      "dashboard-no-data-description":
        "You have no upcoming calls with your caregivers right now. Maybe it’s time to reach out and see if everything is well?",
      "dashboard-greeting":
        "Here are some things to get you started for today.",
      "dashboard-upcoming-calls": "Upcoming calls",
      "video-call-join-error":
        "Unable to join the video call. Please try again later.",
      "careplan-update-successful": "Care Plan update successful.",
      "unable-to-update-careplan": `Unable to update Care Plan. Please try again later.`,
      "milestone-not-found-description":
        "Try simplifying your search or using a relevant medical condition.",
      "no-milestones-selected": "No Milestones selected.",
      "GoodThings-labels-mainTitle": "Good things take time!",
      "GoodThings-labels-secondaryTitle":
        "You haven't been assigned any Caregivers yet. We'll alert you as soon as we assign you one so that you can say hi.",
      "NothingFound-labels-mainTitle": "Nothing Found!",
      "NothingFound-labels-secondaryTitle":
        "Try Using Different terms or making your search less specific.",
      "progress-text": "Progress",
      "checks-text": "Checks",
      "last-score-text": "Last score",
      "light-burden-text": "Light Burden",
      "light-burden-description":
        "Things are going pretty well, but there’s always some room for improvement.",
      "mild-burden-text": "Mild Burden",
      "mild-burden-description":
        "The stress is manageable, but be careful. This kind of burden isn’t healthy in the long run.",
      "heavy-burden-text": "Heavy Burden",
      "heavy-burden-description":
        "The situation is unsustainable. Spending too much time here can cause health problems.",
      "indeterminate-burden-text": "Indeterminate",
      "indeterminate-burden-description":
        "Your Caregiver hasn’t done a progress check for a bit. Perhaps ask them to do one?",
      "mini-survey-no-data-text": "Step by step, one ventures far.",
      "mini-survey-no-data-description":
        "As your Caregiver completes their progress checks, you’ll be able to track their score and responses here.",
      "daily-quote": "Daily quote",
      "view-progress-check": "View Progress Check",
      "progress-answers": "Answers",
      "progress-score": "Burden Score",
      "confirm-remove-description": "This cannot be undone.",
      "careplan-step-3-title": "Review & Finish",
      "careplan-step-3-description1":
        "Take a minute to review the Care Plan you made for ",
      "careplan-step-3-description2":
        ". Once you’re done, click the button below to send ",
      "careplan-step-3-description3": " the fresh new plan.",
      "careplan-step-3-btn": "Finish & send",
      alphabetically: "Alphabetically",
      lastcontact: "Last contact",
      dateregistered: "Date registered",
      burden: "Burden",
    },
  },
}

export default translationObj
