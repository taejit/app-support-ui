import React from "react"
import { render, waitFor, cleanup } from "@testing-library/react"
// import { createMemoryHistory } from "history"
import { CognitoJwtVerifier } from "aws-cognito-jwt-verifier"
// import * as CarecaochServices from "./services/CarecaochServices"
import App from "./App"

jest.mock("aws-cognito-jwt-verifier")
jest.mock("aws-amplify", () => {
  return {
    __esModule: true,
    Auth: {
      federatedSignIn: jest.fn(),
      currentSession: () =>
        Promise.resolve({
          accessToken: { jwtToken: "token" },
        }),
      currentUserInfo: () =>
        Promise.resolve({
          attributes: {
            email: "bramsai@intraedge.com",
            email_verified: true,
            phone_number: "+12254789321",
            phone_number_verified: true,
            sub: "38041359-614f-4c44-9a32-23c3c80506c7",
            family_name: "kiran",
            given_name: "ramsai",
            "custom:location": "Hyderabad Telangana Hyd",
            "custom:pronoun": "he/his",
          },
          username: "acc2ea0f-3a1f-49b9-8b19-7f555ece71d6",
        }),
    },
  }
})

describe("App component test", () => {
  let AppComponent
  beforeEach(() => {
    cleanup()
    AppComponent = App
  })
  afterEach(() => {
    cleanup()
    jest.resetAllMocks()
  })

  // it("Show Home when authorized", async () => {
  //   CarecaochServices.GetCarecoachProfile = jest.fn().mockReturnValue({
  //     data: {
  //       firstName: "John",
  //       lastName: "Doe",
  //       nickName: "Joe",
  //       email: "example@lantern.care",
  //       phone: "6021234567",
  //       pronouns: "He/him",
  //       location: "Gilbert, Arizona",
  //       jobTitle: "Nurse",
  //       jobStartDate: "2021-21-01",
  //       specialities: "MR",
  //       about: "A log describption about the carecoach",
  //       profileImage: "https://signedurl-profile-example.com",
  //     },
  //   })
  //   CognitoJwtVerifier.prototype.checkJwt = jest
  //     .fn()
  //     .mockImplementationOnce(() =>
  //       Promise.resolve(
  //         JSON.stringify({ data: { "cognito:groups": "carecoach" } })
  //       )
  //     )
  //   const history = createMemoryHistory()
  //   let wrapper
  //   await waitFor(() => {
  //     wrapper = render(<AppComponent />)
  //   })
  //   history.push("/dashboard")
  //   const { getAllByText } = wrapper
  //   expect(getAllByText(/^home$/)).toHaveLength(1)
  // })

  it("Show Forbidden component when not authorized", async () => {
    CognitoJwtVerifier.prototype.checkJwt = jest
      .fn()
      .mockImplementationOnce(() =>
        Promise.resolve(JSON.stringify({ data: { "cognito:groups": "admin" } }))
      )
    let wrapper
    await waitFor(() => {
      wrapper = render(<AppComponent />)
    })
    const { getAllByText } = wrapper
    expect(getAllByText(/^forbiddentitle$/)).toHaveLength(1)
  })

  it("Show forbidden when no token to verify claim against", async () => {
    CognitoJwtVerifier.prototype.checkJwt = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve(null))
    let wrapper
    await waitFor(() => {
      wrapper = render(<AppComponent />)
    })
    const { getAllByText } = wrapper
    expect(getAllByText(/^forbiddentitle$/)).toHaveLength(1)
  })
})
