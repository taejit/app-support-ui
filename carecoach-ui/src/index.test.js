import ReactDOM from "react-dom"

jest.mock("react-dom", () => ({ render: jest.fn() }))

test("renders with App and root div", () => {
  // Create and append to document body
  // an HTML element with id = root
  const root = document.createElement("div")
  root.id = "root"
  document.body.appendChild(root)

  // Requires index.js so that react-dom render method is called
  /* eslint-disable global-require */
  require("./index.js")

  expect(ReactDOM.render).toHaveBeenCalled()
})
