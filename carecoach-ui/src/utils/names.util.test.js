import { getFormatedName } from "./names.util"

describe("getPronounsDisplay test", () => {
  it("Passing random string to method", () => {
    expect(getFormatedName("nick", "last")).toEqual("nick last")
  })
  it("Passing random string to method: greater than 15 length", () => {
    expect(getFormatedName("nick", "lastname of caregiver")).toEqual(
      "n. lastname ..."
    )
  })
  it("Passing random string to method: nickname greater than 15 length", () => {
    expect(getFormatedName("nick of caregiver", "lastname")).toEqual(
      "n. lastname"
    )
  })
})
