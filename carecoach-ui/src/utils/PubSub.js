import { v4 } from "uuid"

const topics = {}

export const subscribe = (topic, fn) => {
  if (!topics[topic]) topics[topic] = {}
  const id = v4()
  topics[topic][id] = fn
  return () => {
    topics[topic][id] = null
    delete topics[topic][id]
  }
}
export const publish = (topic, args) => {
  if (!topics[topic]) return
  Object.values(topics[topic]).forEach((fn) => {
    if (fn) fn(args)
  })
}
