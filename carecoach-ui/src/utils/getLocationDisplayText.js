/**
 * locationDescription => this is expected to be a description from google api returned description
 * output=> trimming the country away, as that is not required to display
 */

export default (locationDescription) => {
  return (locationDescription || "").split(", ").slice(0, -1).join(", ")
}
