import {
  getFormattedDate,
  getNoteDate,
  getUpcomingDueDate,
} from "./dateformatting"

describe("Date formatting test", () => {
  it("When pass null", () => {
    expect(getFormattedDate(null)).toEqual(null)
  })

  it("When empty string", () => {
    expect(getFormattedDate("")).toEqual("")
  })

  it("Pass valid date", () => {
    expect(getFormattedDate("2020-12-31T07:53:20.908Z")).toEqual(
      "December 31st, 2020"
    )
  })

  it("Note date display format", () => {
    expect(getNoteDate("2020-12-31T07:53:20.908Z")).toEqual("Dec 31")
  })

  it("upcoming items due date display format", () => {
    expect(
      getUpcomingDueDate("2020-01-09T07:53:20.908Z").replace(",", "")
    ).toEqual("Jan 9")
  })
})
