import getTextFromContentFulData from "./getTextFromContentFulData"

const generateTasksFromMilestone = (milestone) => {
  return milestone?.tasks?.map((task) => {
    return {
      name: task.fields?.name || "",
      isCompleted: task.isCompleted || false,
      link: task.fields?.link || "",
      id: task.sys.id,
    }
  })
}

const getMilestoneObj = (listOfMilestone) => {
  return (listOfMilestone || []).map((milestone) => {
    return {
      name: milestone.fields.name,
      isSelected: milestone.fields.isSelected,
      isCommon: milestone.fields.isCommon,
      description: getTextFromContentFulData(
        milestone.fields.description.content
      ),
      rationale: getTextFromContentFulData(milestone.fields.rationale.content),
      id: milestone.sys.id,
      tasks: milestone.fields.tasks
        ? generateTasksFromMilestone(milestone.fields)
        : [],
    }
  })
}

export default getMilestoneObj
