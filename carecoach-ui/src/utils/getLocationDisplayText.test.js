import getLocationDisplayText from "./getLocationDisplayText"

describe("Location disolay test", () => {
  it("Passing null should not break the method", () => {
    expect(getLocationDisplayText(null)).toEqual("")
  })

  it("Should return without country", () => {
    expect(getLocationDisplayText("City, State, Country")).toEqual(
      "City, State"
    )
  })
})
