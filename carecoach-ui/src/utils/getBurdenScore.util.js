/* eslint-disable */
export const getBurdenScore = (score) => {
  if (score) {
    const val = parseInt(score)
    let percentage = (val / 30 * 100)
    let tag = getTag(val)
    return {
      tag: tag + " Burden",
      displayScore: score + "/" + "30",
      percentage: Math.round(percentage),
      colorCss:
        val < 5
          ? "lightBurdenColor"
          : val < 15
            ? "mediumBurdenColor"
            : "heavyBurdenColor",
    }
  }
  return {
    tag: "Indeterminate",
    displayScore: "N/A",
    percentage: 0,
    colorCss: "indeterminateColor",
  }
}

const getTag = (score) => {
  if (score < 5) return "Light"
  else if (score < 15) return "Mild"
  else return "Heavy"
}
