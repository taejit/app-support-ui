/* eslint import/prefer-default-export: 0 */
import format from "date-fns/format"
import { DateFormat, DBDateFormat, MonthAndDayOfMonth } from "./constant"

export const getFormattedDate = (dateString) => {
  if (dateString?.length) {
    return format(new Date(dateString), DateFormat)
  }
  return dateString
}

export const getDateFormatForDB = (dateObject) => {
  if (dateObject instanceof Date) {
    return format(dateObject, DBDateFormat)
  }
  return dateObject
}

export const getNoteDate = (dateString) => {
  if (dateString) {
    return format(new Date(dateString), "MMM d")
  }
  return dateString
}

export const getNoteTime = (dateString) => {
  if (dateString) {
    return format(new Date(dateString), "h:mm aa")
  }
  return dateString
}

export const getUpcomingDueDate = (dateString) => {
  if (dateString) {
    return format(new Date(dateString), "MMM, d")
  }
  return dateString
}

export const getMilestoneDueDate = (dateObject) => {
  if (dateObject) {
    return format(dateObject, MonthAndDayOfMonth)
  }
  return dateObject
}
