import getPronounsDisplay from "./getPronounsDisplay"

jest.mock("i18next", () => ({
  ...jest.requireActual("i18next"),
  t: (key) => key,
}))

describe("getPronounsDisplay test", () => {
  it("Passing null", () => {
    expect(getPronounsDisplay(null)).toBeNull()
  })

  it("Passing random string to method", () => {
    expect(getPronounsDisplay("randomstring")).toEqual("pronounsother")
  })

  it("Passing valid value he/his", () => {
    expect(getPronounsDisplay("pronounshehisdbvalue")).toEqual("pronounshehis")
  })

  it("Passing valid value she/her", () => {
    expect(getPronounsDisplay("pronounssheherdbvalue")).toEqual(
      "pronounssheher"
    )
  })

  it("Passing valid value they/their", () => {
    expect(getPronounsDisplay("pronounstheytheirdbvalue")).toEqual(
      "pronounstheytheir"
    )
  })
})
