/* eslint import/prefer-default-export: 0 */
export const PhoneRegExp = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/
export const DateFormat = "MMMM do, yyyy"
export const DBDateFormat = "yyyy-MM-dd"
export const MonthAndDayOfMonth = "MMM d"
export const ValidMilestoneAddEditSteps = {
  milestone: "milestone",
  task: "task",
  addtaskoptions: "addtaskoptions",
}

// strings for sorting option values
export const Alphabetically = "alphabetically|ASC|lastname"
export const LastContact = "lastcontact|ASC|NotcontactedLastContacted"
export const DateRegistered = "dateregistered|DESC|earliestfirst"
export const Burden = "burden|DESC|HeavyMildLightIndeterminate"
