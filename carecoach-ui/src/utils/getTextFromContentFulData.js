const getTextFromContentFulData = (contents) => {
  return contents
    .map((content) => {
      if (content.content) {
        return getTextFromContentFulData(content.content)
      } else {
        return content.value
      }
    })
    .join()
}

export default getTextFromContentFulData
