import { getBurdenScore } from "./getBurdenScore.util"

const MockObject = {
  tag: "Mild Burden",
  displayScore: "10/30",
  percentage: 33,
  colorCss: "mediumBurdenColor",
}

describe("getPronounsDisplay test", () => {
  it("Passing null", () => {
    expect(getBurdenScore(null)).toEqual({
      tag: "Indeterminate",
      displayScore: "N/A",
      percentage: 0,
      colorCss: "indeterminateColor",
    })
  })

  it("Passing random score to method", () => {
    expect(getBurdenScore("10")).toStrictEqual(MockObject)
  })
})
