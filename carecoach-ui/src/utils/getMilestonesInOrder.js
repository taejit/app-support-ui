const getMilestonesInOrder = (selectedMilestones) => {
  const mile = selectedMilestones || []
  let withDueDate = mile
    .filter((milestone) => !!milestone.duedate)
    .sort((a, b) => new Date(a.duedate) - new Date(b.duedate))

  let withoutDueDate = mile
    .filter((milestone) => !milestone.duedate)
    .sort((a, b) => a.name.localeCompare(b.name))
  return [...withDueDate, ...withoutDueDate]
}

export default getMilestonesInOrder
