import i18n from "i18next"

const getPronounsDisplay = (pronounsdbvalue) => {
  const t = i18n.t.bind(i18n)

  if (!pronounsdbvalue) {
    return null
  }
  switch (pronounsdbvalue) {
    case t("pronounshehisdbvalue"):
      return t("pronounshehis")
    case t("pronounssheherdbvalue"):
      return t("pronounssheher")
    case t("pronounstheytheirdbvalue"):
      return t("pronounstheytheir")
    default:
      return t("pronounsother")
  }
}

export default getPronounsDisplay
