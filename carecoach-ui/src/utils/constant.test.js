import * as constant from "./constant"

describe("Test constant file loading", () => {
  it("Load variabes", () => {
    expect(constant.PhoneRegExp).toBeDefined()
    expect(constant.DateFormat).toBeDefined()
  })
})
