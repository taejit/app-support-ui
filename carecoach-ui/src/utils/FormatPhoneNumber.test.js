import FormatPhoneNumber from "./FormatPhoneNumber"

describe("Phone number formatting test", () => {
  it("Pass number with country code", () => {
    expect(FormatPhoneNumber("+11234567890")).toEqual("(123) 456-7890")
  })

  it("When without country code", () => {
    expect(FormatPhoneNumber("1234567890")).toEqual("(123) 456-7890")
  })

  it("Pass all alphabet", () => {
    expect(FormatPhoneNumber("abc")).toEqual("abc")
  })
})
