export const getFormatedName = (nickName, lastName) => {
  let fullName = nickName + " " + lastName

  if (fullName.length > 15 && nickName) {
    let newname = nickName[0] + ". " + lastName
    if (newname.length > 15 && newname) {
      return nickName[0] + ". " + lastName.slice(0, 9) + "..."
    }
    return newname
  }
  return fullName
}

export const getFormatedFullName = (name) => {
  if (name && name.length > 13) {
    return name.slice(0, 8) + "..."
  }
  return name
}
