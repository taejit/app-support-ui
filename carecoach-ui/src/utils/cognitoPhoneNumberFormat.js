/*
  phonenumber input format => (123) 456 7890
  output format => +11234567890
*/

export default (phonenumber) => {
  if (phonenumber) {
    return `+1${phonenumber.replace(/[^\d]/g, "")}`
  }
  return phonenumber
}
