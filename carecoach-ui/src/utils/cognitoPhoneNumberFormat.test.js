import cognitoPhoneNumberFormat from "./cognitoPhoneNumberFormat"

describe("Get cognito phone number format", () => {
  it("passin display number", () => {
    expect(cognitoPhoneNumberFormat("(123) 456 7890")).toEqual("+11234567890")
  })
})
