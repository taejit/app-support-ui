import React from "react"
import ReactDOM from "react-dom"
import { Provider } from "react-redux"
import { ConnectedRouter } from "connected-react-router"
import { I18nextProvider } from "react-i18next"
import { ThemeProvider } from "@material-ui/core/styles"
import CssBaseline from "@material-ui/core/CssBaseline"
import theme from "./components/theme"
import store, { history } from "./store"
import "./index.scss"
import App from "./App"
import i18n from "./translations/i18n"
import "./Auth/AwsConfig"
import "./services/AxiosConfig"

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <I18nextProvider i18n={i18n}>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <App />
        </ThemeProvider>
      </I18nextProvider>
    </ConnectedRouter>
  </Provider>,
  document.getElementById("root")
)
