import { publish, subscribe } from "utils/PubSub"

describe("PubSub utils", () => {
  test("PubSub Null ", async () => {
    expect(publish.bind(null)).not.toThrow()
    expect(subscribe.bind(null)).not.toThrow()
  })

  test("PubSub not null ", async () => {
    expect(publish.bind("notification.new", {})).not.toThrow()
    expect(subscribe.bind("notification.new", (message) => {})).not.toThrow()
  })

  test("PubSub not null ", async () => {
    subscribe("notification.new", (message) => {})
    expect(publish.bind("notification.new", {})).not.toThrow()
  })
})
