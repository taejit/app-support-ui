import React from "react"
import renderer from "react-test-renderer"
import { ChatContext, useChat } from "store/ChatContext"
import { PopupContext, usePopup } from "store/PopupContext"

import { GoodThings } from "components/Messaging/GoodThings"

describe("--- GoodThings Component ---", () => {
  it("to render ", async () => {
    const tree = renderer.create(<GoodThings />)

    expect(tree.root.findByProps({ id: "GoodThings-main" }).props).toBeDefined()
    expect(tree.toJSON()).toMatchSnapshot()
  })
})
