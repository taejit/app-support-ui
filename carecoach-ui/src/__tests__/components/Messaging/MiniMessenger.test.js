import React from "react"
import renderer from "react-test-renderer"
import { ChatContext, useChat } from "store/ChatContext"
import { PopupContext, usePopup } from "store/PopupContext"

import MiniMessenger from "components/Messaging/Mini/MiniMessenger"

jest.mock("aws-amplify", () => {
  return {
    __esModule: true,
    Auth: {
      federatedSignIn: jest.fn(),
      currentSession: () =>
        Promise.resolve({
          accessToken: { jwtToken: "token" },
        }),
      currentUserInfo: () =>
        Promise.resolve({
          attributes: {
            email: "bramsai@intraedge.com",
            email_verified: true,
            phone_number: "+12254789321",
            phone_number_verified: true,
            sub: "38041359-614f-4c44-9a32-23c3c80506c7",
            family_name: "kiran",
            given_name: "ramsai",
            "custom:location": "Hyderabad Telangana Hyd",
            "custom:pronoun": "he/his",
          },
          username: "acc2ea0f-3a1f-49b9-8b19-7f555ece71d6",
        }),
    },
  }
})

describe("--- MiniMessenger Component ---", () => {
  const navigation = {
    navigate: jest.fn(),
  }

  it("to render loading", async () => {
    const tree = renderer.create(<MiniMessenger />)

    expect(tree.root.findByProps({ id: "minimessenger" }).props).toBeDefined()
  })

  it("to render ", async () => {
    const data = {
      isConnecting: false,
    }
    let tree
    await renderer.act(() => {
      tree = renderer.create(
        <ChatContext.Provider value={data}>
          <MiniMessenger />
        </ChatContext.Provider>
      )
    })

    expect(tree.toJSON()).toMatchSnapshot()
  })
})
