import React from "react"
import renderer from "react-test-renderer"
import { ChatContext, useChat } from "store/ChatContext"
import { PopupContext, usePopup } from "store/PopupContext"

import { Notifications } from "components/Messaging/Notifications"

describe("--- Notifications Component ---", () => {
  const navigation = {
    navigate: jest.fn(),
  }

  it("to render completely", async () => {
    const data = {
      chatClientState: null,
      notificationChannelState: null,
    }

    const pop = { notificationPanelState: true }

    const tree = renderer.create(<Notifications navigation={navigation} />)

    expect(
      tree.root.findByProps({ id: "Notifications-container-main" }).props
    ).toBeDefined()
  })
})
