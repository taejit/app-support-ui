import React from "react"
import renderer from "react-test-renderer"

import { NotificationProcessor } from "components/Messaging/Notifications"
import { publish } from "utils/PubSub"

describe("--- NotificationProcessor Component ---", () => {
  const navigation = {
    navigate: jest.fn(),
  }

  it("to render completely", async () => {
    const tree = renderer.create(<NotificationProcessor />)

    expect(tree.toJSON()).toMatchSnapshot()
  })

  it("publish to render completely", async () => {
    const tree = renderer.create(<NotificationProcessor />)

    publish("notification.new", {
      action: { actionType: "redirect", payload: { type: "carecoach" } },
    })

    expect(tree.toJSON()).toMatchSnapshot()
  })
})
