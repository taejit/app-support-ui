import React from "react"
import renderer from "react-test-renderer"

import { NotificationList } from "components/Messaging/Notifications/notification-list"

describe("--- NotificationList Component  ---", () => {
  const navigation = {
    navigate: jest.fn(),
  }

  it("to render completely", async () => {
    const tree = renderer.create(<NotificationList messages={[]} />)

    expect(
      tree.root.findByProps({ id: "NotificationList-main" }).props
    ).toBeDefined()
  })

  it("to render Two Messages", async () => {
    const tree = renderer.create(<NotificationList messages={[{}, {}]} />)

    expect(
      tree.root.findByProps({ id: "NotificationList-main" }).props
    ).toBeDefined()
  })
})
