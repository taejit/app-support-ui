import React from "react"
import renderer from "react-test-renderer"
import { ChatContext, useChat } from "store/ChatContext"
import { PopupContext, usePopup } from "store/PopupContext"

import { NotificationIcon } from "components/Messaging/Notifications"

describe("--- NotificationIcon Component ---", () => {
  const navigation = {
    navigate: jest.fn(),
  }

  it("to render completely", async () => {
    const data = {
      chatClientState: null,
      notificationChannelState: null,
    }

    const pop = { notificationPanelState: true }

    const tree = renderer.create(<NotificationIcon />)

    expect(
      tree.root.findByProps({ id: "NotificationIcon" }).props
    ).toBeDefined()
  })
})
