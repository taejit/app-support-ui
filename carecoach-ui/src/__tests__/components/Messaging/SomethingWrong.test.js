import React from "react"
import renderer from "react-test-renderer"
import { ChatContext, useChat } from "store/ChatContext"
import { PopupContext, usePopup } from "store/PopupContext"

import { SomethingWrong } from "components/Messaging/SomethingWrong"

describe("--- SomethingWrong Component ---", () => {
  it("to render ", async () => {
    const tree = renderer.create(<SomethingWrong />)

    expect(
      tree.root.findByProps({ id: "SomethingWrong-main" }).props
    ).toBeDefined()
    expect(tree.toJSON()).toMatchSnapshot()
  })
})
