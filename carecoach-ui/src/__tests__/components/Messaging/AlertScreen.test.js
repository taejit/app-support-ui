import React from "react"
import renderer from "react-test-renderer"
import { ChatContext, useChat } from "store/ChatContext"
import { PopupContext, usePopup } from "store/PopupContext"

import { AlertScreen } from "components/Messaging/alert-screen"

describe("--- AlertScreen Component ---", () => {
  it("to render ", async () => {
    const tree = renderer.create(<AlertScreen />)

    expect(
      tree.root.findByProps({ id: "AlertScreen-main" }).props
    ).toBeDefined()
    expect(tree.toJSON()).toMatchSnapshot()
  })
})
