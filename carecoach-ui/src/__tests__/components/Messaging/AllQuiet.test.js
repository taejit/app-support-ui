import React from "react"
import renderer from "react-test-renderer"
import { ChatContext, useChat } from "store/ChatContext"
import { PopupContext, usePopup } from "store/PopupContext"

import { AllQuiet } from "components/Messaging/AllQuiet"

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "./aboutyou",
  }),
}))

describe("--- v Component ---", () => {
  it("to render ", async () => {
    const tree = renderer.create(<AllQuiet />)

    expect(tree.root.findByProps({ id: "AllQuiet-main" }).props).toBeDefined()
    expect(tree.toJSON()).toMatchSnapshot()
  })
})
