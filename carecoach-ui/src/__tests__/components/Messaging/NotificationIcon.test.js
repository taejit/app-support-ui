import React from "react"
import renderer from "react-test-renderer"
import { ChatContext, useChat } from "store/ChatContext"
import { PopupContext, usePopup } from "store/PopupContext"

import { MessagingIcon } from "components/Messaging/MessagingIcon"

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "./aboutyou",
  }),
}))

describe("--- MessagingIcon Component ---", () => {
  it("to render ", async () => {
    const tree = renderer.create(<MessagingIcon />)

    expect(tree.root.findByProps({ id: "MessagingIcon" }).props).toBeDefined()
    expect(tree.toJSON()).toMatchSnapshot()
  })
})
