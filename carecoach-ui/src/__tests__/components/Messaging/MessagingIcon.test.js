import React from "react"
import renderer from "react-test-renderer"
import { ChatContext, useChat } from "store/ChatContext"
import { PopupContext, usePopup } from "store/PopupContext"
import { Auth } from "aws-amplify"

import { MessagingIcon } from "components/Messaging/MessagingIcon"

const popdata = {
  messagingPanel: false,
}

const chatdata = {
  channel: {},
  chatClient: {
    queryChannels: async () => {
      return Promise.resolve([{ state: { unreadCount: 0 } }])
    },
    on: () => {
      return {
        unsubscribe: () => {},
      }
    },
  },
}

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "./aboutyou",
  }),
}))

describe("--- MessagingIcon Component ---", () => {
  it("to render ", async () => {
    Auth.currentUserInfo = () =>
      Promise.resolve({
        attributes: {
          email: "bramsai@intraedge.com",
          email_verified: true,
          phone_number: "+12254789321",
          phone_number_verified: true,
          sub: "38041359-614f-4c44-9a32-23c3c80506c7",
          family_name: "kiran",
          given_name: "ramsai",
          "custom:location": "Hyderabad Telangana Hyd",
          "custom:pronoun": "he/his",
          "custom:uuid": "acc2ea0f-3a1f-49b9-8b19-7f555ece71d6",
        },
        username: "acc2ea0f-3a1f-49b9-8b19-7f555ece71d6",
      })

    let tree
    await renderer.act(async () => {
      tree = renderer.create(
        <ChatContext.Provider value={chatdata}>
          <PopupContext.Provider value={popdata}>
            <MessagingIcon />
          </PopupContext.Provider>
        </ChatContext.Provider>
      )
    })

    expect(tree.root.findByProps({ id: "MessagingIcon" }).props).toBeDefined()
    expect(tree.toJSON()).toMatchSnapshot()
  })
})
