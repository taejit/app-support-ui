import React from "react"
import renderer from "react-test-renderer"
import { ChatContext } from "store/ChatContext"
import { PopupContext, usePopup } from "store/PopupContext"
import { ChatContext as MainChatContext } from "stream-chat-react"

import { MessagingSidebar } from "components/Messaging/MessagingSidebar"

jest.mock("stream-chat-react")

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "./aboutyou",
  }),
}))

jest.useFakeTimers("modern").setSystemTime(new Date("2020-01-01").getTime())

describe("--- Attachments Component ---", () => {
  it("to render ", async () => {
    const tree = renderer.create(<MessagingSidebar />)

    expect(tree.root.findByProps({ id: "sidebar-main" }).props).toBeDefined()
    expect(tree.toJSON()).toMatchSnapshot()
  })

  it("to render open ", async () => {
    const data = {
      isConnecting: false,
      chatClient: {
        queryChannels: () => Promise.resolve([]),
      },
    }

    const popdata = {
      messagingPanel: false,
    }

    const chatdata = {
      channel: { data: { created_at: {} } },
    }
    let tree
    await renderer.act(() => {
      tree = renderer.create(
        <MainChatContext.Provider value={chatdata}>
          <PopupContext.Provider value={popdata}>
            <ChatContext.Provider value={data}>
              <MessagingSidebar />
            </ChatContext.Provider>
          </PopupContext.Provider>
        </MainChatContext.Provider>
      )
    })

    expect(tree.toJSON()).toMatchSnapshot()
  })

  it("to render attachments ", async () => {
    const data = {
      isConnecting: false,
      chatClient: {
        queryChannels: () => Promise.resolve([]),
      },
    }

    const popdata = {
      messagingPanel: false,
    }

    const chatdata = {
      channel: {
        search: () => Promise.resolve([{}]),
        data: { created_at: {} },
      },
    }
    let tree
    await renderer.act(() => {
      tree = renderer.create(
        <MainChatContext.Provider value={chatdata}>
          <PopupContext.Provider value={popdata}>
            <ChatContext.Provider value={data}>
              <MessagingSidebar />
            </ChatContext.Provider>
          </PopupContext.Provider>
        </MainChatContext.Provider>
      )
    })

    expect(tree.root.findByProps({ id: "attachmentlist" }).props).toBeDefined()

    expect(tree.toJSON()).toMatchSnapshot()
  })
})
