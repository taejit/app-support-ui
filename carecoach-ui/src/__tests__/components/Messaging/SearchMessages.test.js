import React from "react"
import renderer from "react-test-renderer"
import { ChatContext, useChat } from "store/ChatContext"
import { PopupContext, usePopup } from "store/PopupContext"
import { ChatContext as MainChatContext } from "stream-chat-react"

import { SearchMessages } from "components/Messaging/SearchMessages"

jest.mock("stream-chat-react")

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "./aboutyou",
  }),
}))

describe("--- Attachments Component ---", () => {
  it("to render ", async () => {
    const tree = renderer.create(<SearchMessages />)

    expect(tree.root.findByProps({ id: "sidebar-search" }).props).toBeDefined()
    expect(tree.toJSON()).toMatchSnapshot()
  })

  it("to render open ", async () => {
    const data = {
      isConnecting: false,
      chatClient: {
        queryChannels: () => Promise.resolve([]),
      },
    }

    const popdata = {
      messagingPanel: false,
    }

    const chatdata = {
      channel: { data: { created_at: {} } },
    }
    let tree
    await renderer.act(() => {
      tree = renderer.create(
        <MainChatContext.Provider value={chatdata}>
          <PopupContext.Provider value={popdata}>
            <ChatContext.Provider value={data}>
              <SearchMessages />
            </ChatContext.Provider>
          </PopupContext.Provider>
        </MainChatContext.Provider>
      )
    })

    expect(tree.toJSON()).toMatchSnapshot()
  })

  it("to render ", async () => {
    const data = {
      isConnecting: false,
      chatClient: {
        queryChannels: () => Promise.resolve([]),
      },
    }

    const popdata = {
      messagingPanel: false,
    }

    const chatdata = {
      channel: {
        search: () => Promise.resolve([{}]),
        data: { created_at: {} },
      },
    }
    let tree
    await renderer.act(() => {
      tree = renderer.create(
        <MainChatContext.Provider value={chatdata}>
          <PopupContext.Provider value={popdata}>
            <ChatContext.Provider value={data}>
              <SearchMessages />
            </ChatContext.Provider>
          </PopupContext.Provider>
        </MainChatContext.Provider>
      )
    })

    expect(tree.root.findByProps({ id: "sidebar-search" }).props).toBeDefined()

    expect(tree.toJSON()).toMatchSnapshot()
  })
})
