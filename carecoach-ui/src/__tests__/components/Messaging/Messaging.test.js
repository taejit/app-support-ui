import React from "react"
import renderer from "react-test-renderer"
import { ChatContext, useChat } from "store/ChatContext"
import { PopupContext, usePopup } from "store/PopupContext"

import Messaging from "components/Messaging/Messaging"

describe("--- Messaging Component ---", () => {
  const navigation = {
    navigate: jest.fn(),
  }

  it("to render loading", async () => {
    const tree = renderer.create(<Messaging navigation={navigation} />)

    expect(tree.root.findByProps({ id: "loading" }).props).toBeDefined()
  })

  it("to render ", async () => {
    const data = {
      isConnecting: false,
    }
    let tree
    await renderer.act(() => {
      tree = renderer.create(
        <ChatContext.Provider value={data}>
          <Messaging navigation={navigation} />
        </ChatContext.Provider>
      )
    })

    expect(tree.toJSON()).toMatchSnapshot()
  })
})
