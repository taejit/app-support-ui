import React from "react"
import renderer from "react-test-renderer"
import { ChatContext, useChat } from "store/ChatContext"
import { PopupContext, usePopup } from "store/PopupContext"

import { NothingFound } from "components/Messaging/NothingFound"

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "./aboutyou",
  }),
}))

describe("--- v Component ---", () => {
  it("to render ", async () => {
    const tree = renderer.create(<NothingFound />)

    expect(
      tree.root.findByProps({ id: "nothing-found-main" }).props
    ).toBeDefined()
    expect(tree.toJSON()).toMatchSnapshot()
  })
})
