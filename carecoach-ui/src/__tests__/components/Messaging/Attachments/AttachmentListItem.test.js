import React from "react"
import renderer from "react-test-renderer"
import { AttachmentListItem } from "components/Messaging/Attachments/AttachmentListItem"

it("AttachmentListItem with Mock test", () => {
  const singleItem = {
    item: {
      name:
        "244c49c0-a9a7-4be4-ad1d-80eee2ea34c9.Screen%20Shot%202021-04-12%20at%2019.41.42.png",
      extension: "image/png",
      user: "fn_Harshal ln_Tikkas",
      link:
        "https://stream-chat-us-east-c4.imgix.net/1114831/images/244c49c0-a9a7-4be4-ad1d-80eee2ea34c9.Screen%20Shot%202021-04-12%20at%2019.41.42.png?ro=0&s=f331229f6edda10d7514c55f8440fb01",
    },
  }
  const tree = renderer.create(<AttachmentListItem {...singleItem} />).toJSON()
  expect(tree).toMatchSnapshot()
})
