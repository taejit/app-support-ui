import React from "react"
import renderer from "react-test-renderer"
import AttachmentList from "components/Messaging/Attachments/AttachmentList"

it("AttachmentList Component Snapshot", () => {
  const tree = renderer.create(<AttachmentList data={[]} />).toJSON()
  expect(tree).toMatchSnapshot()
})

it("AttachmentList with Mock test", () => {
  const data = [
    {
      user: "fn_Harshal ln_Tikkas",
      mimeType: "image/png",
      extension: "png",
      link:
        "https://stream-chat-us-east-c4.imgix.net/1114831/images/244c49c0-a9a7-4be4-ad1d-80eee2ea34c9.Screen%20Shot%202021-04-12%20at%2019.41.42.png?ro=0&s=f331229f6edda10d7514c55f8440fb01",
      name:
        "244c49c0-a9a7-4be4-ad1d-80eee2ea34c9.Screen%20Shot%202021-04-12%20at%2019.41.42.png",
    },
    {
      user: "fn_Harshal ln_Tikkas",
      mimeType: false,
      extension: "pdf",
      link:
        "https://stream-chat-us-east-c4.imgix.net/1114831/attachments/71dcacab-93c6-4c84-b491-0f2d687bd29c.dummy.pdf?dl=dummy.pdf&s=a3af1e9499138326ac0d6b531577dcde",
      name: "dummy.pdf",
    },
    {
      user: "fn_Harshal ln_Tikkas",
      mimeType: false,
      extension: "pdf",
      link:
        "https://stream-chat-us-east-c4.imgix.net/1114831/attachments/235e08ea-a8f4-45e2-a856-a0352ad0eae4.dummy%20%283%29.pdf?dl=dummy%20%283%29.pdf&s=97f724f409a6c6b37b1e3e97a9e9a6eb",
      name: "dummy (3).pdf",
    },
  ]
  const tree = renderer.create(<AttachmentList data={data} />).toJSON()
  expect(tree).toMatchSnapshot()
})
