import React from "react"
import renderer from "react-test-renderer"

import { AppDataProvider } from "components/AppDataProvider"

describe("AppDataProvider component", () => {
  test("Matches the snapshot", () => {
    const provider = renderer.create(<AppDataProvider />)
    expect(provider.toJSON()).toMatchSnapshot()
  })
})
