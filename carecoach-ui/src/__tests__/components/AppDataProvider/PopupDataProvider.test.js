import React from "react"
import renderer from "react-test-renderer"

import { PopupDataProvider } from "components/AppDataProvider/PopupDataProvider"

describe("PopupDataProvider component", () => {
  test("Matches the snapshot", () => {
    const provider = renderer.create(<PopupDataProvider />)
    expect(provider.toJSON()).toMatchSnapshot()
  })
})
