import React from "react"
import renderer from "react-test-renderer"

import { ChatDataProvider } from "components/AppDataProvider/ChatDataProvider"

describe("ChatDataProvider component", () => {
  test("Matches the snapshot", () => {
    const provider = renderer.create(<ChatDataProvider />)
    expect(provider.toJSON()).toMatchSnapshot()
  })
})
