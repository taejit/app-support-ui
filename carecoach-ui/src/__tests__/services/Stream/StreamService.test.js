import stream from "services/Stream"
jest.mock("stream-chat")

describe("StreamService services", () => {
  const { initClient, initClientWithUserProvider } = stream()

  test("initClientWithUserProvider Null Null", async () => {
    expect(initClientWithUserProvider(null, null)).rejects.toMatch(
      "Provider function is null"
    )
  })

  test("Init", async () => {
    const client = await initClient(null, null)
    expect(client).toBe(undefined)
  })

  test("initClientWithUserProvider NotNull Null", async () => {
    expect(initClientWithUserProvider("123", null)).rejects.toMatch(
      "Provider function is null"
    )
  })

  test("initClientWithUserProvider NotNull NotNull", async () => {
    expect(
      initClientWithUserProvider("123", () => Promise.resolve("123"))
    ).rejects.toMatch("Provider function is null")
  })
})
