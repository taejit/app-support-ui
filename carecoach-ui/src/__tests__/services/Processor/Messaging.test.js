import processor from "services/Processor"

//const { processMessage } = services.processor.messaging({ navigation })

describe("Processosr messaging services", () => {
  test("processMessage Null ", async () => {
    const { processMessage } = processor.messaging()
    expect(processMessage.bind(null)).not.toThrow()
  })

  test("processMessage navigation null ", async () => {
    const { processMessage } = processor.messaging({ navigation: null })
    expect(processMessage.bind({ actionType: "redirect" })).not.toThrow()
  })

  test("processMessage navigation good ", async () => {
    const { processMessage } = processor.messaging({ navigation: {} })
    expect(
      processMessage.bind({
        actionType: "redirect",
        payload: { type: "careplan" },
      })
    ).not.toThrow()
  })
})
