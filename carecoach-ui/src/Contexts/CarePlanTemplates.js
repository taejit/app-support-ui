import { createContext } from "react"

export default createContext({
  carePlanTemplates: {},
  setCarePlanTemplate: () => {},
  templates: [],
  setTemplates: () => {},
})
