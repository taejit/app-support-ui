import redirect from "./Redirect"
import read from "./Read"

export default ({ navigation, client } = {}) => {
  const markReadMessage = (message) => {
    read(client).processMessage(message)
  }

  const processMessage = (message) => {
    const { action: { actionType, payload = {} } = {} } = message || {}

    //mark as read when processed
    markReadMessage(message)

    if (actionType === "redirect") {
      redirect(navigation).processMessage(payload)
    }
  }

  return {
    processMessage,
    markReadMessage,
  }
}
