export default (client) => {
  const processMessage = (message) => {
    if (client && message) {
      if (!message.read) {
        const newMessage = { ...message, read: true }
        try {
          client.updateMessage(newMessage)
        } catch (e) {
          console.error("Message couldn't been update it.", e)
        }
      }
    }
  }

  return {
    processMessage,
  }
}
