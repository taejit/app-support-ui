export default (navigation) => {
  const processMessage = ({ type = undefined }) => {
    switch (type) {
      case "caregiver":
        navigation.push(`/dashboard/caregivers`)
        break

      default:
      // do nothing
    }
  }

  return {
    processMessage,
  }
}
