/* eslint import/prefer-default-export:0 */
import axios from "axios"

// eslint-disable-next-line no-unused-vars
export const GetScheduleCalls = async (uuid) => {
  return axios.get(`/api/v1/carecoaches/${uuid}/scheduled-events`).catch(() => {
    return {}
  })
}

export const GetOpenTokSession = async (carecoachUuid, uuid) => {
  return axios
    .get(
      `/api/v1/carecoaches/${carecoachUuid}/scheduled-events/${uuid}/session`
    )
    .catch(() => {
      return {}
    })
}

export const updateCarecoachVideoSchedule = async (
  uuid,
  scheduleUuid,
  reqObj
) => {
  return axios
    .patch(
      `/api/v1/carecoaches/${uuid}/scheduled-events/${scheduleUuid}`,
      reqObj
    )
    .catch(() => {
      return {}
    })
}
