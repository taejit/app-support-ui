import axios from "axios"
import OnboardingApiService from "./Onboarding.service"

jest.mock("axios")

describe("Onboarding Service", () => {
  beforeEach(() => {
    jest.spyOn(axios, "create").mockReturnThis(() => jest.fn())
  })
  afterEach(() => {
    jest.restoreAllMocks()
  })
  it("should return profile image data", async () => {
    jest.spyOn(axios, "get").mockResolvedValue({
      data: {
        presignedUrl: "testImgUrl",
      },
    })
    const actual = await OnboardingApiService.getCareCoachProfileImage("uuid")
    expect(actual).toEqual({
      data: {
        presignedUrl: "testImgUrl",
      },
    })
  })

  it("should return undefined on image url", async () => {
    jest.spyOn(axios, "default").mockResolvedValue(undefined)
    const actual = await OnboardingApiService.checkProfileImageUrl("testUrl")
    expect(actual).toEqual(undefined)
  })

  it("should get presignedUrl to uploadprofile image", async () => {
    jest.spyOn(axios, "put").mockResolvedValue({
      data: {
        url: "testPresignedUrl",
      },
    })
    const actual = await OnboardingApiService.getPresignedUrlForCareCoachProfileImage(
      "uuid"
    )
    expect(actual).toEqual({
      data: {
        url: "testPresignedUrl",
      },
    })
  })

  it("should upload image with presignedUrl", async () => {
    jest.spyOn(axios, "default").mockResolvedValue({})
    const actual = await OnboardingApiService.uploadImageThroughPresignedUrl(
      "url",
      "fileData"
    )
    expect(actual).toEqual({})
  })

  it("sync profile api", async () => {
    jest.spyOn(axios, "default").mockResolvedValue({})
    const actual = await OnboardingApiService.syncCareCoach(
      {
        fullName: "John Doe",
        firstName: "John",
        lastName: "Doe",
        nickName: "Joe",
        email: "example@lantern.care",
        phone: "+16021234567",
        employerName: "ABC Inc",
        pronouns: "He/him",
        isActive: true,
      },
      "uuid"
    )
    expect(actual).toEqual({})
  })
})
