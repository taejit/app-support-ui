/* eslint import/prefer-default-export:0 */
import axios from "axios"
import contentfulClient from "../Auth/ContentfulConfig"

// eslint-disable-next-line no-unused-vars
export const GetCarePlan = async (uuid) => {
  return axios.get(`/api/v1/caregivers/${uuid}/careplan`).catch(() => {
    return {}
  })
}
export const GetAllCarePlanTemplate = () => {
  return contentfulClient.getEntries({
    content_type: "carePlanTemplate",
  })
}
export const GetAllCarePlanItem = () => {
  return contentfulClient.getEntries({
    content_type: "carePlanItem",
  })
}

export const SaveCarePlan = (carecoachUuid, caregiverUuid, data) => {
  return axios
    .post(
      `/api/v1/carecoaches/${carecoachUuid}/caregivers/${caregiverUuid}/careplan`,
      data
    )
    .catch(() => {
      return {}
    })
}

export const SaveMilestonesInCarePlan = (
  carecoachUuid,
  caregiverUuid,
  data
) => {
  return axios
    .post(
      `/api/v1/carecoaches/${carecoachUuid}/caregivers/${caregiverUuid}/careplan/items`,
      data
    )
    .catch(() => {
      return {}
    })
}
