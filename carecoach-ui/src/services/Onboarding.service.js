import axios from "axios"

const onboardingApiService = {
  getCareCoachProfileImage: async (uuid) => {
    const profileImageResponse = await axios.get(
      `/api/v1/carecoaches/${uuid}/image`
    )
    return profileImageResponse
  },

  checkProfileImageUrl: async (url) => {
    const axiosInstance = axios.create()
    const profileImageResponse = await axiosInstance.get(url)
    return profileImageResponse
  },

  getPresignedUrlForCareCoachProfileImage: async (uuid) => {
    const profileImageResponse = await axios.put(
      `/api/v1/carecoaches/${uuid}/image`
    )
    return profileImageResponse
  },

  uploadImageThroughPresignedUrl: async (url, requestObj) => {
    const axiosInstance = axios.create()
    const response = await axiosInstance({
      method: "PUT",
      url,
      data: requestObj,
    })
    return response
  },
  syncCareCoach: async (requestObj, uuid) => {
    const response = await axios({
      method: "put",
      url: `/api/v1/carecoaches/${uuid}/profile`,
      data: requestObj,
    })
    return response
  },
}

export default onboardingApiService
