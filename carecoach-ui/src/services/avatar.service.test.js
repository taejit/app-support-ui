import ProfileImgService from "./avatar.service"
import OnboardingApiService from "./Onboarding.service"

describe("Avatar Service test cases", () => {
  beforeEach(() => {
    jest
      .spyOn(OnboardingApiService, "getCareCoachProfileImage")
      .mockResolvedValue({
        data: {
          presignedUrl: "testImgUrl",
        },
      })
    jest
      .spyOn(OnboardingApiService, "checkProfileImageUrl")
      .mockResolvedValue({})
  })

  it("Get profile image", async () => {
    const response = await ProfileImgService.GetProfileImage("test-uuid")
    expect(response).toBeTruthy()
    expect(response.status).toBe("uploadSuccess")
  })

  it("Get profile image: 400 error", async () => {
    jest
      .spyOn(OnboardingApiService, "getCareCoachProfileImage")
      .mockRejectedValue({
        response: {
          status: 400,
        },
      })
    const response = await ProfileImgService.GetProfileImage("test-uuid")
    expect(response).toBeTruthy()
    expect(response.status).toBe("getError")
  })

  it("Get profile image: 403 error", async () => {
    jest
      .spyOn(OnboardingApiService, "getCareCoachProfileImage")
      .mockRejectedValue({
        response: {
          status: 403,
        },
      })
    const response = await ProfileImgService.GetProfileImage("test-uuid")
    expect(response).toBeTruthy()
    expect(response.status).toBe("defaultText")
  })

  it("update profile image", async () => {
    jest
      .spyOn(OnboardingApiService, "getPresignedUrlForCareCoachProfileImage")
      .mockResolvedValue({
        data: {
          url: "testPresignedUrl",
        },
      })
    jest
      .spyOn(OnboardingApiService, "uploadImageThroughPresignedUrl")
      .mockResolvedValue({ status: 200 })
    const response = await ProfileImgService.ProcessProfileImage(
      {
        target: {
          files: [{ name: "samplefile.png", size: 999 }],
        },
      },
      "test-uuid"
    )
    expect(response).toBeTruthy()
    expect(response.status).toBe("uploadSuccess")
    expect(
      OnboardingApiService.getPresignedUrlForCareCoachProfileImage
    ).toHaveBeenCalled()
    expect(
      OnboardingApiService.uploadImageThroughPresignedUrl
    ).toHaveBeenCalled()
  })

  it("update profile image: extension error", async () => {
    jest
      .spyOn(OnboardingApiService, "getPresignedUrlForCareCoachProfileImage")
      .mockResolvedValue({
        data: {
          url: "testPresignedUrl",
        },
      })
    jest
      .spyOn(OnboardingApiService, "uploadImageThroughPresignedUrl")
      .mockResolvedValue({})
    const response = await ProfileImgService.ProcessProfileImage(
      {
        target: {
          files: [{ name: "samplefile.png1" }],
        },
      },
      "test-uuid"
    )
    expect(response).toBeTruthy()
    expect(response.status).toBe("extError")
  })

  it("update profile image: size error", async () => {
    jest
      .spyOn(OnboardingApiService, "getPresignedUrlForCareCoachProfileImage")
      .mockResolvedValue({
        data: {
          url: "testPresignedUrl",
        },
      })
    jest
      .spyOn(OnboardingApiService, "uploadImageThroughPresignedUrl")
      .mockResolvedValue({})
    const response = await ProfileImgService.ProcessProfileImage(
      {
        target: {
          files: [{ name: "samplefile.png", size: 999999999 }],
        },
      },
      "test-uuid"
    )
    expect(response).toBeTruthy()
    expect(response.status).toBe("sizeError")
  })

  it("update profile image: upload error uncatched", async () => {
    jest
      .spyOn(OnboardingApiService, "getPresignedUrlForCareCoachProfileImage")
      .mockResolvedValue({
        data: {
          url: "testPresignedUrl",
        },
      })
    jest
      .spyOn(OnboardingApiService, "uploadImageThroughPresignedUrl")
      .mockRejectedValue({})
    const response = await ProfileImgService.ProcessProfileImage(
      {
        target: {
          files: [{ name: "samplefile.png", size: 999 }],
        },
      },
      "test-uuid"
    )
    expect(response).toBeTruthy()
    expect(response.status).toBe("uploadError")
  })

  it("update profile image: upload error", async () => {
    jest
      .spyOn(OnboardingApiService, "getPresignedUrlForCareCoachProfileImage")
      .mockResolvedValue({
        data: {
          url: "testPresignedUrl",
        },
      })
    jest
      .spyOn(OnboardingApiService, "uploadImageThroughPresignedUrl")
      .mockResolvedValue({ status: 500 })
    const response = await ProfileImgService.ProcessProfileImage(
      {
        target: {
          files: [{ name: "samplefile.png", size: 999 }],
        },
      },
      "test-uuid"
    )
    expect(response).toBeTruthy()
    expect(response.status).toBe("uploadError")
  })
})
