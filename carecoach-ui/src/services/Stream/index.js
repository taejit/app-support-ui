import { Config } from "config"
import { StreamChat } from "stream-chat"

const { STREAM: { API_KEY = undefined } = "" } = Config || {}

const context = { loaded: undefined, retryCount: -1 }

export default () => {
  const initClient = async (userId, userToken, retryCount) => {
    if (!userId || !userToken || context.retryCount === retryCount)
      return Promise.resolve()

    return new Promise((resolve, reject) => {
      const chatClient = StreamChat.getInstance(API_KEY, {
        allowServerSideConnect: true,
      })
      chatClient
        .connectUser({ id: userId }, userToken)
        .then(() => {
          context.loaded = true
          context.retryCount = retryCount
          resolve(chatClient)
        })
        .catch((error) => {
          console.error(error)
          reject(error)
        })
    })
  }

  const initClientWithUserProvider = async (userId, provider, retryCount) =>
    new Promise((resolve, reject) => {
      if (provider) {
        provider(userId)
          .then((result) => {
            const { data: { token = "" } = {} } = result
            initClient(userId, token, retryCount)
              .then((client) => resolve(client))
              .catch((error) => {
                console.error(error)
                reject(error)
              })
          })
          .catch((error) => {
            console.error(error)
            reject(error)
          })
      } else {
        const error = "Provider function is null"
        console.error(error)
        reject(error)
      }
    })

  const closeClientGracefully = () => {
    context.loaded = undefined
    context.retryCount = 0
  }

  return {
    initClient,
    initClientWithUserProvider,
    closeClientGracefully,
  }
}
