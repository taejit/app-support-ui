import axios from "axios"
import {
  GetCarecoachProfile,
  GetCarecoachCaregiversCalls,
} from "./CarecaochServices"

describe("Carecoach services", () => {
  it("Profile api invoked", async () => {
    const profileData = {
      firstName: "John",
      lastName: "Doe",
      nickName: "Joe",
      email: "example@lantern.care",
      phone: "6021234567",
      pronouns: "He/him",
      location: "Gilbert, Arizona",
      jobTitle: "Nurse",
      jobStartDate: "2021-21-01",
      specialities: "MR",
      about: "A log describption about the carecoach",
      profileImage: "https://signedurl-profile-example.com",
    }
    axios.get = jest.fn().mockResolvedValue(profileData)
    const profile = await GetCarecoachProfile("abcd-234-df-23hjdfhj")
    expect(profile).toMatchObject(profileData)
  })

  it("Profile api 500 error", async () => {
    axios.get.mockRejectedValueOnce("5xx error")
    const profile = await GetCarecoachProfile("abcd-234-df-23hjdfhj")
    expect(profile).toMatchObject({})
  })
  it("Carecoach Caregivers calls list", async () => {
    const callsList = {
      data: [
        {
          caregiver_uuid: "uuid",
          upcomingCallAt: new Date(),
        },
      ],
    }
    axios.get = jest.fn().mockResolvedValue(callsList)
    const callsListResponse = await GetCarecoachCaregiversCalls(
      "abc1-23hs-sn34-sdsd"
    )
    expect(callsListResponse).toMatchObject(callsList)
  })

  it("Carecoach Caregivers calls list api error", async () => {
    axios.get.mockRejectedValueOnce("5xx error")
    const callsListResponse = await GetCarecoachCaregiversCalls(
      "abc1-23hs-sn34-sdsd"
    )
    expect(callsListResponse).toEqual({})
  })
})
