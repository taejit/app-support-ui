import api from "./Api"
import stream from "./Stream"
import processor from "./Processor"

export default {
  api,
  stream,
  processor,
}
