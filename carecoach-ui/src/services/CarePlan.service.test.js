import axios from "axios"
import { GetCarePlan, SaveCarePlan } from "./CarePlan.service"

describe("Carecoach services", () => {
  it("Care Plan api invoked", async () => {
    const carePlan = {
      isDraft: true,
      items: [
        {
          name: "Cancel Driving Licence",
          rationale:
            "Describe what is the rationale behind it. Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups",
          description:
            "Describe the item properly. Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.",
          dueDate: "2021-02-27",
          tasks: [
            {
              name: '"Read why you need to cancel DL"',
              link: '"https://loremipsum.io/"',
              isCompleted: true,
            },
          ],
        },
      ],
    }
    axios.get = jest.fn().mockResolvedValue(carePlan)
    const carePlanRes = await GetCarePlan("abcd-234-df-23hjdfhj")
    expect(carePlanRes).toMatchObject(carePlan)
  })

  it("Care Plan api 500 error", async () => {
    axios.get.mockRejectedValueOnce("5xx error")
    const carePlan = await GetCarePlan("abcd-234-df-23hjdfhj")
    expect(carePlan).toMatchObject({})
  })
  it("Care Plan create api invoked", async () => {
    axios.post = jest.fn().mockResolvedValue({})
    const carePlanRes = await SaveCarePlan(
      "abcd-234-df-23hjdfhj",
      "abcd-234-df-23hjdfhj",
      {}
    )
    expect(carePlanRes).toMatchObject({})
  })

  it("Care Plan create api 500 error", async () => {
    axios.post.mockRejectedValueOnce("5xx error")
    const carePlanRes = await SaveCarePlan(
      "abcd-234-df-23hjdfhj",
      "abcd-234-df-23hjdfhj",
      {}
    )
    expect(carePlanRes).toMatchObject({})
  })
})
