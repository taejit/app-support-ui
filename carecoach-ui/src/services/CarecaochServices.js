/* eslint import/prefer-default-export:0 */
import axios from "axios"

const GetCarecoachProfile = (uuid) => {
  return axios.get(`/api/v1/carecoaches/${uuid}`).catch(() => {
    return {}
  })
}

const UpdateCarecoachBioProfile = (values, uuid) => {
  return axios.patch(`/api/v1/carecoaches/${uuid}/bio`, values).catch(() => {
    return {}
  })
}

const GetCarecoachCaregiversCalls = async (uuid) => {
  return axios
    .get(`/api/v1/carecoaches/${uuid}/caregivers/contactinfo`)
    .catch(() => {
      return {}
    })
}
export {
  GetCarecoachProfile,
  UpdateCarecoachBioProfile,
  GetCarecoachCaregiversCalls,
}
