/* eslint import/prefer-default-export:0 */
import axios from "axios"
import contentfulClient from "../Auth/ContentfulConfig"
// eslint-disable-next-line no-unused-vars
export const GetCaregiversList = async (uuid) => {
  return axios.get(`/api/v1/carecoaches/${uuid}/caregivers`).catch(() => {
    return {}
  })
}

export const GetCaregiversProfile = (carecoachUuid, caregiverUuid) => {
  return axios
    .get(`/api/v1/carecoaches/${carecoachUuid}/caregivers/${caregiverUuid}`)
    .catch(() => {
      return {}
    })
}

export const GetCaregiversNotes = (carecoachUuid, caregiverUuid) => {
  return axios
    .get(
      `/api/v1/carecoaches/${carecoachUuid}/caregivers/${caregiverUuid}/notes`
    )
    .catch(() => {
      return []
    })
}

export const SaveCaregiversNote = (carecoachUuid, caregiverUuid, data) => {
  return axios
    .post(
      `/api/v1/carecoaches/${carecoachUuid}/caregivers/${caregiverUuid}/notes`,
      data
    )
    .catch(() => {
      return {}
    })
}

export const EditCaregiversNote = (
  carecoachUuid,
  caregiverUuid,
  noteUuid,
  data
) => {
  return axios
    .patch(
      `/api/v1/carecoaches/${carecoachUuid}/caregivers/${caregiverUuid}/notes/${noteUuid}`,
      data
    )
    .catch(() => {
      return {}
    })
}

export const UpdateCareGiversMilestone = (
  carecoachUuid,
  caregiverUuid,
  itemUuid,
  data
) => {
  return axios
    .put(
      `/api/v1/carecoaches/${carecoachUuid}/caregivers/${caregiverUuid}/careplan/items/${itemUuid}`,
      data
    )
    .catch(() => {
      return {}
    })
}

export const DeleteCareGiversMilestone = (
  carecoachUuid,
  caregiverUuid,
  itemUuid
) => {
  return axios
    .delete(
      `/api/v1/carecoaches/${carecoachUuid}/caregivers/${caregiverUuid}/careplan/items/${itemUuid}`
    )
    .catch(() => {
      return {}
    })
}

export const GetCaregiversMiniSurveys = (caregiverUuid) => {
  return axios
    .get(`/api/v1/caregivers/${caregiverUuid}/mini-surveys/responses`)
    .catch(() => {
      return null
    })
}

export const GetAllQuotes = () => {
  return contentfulClient.getEntries({
    content_type: "quoteOfTheDay",
  })
}
