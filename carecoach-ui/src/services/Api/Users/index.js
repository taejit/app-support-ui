import axios from "axios"

export default () => {
  const getMessagingToken = async (uuid) => {
    return axios.get(`/api/v1/users/${uuid}/messaging-token`)
  }

  return { getMessagingToken }
}
