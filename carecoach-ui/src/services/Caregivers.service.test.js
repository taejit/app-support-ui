import axios from "axios"
import {
  GetCaregiversList,
  GetCaregiversNotes,
  SaveCaregiversNote,
  EditCaregiversNote,
  UpdateCareGiversMilestone,
  DeleteCareGiversMilestone,
  GetCaregiversMiniSurveys,
} from "./Caregivers.service"

describe("Carecoach services", () => {
  it("Caregivers List api invoked", async () => {
    const caregiversList = {
      data: [
        {
          uuid: "63013804-29a0-4f89-ab6b-d02c19e17e7c",
          firstName: "jerry",
          lastName: "tom",
          nickName: "cook",
          hasUnReadMessages: false,
          lastContactedDate: "2021-01-09T20:13:48.850Z",
          upcomingCallAt: null,
          recipientNickName: "Charlie",
          relationshipToRecipient: "Parent",
          conditions: null,
          preferredCommunication: "MESSAGING",
        },
      ],
    }
    axios.get = jest.fn().mockResolvedValue(caregiversList)
    const profile = await GetCaregiversList("abcd-234-df-23hjdfhj")
    expect(profile).toMatchObject(caregiversList)
  })

  it("Caregivers List api 500 error", async () => {
    axios.get.mockRejectedValueOnce("5xx error")
    const profile = await GetCaregiversList("abcd-234-df-23hjdfhj")
    expect(profile).toMatchObject({})
  })

  it("Caregivers notes list", async () => {
    const caregiversNotes = {
      data: [
        {
          title: "title1",
          description: "description1",
          createdAt: "2020-09-12",
        },
      ],
    }
    axios.get = jest.fn().mockResolvedValue(caregiversNotes)
    const notesList = await GetCaregiversNotes(
      "abc1-23hs-sn34-sdsd",
      "bnj22-43hu-ss9s"
    )
    expect(notesList).toMatchObject(caregiversNotes)
  })

  it("Caregivers notes list api error", async () => {
    axios.get.mockRejectedValueOnce("5xx error")
    const notesList = await GetCaregiversNotes(
      "abc1-23hs-sn34-sdsd",
      "bnj22-43hu-ss9s"
    )
    expect(notesList).toEqual([])
  })

  it("Save caregivers notes", async () => {
    axios.post = jest.fn().mockResolvedValue({ status: 201 })
    const saveNote = await SaveCaregiversNote(
      "abc1-23hs-sn34-sdsd",
      "bnj22-43hu-ss9s"
    )
    expect(saveNote.status).toEqual(201)
  })

  it("Save note failiure", async () => {
    axios.post.mockRejectedValueOnce("5xx error")
    const saveNote = await SaveCaregiversNote(
      "abc1-23hs-sn34-sdsd",
      "bnj22-43hu-ss9s"
    )
    expect(saveNote).toEqual({})
  })

  it("Edit note", async () => {
    axios.patch = jest.fn().mockResolvedValue({ status: 200 })
    const editNote = await EditCaregiversNote("asbas67d", "hjsdsd", "sdsdsd", {
      title: "New title",
      description: "",
      nextSteps: "",
    })
    expect(editNote.status).toEqual(200)
  })
  it("Edit note failed", async () => {
    axios.patch.mockRejectedValueOnce("5xx error")
    const editNote = await EditCaregiversNote("asbas67d", "hjsdsd", "sdsdsd", {
      title: "New title",
      description: "",
      nextSteps: "",
    })
    expect(editNote).toEqual({})
  })
  it("Update CareGivers Milestone", async () => {
    axios.put = jest.fn().mockResolvedValue({ status: 200 })
    const updateReq = {
      name: "T1 - WAKE UP TestMsg",
      description:
        "Upon hearing a blaring sound, slowly come to consciousness.",
      rationale: "Because you can't sleep all day!",
      duedate: null,
      tasks: [
        {
          uuid: "b1d6e17b-0e06-41a6-9fd8-8f1a4dd64e11",
          name: "T1 - Set my alarm",
          link: "T1 - Set my alarm",
          isCompleted: false,
          createdAt: "2021-03-29T12:11:59.895Z",
          updatedAt: "2021-03-29T12:11:59.894Z",
          id: 0,
        },
      ],
    }
    const editMile = await UpdateCareGiversMilestone(
      "c6295320-204c-49af-b9ee-450da02bc359",
      "0461092b-25e3-4721-a88e-ee16cf1e4c39",
      "bc9911d1-ca7f-4ebf-ae53-efb582b6b497",
      updateReq
    )
    expect(editMile.status).toEqual(200)
  })

  it("Edit milestone failed", async () => {
    axios.put.mockRejectedValueOnce("5xx error")
    const updateReq = {
      name: "T1 - WAKE UP TestMsg",
      description:
        "Upon hearing a blaring sound, slowly come to consciousness.",
      rationale: "Because you can't sleep all day!",
      duedate: null,
      tasks: [
        {
          uuid: "b1d6e17b-0e06-41a6-9fd8-8f1a4dd64e11",
          name: "T1 - Set my alarm",
          link: "T1 - Set my alarm",
          isCompleted: false,
          createdAt: "2021-03-29T12:11:59.895Z",
          updatedAt: "2021-03-29T12:11:59.894Z",
          id: 0,
        },
      ],
    }
    const editMile = await UpdateCareGiversMilestone(
      "c6295320-204c-49af-b9ee-450da02bc359",
      "0461092b-25e3-4721-a88e-ee16cf1e4c39",
      "bc9911d1-ca7f-4ebf-ae53-efb582b6b497",
      updateReq
    )
    expect(editMile).toEqual({})
  })

  it("Delete CareGivers Milestone", async () => {
    axios.put = jest.fn().mockResolvedValue({ status: 200 })
    const deleteMile = await DeleteCareGiversMilestone(
      "c6295320-204c-49af-b9ee-450da02bc359",
      "0461092b-25e3-4721-a88e-ee16cf1e4c39",
      "bc9911d1-ca7f-4ebf-ae53-efb582b6b497"
    )
    expect(deleteMile.status)
  })

  it("Caregivers surveys list", async () => {
    const caregiversSurveys = {
      data: [
        {
          uuid: 1,
          overallScore: 13,
          createdAt: "2021-04-30T07:53:20.908Z",
          responses: [
            {
              question: "Sample mini survey question goes here.",
              answer: "Answer to the mini survey question",
              questionIdentifier: "Q_HEALTH_ID",
              answerIdentifier: "A_HEALTH_ID",
              score: 1,
            },
          ],
        },
      ],
    }
    axios.get = jest.fn().mockResolvedValue(caregiversSurveys)
    const surveysList = await GetCaregiversMiniSurveys("abc1-23hs-sn34-sdsd")
    expect(surveysList).toMatchObject(caregiversSurveys)
  })

  it("Caregivers surveys list api error", async () => {
    axios.get.mockRejectedValueOnce("5xx error")
    const surveysList = await GetCaregiversMiniSurveys("abc1-23hs-sn34-sdsd")
    expect(surveysList).toEqual(null)
  })
})
