import axios from "axios"
import { Auth } from "aws-amplify"

axios.defaults.baseURL = `${window._env_.REACT_APP_CARECOACH_API_URL}`

axios.interceptors.request.use((config) => {
  async function setHeader() {
    const newConfig = { ...config }
    const currentSession = await Auth.currentSession()
    const { idToken } = currentSession
    newConfig.headers.Authorization = (idToken && idToken.jwtToken) || ""
    return newConfig
  }
  return setHeader()
})
