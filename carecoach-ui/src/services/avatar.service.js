import onboardingApiService from "./Onboarding.service"

const allowedImageExt = ["jpg", "gif", "png", "jpeg"]

// eslint-disable-next-line consistent-return
const GetProfileImage = async (uuid) => {
  try {
    const profileImgUrlResponse = await onboardingApiService.getCareCoachProfileImage(
      uuid
    )
    const profileImageUrl =
      profileImgUrlResponse.data && profileImgUrlResponse.data.presignedUrl
    if (profileImageUrl) {
      await onboardingApiService.checkProfileImageUrl(profileImageUrl)
      return { profileImageUrl, status: "uploadSuccess", isLoading: false }
    }
  } catch (error) {
    if (
      (error.response && error.response.status === 404) ||
      (error.response && error.response.status === 403)
    ) {
      return {
        profileImageUrl: null,
        status: "defaultText",
        isLoading: false,
      }
    }
    return { profileImageUrl: null, status: "getError", isLoading: false }
  }
}

// eslint-disable-next-line consistent-return
const ProcessProfileImage = async (event, uuid) => {
  try {
    const { files } = event.target
    if (
      files[0] &&
      files[0].name &&
      !allowedImageExt.includes(files[0].name.split(".").pop().toLowerCase())
    ) {
      return { profileImageUrl: null, status: "extError", isLoading: false }
    }

    if ((files[0].size && files[0].size / 1024 / 1024).toFixed(2) > 1) {
      return { profileImageUrl: null, status: "sizeError", isLoading: false }
    }

    const formData = new FormData()
    formData.append("data", files[0])

    const presignedUrlData = await onboardingApiService.getPresignedUrlForCareCoachProfileImage(
      uuid
    )

    if (presignedUrlData.data.url) {
      const uploadResponse = await onboardingApiService.uploadImageThroughPresignedUrl(
        presignedUrlData.data.url,
        files[0]
      )
      if (uploadResponse.status === 200) {
        return {
          profileImageUrl: uploadResponse.url,
          status: "uploadSuccess",
          isLoading: false,
        }
      }
      return {
        profileImageUrl: null,
        status: "uploadError",
        isLoading: false,
      }
    }
  } catch (error) {
    return { profileImageUrl: null, status: "uploadError", isLoading: false }
  }
}

export default { GetProfileImage, ProcessProfileImage }
