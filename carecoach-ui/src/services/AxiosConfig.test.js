import axios from "axios"
import "./AxiosConfig"
import { Auth } from "aws-amplify"

describe("Axios default setting", () => {
  it("Axios default url set correctly", () => {
    expect(axios.defaults.baseURL).toEqual(
      `${window._env_.REACT_APP_CARECOACH_API_URL}`
    )
  })

  it("Axios interceptor testing for Authorization header", async () => {
    Auth.currentSession = jest
      .fn()
      .mockReturnValue(Promise.resolve({ idToken: { jwtToken: "token" } }))
    const result = await axios.interceptors.request.handlers[0].fulfilled({
      headers: {},
    })
    expect(result.headers).toHaveProperty("Authorization")
  })

  it("Axios interceptor getting correct value", async () => {
    Auth.currentSession = jest
      .fn()
      .mockReturnValue(Promise.resolve({ idToken: { jwtToken: "token" } }))
    const result = await axios.interceptors.request.handlers[0].fulfilled({
      headers: {},
    })
    expect(result.headers.Authorization).toEqual("token")
  })
})
