import axios from "axios"
import * as DashboardAPIservice from "./Dashboard.service.js"

describe("Dashboard services", () => {
  it("Schedules List api invoked", async () => {
    const schedulesList = {
      data: [
        {
          uuid: "1",
          caregiver_uuid: "d6717250-0948-4b31-94d6-ee6bc244259f",
          name: "Nikola Tesla",
          title: "End of life",
          startTime: "2021-04-08T12:05:53Z",
          endTime: "2021-04-08T12:20:53Z",
        },
        {
          uuid: "2",
          caregiver_uuid: "777f7e96-76b9-449b-a141-58016fe23eae",
          name: "Asim Searah",
          title: "Legalese",
          startTime: "2021-04-08T07:36:53Z",
          endTime: "2021-04-08T07:51:53Z",
        },
        {
          uuid: "3",
          caregiver_uuid: "a2bda6b7-1838-4a51-949a-8c86c169cada",
          name: "Annette Olzon",
          title: "Inurance is Annoying",
          startTime: "2021-04-10T02:36:53Z",
          endTime: "2021-04-10T02:51:53Z",
        },
        {
          uuid: "4",
          caregiver_uuid: "95683eac-353c-4cff-8fec-85004b8e8abf",
          name: "Ashley James",
          title: "Nutrition",
          startTime: "2021-04-11T11:36:53Z",
          endTime: "2021-04-11T11:51:53Z",
        },
        {
          uuid: "5",
          caregiver_uuid: "95683eac-353c-4cff-8fec-85004b8e8ab2",
          name: "Kristen Brown",
          title: "Memory Activities",
          startTime: "2021-04-11T08:30:53Z",
          endTime: "2021-04-11T09:00:53Z",
        },
      ],
    }
    axios.get = jest.fn().mockResolvedValue(schedulesList)
    const schedules = await DashboardAPIservice.GetScheduleCalls(
      "abcd-234-df-23hjdfhj"
    )
    expect(schedules).toMatchObject(schedulesList)
  })

  it("Schedules List api 500 error", async () => {
    axios.get.mockRejectedValueOnce("5xx error")
    const schedules = await DashboardAPIservice.GetScheduleCalls(
      "abcd-234-df-23hjdfhj"
    )
    expect(schedules).toMatchObject({})
  })

  it("Opentok session api invoked", async () => {
    const session = {
      sessionId: "test-session",
      token: "test-token",
    }
    axios.get = jest.fn().mockResolvedValue(session)
    const sessionResp = await DashboardAPIservice.GetOpenTokSession(
      "abcd-234-df-23hjdfhj"
    )
    expect(sessionResp).toMatchObject(session)
  })

  it("Opentok session 500 error", async () => {
    axios.get.mockRejectedValueOnce("5xx error")
    const sessionResp = await DashboardAPIservice.GetOpenTokSession(
      "abcd-234-df-23hjdfhj"
    )
    expect(sessionResp).toMatchObject({})
  })

  it("update carecoach video call data api invoked", async () => {
    axios.patch = jest.fn().mockResolvedValue({ status: 200 })
    const updateResp = await DashboardAPIservice.updateCarecoachVideoSchedule(
      "abcd-234-df-23hjdfhj",
      "abcd-234-df-23hjdfhj",
      { time: new Date() }
    )
    expect(updateResp).toMatchObject({ status: 200 })
  })

  it("update carecoach video call data 500 error", async () => {
    axios.patch.mockRejectedValueOnce("5xx error")
    const updateResp = await DashboardAPIservice.updateCarecoachVideoSchedule(
      "abcd-234-df-23hjdfhj",
      "abcd-234-df-23hjdfhj",
      { time: new Date() }
    )
    expect(updateResp).toMatchObject({})
  })
})
