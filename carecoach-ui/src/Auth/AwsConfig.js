import Amplify, { Auth, Analytics } from "aws-amplify"

// Uncomment below line to enable debug logging
// Amplify.Logger.LOG_LEVEL = "DEBUG"
/* eslint no-underscore-dangle:0 */
Amplify.configure({
  Auth: {
    region: `${window._env_.Aws_Region}`,
    userPoolId: `${window._env_.Cognito_Pool_Id}`,
    userPoolWebClientId: `${window._env_.Cognito_Carecoach_Client_Id}`,
    identityPoolId: `${window._env_.Cognito_Identity_Pool_Id}`,
    authenticationFlowType: "USER_PASSWORD_AUTH",
    storage: window.sessionStorage,
    oauth: {
      domain: `${window._env_.Cognito_Base_Url}`,
      scope: ["profile", "openid", "aws.cognito.signin.user.admin"],
      redirectSignIn: `${window._env_.Carecoach_Home}`,
      redirectSignOut: `${window._env_.Carecoach_Home}`,
      responseType: "code",
    },
  },
  Analytics: {
    // OPTIONAL - disable Analytics if true
    disabled: false,
    // OPTIONAL - Allow recording session events. Default is true.
    autoSessionRecord: true,

    AWSPinpoint: {
      // OPTIONAL -  Amazon Pinpoint App Client ID
      appId: window._env_.Pinpoint_Project_Id,
      // OPTIONAL -  Amazon service region
      region: window._env_.Aws_Region,
      // OPTIONAL - Default Endpoint Information

      endpoint: {
        attributes: {
          type: ["carecoach"],
        },
      },

      // Buffer settings used for reporting analytics events.
      // OPTIONAL - The buffer size for events in number of items.
      bufferSize: 1000,

      // OPTIONAL - The interval in milliseconds to perform a buffer check and flush if necessary.
      flushInterval: 5000, // 5s

      // OPTIONAL - The number of events to be deleted from the buffer when flushed.
      flushSize: 100,

      // OPTIONAL - The limit for failed recording retries.
      resendLimit: 5,
    },
  },
})

Analytics.autoTrack("pageView", {
  // REQUIRED, turn on/off the auto tracking
  enable: true,
  // OPTIONAL, the event name, by default is 'pageView'
  eventName: "pageView",
  // OPTIONAL, the attributes of the event, you can either pass an object or a function
  // which allows you to define dynamic attributes
  attributes: {},
  // when using function
  // attributes: () => {
  //    const attr = somewhere();
  //    return {
  //        myAttr: attr
  //    }
  // },
  // OPTIONAL, by default is 'multiPageApp'
  // you need to change it to 'SPA' if your app is a single-page app like React
  type: "SPA",
  // OPTIONAL, the service provider, by default is the Amazon Pinpoint
  provider: "AWSPinpoint",
  // OPTIONAL, to get the current page url
  getUrl: () => {
    // the default function
    return window.location.hash.substr(1) || "/"
  },
})

Analytics.autoTrack("event", {
  // REQUIRED, turn on/off the auto tracking
  enable: true,
  // OPTIONAL, events you want to track, by default is 'click'
  events: ["click"],
  // OPTIONAL, the prefix of the selectors, by default is 'data-amplify-analytics-'
  // in order to avoid collision with the user agent, according to https://www.w3schools.com/tags/att_global_data.asp
  // always put 'data' as the first prefix
  selectorPrefix: "data-amplify-analytics-",
  // OPTIONAL, the service provider, by default is the Amazon Pinpoint
  provider: "AWSPinpoint",
  // OPTIONAL, the default attributes of the event, you can either pass an object or a function
  // which allows you to define dynamic attributes
  attributes: {},
  // when using function
  // attributes: () => {
  //    const attr = somewhere();
  //    return {
  //        myAttr: attr
  //    }
  // }
})

export default Auth.configure()
