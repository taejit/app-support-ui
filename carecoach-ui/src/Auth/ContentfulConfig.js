// https://www.contentful.com/developers/docs/javascript/tutorials/using-js-cda-sdk/#retrieving-entries-with-search-parameters
// https://www.contentful.com/developers/docs/references/content-delivery-api/

import { createClient } from "contentful"

const client = createClient({
  space: `${window._env_.Contentful_Space}`,
  accessToken: `${window._env_.Contentful_AccessToken}`,
})

export default client
