import React, { useState } from "react"

const MESSAGING_SUFFIX = "messaging"

export const ChatContext = React.createContext({
  chatClient: undefined,
  setChatClient: () => {
    // This is intentional
  },
  messagingChannel: undefined,
  setMessagingChannel: () => {
    // This is intentional
  },
  notificationChannel: undefined,
  setNotificationChannel: () => {
    // This is intentional
  },
  tryAgain: undefined,
  setTryAgain: () => {
    // This is intentional
  },
  newNotifications: false,
  setNewNotifications: () => {
    // This is intentional
  },
  newMessages: false,
  setNewMessages: () => {
    // This is intentional
  },
  notificationMessages: { loadTime: 0, messages: [] },
  setNotificationMessages: () => {
    // This is intentional
  },
  isConnecting: true,
  setIsConnecting: () => {
    // This is intentional
  },
})

export const useChat = () => {
  const [chatClient, setChatClient] = useState()
  const [messagingChannel, setMessagingChannel] = useState()
  const [notificationChannel, setNotificationChannel] = useState()
  const [tryAgain, setTryAgain] = useState(0)
  const [newNotifications, setNewNotifications] = useState(false)
  const [newMessages, setNewMessages] = useState(false)
  const [notificationMessages, setNotificationMessages] = useState({
    loadTime: 0,
    messages: [],
  })
  const [isConnecting, setIsConnecting] = useState(true)

  const getChannelInfo = async (caregiverUuid) => {
    const careGiverChannelName = `${caregiverUuid}-${MESSAGING_SUFFIX}`
    const client = chatClient
    if (client !== undefined && client !== null) {
      try {
        const channel = client?.channel("messaging", careGiverChannelName)
        await channel?.watch()

        const lastMessageDate = channel?.state.last_message_at

        return {
          unreadMessages: channel?.state.unreadCount > 0,
          lastMessageDate,
        }
      } catch (error) {
        console.error(error)
      }
    }

    return { unreadMessages: undefined, lastMessage: undefined }
  }

  return {
    chatClient,
    setChatClient,
    messagingChannel,
    setMessagingChannel,
    notificationChannel,
    setNotificationChannel,
    tryAgain,
    setTryAgain,
    newNotifications,
    setNewNotifications,
    newMessages,
    setNewMessages,
    notificationMessages,
    setNotificationMessages,
    isConnecting,
    setIsConnecting,
    getChannelInfo,
  }
}
