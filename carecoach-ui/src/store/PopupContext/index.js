import React, { useState } from "react"

export const PopupContext = React.createContext({
  notificationPanelState: undefined,
  setNotificationPanelState: () => {
    // this is intentional
  },
  messagingPanel: undefined,
  setMessagingPanel: () => {
    // this is intentional
  },
  miniMessagingPanel: undefined,
  setMiniMessagingPanel: () => {
    // this is intentional
  },
  miniMessagingChannel: undefined,
  setMiniMessagingChannel: () => {
    // this is intentional
  },
})

export const usePopup = () => {
  const [notificationPanelState, setNotificationPanelState] = useState(false)
  const [messagingPanel, setMessagingPanel] = useState(false)
  const [miniMessagingPanel, setMiniMessagingPanel] = useState(false)
  const [miniMessagingChannel, setMiniMessagingChannel] = useState()

  return {
    notificationPanelState,
    setNotificationPanelState,
    messagingPanel,
    setMessagingPanel,
    miniMessagingPanel,
    setMiniMessagingPanel,
    miniMessagingChannel,
    setMiniMessagingChannel,
  }
}
