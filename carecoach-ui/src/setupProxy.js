// eslint-disable-next-line import/no-extraneous-dependencies
const createProxyMiddleware = require("http-proxy-middleware")

const apiProxy = process.env.API_PROXY || "https://dev.lanternapp.care"

module.exports = function (app) {
  app.use(
    "/api",
    createProxyMiddleware({
      target: apiProxy,
      changeOrigin: true,
      logLevel: "debug",
    })
  )
}
