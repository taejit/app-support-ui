import React from "react"
import Button from "@material-ui/core/Button"
import Typography from "@material-ui/core/Typography"
import { useTranslation } from "react-i18next"
import Logout from "../Logout"
import "./Forbidden.scss"

const Forbidden = () => {
  const { t } = useTranslation()
  return (
    <div className="forbidden">
      <Typography id="forbiddentitle" variant="h2" gutterBottom>
        {t("forbiddentitle")}
      </Typography>
      <Typography id="forbiddennopermission" variant="h6" gutterBottom>
        {t("forbiddennopermission")}
      </Typography>
      <Typography id="forbiddencontactadmin" variant="h6" gutterBottom>
        {t("forbiddencontactadmin")}
      </Typography>
      <Button
        id="forbiddengotologin"
        variant="contained"
        color="primary"
        className="go-to-login"
        onClick={Logout}
      >
        {t("forbiddengotologin")}
      </Button>
    </div>
  )
}

export default Forbidden
