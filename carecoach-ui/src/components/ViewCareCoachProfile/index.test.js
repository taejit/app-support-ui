import React from "react"
import { render, waitFor, fireEvent } from "@testing-library/react"
import { Auth } from "aws-amplify"
import * as CareCoachServices from "../../services/CarecaochServices"
import ViewCareCoachProfile from "."
import * as ProfileImageService from "../../services/avatar.service"

jest.mock("../CareCoachAvatar", () => {
  return {
    __esModule: true,
    default: () => {
      return <div />
    },
  }
})

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "/edit",
    success: true,
  }),
  useHistory: () => ({ push: () => {} }),
}))

describe("ViewCareCoachProfile test", () => {
  beforeEach(() => {
    Auth.currentUserInfo = () =>
      Promise.resolve({
        attributes: {
          email: "bramsai@intraedge.com",
          email_verified: true,
          phone_number: "+12254789321",
          phone_number_verified: true,
          sub: "38041359-614f-4c44-9a32-23c3c80506c7",
          family_name: "kiran",
          given_name: "ramsai",
          "custom:location": "Hyderabad Telangana Hyd",
          "custom:pronoun": "he/his",
        },
        username: "acc2ea0f-3a1f-49b9-8b19-7f555ece71d6",
      })
    ProfileImageService.GetCarecoachProfile = () =>
      Promise.resolve({
        profileImageUrl: "test-profileImageUrl",
        status: "uploadSuccess",
        isLoading: false,
      })
  })
  it("Verify component show all data", async () => {
    CareCoachServices.GetCarecoachProfile = () =>
      Promise.resolve({
        data: {
          uuid: "c1260a8b-28f3-4b94-88ff-9bdb3da94f37",
          firstName: "John",
          lastName: "Coach",
          email: "user@example.com",
          phone: "6021234567",
          isActive: true,
          jobTitle: "Nurse",
          jobStartDate: "2020-12-31",
          specialties: "String values",
          bio: "A long description for the bio of the care coach in rich text",
          profileImage: "https://signedurl-profile-example.com",
          createdAt: "2020-12-31T07:53:20.908Z",
          updatedAt: "2020-12-31T07:53:20.908Z",
        },
      })

    let wrapper
    await waitFor(() => {
      wrapper = render(<ViewCareCoachProfile />)
    })
    const { queryByText, container } = wrapper
    const editButton = container.querySelector("#editprofilebutton")
    await waitFor(() => {
      fireEvent.click(editButton)
    })
    expect(queryByText(/^myprofile$/)).toBeDefined()
    expect(container.querySelector("#name").innerHTML).toEqual("John Coach")
    expect(container.querySelector("#phone").innerHTML).toEqual(
      "(602) 123-4567"
    )
  })

  it("When no user info from auth", async () => {
    Auth.currentUserInfo = () => Promise.resolve(null)
    CareCoachServices.GetCarecoachProfile = () => Promise.resolve(null)
    let wrapper
    await waitFor(() => {
      wrapper = render(<ViewCareCoachProfile />)
    })
    const { queryByText, container } = wrapper
    expect(queryByText(/^myprofile$/)).toBeDefined()
    expect(container.querySelector("#phone").innerHTML).toEqual("")
  })

  it("When there are no data from service", async () => {
    CareCoachServices.GetCarecoachProfile = () => Promise.resolve({})
    let wrapper
    await waitFor(() => {
      wrapper = render(<ViewCareCoachProfile />)
    })
    const { queryByText, container } = wrapper
    expect(queryByText(/^myprofile$/)).toBeDefined()
    expect(container.querySelector("#phone").innerHTML).toEqual("")
  })
})
