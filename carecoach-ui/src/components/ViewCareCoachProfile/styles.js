import { makeStyles } from "@material-ui/core/styles"

const styles = makeStyles((theme) => ({
  myprofile: {
    paddingTop: "100px",
    paddingLeft: "16px",
    fontSize: "42px",
    fontWeight: "bold",
    letterSpacing: "-0.35px",
    color: theme.palette.darkBlue,
  },
  title: {
    fontSize: "14px",
    lineHeight: "1.5",
    fontWeight: "600",
    color: theme.palette.nightShade,
    padding: "16px",
  },
  editprofilebutton: {
    textTransform: "none",
    borderRadius: "25px",
    padding: "5px 30px 5px 30px",
    border: `solid 1.5px ${theme.palette.morningGlory}`,
    marginLeft: "15px",
    marginBottom: "10px",
  },
  avatardisplay: {
    display: "inline-block",
    "& .profile-img-div-2": {
      float: "right",
    },
    "&.MuiGrid-item": {
      paddingTop: "80px",
    },
  },
  changepasswordcontainer: {
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center",
  },
  changemypassword: {
    cursor: `pointer`,
    paddingTop: "80px",
    fontSize: "16px",
    fontWeight: "500",
    float: "right",
    color: theme.palette.darkBlue,
    margin: "10px",
  },
  keysicon: {
    cursor: `pointer`,
    height: "54px",
    width: "54px",
    padding: "15px",
    backgroundColor: theme.palette.cloud,
    borderRadius: "12px",
    position: "relative",
    top: "22%",
  },
}))

export default styles
