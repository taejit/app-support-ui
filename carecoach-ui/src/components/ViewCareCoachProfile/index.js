import React, { useEffect, useState } from "react"
import { useHistory, useLocation } from "react-router-dom"
import {
  Grid,
  Button,
  Typography,
  Table,
  TableBody,
  TableContainer,
} from "@material-ui/core"
import "./index.scss"
import { Auth } from "aws-amplify"
import { useTranslation } from "react-i18next"
import isEmpty from "lodash/isEmpty"
import { ReactComponent as KeysIcon } from "../../Assets/keys.svg"
import { GetCarecoachProfile } from "../../services/CarecaochServices"
import FormatPhoneNumber from "../../utils/FormatPhoneNumber"
import { getFormattedDate } from "../../utils/dateformatting"
import SnackbarMessage from "../SnackbarMessage"
import CareCoachAvatar from "../CareCoachAvatar"
import ChangePassword from "../ChangePassword"
import StyledTableRow from "../CommonStyles/StyledTableRow"
import StyledTableCell from "../CommonStyles/StyledTableCell"
import useStyles from "./styles"

const ViewCareCoachProfile = () => {
  const viewLocation = useLocation()
  const [showEditSuccess] = useState({
    open: !!(viewLocation && viewLocation.success),
    message: "Profile updated.",
  })

  const { t } = useTranslation()
  const history = useHistory()
  const [profile, setProfile] = useState({})
  const [openChangePassword, setOpenChangePassword] = useState(false)
  const classes = useStyles()

  const editProfile = () => {
    history.push({
      pathname: "/dashboard/edit",
      profileData: profile,
    })
  }

  useEffect(() => {
    async function getCareCoachProfile() {
      const currentUser = await Auth.currentUserInfo()
      if (currentUser) {
        const { data } = await GetCarecoachProfile(
          currentUser.attributes["custom:uuid"]
        )
        if (data) {
          setProfile(data)
        }
      }
    }
    getCareCoachProfile()
  }, [])

  const getPronounsDisplay = (pronounsdbvalue) => {
    if (!pronounsdbvalue) {
      return null
    }
    switch (pronounsdbvalue) {
      case t("pronounshehisdbvalue"):
        return t("pronounshehis")
      case t("pronounssheherdbvalue"):
        return t("pronounssheher")
      case t("pronounstheytheirdbvalue"):
        return t("pronounstheytheir")
      default:
        return profile.pronouns
    }
  }

  const openChangePasswordDrawer = () => {
    setOpenChangePassword(true)
  }

  const onChangePaswordDrawerClose = () => {
    setOpenChangePassword(false)
  }

  return (
    <>
      <SnackbarMessage {...showEditSuccess} />
      {openChangePassword && (
        <ChangePassword
          open={openChangePassword}
          onClose={onChangePaswordDrawerClose}
        />
      )}
      <Grid container className="viewprofile-container">
        <Grid item xs>
          <Grid container spacing={8}>
            <Grid container key="header" item xs={12} spacing={2}>
              <Grid item xs={5}>
                <Typography
                  className={classes.myprofile}
                  variant="h1"
                  id="myprofiletitle"
                  component="div"
                >
                  {t("myprofile")}
                  <Button
                    disabled={isEmpty(profile)}
                    variant="outlined"
                    className={classes.editprofilebutton}
                    id="editprofilebutton"
                    color="primary"
                    onClick={() => editProfile()}
                  >
                    {t("edit")}
                  </Button>
                </Typography>
              </Grid>
              <Grid item xs={2} className={classes.avatardisplay}>
                <CareCoachAvatar displayOrEdit />
              </Grid>
              <Grid
                item
                xs={5}
                className={classes.changepasswordcontainer}
                onClick={openChangePasswordDrawer}
              >
                <Typography
                  className={classes.changemypassword}
                  id="changemypassword"
                  component="div"
                >
                  {t("view-profile-change-my-password")}
                </Typography>
                <KeysIcon className={classes.keysicon} />
              </Grid>
            </Grid>
            <Grid key="aboutyou" item xs={7}>
              <Typography
                className={classes.title}
                variant="h6"
                id="aboutyoutitle"
                component="div"
              >
                {t("profileviewaboutyou")}
              </Typography>
              <TableContainer>
                <Table className={classes.table} aria-label="customized table">
                  <TableBody>
                    <StyledTableRow key="name">
                      <StyledTableCell component="th" scope="row">
                        {t("name")}
                      </StyledTableCell>
                      <StyledTableCell align="left" id="name">
                        {profile.firstName} {profile.lastName}
                      </StyledTableCell>
                    </StyledTableRow>
                    <StyledTableRow key="pronouns">
                      <StyledTableCell component="th" scope="row">
                        {t("pronouns")}
                      </StyledTableCell>
                      <StyledTableCell align="left" id="pronouns">
                        {getPronounsDisplay(profile.pronouns)}
                      </StyledTableCell>
                    </StyledTableRow>
                    <StyledTableRow key="email">
                      <StyledTableCell component="th" scope="row">
                        {t("email")}
                      </StyledTableCell>
                      <StyledTableCell align="left" id="email">
                        {profile.email}
                      </StyledTableCell>
                    </StyledTableRow>
                    <StyledTableRow key="mobilephone">
                      <StyledTableCell component="th" scope="row">
                        {t("mobilephone")}
                      </StyledTableCell>
                      <StyledTableCell align="left" id="phone">
                        {FormatPhoneNumber(profile.phone)}
                      </StyledTableCell>
                    </StyledTableRow>
                    <StyledTableRow key="location">
                      <StyledTableCell component="th" scope="row">
                        {t("location")}
                      </StyledTableCell>
                      <StyledTableCell align="left" id="location">
                        {profile.location}
                      </StyledTableCell>
                    </StyledTableRow>
                    <StyledTableRow key="aboutstatement">
                      <StyledTableCell component="th" scope="row">
                        {t("aboutstatement")}
                      </StyledTableCell>
                      <StyledTableCell align="left" id="aboutstatement">
                        {profile.about}
                      </StyledTableCell>
                    </StyledTableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </Grid>
            <Grid key="whatyoudo" item xs={5}>
              <Typography
                className={classes.title}
                variant="h6"
                id="whatyoudotitle"
                component="div"
              >
                {t("whatyoudo")}
              </Typography>
              <TableContainer>
                <Table className={classes.table} aria-label="customized table">
                  <TableBody>
                    <StyledTableRow key="credentials">
                      <StyledTableCell component="th" scope="row">
                        {t("care-coach-profile-credentials")}
                      </StyledTableCell>
                      <StyledTableCell align="left" id="specialities">
                        {profile.specialities}
                      </StyledTableCell>
                    </StyledTableRow>
                    <StyledTableRow key="startdate">
                      <StyledTableCell component="th" scope="row">
                        {t("startdate")}
                      </StyledTableCell>
                      <StyledTableCell align="left" id="jobstartdate">
                        {getFormattedDate(profile.jobStartDate)}
                      </StyledTableCell>
                    </StyledTableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  )
}

export default ViewCareCoachProfile
