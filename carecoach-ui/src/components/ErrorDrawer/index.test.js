import React from "react"
import { render, waitFor, fireEvent } from "@testing-library/react"
import ErrorDrawer from "./index"

test("renders Left Drawer page", async () => {
  let wrapper
  let testChildren = <div>Test Data</div>
  let onClose = jest.fn()
  await waitFor(() => {
    wrapper = render(
      <ErrorDrawer onClose={onClose}>{testChildren}</ErrorDrawer>
    )
  })
  const { container, queryByText } = wrapper
  expect(queryByText("Test Data")).toBeDefined()
  const crossmark_close = container.querySelector("#crossmark_close")
  expect(crossmark_close).toBeDefined()
  await waitFor(() => {
    fireEvent.click(crossmark_close)
  })
})
