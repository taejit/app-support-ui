import React, { useState, useEffect } from "react"
import PropTypes from "prop-types"
import { Drawer, Box } from "@material-ui/core"
import { ReactComponent as CrossMark } from "../../Assets/cross.svg"
import { ReactComponent as BackArrow } from "../../Assets/CarePlans/arrow-back.svg"
import useStyles from "./styles"

const ErrorDrawer = ({
  open,
  onClose,
  children,
  anchor,
  standalone,
  showBackArrow,
  hideCrossClose,
}) => {
  const classes = useStyles()

  const [openDrawer, setOpenDrawer] = useState(open)
  const toggleDrawer = (toggleOpen) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return
    }
    setOpenDrawer(toggleOpen)
    if (onClose) {
      onClose()
    }
  }

  useEffect(() => {
    setOpenDrawer(open)
  }, [open])

  return (
    <Drawer
      className={classes.root}
      anchor={anchor}
      open={openDrawer}
      onClose={toggleDrawer(false)}
      PaperProps={{
        style: {
          position: standalone ? "inherit" : "absolute",
          width: standalone ? 456 : 360,
        },
      }}
      BackdropProps={{
        style: { position: standalone ? "inherit" : "absolute" },
      }}
      variant={standalone ? "temporary" : "persistent"}
      rSlideProps={{
        direction: anchor ? (anchor == "right" ? "left" : "right") : "left",
      }}
    >
      {showBackArrow && (
        <BackArrow
          data-testid="backarrow"
          onClick={onClose}
          className={classes.backArrow}
        />
      )}

      {!hideCrossClose ? (
        <Box onClick={toggleDrawer(false)} id="crossmark_close">
          <CrossMark className={classes.crossmark} />
        </Box>
      ) : null}
      <Box className={classes.leftDrawerbody}>{children}</Box>
    </Drawer>
  )
}

ErrorDrawer.defaultProps = {}

ErrorDrawer.propTypes = {
  open: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
}

export default ErrorDrawer
