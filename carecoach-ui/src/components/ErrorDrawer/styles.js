import { makeStyles } from "@material-ui/core"

export default makeStyles((theme) => ({
  root: {
    "& .MuiDrawer-paper": {
      width: 360,
      padding: "40px 20px 40px 20px",
    },
    backdropFilter: "blur(6px)",
    zIndex: 1,
  },
  crossmark: {
    float: "right",
    cursor: "pointer",
  },
  leftDrawerbody: {
    // paddingTop: 30,
    textAlign: "center",
    height: "calc(100vh - 84px)",
    display: "flex",
    overflowY: "auto",
    alignItems: "center",
    flexDirection: "column",
    background: theme.palette.white,
  },
  backArrow: {
    cursor: "pointer",
  },
}))
