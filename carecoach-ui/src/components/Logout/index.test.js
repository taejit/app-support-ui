import { Auth } from "aws-amplify"
import Logout from "."

describe("Logout invoking amplify method", () => {
  it("Logout from application", () => {
    jest.spyOn(Auth, "signOut")
    Logout()
    expect(Auth.signOut).toHaveBeenCalled()
  })
})
