import { Auth } from "aws-amplify"

const Logout = () => {
  Auth.signOut()
}

export default Logout
