import { makeStyles } from "@material-ui/core"

export default makeStyles((theme) => ({
  hintText: {
    fontSize: `12px`,
    fontWeight: `500`,
    marginTop: "8px",
    color: theme.palette.twilight,
  },
}))
