import React, { useState, useEffect } from "react"
import { useTranslation } from "react-i18next"
import isEqual from "lodash/isEqual"
import { useFormik } from "formik"
import * as Yup from "yup"
import { TextField, Grid, Typography } from "@material-ui/core"
import FormStyles from "../CommonStyles/FormikFormStyles"
import DrawerTitle from "../CommonStyles/DrawerTitle"
import FormFooter from "../FormikFormFooter"
import useStyles from "./styles"

const AddEditTask = (props) => {
  const { t } = useTranslation()
  const drawerTitle = DrawerTitle()
  const formStyles = FormStyles()
  const [hasChanged, setHasChanged] = useState(false)
  const classes = useStyles()

  const { name, link, onDelete } = props
  const editMode = !!name

  const formik = useFormik({
    initialValues: {
      name: name || "",
      link: link || "",
    },
    validateOnChange: true,
    validationSchema: Yup.object({
      name: Yup.string()
        .nullable()
        .min(1)
        .max(60)
        .required(
          `${t("edit-task-name")} ${t("edit-milestone-validation-message")}`
        ),
      link: Yup.string()
        .nullable()
        .matches(
          /^(https|http):\/\/(www.)?[a-z0-9-]+(\.[a-z]{2,}){1,3}(#?\/?[a-zA-Z0-9#-]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/,
          `${t("edit-task-link-invalid-msg")}`
        ),
    }),
    onSubmit: async (values) => {
      props.onSave({ ...values, id: props.id })
    },
  })

  useEffect(() => {
    setHasChanged(!isEqual(formik.values, formik.initialValues))
  }, [formik.values, formik.initialValues])

  return (
    <form onSubmit={formik.handleSubmit} className={formStyles.form}>
      <Grid container>
        <Grid item xs={12}>
          <Typography
            id="edittasktitle"
            component="div"
            className={drawerTitle.secondaryTitle}
          >
            {editMode
              ? t("edit-task-drawer-title-edit-mode")
              : t("edit-task-drawer-title-add-scratch-mode")}
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Grid item xs={12} className={formStyles.grid}>
            <TextField
              fullWidth
              multiline
              rowsMax={2}
              className={formStyles.text}
              inputProps={{ maxLength: 60 }}
              id="taskname"
              label={t("edit-task-name")}
              type="text"
              {...formik.getFieldProps("name")}
            />
            {formik.touched.name && formik.errors.name && (
              <div className={formStyles.error}>{formik.errors.name}</div>
            )}
          </Grid>
          <Grid item xs={12} className={formStyles.grid}>
            <TextField
              fullWidth
              multiline
              rowsMax={3}
              className={formStyles.text}
              inputProps={{ maxLength: 90 }}
              id="tasklink"
              label={t("edit-task-link")}
              type="text"
              {...formik.getFieldProps("link")}
            />
            <Typography className={classes.hintText}>
              {t("edit-task-link-description")}
            </Typography>
            {formik.touched.link && formik.errors.link && (
              <div className={formStyles.error}>{formik.errors.link}</div>
            )}
          </Grid>
        </Grid>
      </Grid>

      <FormFooter
        {...{
          editMode,
          hasChanged,
          showGoBack: true,
          isFormValid: formik.isValid,
          onClickSuccess: () => formik.submitForm(),
          onClickRemove: () => onDelete(props),
          backArrowClick: props.backArrowClick,
        }}
      />
    </form>
  )
}

export default AddEditTask
