import React from "react"
import { render, waitFor } from "@testing-library/react"
import { Auth } from "aws-amplify"
import EditAvatar from "./index"
import OnboardingApiService from "../../services/Onboarding.service"

describe("EditAvatar component testing", () => {
  beforeEach(() => {
    jest.spyOn(Auth, "currentAuthenticatedUser").mockResolvedValue(
      Promise.resolve({
        attributes: {
          email: "bramsai@intraedge.com",
          email_verified: true,
          phone_number: "+12254789321",
          phone_number_verified: true,
          sub: "38041359-614f-4c44-9a32-23c3c80506c7",
          family_name: "kiran",
          given_name: "ramsai",
          "custom:location": "Hyderabad Telangana",
          "custom:pronoun": "he/his",
          "custom:uuid": "test-uuid",
          "custom:job_start_date": new Date("2021-01-30"),
          "custom:bio": "bio",
          "custom:credentials": "test-credentials",
        },
        username: "acc2ea0f-3a1f-49b9-8b19-7f555ece71d6",
      })
    )
    jest.spyOn(Auth, "updateUserAttributes").mockResolvedValue({})
    jest.spyOn(Auth, "currentUserInfo").mockResolvedValue(
      Promise.resolve({
        attributes: {
          email: "bramsai@intraedge.com",
          email_verified: true,
          phone_number: "+12254789321",
          phone_number_verified: true,
          sub: "38041359-614f-4c44-9a32-23c3c80506c7",
          family_name: "kiran",
          given_name: "ramsai",
          "custom:location": "Hyderabad Telangana",
          "custom:pronoun": "he/his",
          "custom:uuid": "test-uuid",
          "custom:job_start_date": new Date("2021-01-30"),
          "custom:bio": "bio",
          "custom:credentials": "test-credentials",
        },
        username: "acc2ea0f-3a1f-49b9-8b19-7f555ece71d6",
      })
    )
    jest
      .spyOn(OnboardingApiService, "getCareCoachProfileImage")
      .mockResolvedValue({
        data: {
          presignedUrl: "testImgUrl",
        },
      })
    jest
      .spyOn(OnboardingApiService, "checkProfileImageUrl")
      .mockResolvedValue({})

    jest.spyOn(OnboardingApiService, "syncCareCoach").mockResolvedValue({
      status: 202,
    })
  })

  it("Should display EditAvatar page", async () => {
    let wrapper
    await waitFor(async () => {
      wrapper = render(<EditAvatar open />)
    })
    const { queryByText } = wrapper
    expect(queryByText(/^editmyprofilepicture$/)).toBeDefined()
  })

  it("Should call onclose of EditAvatar page", async () => {
    let wrapper
    await waitFor(async () => {
      wrapper = render(<EditAvatar open />)
    })
    const { container } = wrapper
    const wrapperDrawer = container.querySelector("Drawer")
    expect(wrapperDrawer).toBeDefined()
  })

  it("get profile image url rejects with error", async () => {
    jest
      .spyOn(OnboardingApiService, "getCareCoachProfileImage")
      .mockRejectedValue({
        response: {
          status: 400,
        },
      })
    let wrapper
    await waitFor(() => {
      wrapper = render(<EditAvatar open />)
    })
    const { container } = wrapper
    const getErrorText = container.querySelector("#getError")
    expect(getErrorText).toBeDefined()
  })

  it("check profile image url sends 404 status", async () => {
    jest.spyOn(OnboardingApiService, "checkProfileImageUrl").mockRejectedValue({
      response: {
        status: 404,
      },
    })
    let wrapper
    await waitFor(() => {
      wrapper = render(<EditAvatar open />)
    })
    const { container } = wrapper
    const defaultText = container.querySelector("#defaultText")
    expect(defaultText).toBeDefined()
  })

  it("update profile image", async () => {
    jest
      .spyOn(OnboardingApiService, "getPresignedUrlForCareCoachProfileImage")
      .mockResolvedValue({
        data: {
          url: "testPresignedUrl",
        },
      })
    jest
      .spyOn(OnboardingApiService, "uploadImageThroughPresignedUrl")
      .mockResolvedValue({ status: 200 })
    let wrapper
    let Open = true
    await waitFor(() => {
      wrapper = render(<EditAvatar open={Open} />)
    })
    const { container } = wrapper
    const defaultText = container.querySelector("#ccUploadButton")
    expect(defaultText).toBeDefined()
    const uploadBtn = container.querySelector("#profile-image-button-file")
    expect(uploadBtn).toBeDefined()
  })

  it("update profile image: extension error", async () => {
    jest
      .spyOn(OnboardingApiService, "getPresignedUrlForCareCoachProfileImage")
      .mockResolvedValue({
        data: {
          url: "testPresignedUrl",
        },
      })
    jest
      .spyOn(OnboardingApiService, "uploadImageThroughPresignedUrl")
      .mockResolvedValue({})
    let wrapper
    await waitFor(() => {
      wrapper = render(<EditAvatar open={true} />)
    })
    const { container } = wrapper
    const defaultText = container.querySelector("#ccUploadButton")
    expect(defaultText).toBeDefined()
    const uploadBtn = container.querySelector("#profile-image-button-file")
    expect(uploadBtn).toBeDefined()
  })

  it("update profile image: size error", async () => {
    jest
      .spyOn(OnboardingApiService, "getPresignedUrlForCareCoachProfileImage")
      .mockResolvedValue({
        data: {
          url: "testPresignedUrl",
        },
      })
    jest
      .spyOn(OnboardingApiService, "uploadImageThroughPresignedUrl")
      .mockResolvedValue({})
    let wrapper
    await waitFor(() => {
      wrapper = render(<EditAvatar open={true} />)
    })
    const { container } = wrapper
    const defaultText = container.querySelector("#ccUploadButton")
    expect(defaultText).toBeDefined()
    const uploadBtn = container.querySelector("#profile-image-button-file")
    expect(uploadBtn).toBeDefined()
  })
})
