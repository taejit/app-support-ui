import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { useTranslation } from "react-i18next"
import { Grid, Typography, Button, Box } from "@material-ui/core"
import { ReactComponent as UserIcon } from "../../Assets/user.svg"
import CareCoachAvatar from "../CareCoachAvatar"
import RightDrawer from "../RightDrawer"
import useStyles from "./styles"

const EditAvatar = ({ open, onClose }) => {
  const classes = useStyles()
  const { t } = useTranslation()
  const [openDrawer, setOpenDrawer] = useState(open)

  useEffect(() => {
    setOpenDrawer(open)
  }, [open])

  return (
    <RightDrawer open={openDrawer} onClose={onClose}>
      <Grid container>
        <Grid item xs={11} className={classes.editprofilepicturecontainer}>
          <UserIcon className={classes.usericon} />
          <Typography component="div" className={classes.editmyprofiletext}>
            {t("editmyprofilepicture")}
          </Typography>
          <CareCoachAvatar displayOrEdit={false} />
        </Grid>
      </Grid>
      <Box className={classes.donebuttonposition}>
        <Button className={classes.donebutton} onClick={onClose}>
          {t("edit-profile-picture-done")}
        </Button>
      </Box>
    </RightDrawer>
  )
}

EditAvatar.defaultProps = {
  onClose: () => {},
}

EditAvatar.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func,
}

export default EditAvatar
