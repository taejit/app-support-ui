import { makeStyles } from "@material-ui/core/styles"

const styles = makeStyles((theme) => ({
  root: {
    "& .MuiDrawer-paper": {
      width: "456px",
      padding: "34px",
    },
    "& .MuiBackdrop-root": {
      backdropFilter: "blur(6px)",
    },
  },
  editmyprofiletext: {
    fontSize: "27px",
    fontWeight: "bold",
    color: theme.palette.darkBlue,
    height: "34px",
    letterSpacing: "normal",
    marginBottom: "36px",
  },
  usericon: {
    height: "48px",
    width: "48px",
    "& g": {
      stroke: theme.palette.darkBlue,
    },
  },
  editprofilepicturecontainer: {
    "& .upload-inprogress": {
      "& div:first-child": {
        paddingTop: "0%",
      },
    },
    "& .avatar-display": {
      "& div:first-child": {
        paddingTop: "0%",
      },
    },
  },
  donebuttonposition: {
    width: "80%",
    height: "fit-content",
    textAlign: "center",
    backgroundColor: theme.palette.lanternGold,
    borderRadius: "25px",
    position: "absolute",
    bottom: "15px",
  },
  donebutton: {
    "& :hover": {
      backgroundColor: theme.palette.lanternGold,
    },
    backgroundColor: theme.palette.lanternGold,
    borderRadius: "25px",
    width: "80%",
    height: "fit-content",
    fontSize: "16px",
    fontWeight: "500",
    color: theme.palette.darkBlue,
    textTransform: "none",
  },
}))

export default styles
