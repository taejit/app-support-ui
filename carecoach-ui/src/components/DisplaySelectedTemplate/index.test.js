import React from "react"
import renderer from "react-test-renderer"
import DisplaySelectedTemplate from "."

describe("DisplaySelectedTemplate component test", () => {
  it("lodaing without error", () => {
    const tree = renderer.create(<DisplaySelectedTemplate />).toJSON()
    expect(tree).toMatchSnapshot()
  })
  it("when one template was selected", () => {
    const template = [
      {
        name: `This is template name`,
      },
    ]
    const tree = renderer
      .create(<DisplaySelectedTemplate selectedTemplate={template} />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})
