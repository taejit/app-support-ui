import React from "react"
import { useTranslation } from "react-i18next"
import { Box, Typography, Button } from "@material-ui/core"
import { ReactComponent as ClearSlectedTemplateIcon } from "../../Assets/CarePlans/cross-icon.svg"
import useStyles from "./styles"

const DisplaySelectedTemplate = ({
  selectedTemplate,
  onButtonClick,
  noSelectionText,
}) => {
  const { t } = useTranslation()
  const classes = useStyles()

  return (
    <Box className={classes.selectedTemplateDiv}>
      {(selectedTemplate || []).length === 0 ? (
        <Typography className={classes.noTemplateText}>
          {noSelectionText}
        </Typography>
      ) : (
        <Box className={classes.selectioncontainer}>
          {selectedTemplate.map((selected) => {
            return (
              <Button
                key={selected.name}
                variant="outlined"
                className={classes.selectedTemplateBtn}
                onClick={() => onButtonClick(selected)}
              >
                {selected.name}
                <ClearSlectedTemplateIcon className={classes.crossbtn} />
              </Button>
            )
          })}
        </Box>
      )}
    </Box>
  )
}

export default DisplaySelectedTemplate
