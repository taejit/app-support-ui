import { makeStyles } from "@material-ui/core"

export default makeStyles((theme) => ({
  notecardcontainer: {
    "&:hover": {
      boxShadow: "0 4px 18px -4px rgba(24, 24, 43, 0.32)",
    },
    flex: "0 1 31.85%",
    borderRadius: "4px",
    margin: "8px",
    backgroundColor: theme.palette.white,
    cursor: "pointer",
  },
  titlecontainer: {
    display: "table",
    tableLayout: "fixed",
    width: "100%",
  },
  notetitle: {
    display: "table-cell",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
    overflow: "hidden",
    paddingTop: "16px",
    paddingLeft: "21px",
    paddingRight: "21px",
    fontSize: "16px",
    fontWeight: "500",
    lineHeight: "1.5",
    letterSpacing: "normal",
    color: theme.palette.darkBlue,
  },
  notebody: {
    height: "calc(100% - 107px)",
    margin: "6px 21px 18px 21px",
    fontSize: "14px",
    fontWeight: "500",
    lineHeight: "1.5",
    letterSpacing: "normal",
    color: theme.palette.twilight,
  },
  notefooter: {
    padding: "11px 21px 13px 21px",
    fontSize: "12px",
    fontWeight: "500",
    lineHeight: "1.5",
    letterSpacing: "normal",
    color: theme.palette.nightShade,
    borderTop: `2px solid ${theme.palette.cloud}`,
  },
}))
