import React from "react"
import PropTypes from "prop-types"
import { Box, Typography } from "@material-ui/core"
import { getNoteDate, getNoteTime } from "../../utils/dateformatting"
import useStyles from "./styles"

const NoteCard = (props) => {
  const { title, description, updatedAt } = props
  const classes = useStyles()

  return (
    <Box
      className={classes.notecardcontainer}
      onClick={() => props.onClick(props)}
    >
      <Box className={classes.titlecontainer}>
        <Typography
          className={classes.notetitle}
          variant="h6"
          id="notetitle"
          component="div"
        >
          {title}
        </Typography>
      </Box>
      <Typography
        className={classes.notebody}
        variant="h6"
        id="notebody"
        component="div"
      >
        {description &&
          (description.length > 100
            ? `${description.slice(0, 100)}...`
            : description)}
      </Typography>
      <Typography
        className={classes.notefooter}
        variant="h6"
        id="notecreatedat"
        component="div"
      >
        {updatedAt && `${getNoteDate(updatedAt)} at ${getNoteTime(updatedAt)}`}
      </Typography>
    </Box>
  )
}

NoteCard.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  createdAt: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
}
export default NoteCard
