import React from "react"
import renderer from "react-test-renderer"
import { render, waitFor, fireEvent } from "@testing-library/react"
import NoteCard from "."

jest.mock("../../utils/dateformatting", () => {
  return {
    ...jest.requireActual("../../utils/dateformatting"),
    getNoteDate: (date) => date,
    getNoteTime: (time) => time,
  }
})

describe("NoteCard component check", () => {
  it("Note display with more than 100 characters body", () => {
    const note = {
      title: "Sample title",
      description:
        "Should be more than 100 characters. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      updatedAt: "2020-12-31T07:53:20.908Z",
    }
    const tree = renderer
      .create(<NoteCard {...note} onClick={jest.fn()} />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })

  it("Note display with less than 100 characters body", () => {
    const note = {
      title: "Sample title",
      description: "Should be more than 100 characters.",
      updatedAt: "2020-12-30T07:53:20.908Z",
    }
    const tree = renderer
      .create(<NoteCard {...note} onClick={jest.fn()} />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })

  it("Should give the data on click", async () => {
    const note = {
      title: "Sample title",
      description: "Should be more than 100 characters.",
      updatedAt: "2020-12-30T07:53:20.908Z",
    }
    const cb = jest.fn()
    const { container } = render(<NoteCard {...note} onClick={cb} />)
    const divContainer = container.querySelector(
      ".makeStyles-notecardcontainer-1"
    )
    await waitFor(() => {
      fireEvent.click(divContainer)
    })
    expect(cb).toHaveBeenCalled()
  })
})
