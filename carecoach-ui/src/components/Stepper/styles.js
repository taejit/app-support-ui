import { makeStyles } from "@material-ui/core"

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  stepper: {
    paddingTop: `0`,
    paddingBottom: `0`,
    paddingLeft: `11px`,
  },
  stepperroot: {
    padding: 0,
    "& .Mui-disabled": {
      "& text": {
        fill: theme.palette.darkBlue,
      },
    },
  },
  stepcontainer: {
    padding: 0,
  },
  stepicon: {
    height: `36px`,
    width: `36px`,
    fill: theme.palette.white,
    stroke: theme.palette.nightShade,
    borderRadius: `50%`,
  },
  steptext: {
    fontSize: `10px`,
    fontWeight: "300",
    strokeWidth: `0.2`,
  },
  activestep: {
    fill: theme.palette.nightShade,
    stroke: theme.palette.white,
  },
  completed: {
    display: "inline-block",
    fill: theme.palette.valid,
    strokeWidth: `0`,
  },
  step: {
    width: "116px",
    margin: "10px 12px 0px 18px",
    fontFamily: "Montserrat",
    fontSize: "12px",
    fontWeight: 500,
    lineHeight: 1.5,
    color: theme.palette.nightShade,
  },
}))

export default useStyles
