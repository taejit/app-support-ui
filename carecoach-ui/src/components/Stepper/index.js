import React, { useEffect } from "react"
import { withStyles, StepLabel, StepConnector } from "@material-ui/core"
import Stepper from "@material-ui/core/Stepper"
import Step from "@material-ui/core/Step"
import useStyles from "./styles"

export default function HorizontalLinearStepper({
  currentStep,
  clickedStepper,
}) {
  const PlanStepConnector = withStyles((theme) => ({
    alternativeLabel: {
      top: 18,
      left: `calc(-50% + 17px)`,
      right: `calc(-50% + 17px)`,
    },
    line: {
      height: 2,
      width: `28%`,
      border: 0,
      backgroundColor: theme.palette.nightShade,
      borderRadius: 1,
    },
    stepperroot: {
      cursor: "pointer",
    },
  }))(StepConnector)

  const StepLabelStyled = withStyles(() => ({
    iconContainer: {
      padding: `0`,
      cursor: "pointer",
    },
  }))(StepLabel)

  const classes = useStyles()
  const [activeStep, setActiveStep] = React.useState(0)
  const steps = ["", "", ""]

  useEffect(() => {
    setActiveStep(currentStep - 1)
  }, [currentStep])

  return (
    <div className={classes.root}>
      <Stepper
        alternativeLabel
        activeStep={activeStep}
        connector={<PlanStepConnector />}
        className={classes.stepper}
      >
        {steps.map((label, index) => {
          return (
            <Step
              key={`label${index}`}
              id={`step_${index}`}
              className={classes.stepperroot}
              onClick={() => clickedStepper(index + 1)}
              // disabled={currentStep == index}
            >
              <StepLabelStyled
                StepIconProps={{
                  classes: {
                    root: classes.stepicon,
                    active: classes.activestep,
                    text: classes.steptext,
                    completed: classes.completed,
                  },
                }}
              >
                {label}
              </StepLabelStyled>
            </Step>
          )
        })}
        <div className={classes.step}>
          STEP {currentStep} of {steps.length}
        </div>
      </Stepper>
    </div>
  )
}
