import { version } from "../../../package.json"
import React from "react"
import { useTranslation } from "react-i18next"
import { useLocation, useHistory } from "react-router-dom"
import { Drawer, Typography } from "@material-ui/core"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import ListItemIcon from "@material-ui/core/ListItemIcon"
import ListItemText from "@material-ui/core/ListItemText"
import Box from "@material-ui/core/Box"
import { ReactComponent as HomeIcon } from "../../Assets/home.svg"
import { ReactComponent as UserIcon } from "../../Assets/users.svg"
import { ReactComponent as CircleIcon } from "../../Assets/circle.svg"
import { ReactComponent as LearningIcon } from "../../Assets/learning.svg"
import { ReactComponent as MarketplaceIcon } from "../../Assets/marketplace.svg"
import { ReactComponent as LanternLogoIcon } from "../../Assets/lantern-logo-dark.svg"
import useStyles from "./styles"

export default function PermanentDrawerLeft() {
  const classes = useStyles()
  const { t } = useTranslation()
  const location = useLocation()
  const { pathname } = location
  const history = useHistory()

  const getWebAppInfo = () => {
    const { ENV, BUILD_ID } = window._env_
    const split = ENV.split("-")
    const fEnv = split.length === 2 ? `${split[1]}` : ""
    const environment = `${ENV.charAt(0)}${fEnv}`.toUpperCase()
    return `${version} ${environment}-${BUILD_ID}`
  }

  const redirectToView = (path) => {
    history.push({
      pathname: `/dashboard/${path}`,
      success: true,
    })
  }

  const redirectToContentful = () => {
    window.open(`https://be.contentful.com/login`)
  }

  return (
    <div className={classes.root}>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
        anchor="left"
      >
        <div className={classes.toolbar}>
          <LanternLogoIcon className={classes.lanternlogo} />
        </div>
        <List>
          <ListItem
            button
            key={t("home")}
            onClick={() => redirectToView("home")}
          >
            <Box
              component="div"
              className={
                pathname.includes("/home")
                  ? classes.activebuttonbar
                  : classes.inactivebuttonbar
              }
            />
            <ListItemIcon
              className={
                pathname.includes("/home")
                  ? classes.buttoniconactive
                  : classes.buttonicon
              }
            >
              <HomeIcon />
            </ListItemIcon>
            <ListItemText primary={t("home")} />
          </ListItem>
          <ListItem
            button
            key={t("mycaregivers")}
            onClick={() => redirectToView("caregivers")}
          >
            <Box
              component="div"
              className={
                pathname.includes("/caregivers")
                  ? classes.activebuttonbar
                  : classes.inactivebuttonbar
              }
            />
            <ListItemIcon
              className={
                pathname.includes("/caregivers")
                  ? classes.buttoniconactive
                  : classes.buttonicon
              }
            >
              <UserIcon />
            </ListItemIcon>
            <ListItemText primary={t("mycaregivers")} />
          </ListItem>
          <ListItem button key={`learning`} onClick={redirectToContentful}>
            <ListItemIcon className={classes.buttonicon}>
              <LearningIcon />
            </ListItemIcon>
            <ListItemText primary={t("learning")} />
          </ListItem>
          <ListItem button key={`marketplace`} onClick={redirectToContentful}>
            <ListItemIcon className={classes.buttonicon}>
              <MarketplaceIcon />
            </ListItemIcon>
            <ListItemText primary={t("marketplace")} />
          </ListItem>
        </List>
        <Box className={classes.footer}>
          <Typography component="h1" className={classes.footertext}>
            {t("sidebarfootertext")}
          </Typography>
          <Typography component="span" className={classes.footertext}>
            {t("appversion")}{" "}
            <Typography component="span" className={classes.versionnumber}>
              {getWebAppInfo()}
            </Typography>
          </Typography>
        </Box>
      </Drawer>
    </div>
  )
}
