import { makeStyles } from "@material-ui/core/styles"

const drawerWidth = 276
const styles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
    paddingLeft: "15px",
    backgroundColor: theme.palette.darkBlue,
    color: theme.palette.white,
  },
  // necessary for content to be below app bar
  toolbar: {
    marginTop: "48px",
    marginBottom: "40px",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  lanternlogo: {
    width: "80%",
    height: "auto",
    padding: "0 15px 0 15px",
  },
  buttonicon: {
    padding: "15px",
    marginRight: "15px",
    borderRadius: "12px",
    backgroundColor: theme.palette.nightShade,
    minWidth: `54px`,
    "& g": {
      stroke: theme.palette.lanternGold,
    },
  },
  buttoniconactive: {
    minWidth: `54px`,
    padding: "15px",
    marginLeft: `-2px`,
    marginRight: "15px",
    borderRadius: "12px",
    backgroundColor: theme.palette.lanternGold,
    "& g": {
      stroke: theme.palette.darkBlue,
      borderRadius: "12px",
    },
  },
  activebuttonbar: {
    height: "100%",
    padding: "30px 2px",
    marginLeft: "-31px",
    marginRight: "30px",
    backgroundColor: theme.palette.lanternGold,
  },
  inactivebuttonbar: {
    height: "100%",
    padding: "30px 1px",
    marginLeft: "-31px",
    marginRight: "30px",
    backgroundColor: theme.palette.darkBlue,
  },
  footer: {
    marginTop: "auto",
    marginBottom: "36px",
    marginLeft: "24px",
  },
  footertext: {
    fontSize: "12px",
    fontWeight: "500",
    color: theme.palette.brightShade,
  },
  versionnumber: {
    fontSize: "12px",
    fontWeight: "bold",
  },
}))

export default styles
