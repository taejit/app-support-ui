import React from "react"
import { Box, Typography } from "@material-ui/core"
import { ReactComponent as NumberOfTask } from "../../Assets/CarePlans/circle-checkmark.svg"
import useStyles from "./styles"
import clsx from "clsx"

const MilestoneCard = (props) => {
  const { rationale, description, name, duedatestring, numberOfTask } = props
  const classes = useStyles()
  return (
    <Box
      id={props.id}
      className={clsx(
        classes.milestonecontainer,
        props.onClick ? "" : classes.restrictClick
      )}
      onClick={() => props.onClick && props.onClick(props)}
    >
      <Box className={classes.linedot}></Box>
      <Box className={classes.groupline}></Box>
      <Box>
        <Typography className={classes.rationale}>{rationale}</Typography>
      </Box>
      <Box className={classes.whiterow}>
        <Box className={classes.maincontent}>
          <Typography className={classes.name}>{name}</Typography>
          <Typography className={classes.description}>{description}</Typography>
        </Box>
        {
          <Box className={classes.task}>
            {duedatestring}
            <Typography className={classes.number}>{numberOfTask}</Typography>
            <NumberOfTask className={classes.numbericon} />
          </Box>
        }
      </Box>
    </Box>
  )
}

export default MilestoneCard
