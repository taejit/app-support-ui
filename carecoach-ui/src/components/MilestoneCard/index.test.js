import React from "react"
import renderer from "react-test-renderer"
import MilestoneCard from "."

describe("MilestoneCard component testing", () => {
  it("Render the component", () => {
    const tree = renderer.create(<MilestoneCard />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
