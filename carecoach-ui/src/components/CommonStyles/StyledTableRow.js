import { withStyles } from "@material-ui/core/styles"
import { TableRow } from "@material-ui/core"

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.cloud,
      fontSize: "12px",
      fontWeight: "600",
      color: theme.palette.nightShade,
    },
    "& th:nth-child(1)": {
      fontSize: "12px",
      fontWeight: "600",
      color: theme.palette.nightShade,
      borderTopLeftRadius: "4px",
      borderBottomLeftRadius: "4px",
    },
    "& td:nth-child(2)": {
      fontSize: "15px",
      fontWeight: "500",
      color: theme.palette.nightShade,
      borderTopRightRadius: "4px",
      borderBottomRightRadius: "4px",
    },
  },
}))(TableRow)

export default StyledTableRow
