import { makeStyles } from "@material-ui/core"

export default makeStyles((theme) => ({
  icon: {
    height: "48px",
    width: "48px",
    "& g": {
      stroke: theme.palette.darkBlue,
    },
  },
}))
