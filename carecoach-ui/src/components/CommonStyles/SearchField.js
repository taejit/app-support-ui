import { makeStyles } from "@material-ui/core"

export default makeStyles((theme) => ({
  icon: {
    height: "48px",
    width: "48px",
    "& g": {
      stroke: theme.palette.darkBlue,
    },
  },

  commonText: {
    marginTop: "36px",
    fontSize: "12px",
    fontWeight: 600,
    marginBottom: "12px",
    color: theme.palette.nightShade,
  },

  searchField: {
    fontSize: "16px",
    fontWeight: 500,
    "& .MuiInput-root": {
      paddingBottom: "8px",
    },
    "& .MuiInputBase-input": {
      color: theme.palette.darkBlue,
      fontWeight: 500,

      "&::placeholder": {
        color: theme.palette.twilight,
        opacity: 1,
      },
    },
    "& .MuiInput-underline:before": {
      borderBottom: `1.5px solid ${theme.palette.dove}`,
    },
    "& .MuiInput-underline:after": {
      borderBottom: `2px solid ${theme.palette.morningGlory}`,
    },
    "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
      borderColor: theme.palette.morningGlory,
    },
  },
  searchList: {
    marginTop: "36px",
    maxHeight: "295px",
    overflowY: "auto",
  },
  templateNotFound: {
    display: "Flex",
    marginTop: "37px",
    marginBottom: "37px",
    flexDirection: "column",
    alignItems: "center",
  },
  nothingFound: {
    paddingTop: "16px",
    fontSize: "18px",
    fontWeight: 500,
    color: theme.palette.darkBlue,
  },
  nothingFoundDescription: {
    textAlign: "center",
    paddingTop: "10px",
    fontSize: "14px",
    fontWeight: 500,
    color: theme.palette.nightShade,
  },
}))
