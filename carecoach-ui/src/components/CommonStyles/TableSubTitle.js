import { makeStyles } from "@material-ui/core/styles"

export default makeStyles((theme) => ({
  subtitle: {
    fontSize: "14px",
    fontWeight: "600",
    lineHeight: "1.5",
    letterSpacing: "normal",
    color: theme.palette.nightShade,
  },
}))
