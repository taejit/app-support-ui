import { makeStyles } from "@material-ui/core"

export default makeStyles((theme) => ({
  actionbtn: {
    "&:hover": {
      backgroundColor: theme.palette.lanternGold,
    },
    "&:disable": {
      opacity: "0.5",
    },
    fontSize: "16px",
    borderRadius: "25px",
    padding: "16px 42px",
    backgroundColor: theme.palette.lanternGold,
    color: theme.palette.darkBlue,
  },
}))
