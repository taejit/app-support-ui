import { makeStyles } from "@material-ui/core/styles"

export default makeStyles((theme) => ({
  pagetitle: {
    fontSize: "36px",
    fontWeight: "bold",
    lineHeight: "1.17",
    letterSpacing: "normal",
    color: theme.palette.darkBlue,
    margin: "72px",
  },
}))
