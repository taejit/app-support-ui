import { withStyles } from "@material-ui/core/styles"
import { TableCell } from "@material-ui/core"

const StyledTableCell = withStyles((theme) => ({
  root: {
    border: "none",
  },
  head: {
    backgroundColor: theme.palette.cloud,
    color: theme.palette.nightShade,
  },
}))(TableCell)

export default StyledTableCell
