import { makeStyles } from "@material-ui/core"

export default makeStyles((theme) => ({
  actionbtn: {
    marginTop: "24px",
    marginBottom: "24px",
    borderRadius: "25px",
    padding: "11px 30px",
    border: `solid 1.5px ${theme.palette.morningGlory}`,
    color: theme.palette.morningGlory,
  },
  dangeractionbtn: {
    marginTop: "24px",
    borderRadius: "25px",
    padding: "11px 30px",
    border: `solid 1.5px ${theme.palette.rose}`,
    color: theme.palette.rose,
  },
}))
