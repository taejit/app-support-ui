import { makeStyles } from "@material-ui/core"

export default makeStyles((theme) => ({
  form: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  grid: {
    paddingTop: "16px",
  },
  error: {
    color: "red",
  },
  message: {
    paddingLeft: "0",
    paddingTop: "16px",
    paddingBottom: "16px",
  },
  footer: {
    width: "100%",
    marginTop: "auto",
  },
  text: {
    "& textarea": {
      fontSize: `14px`,
      fontWeight: `500`,
      color: theme.palette.darkBlue,
      lineHeight: `1.29`,
      letterSpacing: `normal`,
    },
  },
  datepicker: {
    width: `100%`,
    "& input": {
      fontSize: `14px`,
      fontWeight: `500`,
      color: theme.palette.darkBlue,
      lineHeight: `1.29`,
      letterSpacing: `normal`,
    },
  },
  buttonplacement: {
    marginBottom: "24px",
    marginTop: "24px",
    display: "flex",
    justifyContent: "space-between",
    width: "100%",
  },
  cancelbtn: {
    color: theme.palette.morningGlory,
    fontSize: "15px",
    fontWeight: "600",
    padding: "0",
  },
  successbtn: {
    "&:hover": {
      backgroundColor: theme.palette.lanternGold,
    },
    fontSize: "16px",
    fontWeight: "500",
    color: theme.palette.darkBlue,
    borderRadius: "25px",
    backgroundColor: theme.palette.lanternGold,
    padding: "10px 40px 10px 40px",
    "&:disabled": {
      opacity: "0.5",
    },
  },
  border: {
    width: "150%",
    height: "1px",
    borderTop: `solid 1.5px ${theme.palette.cloud}`,
    marginLeft: "-48px",
    marginRight: "-48px",
  },
  removebtngrid: {
    width: "100%",
    paddingBottom: "36px",
  },
  removebtn: {
    width: "100%",
  },
  nochangemessage: {
    fontSize: "14px",
    fontWeight: "500",
    lineHeight: "1.5",
    color: theme.palette.twilight,
  },
  nochangemagegrid: {
    width: "100%",
    textAlign: "center",
  },
  endcallbtn: {
    "&:hover": {
      backgroundColor: theme.palette.rose,
    },
    fontSize: "14px",
    fontWeight: "500",
    color: theme.palette.white,
    borderRadius: "25px",
    backgroundColor: theme.palette.rose,
    padding: "6px 24px",
  },
  nevermindbtn: {
    color: theme.palette.skyBlue,
    fontSize: "12px",
    fontWeight: "600",
    marginRight: "10px",
  },
}))
