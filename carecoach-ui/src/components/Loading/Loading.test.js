import React from "react"
import renderer from "react-test-renderer"
import Loading from "."

describe("Loading component test", () => {
  it("CircularProgress component loading", () => {
    const tree = renderer.create(<Loading />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
