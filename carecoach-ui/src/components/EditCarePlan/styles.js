import { makeStyles } from "@material-ui/core"

export default makeStyles(() => ({
  addoptionsoverride: {
    height: `100%`,
    display: `flex`,
    flexDirection: `column`,
    "& #title": {
      marginTop: `12px`,
    },
    "& #subscript": {
      marginBottom: `60px`,
    },
  },
  editcareplanfooter: {
    display: `flex`,
    flexDirection: `column`,
    marginTop: `auto`,
    "& .footer": {
      marginTop: `none`,
    },
  },
  topbox: {
    marginTop: `10px`,
  },
}))
