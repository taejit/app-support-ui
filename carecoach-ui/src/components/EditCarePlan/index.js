import React, { useState } from "react"
import PropTypes from "prop-types"
import { useTranslation } from "react-i18next"
import { Auth } from "aws-amplify"
import { useParams } from "react-router-dom"
import { SaveMilestonesInCarePlan } from "../../services/CarePlan.service"
import RightDrawer from "../RightDrawer"
import AddMilestoneOptions from "./AddMilestoneOptions"
import SelectExistingMilestone from "./SelectExistingMilestone"
import AddEditMilestoneForm from "../AddEditMilestoneForm"
import { ValidMilestoneAddEditSteps as ValidSteps } from "../../utils/constant"

const EditCarePlan = (props) => {
  const [snackbarMessage, setSnackbarMessage] = useState({
    open: false,
    message: "",
    onClose: () =>
      setSnackbarMessage({
        ...snackbarMessage,
        open: false,
        children: null,
      }),
  })
  const { caregiversUuid } = useParams()
  const { t } = useTranslation()
  const [stepsForAddFromScratch, setStepsForAddFromScratch] = useState(null)
  const [hideCrossClose, setHideCrossClose] = useState(false)
  const [showBackArrow, setshowBackArrow] = useState(false)
  const [currentStep, setCurrentStep] = useState(`1`)

  const setUIForStep = (step, hideCrossClose, showBackArrow) => {
    setCurrentStep(step)
    setHideCrossClose(hideCrossClose)
    setshowBackArrow(showBackArrow)
  }

  const addFromScratch = () => {
    setUIForStep(`2b`, true, true)
    setStepsForAddFromScratch(ValidSteps.milestone)
  }

  const addMilestoneFromExisting = () => {
    setUIForStep(`2a`, true, true)
  }

  const goToOptionsPage = () => {
    if (currentStep === `2b`) {
      if (stepsForAddFromScratch === ValidSteps.milestone) {
        setUIForStep(`1`, false, false)
      } else if (stepsForAddFromScratch === ValidSteps.task) {
        setStepsForAddFromScratch(ValidSteps.milestone)
        setUIForStep(currentStep, true, true)
      }
    } else setUIForStep(`1`, false, false)
  }

  const saveMilestonesInCarePlan = async (milestonesToAdd) => {
    const currentUser = await Auth.currentUserInfo()
    const newPlan = [...props.existingCarePlan.items, ...milestonesToAdd].map(
      (milestone) => {
        delete milestone.id
        if (milestone.duedate) milestone.dueDate = milestone.duedate
        milestone.tasks.forEach((task) => {
          delete task.id
        })
        return milestone
      }
    )
    const carePlanSave = await SaveMilestonesInCarePlan(
      currentUser.attributes["custom:uuid"],
      caregiversUuid,
      newPlan
    )
    props.onClose(carePlanSave?.status)
  }

  const setUIForScratchFlow = (step) => {
    setStepsForAddFromScratch(step)
  }
  return (
    <>
      {
        <RightDrawer
          open={props.open}
          onClose={props.onClose}
          hideCrossClose={hideCrossClose}
          showBackArrow={showBackArrow}
          backArrowClick={() => goToOptionsPage()}
        >
          {currentStep === `1` && (
            <AddMilestoneOptions
              onClose={props.onClose}
              addFromExisting={addMilestoneFromExisting}
              addFromScratch={addFromScratch}
            />
          )}

          {currentStep === `2a` && (
            <SelectExistingMilestone
              onGoBack={goToOptionsPage}
              saveMilestones={saveMilestonesInCarePlan}
            />
          )}
          {currentStep === `2b` && (
            <AddEditMilestoneForm
              showGoBack={true}
              currentStep={stepsForAddFromScratch}
              goToStep={setUIForScratchFlow}
              backArrowClick={goToOptionsPage}
              hideTitleIcon={true}
              addModeTitle={t("add-from-scratch")}
              onSave={(milestoneObj) =>
                saveMilestonesInCarePlan([milestoneObj])
              }
            />
          )}
        </RightDrawer>
      }
    </>
  )
}

EditCarePlan.defaultProps = {
  onClose: () => {
    // intentional
  },
  existingCarePlan: { items: [] },
}

EditCarePlan.propTypes = {
  onClose: PropTypes.func,
  existingCarePlan: PropTypes.object,
}
export default EditCarePlan
