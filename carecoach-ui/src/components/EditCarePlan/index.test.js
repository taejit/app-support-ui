import React from "react"
import { fireEvent, render, screen, waitFor } from "@testing-library/react"
import EditCarePlan from "."
import * as CarePlanServices from "../../services/CarePlan.service"

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useParams: () => ({ caregiversUuid: "abc-123-hdfhj-1234" }),
}))

jest.mock("aws-amplify", () => {
  return {
    __esModule: true,
    Auth: {
      currentUserInfo: () =>
        Promise.resolve({
          attributes: {
            "custom:uuid": "Hyderabad Telangana Hyd",
          },
        }),
    },
  }
})

describe("EditCarePlan component", () => {
  it("Loading options page", () => {
    render(<EditCarePlan {...{ open: true }} />)
    expect(screen.getByText(/^add-milestones$/)).toBeDefined()
  })
  it("Load search milestone page and go back to options page", async () => {
    render(<EditCarePlan {...{ open: true }} />)
    const searchExistingMilestone = screen.getByText(
      /^search-existing-milestones$/
    )
    expect(searchExistingMilestone).toBeDefined()
    await waitFor(() => {
      fireEvent.click(searchExistingMilestone)
    })
    expect(screen.getByText(/^search-milestones-withoutdots$/)).toBeDefined()
    expect(screen.getByText(/^no-milestones-selected$/)).toBeDefined()
    const backArrow = screen.getByTestId("backarrow")
    await waitFor(() => {
      fireEvent.click(backArrow)
    })
    expect(screen.getByText(/^add-milestones$/)).toBeDefined()
  })

  it("Select milestones and save", async () => {
    CarePlanServices.GetAllCarePlanItem = jest.fn().mockResolvedValue({
      items: [
        {
          fields: {
            name: "milestone name",
            rationale: {
              content: [
                {
                  content: [
                    {
                      value: "template description",
                    },
                  ],
                },
              ],
            },
            description: { content: [] },
            isCommon: true,
            tasks: [
              {
                fields: {
                  name: "task name",
                  link: "some link",
                  isCompleted: false,
                },
                sys: "someit",
              },
            ],
          },
          sys: {
            id: "someid",
          },
        },
      ],
    })
    render(
      <EditCarePlan {...{ open: true, existingCarePlan: { items: [] } }} />
    )
    const searchExistingMilestone = screen.getByText(
      /^search-existing-milestones$/
    )
    await waitFor(() => {
      fireEvent.click(searchExistingMilestone)
    })
    const addmilestoneBtn = screen
      .getByText(/^add-milestones$/)
      .closest("button")
    expect(addmilestoneBtn).toBeDisabled()

    const milestonerow = screen.getByText(/^milestone name$/)
    await waitFor(() => {
      fireEvent.click(milestonerow)
    })

    expect(addmilestoneBtn).not.toBeDisabled()
    jest
      .spyOn(CarePlanServices, "SaveMilestonesInCarePlan")
      .mockResolvedValue({ status: 201 })
    await waitFor(() => {
      fireEvent.click(addmilestoneBtn)
    })
    expect(CarePlanServices.SaveMilestonesInCarePlan).toHaveBeenCalled()
  })
})
