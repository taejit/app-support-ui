import React from "react"
import { useTranslation } from "react-i18next"
import { Box } from "@material-ui/core"
import AddOptions from "../AddOptions"
import DrawerIconStyle from "../CommonStyles/DrawerIcon"
import { ReactComponent as CarePlanIcon } from "../../Assets/care-plan.svg"
import useStyles from "./styles"

const AddMilestoneOptions = (props) => {
  const { t } = useTranslation()
  const iconStyle = DrawerIconStyle()
  const classes = useStyles()

  return (
    <Box className={classes.addoptionsoverride}>
      <CarePlanIcon className={iconStyle.icon} />
      <AddOptions
        {...{
          title: t("add-milestones"),
          subscript: t("add-new-task-subscript"),
          searchOption1Text: t("search-existing-milestones"),
          searchOption2Text: t("add-from-scratch"),
          goBackText: t("cancel"),
          backArrowClick: props.onClose,
          addFromExisting: props.addFromExisting,
          addNewTaskFromScratch: props.addFromScratch,
        }}
      />
    </Box>
  )
}

export default AddMilestoneOptions
