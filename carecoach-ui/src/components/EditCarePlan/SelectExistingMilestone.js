import React, { useEffect, useState } from "react"
import { useTranslation } from "react-i18next"
import { Box, Typography } from "@material-ui/core"
import FormFooter from "../FormikFormFooter"
import TemplatesSearch from "../CaregiversCarePlan/TemplatesSearch"
import { GetAllCarePlanItem } from "../../services/CarePlan.service"
import DrawerTitle from "../CommonStyles/DrawerTitle"
import getMilestoneObj from "../../utils/getMilestoneObj"
import CarePlanTemplate from "../../Contexts/CarePlanTemplates"
import DisplaySelectedMilestones from "../DisplaySelectedTemplate"
import useStyles from "./styles"

const SearchExistingMilestone = (props) => {
  const currentStep = 2
  const { t } = useTranslation()
  const classes = useStyles()
  const drawerStyle = DrawerTitle()
  const [selectedMilestone, setSelectedMilestone] = useState([])
  const [milestones, setMilestones] = useState([])

  const setMilestoneSelection = (item, isMultiselect) => {
    let data = milestones
    data.forEach((template, index) => {
      if (template.id === item.id) {
        data[index].isSelected = !data[index].isSelected
      } else if (!isMultiselect) {
        data[index].isSelected = false
      }
    })
    setMilestones([...data])
    setSelectedMilestone(data.filter((milestone) => milestone.isSelected))
  }

  const saveMilestones = () => {
    props.saveMilestones(selectedMilestone)
  }

  useEffect(() => {
    async function getMilestones() {
      // get the whole entry of milestone from contentful
      const allMilestones = await GetAllCarePlanItem()
      setMilestones([...getMilestoneObj(allMilestones.items)])
    }
    getMilestones()
  }, [])

  return (
    <CarePlanTemplate.Provider value={{ templates: milestones }}>
      <Box className={classes.topbox}>
        <Typography className={drawerStyle.title}>
          {t("search-milestones-withoutdots")}
        </Typography>
        <TemplatesSearch
          currentStep={currentStep}
          nothingFoundDescription={t("milestone-not-found-description")}
          searchPlaceholder={t("search-milestones-withoutdots")}
          onTemplateSelection={(args) => setMilestoneSelection(args, true)}
        />
      </Box>
      <Box className={classes.editcareplanfooter}>
        <DisplaySelectedMilestones
          noSelectionText={t("no-milestones-selected")}
          selectedTemplate={selectedMilestone}
          onButtonClick={(args) => setMilestoneSelection(args, true)}
        />
        <FormFooter
          editMode={false}
          isFormValid={true}
          hasChanged={!!selectedMilestone.length}
          showGoBack={true}
          backArrowClick={props.onGoBack}
          onClickSuccess={saveMilestones}
          addModeSuccessLabel={t("add-milestones")}
        />
      </Box>
    </CarePlanTemplate.Provider>
  )
}

export default SearchExistingMilestone
