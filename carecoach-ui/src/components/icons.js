import { ReactComponent as CrossMark } from "../Assets/cross.svg"
import { ReactComponent as MaximizeMark } from "../Assets/maximize.svg"
import { ReactComponent as ArrowBackMark } from "../Assets/arrow-back.svg"

import { makeStyles } from "@material-ui/core/styles"

const styles = makeStyles((theme) => ({
  icon: {
    width: 24,
    height: 24,
    cursor: "pointer",
    "& g": {
      stroke: theme.palette.darkBlue,
    },
  },
  iconAlt: {
    width: 24,
    height: 24,
    cursor: "pointer",
    "& g": {
      stroke: "#fbca71",
    },
  },
}))

export const CloseIcon = ({ alt }) => {
  const classes = styles()
  const tmpClass = alt ? classes.iconAlt : classes.icon
  return <CrossMark className={tmpClass} />
}

export const MaximizeIcon = ({ alt }) => {
  const classes = styles()
  const tmpClass = alt ? classes.iconAlt : classes.icon
  return <MaximizeMark className={tmpClass} />
}

export const ArrowBackIcon = ({ alt }) => {
  const classes = styles()
  const tmpClass = alt ? classes.iconAlt : classes.icon
  return <ArrowBackMark className={tmpClass} />
}
