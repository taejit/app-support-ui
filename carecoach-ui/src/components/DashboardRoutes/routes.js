import React from "react"

const Routes = [
  {
    path: "/dashboard/caregivers/:caregiverUuid/careplan/create",
    name: "Create Care Plan",
    component: React.lazy(() => import("../CreateCarePlan")),
  },
  {
    path: "/dashboard/view",
    name: "View care coach profile",
    component: React.lazy(() => import("../ViewCareCoachProfile")),
    exact: true,
  },
  {
    path: "/dashboard/edit",
    name: "Edit care coach profile",
    component: React.lazy(() => import("../EditCareCoachProfile")),
    exact: true,
  },
  {
    path: "/dashboard/home",
    name: "Care Coach home",
    component: React.lazy(() => import("../Home")),
    exact: true,
  },
  {
    path: "/dashboard/caregivers/:caregiverUuid",
    name: "Caregivers Landing",
    component: React.lazy(() => import("../CaregiversLanding")),
  },
  {
    path: "/dashboard/caregivers",
    name: "My Caregivers",
    component: React.lazy(() => import("../CaregiversList")),
  },
  {
    path: "/dashboard/messaging",
    name: "Messaging",
    component: React.lazy(() => import("../Messaging")),
  },
]

export default Routes
