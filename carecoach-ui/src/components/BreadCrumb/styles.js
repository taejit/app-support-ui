import { makeStyles } from "@material-ui/core/styles"

export default makeStyles((theme) => ({
  root: {
    display: "column",
    width: "100%",
    height: "100%",

    "& .MuiBreadcrumbs-separator": {
      marginLeft: "4px",
      marginRight: "4px",
    },
  },
  title: {
    fontSize: "18px",
    fontWeight: "500",
    color: theme.palette.darkBlue,
  },
  navTitle: {
    fontSize: "12px",
    fontWeight: "500",
    color: theme.palette.twilight,
    cursor: "pointer",
  },
  navCurrentTitle: {
    fontSize: "12px",
    fontWeight: "500",
    color: theme.palette.darkBlue,
  },
}))
