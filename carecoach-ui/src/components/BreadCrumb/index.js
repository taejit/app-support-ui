import React from "react"
import { useHistory } from "react-router-dom"
import { Breadcrumbs, Box, Typography } from "@material-ui/core"
import { ReactComponent as NavigateNextIcon } from "../../Assets/breadcrumb-arrow.svg"
import useStyles from "./styles"

const Breadcrumb = ({ breadCrumbData }) => {
  const classes = useStyles()
  const history = useHistory()

  const handleBreadCrumbClick = (data) => {
    history.push({
      pathname: data.link,
    })
  }

  return (
    <div className={classes.root}>
      <Typography className={classes.title}>{breadCrumbData.title}</Typography>
      <Breadcrumbs separator={<NavigateNextIcon />} aria-label="breadcrumb">
        {breadCrumbData.navigationLinks?.map((breadCrumb, index) => {
          return (
            <Box
              className={
                index !== breadCrumbData.navigationLinks.length - 1
                  ? classes.navTitle
                  : classes.navCurrentTitle
              }
              key={index}
              onClick={() => handleBreadCrumbClick(breadCrumb)}
            >
              <span className="capitalize">{breadCrumb.title}</span>
            </Box>
          )
        })}
      </Breadcrumbs>
    </div>
  )
}

export default Breadcrumb
