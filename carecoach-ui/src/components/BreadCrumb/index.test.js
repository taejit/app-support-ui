import React from "react"
import { render, waitFor } from "@testing-library/react"
import Breadcrumb from "./index"
import CaregiversProfileContext from "../../Contexts/CaregiversProfileContext"

describe("Breadcrumb component testing", () => {
  it("Should display Breadcrumb page", async () => {
    const caregiverProfile = {
      uuid: "c1260a8b-28f3-4b94-88ff-9bdb3da94f37",
      firstName: "John",
      lastName: "Coach",
      nickName: "Charlie",
      email: "example@lantern.care",
      phone: "+16021234567",
      employerName: "ABC Inc",
      pronouns: "he/him",
      location: "Gilbert, Arizona",
      role: "Primary Caregiver",
      timeSpentInRole: "Less than 6 months",
      recipient: {
        uuid: "c1260a8b-28f3-4b94-88ff-9bdb3da94f37",
        firstName: "John",
        lastName: "Coach",
        nickName: "Charlie",
        pronouns: "He/him",
        dateOfBirth: "2020-12-31",
        relationship: "he/him",
        location: "Gilbert, Arizona",
        livingArrangement: "Lives indepedently",
        livingArrangementDescription: "Lives among the wolves",
        conditions: ["Aging", "Diabetes", "Dementia", "Other1", "Other2"],
      },
    }
    const breadCrumbData = {
      title: "Care Plan",
      navigationLinks: [
        {
          link: "/dashboard/caregivers",
          title: "My Caregivers",
        },
        {
          link: null,
          title: null,
          showCaregiverName: true,
        },
        {
          link: null,
          title: "Care Plan",
        },
      ],
    }
    let wrapper
    await waitFor(async () => {
      wrapper = render(
        <CaregiversProfileContext.Provider value={{ caregiverProfile }}>
          <Breadcrumb breadCrumbData={breadCrumbData} />
        </CaregiversProfileContext.Provider>
      )
    })
    const { queryByText } = wrapper
    expect(queryByText(/^capitalize$/)).toBeDefined()
  })
})
