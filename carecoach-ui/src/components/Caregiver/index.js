/* eslint-disable */
import { Card, CardContent, Box } from "@material-ui/core"
import React from "react"
import PropTypes from "prop-types"
import moment from "moment"
import "./index.scss"
import { getFormatedName, getFormatedFullName } from "../../utils/names.util"
import { getBurdenScore } from "../../utils/getBurdenScore.util"
import LinearProgress from '@material-ui/core/LinearProgress';

const Caregiver = ({ caregiverObj, variant }) => {
  const getLastContactedValue = (caregiver, shallReturnClass) => {
    const lastContactedDate = moment(caregiverObj.lastContactedDate)
    const upcomingCallAt = moment(caregiverObj.upcomingCallAt)
    const lastMessageDate = moment(caregiverObj.lastMessageDate)
    const currentDate = moment().set({
      hour: 0,
      minute: 0,
      second: 0,
      millisecond: 0,
    })
    if (caregiver.upcomingCallAt) {
      if (upcomingCallAt.diff(currentDate, "days") === 0) {
        if (shallReturnClass) return "lastContactedOld"
        return `Call today at ${upcomingCallAt.format("h:mm a")}`
      }
      if (shallReturnClass) return "notContactedAtAll"
      return `Upcoming call ${upcomingCallAt.format("MMM D")}`
    }
    if (caregiver.lastContactedDate || caregiver.lastMessageDate) {
      if (shallReturnClass) return "lastContacted"
      if (!caregiver.lastContactedDate)
        return `Last contacted ${lastMessageDate.format("MMM D")}`
      if (!caregiver.lastMessageDate)
        return `Last contacted ${lastContactedDate.format("MMM D")}`
      const lastContactDate = lastMessageDate
        ? moment.max([lastContactedDate, lastMessageDate])
        : lastContactedDate
      return `Last contacted ${lastContactDate.format("MMM D")}`
    }
    if (shallReturnClass) return "notContactedAtAll"
    return "Not contacted yet!"
  }

  let appliedVariant = variant ? variant : false
  const { percentage, tag, colorCss, displayScore } = getBurdenScore(caregiverObj.burdenScore)
  return (
    <>
      {caregiverObj && (
        <Card className={`${appliedVariant ? "borderRadiusCard" : "borderRadiusBottomZero"}  customCaregiverCard`}>
          <CardContent className="customCaregiverCardContent">
            <Box
              height="176px"
              display="flex"
              alignItems="center"
              justifyContent="center"
              className={appliedVariant ? "caregiverNewData" : "caregiverData"}
            >
              <Box
                display="flex"
                flexDirection="column"
                alignItems="center"
                justifyContent="center"
                className={appliedVariant ? "caregiverInrBox" : ""}
              >
                <Box
                  width="72px"
                  height="72px"
                  borderRadius="50%"
                  className={"caregiverAvatar"}
                  display="flex"
                  alignItems="center"
                  justifyContent="center"
                  fontSize={16}
                  fontWeight="500"
                >
                  <Box mt="3px" position="absolute">
                    {caregiverObj.nickName[0] &&
                      caregiverObj.nickName[0].toUpperCase()}
                    {caregiverObj.lastName[0] &&
                      caregiverObj.lastName[0].toUpperCase()}
                  </Box>
                  {caregiverObj.hasUnReadMessages && (
                    <Box
                      width="36px"
                      height="36px"
                      className="messageIcon"
                      display="flex"
                      alignItems="center"
                      justifyContent="center"
                      position="relative"
                      right="-28px"
                      top="-30px"
                    >
                      <img
                        src="./images/caregivers/message-icon.svg"
                        alt="message-icon"
                        width="16"
                        height="16"
                      />
                    </Box>
                  )}
                </Box>
                <Box
                  fontSize={18}
                  fontWeight="600"
                  pt="12px"
                  className="primaryText capitalize formatedName"
                >
                  {caregiverObj.nickName
                    ? getFormatedName(
                      caregiverObj.nickName,
                      caregiverObj.lastName
                    )
                    : getFormatedName(
                      caregiverObj.firstName,
                      caregiverObj.lastName
                    )}
                </Box>
                <Box
                  fontSize={14}
                  fontWeight="600"
                  pt="2px"
                  className={getLastContactedValue(caregiverObj, true)}
                >
                  {getLastContactedValue(caregiverObj, false)}
                </Box>
                {appliedVariant &&
                  <Box width="100%">
                    <Box
                      width="100%"
                      display="flex"
                      alignItems="center"
                      justifyContent="center"
                      pt="8px"
                    >
                      <img
                        src="./images/caregivers/icon-heart.svg"
                        alt="recipient-name"
                        width="16"
                        height="16"
                      />
                      <Box pl="8px" className="capitalize" className="fontSize12">
                        {caregiverObj.recipientNickName
                          ? getFormatedFullName(caregiverObj.recipientNickName)
                          : getFormatedFullName(caregiverObj.recipientFirstName)}{" "}
                        ({caregiverObj.relationshipToRecipient})
                      </Box>
                    </Box>
                    <Box mt="24px">
                      {/* <BorderLinearProgress variant="determinate" value={50} /> */}
                      <LinearProgress
                        variant="determinate"
                        value={percentage}
                        classes={{
                          root: "linearprogress",
                          barColorPrimary: colorCss
                        }} />
                      <Box flex mt="8px" className="fontSize12 progressInfo">
                        <div style={{ float: "left" }}>{tag}</div>
                        <div style={{ float: "right" }}>{displayScore}</div>
                      </Box>
                    </Box>
                  </Box>}
              </Box>
            </Box>
          </CardContent>
        </Card>
      )}
    </>
  )
}

Caregiver.propTypes = {
  caregiverObj: PropTypes.object.isRequired,
}

export default Caregiver
