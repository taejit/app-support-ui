import React, { useContext, useEffect, useState } from "react"
import PropTypes from "prop-types"
import { useTranslation } from "react-i18next"
import isEqual from "lodash/isEqual"
import { useFormik } from "formik"
import * as Yup from "yup"
import { Auth } from "aws-amplify"
import { TextField, Grid, Typography } from "@material-ui/core"
import {
  SaveCaregiversNote,
  EditCaregiversNote,
} from "../../services/Caregivers.service"
import CaregiversContext from "../../Contexts/CaregiversProfileContext"
import DrawerIconStyle from "../CommonStyles/DrawerIcon"
import DrawerTitle from "../CommonStyles/DrawerTitle"
import OtlinedButton from "../CommonStyles/OutlineButton"
import FormStyles from "../CommonStyles/FormikFormStyles"
import RightDrawer from "../RightDrawer"
import FormFooter from "../FormikFormFooter"
import { ReactComponent as StickyNoteIcon } from "../../Assets/sticky-note.svg"

const AddEditNote = ({
  open,
  onClose,
  title,
  description,
  nextStep,
  uuid: noteUuid,
  isActive,
}) => {
  const editMode = !!noteUuid
  const [hasChanged, setHasChanged] = useState(false)
  const [openDrawer] = useState(open)
  const [showError, setShowError] = useState(false)
  const { t } = useTranslation()
  const iconStyle = DrawerIconStyle()
  const drawerTitle = DrawerTitle()
  const formStyles = FormStyles()

  const { caregiverProfile } = useContext(CaregiversContext)
  const formik = useFormik({
    initialValues: {
      title: title || "",
      description: description || "",
      nextStep: nextStep || "",
      isActive,
    },
    validateOnChange: true,
    validateOnMount: true,
    validationSchema: Yup.object({
      title: Yup.string()
        .min(1)
        .max(50)
        .required(t("caregivers-add-note-title-is-requied")),
      description: Yup.string()
        .min(1)
        .required(t("caregivers-add-note-body-is-requied")),
      nextStep: Yup.string(),
    }),
    onSubmit: async (values) => {
      const currentUser = await Auth.currentUserInfo()
      if (currentUser) {
        if (editMode) {
          const noteEdited = await EditCaregiversNote(
            currentUser.attributes["custom:uuid"],
            caregiverProfile.uuid,
            noteUuid,
            values
          )
          if (noteEdited?.status === 200) {
            onClose(
              `"${values.title}" ${t("caregivers-add-note-updated-success")}.`
            )
            setShowError(false)
          } else {
            // show error message
            setShowError(true)
          }
        } else {
          const noteSave = await SaveCaregiversNote(
            currentUser.attributes["custom:uuid"],
            caregiverProfile.uuid,
            values
          )
          if (noteSave?.status === 201) {
            setShowError(false)
            onClose(
              `"${values.title}" ${t("caregivers-add-note-added-success")}.`
            )
          } else {
            // show error message
            setShowError(true)
          }
        }
      }
    },
  })

  // delete note will be soft delete, because we need to undo
  const removeNote = async () => {
    const currentUser = await Auth.currentUserInfo()
    const removeResp = await EditCaregiversNote(
      currentUser.attributes["custom:uuid"],
      caregiverProfile.uuid,
      noteUuid,
      {
        isActive: false,
      }
    )
    if (removeResp?.status === 200) {
      onClose(`"${title}" ${t("caregivers-add-note-removed-success")}.`, true)
    } else {
      setShowError(true)
    }
  }

  useEffect(() => {
    setHasChanged(!isEqual(formik.values, formik.initialValues))
  }, [formik.values, formik.initialValues])

  return (
    <RightDrawer open={openDrawer} onClose={onClose}>
      <form onSubmit={formik.handleSubmit} className={formStyles.form}>
        <Grid container>
          <Grid item xs={12}>
            <StickyNoteIcon className={iconStyle.icon} />
            <Typography
              id="rightdrawertitle"
              component="div"
              className={drawerTitle.title}
            >
              {editMode
                ? t("caregivers-notes-edit-note")
                : t("caregivers-add-note")}
            </Typography>
          </Grid>
          <Grid item xs={12} className={formStyles.grid}>
            <TextField
              fullWidth
              multiline
              rowsMax={2}
              inputProps={{ maxLength: 50 }}
              className={formStyles.text}
              id="noteTitle"
              label={t("caregivers-add-note-title-label")}
              type="text"
              {...formik.getFieldProps("title")}
            />
            {formik.touched.title && formik.errors.title && (
              <div className={formStyles.error}>{formik.errors.title}</div>
            )}
          </Grid>
          <Grid item xs={12} className={formStyles.grid}>
            <TextField
              fullWidth
              multiline
              rowsMax={10}
              className={formStyles.text}
              id="noteBody"
              label={t("caregivers-add-note-body-label")}
              type="text"
              {...formik.getFieldProps("description")}
            />
            {formik.touched.description && formik.errors.description && (
              <div className={formStyles.error}>
                {formik.errors.description}
              </div>
            )}
          </Grid>
          <Grid item xs={12} className={formStyles.grid}>
            <TextField
              fullWidth
              multiline
              rowsMax={10}
              className={formStyles.text}
              id="noteNextSteps"
              label={t("caregivers-add-note-nextstep-label")}
              type="text"
              {...formik.getFieldProps("nextStep")}
            />
          </Grid>
        </Grid>

        <FormFooter
          {...{
            errorMessage: showError && t("change-password-error-message"),
            editMode,
            hasChanged,
            onClose: onClose,
            isFormValid: formik.isValid,
            onClickRemove: removeNote,
            onClickSuccess: () => formik.submitForm(),
          }}
        />
      </form>
    </RightDrawer>
  )
}

AddEditNote.defaultProps = {
  title: "",
  description: "",
  nextStep: "",
  uuid: "",
  isActive: true,
}
AddEditNote.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  description: PropTypes.string,
  nextStep: PropTypes.string,
  uuid: PropTypes.string,
  isActive: PropTypes.bool,
}

export default AddEditNote
