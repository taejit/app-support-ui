import React from "react"
import { render, waitFor, fireEvent, screen } from "@testing-library/react"
import * as CaregiversService from "../../services/Caregivers.service"
import AddEditNote from "."

jest.mock("aws-amplify", () => {
  return {
    __esModule: true,
    Auth: {
      currentUserInfo: () =>
        Promise.resolve({
          attributes: {
            email: "bramsai@intraedge.com",
            email_verified: true,
            phone_number: "+12254789321",
            phone_number_verified: true,
            sub: "38041359-614f-4c44-9a32-23c3c80506c7",
            family_name: "kiran",
            given_name: "ramsai",
            "custom:location": "Hyderabad Telangana Hyd",
            "custom:pronoun": "he/his",
          },
          username: "acc2ea0f-3a1f-49b9-8b19-7f555ece71d6",
        }),
    },
  }
})

describe("AddEditNotes", () => {
  it("Load component", () => {
    const wrapper = render(<AddEditNote open onClose={jest.fn()} />)
    expect(wrapper).toBeDefined()
  })

  it("Validation of title form element", async () => {
    await waitFor(() => {
      render(<AddEditNote open onClose={jest.fn()} />)
    })
    const title = screen.getByLabelText(/^caregivers-add-note-title-label$/)
    await waitFor(() => {
      fireEvent.change(title, {
        target: {
          value: "",
          id: "noteTitle",
        },
      })
      fireEvent.blur(title, {
        target: {
          value: "",
          id: "noteTitle",
        },
      })
    })
    expect(
      screen.getByText(/^caregivers-add-note-title-is-requied$/)
    ).toBeDefined()
  })

  it("Validation of body form element", async () => {
    await waitFor(() => {
      render(<AddEditNote open onClose={jest.fn()} />)
    })
    const body = screen.getByLabelText(/^caregivers-add-note-body-label$/)
    await waitFor(() => {
      fireEvent.change(body, {
        target: {
          value: "",
          id: "noteBody",
        },
      })
      fireEvent.blur(body, {
        target: {
          value: "",
          id: "noteBody",
        },
      })
    })
    expect(
      screen.getByText(/^caregivers-add-note-body-is-requied$/)
    ).toBeDefined()
  })

  it("Submit the form without next step data", async () => {
    jest
      .spyOn(CaregiversService, "SaveCaregiversNote")
      .mockResolvedValue({ status: 201 })
    await waitFor(() => {
      render(<AddEditNote open onClose={jest.fn()} />)
    })
    const title = screen.getByLabelText(/^caregivers-add-note-title-label$/)
    const body = screen.getByLabelText(/^caregivers-add-note-body-label$/)
    const addBtn = screen.getByRole("button", {
      name: /^general-form-add-text$/,
    })
    await waitFor(() => {
      fireEvent.change(title, {
        target: {
          value: "Some title",
          id: "noteTitle",
        },
      })
      fireEvent.change(body, {
        target: {
          value: "Body sample",
          id: "noteBody",
        },
      })
    })
    await waitFor(() => {
      fireEvent.submit(addBtn)
    })
    expect(CaregiversService.SaveCaregiversNote).toHaveBeenCalled()
  })

  it("Load the component in edit mode and delete a note", async () => {
    jest
      .spyOn(CaregiversService, "EditCaregiversNote")
      .mockResolvedValue({ status: 200 })
    const note = {
      title: "Sample title",
      description: "Should be more than 100 characters.",
      nextStep: "next step",
      uuid: "acd-12jskd-hj3",
      createdAt: "2020-12-30T07:53:20.908Z",
      isActive: true,
      open: true,
      onClose: jest.fn(),
    }
    render(<AddEditNote {...note} />)
    const title = screen.getByLabelText(/^caregivers-add-note-title-label$/)
    const body = screen.getByLabelText(/^caregivers-add-note-body-label$/)
    expect(title.value).toEqual(note.title)
    expect(body.value).toEqual(note.description)

    const removeNoteBtn = screen.getByRole("button", {
      name: /^general-form-remove-text$/,
    })
    await waitFor(() => {
      fireEvent.click(removeNoteBtn)
    })
    expect(CaregiversService.EditCaregiversNote).toHaveBeenCalled()
  })

  it("Edit note validation", async () => {
    jest
      .spyOn(CaregiversService, "EditCaregiversNote")
      .mockResolvedValue({ status: 200 })
    const note = {
      title: "Sample title",
      description: "Should be more than 100 characters.",
      nextStep: "next step",
      uuid: "acd-12jskd-hj3",
      createdAt: "2020-12-30T07:53:20.908Z",
      isActive: true,
      open: true,
      onClose: jest.fn(),
    }
    render(<AddEditNote {...note} />)
    const title = screen.getByLabelText(/^caregivers-add-note-title-label$/)
    await waitFor(() => {
      fireEvent.change(title, {
        target: {
          value: "Some title",
          id: "noteTitle",
        },
      })
    })
    const saveChangesBtn = screen.getByRole("button", {
      name: /^general-form-save-changes-text$/,
    })
    await waitFor(() => {
      fireEvent.submit(saveChangesBtn)
    })
    expect(CaregiversService.EditCaregiversNote).toHaveBeenCalled()
  })
})
