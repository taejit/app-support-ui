import React, { useState, useEffect, useContext } from "react"
import {
  Chat,
  Channel,
  MessageInput,
  ChannelList,
  MessageList,
} from "stream-chat-react"

import { Auth } from "aws-amplify"
import { ChatContext } from "store/ChatContext"
import { GoodThings } from "./GoodThings"
import { SomethingWrong } from "./SomethingWrong"
import { MessagingSidebar } from "./MessagingSidebar"

import "stream-chat-react/dist/css/index.css"

import "./index.scss"

const theme = "messaging light"

const Messaging = () => {
  const [user, setUser] = useState()
  const [allQuiet, setAllQuiet] = useState(true)
  const [isLoading, setIsLoading] = useState(true)

  const { chatClient, isConnecting } = useContext(ChatContext)

  useEffect(() => {
    const loadAuth = async () => {
      const authInfo = await Auth.currentUserInfo()
      const { attributes: { "custom:uuid": carecoachUuid = "" } = {} } =
        authInfo || {}

      setUser(carecoachUuid)
    }

    const checkChannels = async () => {
      if (chatClient && user) {
        const filter = {
          type: "messaging",
          members: { $in: [user] },
          id: { $nin: [user, `${user}-messaging`, `${user}-notification`] },
        }

        setIsLoading(true)

        const channels = await chatClient
          .queryChannels(filter)
          .catch((err) => console.error("Filter Channels is failing", err))

        setIsLoading(false)

        if (channels && channels.length > 0) {
          setAllQuiet(false)
        }
      }
    }

    loadAuth().then(() => checkChannels())
  }, [chatClient, user])

  if (isConnecting || !user || isLoading)
    return <div id="loading">Loading...</div>

  if (!chatClient) return <SomethingWrong theme="dark" />
  if (allQuiet) return <GoodThings theme="dark" />

  return (
    <Chat client={chatClient} theme={theme}>
      <div id="Messaging" className="wrapper">
        <div className="left">
          <ChannelList
            filters={{
              members: { $in: [user] },
              id: { $nin: [user, `${user}-messaging`, `${user}-notification`] },
            }}
            options={{ state: true, watch: true, presence: true }}
          />
          <Channel>
            <MessageList messageActions={["react"]} />
            <MessageInput />
          </Channel>
        </div>
        <div className="right">
          <MessagingSidebar />
        </div>
      </div>
    </Chat>
  )
}

export default Messaging
