import React from "react"
import { useTranslation } from "react-i18next"
import { AlertScreen } from "./alert-screen"

export const NothingFound = () => {
  const { t } = useTranslation()

  return (
    <AlertScreen
      id="nothing-found-main"
      title={t("NothingFound-labels-mainTitle")}
      description={t("NothingFound-labels-secondaryTitle")}
      image={<img src="assets/images/nothingFound.png" alt="Nothing Found" />}
    />
  )
}
