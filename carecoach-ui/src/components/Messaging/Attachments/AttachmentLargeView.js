import React, { useState } from "react"
import { Attachments } from "./Attachments"
import { Typography } from "@material-ui/core"
import { ArrowBackIcon } from "components/icons"

export const AttachmentLargeView = ({ onFullScreen }) => {
  const [count, setCount] = useState(0)
  const handleCount = (count) => {
    setCount(count)
  }
  return (
    <div>
      <div
        className="attachments-title-full"
        onClick={() => onFullScreen()}
        role="button"
        onKeyPress={() => onFullScreen()}
        tabIndex={0}
        style={{ cursor: "pointer" }}
      >
        <span
          className={
            "attachments-title-back " + `${count ? "" : "disableArrow"}`
          }
        >
          <ArrowBackIcon />
        </span>
        <Typography className="messaging-sidebar-title">Attachments</Typography>
      </div>
      <div>
        <Attachments unlimit={true} handleCount={handleCount} />
      </div>
    </div>
  )
}
