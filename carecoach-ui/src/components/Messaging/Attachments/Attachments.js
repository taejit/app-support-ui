import React, { useState, useContext, useEffect } from "react"
import { ChatContext as MainChatContext } from "stream-chat-react"
import mime from "mime-types"
import Url from "url-parse"
import AttachmentList from "./AttachmentList"

const processMessages = (messages) => {
  return messages
    .map((msg) => msg.message)
    .filter(
      (msg) =>
        msg?.attachments.length > 0 &&
        !["giphy"].includes(msg?.attachments[0].type)
    )
    .map((msg) => {
      if (msg?.attachments.length > 0) {
        const url = new Url(msg?.attachments[0].image_url)
        const mimeType = mime.lookup(url.pathname)
        let extension = mime.extension(mimeType) || ""

        const pieces = url.pathname.split("/")
        const name = msg?.attachments[0].title || pieces[pieces.length - 1]

        if (!mimeType) {
          if (msg?.attachments[0].type && msg?.attachments[0].type == "image") {
            extension = "jpeg"
          } else {
            const extpieces = name.split(".")
            extension = extpieces[extpieces.length - 1].slice(-4)
          }
        }

        return {
          user: msg?.user?.name,
          mimeType,
          extension,
          link: msg?.attachments[0].image_url || msg?.attachments[0].asset_url,
          name,
        }
      }
    })
}

const loadMessages = async (ch, limit, offset) => {
  return new Promise((resolve, reject) => {
    if (ch) {
      const messageFilters = { attachments: { $exists: true } }

      ch.search(messageFilters, {
        limit: limit,
        offset: offset,
      })
        .then((data) => {
          resolve(processMessages(data.results))
        })
        .catch((error) => {
          reject(error)
        })
    }
  })
}

export const Attachments = ({ limit = 0, unlimit = false, handleCount }) => {
  const { channel } = useContext(MainChatContext)
  const [attachments, setAttachments] = useState([])

  useEffect(() => {
    loadMessages(channel, limit, 0).then((list) => {
      setAttachments(list)
      handleCount(list.length)
    })
  }, [channel])

  if (!channel || channel == undefined)
    return <div id="loading">Loading...</div>

  return (
    <div id="attachmentlist" className="attachmentlist">
      <AttachmentList data={attachments} />
    </div>
  )
}
