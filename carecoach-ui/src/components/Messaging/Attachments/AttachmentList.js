import React from "react"
import { AttachmentListItem } from "./AttachmentListItem"

const AttachmentList = (props) => {
  const { data } = props
  return data.map((item, index) => {
    return <AttachmentListItem item={item} key={index} />
  })
}

export default AttachmentList
