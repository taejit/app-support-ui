import React from "react"
import ListItem from "@material-ui/core/ListItem"
import ListItemText from "@material-ui/core/ListItemText"
import ListItemAvatar from "@material-ui/core/ListItemAvatar"
import { FileIcon } from "react-file-utils"

const maxString = (str, limit) => {
  return str.length >= limit ? `...${str.slice((limit - 3) * -1)}` : str
}

export const AttachmentListItem = ({
  item: { name, extension, user, link, mime_type },
}) => {
  const ListItemLink = (props) => {
    return (
      <ListItem
        button
        component="a"
        {...props}
        style={{ paddingLeft: 0 }}
        target="_blank"
      />
    )
  }

  //extension.toUpperCase()
  //<img src={link} alt={name} className="thumbnails" />

  const imageStyle =
    extension === "jpeg"
      ? {
          backgroundImage: `url("${link}")`,
        }
      : {}

  return (
    <ListItemLink href={link}>
      <ListItemAvatar className="attachmentsIcon" style={imageStyle}>
        {extension === "jpeg" ? null : (
          <FileIcon big={true} filename={name} mimeType={mime_type} size={30} />
        )}
      </ListItemAvatar>
      <ListItemText
        primary={maxString(name, 27) || "Attachment"}
        secondary={`Added by ${user}`}
      />
    </ListItemLink>
  )
}
