import React, { useState } from "react"
import { Attachments } from "./Attachments"
import ChevronRightIcon from "@material-ui/icons/ChevronRight"
import { Typography } from "@material-ui/core"

export const AttachmentSmallView = ({ onFullScreen }) => {
  const [count, setCount] = useState(0)
  const handleCount = (count) => {
    setCount(count)
  }
  return (
    <div>
      <div
        className="attachments-title"
        onClick={count == 0 ? null : () => onFullScreen()}
        role="button"
        onKeyPress={count == 0 ? null : () => onFullScreen()}
        tabIndex={0}
        style={{ cursor: "pointer" }}
      >
        <Typography className="messaging-sidebar-title">Attachments</Typography>
        <span className={"rightArrow " + `${count ? "" : "disableArrow"}`}>
          <ChevronRightIcon />
        </span>
      </div>
      <div>
        <Attachments limit={3} handleCount={handleCount} />
      </div>
    </div>
  )
}
