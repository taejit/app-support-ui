import React from "react"
import { useTranslation } from "react-i18next"
import { AlertScreen } from "./alert-screen"

export const AllQuiet = () => {
  const { t } = useTranslation()

  return (
    <AlertScreen
      id="AllQuiet-main"
      title={t("AllQuiet-labels-mainTitle")}
      description={t("AllQuiet-labels-secondaryTitle")}
      image={<img src="assets/images/thumbs-up.png" alt="calendar" />}
    />
  )
}
