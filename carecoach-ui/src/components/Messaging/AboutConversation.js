import React, { useContext } from "react"
import { ListItemText } from "@material-ui/core"
import { ChatContext as MainChatContext } from "stream-chat-react"
import moment from "moment"
import "./index.scss"

export const AboutConversation = () => {
  const { channel } = useContext(MainChatContext)

  const { data: { created_at: date } = {} } = channel || {}

  const formatted = moment(date).format("LL")

  return (
    <div className="sidebar-about">
      <ListItemText
        primary={"About this conversation"}
        secondary={`Began ${formatted}`}
      />
    </div>
  )
}
