import React from "react"
import "./index.scss"

export const AlertScreen = ({
  title = "",
  description = "",
  image,
  children,
  theme,
}) => {
  return (
    <div
      id="AlertScreen-main"
      className={`alertscreen_container ${theme && "dark"}`}
    >
      <div className="alertscreen_containerTop">
        <div className="alertScreen_img">{image}</div>
        <h1 className="alertscreen_headingText">{title}</h1>
        <h2 className="alertscreen_body">{description}</h2>
      </div>
      <div className="alertscreen_containerBotton">{children}</div>
    </div>
  )
}
