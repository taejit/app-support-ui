import React, { useContext } from "react"
import { Button } from "@material-ui/core"
import { useTranslation } from "react-i18next"
import { ChatContext } from "store/ChatContext"
import { AlertScreen } from "./alert-screen"

export const SomethingWrong = ({ theme }) => {
  const { t } = useTranslation()

  const { tryAgain, setTryAgain } = useContext(ChatContext)

  return (
    <AlertScreen
      id="SomethingWrong-main"
      title={t("SomethingWrong-labels-mainTitle")}
      description={t("SomethingWrong-labels-secondaryTitle")}
      image={<img src="assets/images/image-error.png" alt="calendar" />}
      theme={theme}
    >
      <Button
        variant="outlined"
        className="try-again-button"
        id="ccTryAgain"
        component="span"
        onClick={() => {
          setTryAgain(tryAgain + 1)
        }}
      >
        Try again
      </Button>
    </AlertScreen>
  )
}
