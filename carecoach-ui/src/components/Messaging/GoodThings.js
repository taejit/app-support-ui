import React from "react"
import { useTranslation } from "react-i18next"
import { AlertScreen } from "./alert-screen"

export const GoodThings = ({ theme }) => {
  const { t } = useTranslation()

  return (
    <AlertScreen
      id="GoodThings-main"
      title={t("GoodThings-labels-mainTitle")}
      description={t("GoodThings-labels-secondaryTitle")}
      image={<img src="assets/images/hourglass.png" alt="calendar" />}
      theme={theme}
    />
  )
}
