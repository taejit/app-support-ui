import React, { useContext, useState, useEffect } from "react"
import IconButton from "@material-ui/core/IconButton"
import Paper from "@material-ui/core/Paper"
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer"
import Typography from "@material-ui/core/Typography"
import AppBar from "@material-ui/core/AppBar"
import Toolbar from "@material-ui/core/Toolbar"
import { Badge } from "@material-ui/core"
import { useHistory } from "react-router-dom"
import { PopupContext } from "store/PopupContext"
import { ReactComponent as BellIcon } from "Assets/bell.svg"
import { ChatContext } from "store/ChatContext"
import { AllQuiet } from "../AllQuiet"
import { SomethingWrong } from "../SomethingWrong"
import { NotificationList } from "./notification-list"
import services from "services"
import { CloseIcon } from "components/icons"

import "stream-chat-react/dist/css/index.css"
import "./index.scss"

const ConditionalContent = ({ channel, messages, messageProcessor }) => {
  if (channel) {
    return messages.length === 0 ? (
      <AllQuiet />
    ) : (
      <NotificationList
        id="NotificationList-main"
        messages={messages}
        messageProcessor={messageProcessor}
      />
    )
  }
  return <SomethingWrong />
}

const Notifications = () => {
  const history = useHistory()

  const { notificationPanelState, setNotificationPanelState } = useContext(
    PopupContext
  )

  const {
    notificationChannel: channel,
    chatClient,
    notificationMessages,
    isConnecting,
  } = useContext(ChatContext)

  const { processMessage } = services.processor.messaging({
    navigation: history,
    client: chatClient,
  })

  const [messages, setMessages] = useState([])
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    const { messages: messagesList = [] } = notificationMessages || {}
    setMessages(messagesList)
    setIsLoading(false)
  }, [notificationMessages])

  return (
    <div className="notification-main" id="Notifications-container-main">
      <SwipeableDrawer
        BackdropProps={{ invisible: true }}
        classes={{ paper: "notification_root" }}
        open={notificationPanelState}
        anchor="right"
        onClose={() => setNotificationPanelState(false)}
        onOpen={() => {
          // this is intentional
        }}
        disableSwipeToOpen
      >
        <div className={"notificationpanel"}>
          <AppBar className={"notification_bar"} position="static">
            <Toolbar className={"notification_toolbarCss"}>
              <Typography className={"notification_title"}>
                Notifications
              </Typography>
              <IconButton color="inherit">
                <Badge color="secondary">
                  <BellIcon />
                </Badge>
              </IconButton>
              <IconButton onClick={() => setNotificationPanelState(false)}>
                <CloseIcon />
              </IconButton>
            </Toolbar>
          </AppBar>
          <Paper className={"notificationpanellist"}>
            {isLoading || isConnecting ? (
              <div>Loading...</div>
            ) : (
              <ConditionalContent
                channel={channel}
                messages={messages}
                messageProcessor={processMessage}
              />
            )}
          </Paper>
        </div>
      </SwipeableDrawer>
    </div>
  )
}

export default Notifications
