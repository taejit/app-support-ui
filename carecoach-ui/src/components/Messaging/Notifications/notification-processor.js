import React, { useEffect } from "react"
import { subscribe } from "utils/PubSub"

export const NotificationProcessor = ({ navigation }) => {
  useEffect(async () => {
    let isMounted = true
    const unsubscribe = subscribe("notification.new", (message) => {
      if (isMounted) {
        const { action: { actionType, payload: { type } = {} } = {} } =
          message || {}
        if (actionType === "redirect" && type === "carecoach") {
          console.log(message)
          // DO SOMETHING
        }
      }
    })

    return () => {
      isMounted = false

      unsubscribe()
    }
  }, [])

  return null
}
