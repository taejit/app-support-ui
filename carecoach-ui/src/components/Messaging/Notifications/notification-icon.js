import React, { useContext, useState, useEffect } from "react"
import { IconButton, Badge } from "@material-ui/core"
import { makeStyles } from "@material-ui/core/styles"
import { ChatContext } from "store/ChatContext"
import { PopupContext } from "store/PopupContext"
import { ReactComponent as BellIcon } from "Assets/bell.svg"

export const NotificationIcon = ({ navigation }) => {
  const [hasNewMessages, setHasNewMessages] = useState(false)
  const { notificationChannel, newNotifications } = useContext(ChatContext)

  const { notificationPanelState, setNotificationPanelState } = useContext(
    PopupContext
  )

  useEffect(() => {
    setHasNewMessages(newNotifications)
  }, [newNotifications])

  return (
    <div
      style={
        notificationPanelState
          ? {
              borderBottom: "solid",
              height: "100%",
              alignItems: "center",
              display: "flex",
            }
          : {}
      }
    >
      <IconButton
        id="NotificationIcon"
        onClick={() => {
          if (notificationChannel) {
            notificationChannel.markRead()
          }
          setNotificationPanelState(true)
        }}
        aria-label="show 17 new notifications"
        color="inherit"
      >
        {hasNewMessages > 0 ? (
          <Badge badgeContent={""} variant="dot" color="secondary">
            <BellIcon />
          </Badge>
        ) : (
          <BellIcon />
        )}
      </IconButton>
    </div>
  )
}
