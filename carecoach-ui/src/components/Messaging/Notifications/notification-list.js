import React from "react"
import moment from "moment"
import { makeStyles } from "@material-ui/core/styles"

const useStyles = makeStyles((theme) => ({
  root: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: theme.palette.cloud,
  },
  box: {
    paddingTop: 4,
    paddingBottom: 4,
  },
  group: {
    paddingTop: 4,
    paddingBottom: 4,
    fontSize: "15px",
    fontWeight: 500,
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: 1.4,
    letterSpacing: "normal",
    color: theme.palette.darkBlue,
    cursor: "pointer",
  },
  itemSubText: {
    paddingTop: 0,
    paddingBottom: 0,
    margin: 0,
    fontSize: "12px",
    fontWeight: 500,
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: 1.5,
    letterSpacing: "normal",
    color: theme.palette.twilight,
  },
  itemText: {
    paddingTop: 0,
    paddingBottom: 0,
    margin: 0,
    fontSize: "15px",
    fontWeight: 500,
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: 1.5,
    letterSpacing: "normal",
    color: theme.palette.darkBlue,
  },
  itemTextAlt: {
    paddingTop: 0,
    paddingBottom: 0,
    margin: 0,
    fontSize: "15px",
    fontWeight: 500,
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: 1.5,
    letterSpacing: "normal",
    color: theme.palette.darkBlue,
    opacity: "0.5",
  },
  groupHeaderTitle: {
    paddingTop: 6,
    paddingBottom: 6,
    fontSize: "12px",
    fontWeight: 600,
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: 1.5,
    letterSpacing: "normal",
    color: theme.palette.nightShade,
  },
}))

const MAX_MESSAGES = 100

const renderItems = (list, today, classes, messageProcessor) => {
  if (!Array.isArray(list) || list.length === 0) return []

  return list.map((message) => {
    let format = "ll"

    if (moment(message.created_at).isSame(today, "d")) {
      format = "LT"
    } else if (moment(message.created_at).isSame(today, "week")) {
      format = "dddd"
    }

    const formatted = moment(message.created_at).format(format)
    return (
      <div
        onClick={() => messageProcessor(message)}
        className={classes.group}
        key={message.id}
        aria-hidden="true"
      >
        <p className={message.read ? classes.itemTextAlt : classes.itemText}>
          {message.text}
        </p>
        <p className={classes.itemSubText}>{formatted}</p>
      </div>
    )
  })
}

export const NotificationList = ({ messages, messageProcessor }) => {
  const classes = useStyles()

  const filteredDeleted = messages.filter(
    (message) => message.deleted_at === undefined
  )

  // @ts-ignore
  filteredDeleted.sort(
    (a, b) => new Date(b.created_at) - new Date(a.created_at)
  )

  const reversed = filteredDeleted.slice(0, MAX_MESSAGES)

  const today = moment().clone().startOf("day")

  const listToday = reversed.filter((message) =>
    moment(message.created_at).isSame(today, "d")
  )

  const listEarlier = reversed.filter(
    (message) => !moment(message.created_at).isSame(today, "d")
  )

  const listItemsToday = renderItems(
    listToday,
    today,
    classes,
    messageProcessor
  )
  const listItemsEarlier = renderItems(
    listEarlier,
    today,
    classes,
    messageProcessor
  )

  return (
    <div
      id="NotificationList-main"
      style={{
        justifyContent: "space-between",
        paddingTop: 30,
        paddingLeft: 24,
        paddingRight: 24,
        paddingBottom: "150px",
      }}
    >
      {listItemsToday.length > 0 && (
        <div className={classes.box}>
          <p className={classes.groupHeaderTitle}>TODAY</p>
          {listItemsToday}
        </div>
      )}

      {listItemsEarlier.length > 0 && (
        <div className={classes.box}>
          <p className={classes.groupHeaderTitle}>EARLIER</p>
          {listItemsEarlier}
        </div>
      )}
    </div>
  )
}
