import React, { useContext, useState, useEffect } from "react"
import {
  Chat,
  ChannelList,
  ChatContext as MainChatContext,
} from "stream-chat-react"
import IconButton from "@material-ui/core/IconButton"
import Paper from "@material-ui/core/Paper"
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer"
import Typography from "@material-ui/core/Typography"
import AppBar from "@material-ui/core/AppBar"
import Toolbar from "@material-ui/core/Toolbar"
import { useHistory } from "react-router-dom"
import { Auth } from "aws-amplify"
import { PopupContext } from "store/PopupContext"
import { ChatContext } from "store/ChatContext"
import { GoodThings } from "../GoodThings"
import { SomethingWrong } from "../SomethingWrong"
import { CloseIcon, MaximizeIcon } from "components/icons"

import "stream-chat-react/dist/css/index.css"
import "./index.scss"

export const DetectChannelChange = () => {
  const { channel } = useContext(MainChatContext)
  const { setMiniMessagingPanel, setMiniMessagingChannel } = useContext(
    PopupContext
  )

  useEffect(() => {
    if (channel) {
      // console.log(channel.data.name)
      setMiniMessagingChannel(channel)
      setMiniMessagingPanel(true)
    }
  }, [channel])

  return null
}

const ConditionalContent = ({ chatClient, allQuiet, user }) => {
  if (chatClient) {
    return allQuiet ? (
      <GoodThings />
    ) : (
      <Chat client={chatClient} theme="messaging light">
        <DetectChannelChange />
        <ChannelList
          setActiveChannelOnMount={false}
          filters={{
            members: { $in: [user] },
            id: { $nin: [user, `${user}-messaging`, `${user}-notification`] },
            //id: { $nin: [user, `${user}-messaging`, `${user}-notification`] },
          }}
          options={{ state: true, watch: true, presence: true }}
        />
      </Chat>
    )
  }
  return <SomethingWrong />
}

const MiniList = () => {
  const history = useHistory()

  const {
    messagingPanel,
    setMessagingPanel,
    setMiniMessagingPanel,
  } = useContext(PopupContext)

  const { chatClient, isConnecting } = useContext(ChatContext)

  const [user, setUser] = useState()
  const [allQuiet, setAllQuiet] = useState(true)

  useEffect(() => {
    const loadAuth = async () => {
      const authInfo = await Auth.currentUserInfo()
      const { attributes: { "custom:uuid": carecoachUuid = "" } = {} } =
        authInfo || {}

      setUser(carecoachUuid)
    }

    const checkChannels = async () => {
      if (!chatClient) return

      const filter = {
        type: "messaging",
        members: { $in: [user] },
        id: { $nin: [user, `${user}-messaging`, `${user}-notification`] },
      }

      const channels = await chatClient
        .queryChannels(filter)
        .catch((err) => console.error("Filter Channels is failing", err))

      if (channels && channels.length > 0) {
        setAllQuiet(false)
      }
    }

    loadAuth().then(() => checkChannels())
  }, [chatClient])

  const close = () => {
    setMessagingPanel(false)
  }

  const maximize = () => {
    history.push(`/dashboard/messaging`)
    setMessagingPanel(false)
    setMiniMessagingPanel(false)
  }

  return (
    <div className="minilist" id="minilist-container-main">
      <SwipeableDrawer
        BackdropProps={{ invisible: true }}
        classes={{ paper: "minilist_root" }}
        open={messagingPanel}
        anchor="right"
        elevation={0}
        onClose={() => close()}
        onOpen={() => {
          // this is intentional
        }}
        disableSwipeToOpen
      >
        <div className={"minilistpanel"}>
          <AppBar className={"minilist_bar"} position="static">
            <Toolbar className={"minilist_toolbarCss"}>
              <Typography className={"minilist_title"}>Messages</Typography>
              <IconButton onClick={() => maximize()}>
                <MaximizeIcon />
              </IconButton>
              <IconButton onClick={() => close()}>
                <CloseIcon />
              </IconButton>
            </Toolbar>
          </AppBar>
          <Paper className={"minilistpanellist"}>
            {isConnecting || !user ? (
              <div id="loading">Loading...</div>
            ) : (
              <ConditionalContent
                chatClient={chatClient}
                allQuiet={allQuiet}
                user={user}
              />
            )}
          </Paper>
        </div>
      </SwipeableDrawer>
    </div>
  )
}

export default MiniList
