import React, { useContext, useState, useEffect, useRef } from "react"
import { Chat, Channel, MessageList, MessageInput } from "stream-chat-react"
import IconButton from "@material-ui/core/IconButton"
import Paper from "@material-ui/core/Paper"
import Typography from "@material-ui/core/Typography"
import AppBar from "@material-ui/core/AppBar"
import Toolbar from "@material-ui/core/Toolbar"
import { useHistory } from "react-router-dom"
import { useTheme, makeStyles } from "@material-ui/core/styles"
import { useSwipeable } from "react-swipeable"
import clsx from "clsx"
import { PopupContext } from "store/PopupContext"
import { ChatContext } from "store/ChatContext"
import { GoodThings } from "../GoodThings"
import { SomethingWrong } from "../SomethingWrong"
import { CloseIcon, MaximizeIcon } from "components/icons"

import "stream-chat-react/dist/css/index.css"
import "./index.scss"

const useStyles = makeStyles((theme) => ({
  root: {
    width: 70,
    maxWidth: 70,
    minWidth: 70,
    [theme.breakpoints.down("md")]: {
      width: 0,
      maxWidth: 0,
      minWidth: 0,
    },
  },
  panel: {
    borderRadius: "15px",
    position: "absolute",
    width: "360px",
    backgroundColor: theme.palette.background.paper,
    boxShadow:
      "0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)",
    height: "50vh",
    minHeight: "440px",
    bottom: "2vh",
    right: "max(17vw, 380px)",
    margin: 0,
    zIndex: 1000,
    visibility: "hidden",
    overflow: "hidden",
    [theme.breakpoints.down("md")]: {
      boxShadow: "none",
      "&.opened": {
        boxShadow:
          "0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)",
      },
    },
    transition: theme.transitions.create(["transform"], {
      easing: theme.transitions.easing.easeInOut,
      duration: theme.transitions.duration.standard,
    }),
    "&.opened": {
      visibility: "visible",
    },
  },
}))

const ConditionalContent = ({ chatClient, channel }) => {
  if (chatClient) {
    return (
      <Chat client={chatClient} theme="messaging light">
        <Channel channel={channel}>
          <MessageList messageActions={["react"]} />
          <MessageInput />
        </Channel>
      </Chat>
    )
  }
  return <SomethingWrong />
}

const MiniMessenger = () => {
  const history = useHistory()

  const {
    miniMessagingPanel,
    setMiniMessagingPanel,
    miniMessagingChannel,
    setMiniMessagingChannel,
  } = useContext(PopupContext)

  const { chatClient, isConnecting } = useContext(ChatContext)

  const [anchorEl, setAnchorEl] = useState(null)
  const ref = useRef()

  useEffect(() => {
    if (miniMessagingPanel) {
      setAnchorEl(ref.current)
    }
  }, [miniMessagingPanel])

  const handlers = useSwipeable({
    onSwipedLeft: () => {
      return (
        miniMessagingPanel &&
        theme.direction === "rtl" &&
        setMiniMessagingPanel(false)
      )
    },
    onSwipedRight: () => {
      return (
        miniMessagingPanel &&
        theme.direction === "ltr" &&
        setMiniMessagingPanel(false)
      )
    },
  })

  const close = () => {
    setAnchorEl(null)
    setMiniMessagingChannel(null)
    setMiniMessagingPanel(false)
  }

  const maximize = () => {
    history.push(`/dashboard/messaging`)
    setMiniMessagingPanel(false)
  }

  const theme = useTheme()
  const classes = useStyles()

  const open = Boolean(anchorEl) && miniMessagingChannel
  const id = open ? "simple-popover" : undefined

  return (
    <div id={id} className={classes.root} {...handlers}>
      <div
        id="minimessenger"
        className={clsx(
          classes.panel,
          { opened: miniMessagingPanel },
          "minimessenger flex flex-col max-w-full"
        )}
        ref={ref}
      >
        <AppBar className={"minimessenger_bar"} position="static">
          <Toolbar className={"minimessenger_toolbarCss"}>
            <Typography className={"minimessenger_title"}>
              {miniMessagingChannel
                ? miniMessagingChannel?.data?.name
                : "Messages"}
            </Typography>
            <IconButton onClick={() => maximize()}>
              <MaximizeIcon alt />
            </IconButton>
            <IconButton onClick={() => close()}>
              <CloseIcon alt />
            </IconButton>
          </Toolbar>
        </AppBar>
        <Paper className={"minimessengerpanellist"}>
          {isConnecting || !miniMessagingChannel ? (
            <div id="loading">Loading...</div>
          ) : (
            <ConditionalContent
              chatClient={chatClient}
              channel={miniMessagingChannel}
            />
          )}
        </Paper>
      </div>
    </div>
  )
}

export default MiniMessenger
