import React, { useContext, useState, useEffect } from "react"
import { IconButton, Badge } from "@material-ui/core"
import { useHistory, useLocation } from "react-router-dom"
import { PopupContext } from "store/PopupContext"
import { ChatContext } from "store/ChatContext"
import { ReactComponent as ImageIcon } from "../../Assets/messaging.svg"
import { Auth } from "aws-amplify"

const voidFunc = () => {
  // this is intentional
}

const emptyResult = {
  unsubscribe: voidFunc,
}

export const MessagingIcon = () => {
  const history = useHistory()
  const location = useLocation()

  const { messagingPanel, setMessagingPanel, miniMessagingPanel } = useContext(
    PopupContext
  )
  const { chatClient } = useContext(ChatContext)

  const [hasNewMessages, setHasNewMessages] = useState(0)

  useEffect(() => {
    let isMounted = true
    let userUuid = undefined
    const loadAuth = async () => {
      const authInfo = await Auth.currentUserInfo()
      const { attributes: { "custom:uuid": carecoachUuid = "" } = {} } =
        authInfo || {}

      userUuid = carecoachUuid
      return carecoachUuid
    }

    const checkChannels = async () => {
      if (chatClient && userUuid) {
        const filter = {
          type: "messaging",
          members: { $in: [userUuid] },
          id: {
            $nin: [
              userUuid,
              `${userUuid}-messaging`,
              `${userUuid}-notification`,
            ],
          },
        }

        const channels = await chatClient
          .queryChannels(filter)
          .catch((err) => console.error("Filter Channels is failing", err))

        const unreadTotal = channels
          .map((channel) => {
            const {
              state: { unreadCount },
            } = channel
            return unreadCount
          })
          .reduce((counter, val) => counter + val, 0)

        setHasNewMessages(unreadTotal)

        return chatClient.on("message.new", (event) =>
          setHasNewMessages(unreadTotal + 1)
        )
      }

      return emptyResult
    }

    const result = { ...emptyResult }

    loadAuth().then(() => {
      if (isMounted) {
        checkChannels().then((onResult) => {
          if (isMounted) {
            result.unsubscribe = onResult.unsubscribe
          } else {
            onResult.unsubscribe()
          }
        })
      }
    })
    return () => {
      isMounted = false
      result.unsubscribe()
    }
  }, [chatClient, messagingPanel, miniMessagingPanel])

  return (
    <div
      style={
        messagingPanel || location.pathname == "/dashboard/messaging"
          ? {
              borderBottom: "solid",
              height: "100%",
              alignItems: "center",
              display: "flex",
            }
          : {}
      }
    >
      <IconButton
        id="MessagingIcon"
        onClick={() => {
          if (location.pathname == "/dashboard/messaging") {
            history.goBack()
          } else {
            // history.push(`/dashboard/messaging`)
            setMessagingPanel(true)
          }
        }}
        aria-label="show messages"
        color="inherit"
      >
        {hasNewMessages > 0 ? (
          <Badge badgeContent={""} variant="dot" color="secondary">
            <ImageIcon />
          </Badge>
        ) : (
          <ImageIcon />
        )}
      </IconButton>
    </div>
  )
}
