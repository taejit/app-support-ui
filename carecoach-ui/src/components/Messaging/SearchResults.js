import React from "react"
import ListItemText from "@material-ui/core/ListItemText"
import Avatar from "@material-ui/core/Avatar"
import moment from "moment"
import { makeStyles } from "@material-ui/core/styles"

import "./index.scss"

const useStyles = makeStyles((theme) => ({
  avatar: {
    backgroundColor: theme.palette.nightShade,
    marginRight: 12,
    marginTop: 8,
  },
  primaryCls: {
    fontSize: 15,
  },
  secondaryCls: {
    fontSize: 12,
    color: theme.palette.twinight,
    marginTop: 4,
  },
}))

const Message = ({
  message: {
    user: { name },
    text,
    date,
  },
}) => {
  const classes = useStyles()
  const formatted = moment(date).format("lll")
  return (
    <div className="search-results-msg">
      <Avatar className={classes.avatar}>
        {name && name.length > 0 ? name[0].toUpperCase() : ""}
      </Avatar>
      <ListItemText
        primary={text}
        secondary={formatted}
        classes={{
          primary: classes.primaryCls,
          secondary: classes.secondaryCls,
        }}
      />
    </div>
  )
}

export const SearchResults = ({ data }) => {
  const results = data.map((message, index) => {
    return <Message message={message} key={index} />
  })

  return <div className="sidebar-search-results">{results}</div>
}
