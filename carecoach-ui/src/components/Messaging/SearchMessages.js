import React, { useEffect, useState, useContext } from "react"
import { InputAdornment, Input, IconButton } from "@material-ui/core"
import { ChatContext as MainChatContext } from "stream-chat-react"
import { ReactComponent as SearchIcon } from "../../Assets/Caregivers/search-icon.svg"
import { ReactComponent as ClearIcon } from "../../Assets/Caregivers/search-cross-icon.svg"
import "./index.scss"

const searchMessages = async (ch, searchValue) => {
  return new Promise((resolve, reject) => {
    if (ch) {
      const messageFilters = { text: { $q: searchValue } }

      ch.search(messageFilters, {
        limit: 100,
        offset: 0,
      })
        .then((data) => {
          if (data && data.results && data.results.length > 0) {
            const msgs = data.results
              .map((msg) => msg.message)
              .map((msg) => {
                return {
                  text: msg.text,
                  user: {
                    name: msg.user.name,
                    firstName: msg.user.firstName,
                    lastName: msg.user.lastName,
                  },
                  date: msg.created_at,
                }
              })
            resolve(msgs)
          } else {
            resolve([])
          }
        })
        .catch((error) => {
          reject(error)
        })
    }
  })
}

const EmptyFunction = () => {
  //this is intentional
}

export const SearchMessages = ({
  onSearchResults = EmptyFunction,
  onSearchValue = EmptyFunction,
}) => {
  const { channel } = useContext(MainChatContext)
  const [clear, setClear] = useState(false)
  const [value, setValue] = useState("")

  useEffect(() => {
    let isMounted = true
    setClear(value !== "")
    if (channel && value !== "") {
      searchMessages(channel, value)
        .then((data) => {
          if (isMounted) {
            onSearchResults(data)
            onSearchValue(value)
          }
        })
        .catch(() => {
          if (isMounted) {
            onSearchResults([])
            onSearchValue("")
          }
        })
    } else {
      onSearchResults([])
      onSearchValue(value)
    }
    return () => {
      isMounted = false
    }
  }, [value, channel])

  return (
    <div id="sidebar-search" className="sidebar-search">
      <Input
        id="search-conversation"
        type="text"
        placeholder="Search conversation..."
        value={value}
        onChange={(event) => {
          setValue(event.target.value)
          // onSearchResults(event.target.value !== "" ? [{}] : [])
        }}
        endAdornment={
          <InputAdornment position="end">
            {clear ? (
              <IconButton
                onClick={() => {
                  // setClear(false)
                  setValue("")
                }}
                type="submit"
                aria-label="search"
              >
                <ClearIcon style={{ color: "#3c75c8", fontSize: 30 }} />
              </IconButton>
            ) : (
              <IconButton>
                <SearchIcon style={{ color: "#3c75c8" }} />
              </IconButton>
            )}
          </InputAdornment>
        }
      />
    </div>
  )
}
