import React, { useState, useEffect } from "react"
import { AttachmentSmallView, AttachmentLargeView } from "./Attachments"
import { AboutConversation } from "./AboutConversation"
import { SearchMessages } from "./SearchMessages"
import { SearchResults } from "./SearchResults"
import { NothingFound } from "./NothingFound"
import "./index.scss"

export const MessagingSidebar = () => {
  const [fullScreen, setFullScreen] = useState("")
  const [searchVal, setSearchVal] = useState("")
  const [searchResults, setSearchResults] = useState([])
  const [results, setResults] = useState(null)

  const getResults = () => {
    if (searchVal) {
      return searchResults.length > 0 ? (
        <SearchResults data={searchResults} />
      ) : (
        <NothingFound />
      )
    }
    return (
      <AttachmentSmallView onFullScreen={() => setFullScreen("attachments")} />
    )
  }

  useEffect(() => {
    let isMounted = true
    setResults(getResults)
    return () => {
      isMounted = false
    }
  }, [searchVal, searchResults])

  if (fullScreen === "") {
    return (
      <div id="sidebar-main" className="sidebar-main">
        <AboutConversation />
        <SearchMessages
          onSearchResults={(results) => {
            setSearchResults(results)
          }}
          onSearchValue={(val) => {
            setSearchVal(val)
          }}
        />
        {results}
      </div>
    )
  } else if (fullScreen === "attachments") {
    return (
      <div className="sidebar-main-full">
        <AttachmentLargeView onFullScreen={() => setFullScreen("")} />
      </div>
    )
  }
}
