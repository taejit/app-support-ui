import React from "react"
import { Auth } from "aws-amplify"
import {
  render,
  waitFor,
  fireEvent,
  getByText,
  within,
  wait,
} from "@testing-library/react"
import { Router } from "react-router-dom"
import { createMemoryHistory } from "history"
import { ChatContext as MainChatContext } from "stream-chat-react"
import { ChatContext, useChat } from "store/ChatContext"

import * as CaregiverServices from "../../services/Caregivers.service"
import * as CarecaochApiService from "../../services/CarecaochServices"
import CaregiversList from "."
import {
  Alphabetically,
  LastContact,
  DateRegistered,
  Burden,
} from "../../utils/constant"

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "./edit",
    success: true,
  }),
  useHistory: () => ({ push: () => {} }),
}))

describe("CaregiversList test", () => {
  let ChatContextData
  beforeEach(() => {
    CaregiverServices.GetCaregiversList = () =>
      Promise.resolve({
        data: [
          {
            uuid: "c46d1aa6-217d-4147-85da-29cb371c0daf",
            firstName: "Andrew",
            lastName: "smith",
            nickName: "Tim",
            hasUnReadMessages: false,
            lastContactedDate: "2021-01-06T11:24:36.890Z",
            upcomingCallAt: null,
            recipientNickName: "Charlie",
            relationshipToRecipient: "Parent",
            conditions: null,
            preferredCommunication: "CALLS",
            registeredDate: "2021-01-06T11:24:36.890Z",
            burdenScore: undefined,
          },
          {
            uuid: "bac1ba98-e57e-4a38-b0ca-27c0814a5a41",
            firstName: "Bill",
            lastName: "Clint",
            nickName: "cook",
            hasUnReadMessages: true,
            lastContactedDate: null,
            upcomingCallAt: null,
            recipientNickName: "Charlie",
            relationshipToRecipient: "Parent",
            conditions: ["Kidney Disease"],
            preferredCommunication: "CALLS",
            registeredDate: "2021-02-06T11:24:36.890Z",
            burdenScore: 20,
          },
          {
            uuid: "01dfd721-a6a6-451b-9ea2-9bf4e208571d",
            firstName: "Bob",
            lastName: "Cathy",
            nickName: "cook",
            hasUnReadMessages: false,
            lastContactedDate: "2021-02-11T07:33:45.137Z",
            upcomingCallAt: new Date(),
            recipientNickName: "Charlie",
            relationshipToRecipient: "Parent",
            conditions: [
              "Alzheimer's",
              "Aging",
              "Dementia",
              "Obesity",
              "Developmental Trauma",
            ],
            preferredCommunication: "MESSAGING",
            registeredDate: "2021-03-06T11:24:36.890Z",
            burdenScore: 17,
          },
          {
            uuid: "0a67a639-f1b1-490b-85b2-f4db6ac0b08f",
            firstName: "cart",
            lastName: "jessy",
            nickName: "eon",
            hasUnReadMessages: true,
            lastContactedDate: "2021-01-01T08:21:23.831Z",
            upcomingCallAt: null,
            recipientNickName: "Charlie",
            relationshipToRecipient: "Parent",
            conditions: null,
            preferredCommunication: "MESSAGING",
            registeredDate: "2021-04-06T11:24:36.890Z",
            burdenScore: 7,
          },
          {
            uuid: "5a5f5f61-bb8f-4dc9-a324-f68b9c9ff200",
            firstName: "James",
            lastName: "Jhon",
            nickName: "eon",
            hasUnReadMessages: true,
            lastContactedDate: "2021-02-13T02:48:59.965Z",
            upcomingCallAt: new Date(),
            recipientNickName: "Charlie",
            relationshipToRecipient: "Parent",
            conditions: ["Aging", "Dementia"],
            preferredCommunication: "CALL",
            registeredDate: "2021-05-06T11:24:36.890Z",
            burdenScore: 12,
          },
          {
            uuid: "63013804-29a0-4f89-ab6b-d02c19e17e7c",
            firstName: "jerry",
            lastName: "tom",
            nickName: "cook",
            hasUnReadMessages: false,
            lastContactedDate: "2021-01-09T20:13:48.850Z",
            upcomingCallAt: null,
            recipientNickName: "Charlie",
            relationshipToRecipient: "Parent",
            conditions: ["Aging", "Dementia"],
            preferredCommunication: "MESSAGING",
            registeredDate: "2020-01-06T11:24:36.890Z",
            burdenScore: 22,
          },
        ],
      })
    CarecaochApiService.GetCarecoachCaregiversCalls = () =>
      Promise.resolve({
        status: 200,
        data: [
          {
            caregiver_uuid: "bac1ba98-e57e-4a38-b0ca-27c0814a5a41",
            lastContactedDate: "2021-04-26T17:26:06.647Z",
          },
          {
            caregiver_uuid: "63013804-29a0-4f89-ab6b-d02c19e17e7c",
            lastContactedDate: "2021-04-24T17:26:06.647Z",
          },
        ],
      })
    jest.spyOn(Auth, "currentUserInfo").mockResolvedValue(
      Promise.resolve({
        attributes: {
          email: "bramsai@intraedge.com",
          email_verified: true,
          phone_number: "+12254789321",
          phone_number_verified: true,
          sub: "38041359-614f-4c44-9a32-23c3c80506c7",
          family_name: "kiran",
          given_name: "ramsai",
          "custom:location": "Hyderabad Telangana Hyd",
          "custom:pronoun": "he/his",
        },
        username: "acc2ea0f-3a1f-49b9-8b19-7f555ece71d6",
      })
    )
    ChatContextData = {
      isConnecting: false,
      chatClient: {
        queryChannels: () => Promise.resolve([]),
      },
      getChannelInfo: jest.fn().mockReturnValue({
        unreadMessages: false,
        lastMessage: {
          created_at: new Date(),
        },
      }),
    }
  })
  it("Verify component show all data", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(
        <ChatContext.Provider value={ChatContextData}>
          <CaregiversList />
        </ChatContext.Provider>
      )
    })
    const { queryByText } = wrapper
    expect(queryByText(/^my-caregivers-text$/)).toBeDefined()
  })
  it("Show components no data img if no data from auth", async () => {
    jest.spyOn(Auth, "currentUserInfo").mockResolvedValue()
    let wrapper
    await waitFor(() => {
      wrapper = render(<CaregiversList />)
    })
    const { container } = wrapper
    const caregiverNoDataImgId = container.querySelector("#caregiverImg")
    expect(caregiverNoDataImgId).toBeDefined()
  })
  it("Verify component redirect to profile view", async () => {
    const history = createMemoryHistory()
    history.push = jest.fn()
    let wrapper
    await waitFor(() => {
      wrapper = render(
        <Router history={history}>
          <ChatContext.Provider value={ChatContextData}>
            <CaregiversList />
          </ChatContext.Provider>
        </Router>
      )
    })
    const { queryByText, container } = wrapper
    const caregiverCardBtn = container.querySelector("#ccCaregiverCard")
    await waitFor(() => {
      fireEvent.click(caregiverCardBtn)
    })
    expect(queryByText(/^my-caregivers-text$/)).toBeDefined()
  })
  it("Search caregivers", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(
        <ChatContext.Provider value={ChatContextData}>
          <CaregiversList />
        </ChatContext.Provider>
      )
    })
    const { container } = wrapper
    const caregiverSearchField = container.querySelector("#ccSearchCaregiver")
    const caregiverClearSearchBtn = container.querySelector(
      "#ccClearSearchIcon"
    )
    await waitFor(() => {
      fireEvent.change(caregiverSearchField, {
        target: {
          value: "cat",
          id: "ccSearchCaregiver",
        },
      })
      fireEvent.blur(caregiverSearchField, {
        target: {
          value: "cat",
          id: "ccSearchCaregiver",
        },
      })
    })
    expect(caregiverClearSearchBtn).toBeDefined()
  })

  it("Verify by default caregivers are in alphabetical order based on last name", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(
        <ChatContext.Provider value={ChatContextData}>
          <CaregiversList />
        </ChatContext.Provider>
      )
    })
    const { getAllByTestId } = wrapper
    const caregivers = getAllByTestId("caregiverscard")
    expect(getByText(caregivers[0], /cook Cathy/)).toBeDefined()
    expect(getByText(caregivers[1], /cook Clint/)).toBeDefined()
    expect(getByText(caregivers[2], /eon jessy/)).toBeDefined()
    expect(getByText(caregivers[3], /eon Jhon/)).toBeDefined()
    expect(getByText(caregivers[4], /Tim smith/)).toBeDefined()
    expect(getByText(caregivers[5], /cook tom/)).toBeDefined()
  })

  it("Verify sorting by burden score and searching", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(
        <ChatContext.Provider value={ChatContextData}>
          <CaregiversList />
        </ChatContext.Provider>
      )
    })
    const { container, getByRole, debug, getAllByTestId } = wrapper
    const selectionBox = container.querySelector(".sort-selection-dropdown")
    expect(selectionBox).toBeDefined()

    // sorting by burden score
    await waitFor(() => {
      fireEvent.mouseDown(getByRole("button"))
    })
    const listbox = within(getByRole("listbox"))
    await waitFor(() => {
      fireEvent.mouseDown(listbox.getByText("alphabetically"))
    })
    await waitFor(() => {
      fireEvent.click(listbox.getByText("burden"))
    })
    const caregivers = getAllByTestId("caregiverscard")
    expect(getByText(caregivers[0], /cook tom/)).toBeDefined()
    expect(getByText(caregivers[5], /Tim smith/)).toBeDefined()
    expect(getByText(caregivers[2], /cook Cathy/i)).toBeDefined()
    expect(getByText(caregivers[1], /cook Clint/)).toBeDefined()
    expect(getByText(caregivers[4], /eon jessy/)).toBeDefined()
    expect(getByText(caregivers[3], /eon Jhon/)).toBeDefined()

    // perform searching on top of sorted above
    const caregiverSearchField = container.querySelector("#ccSearchCaregiver")
    await waitFor(() => {
      fireEvent.change(caregiverSearchField, {
        target: {
          value: "cook",
          id: "ccSearchCaregiver",
        },
      })
      fireEvent.blur(caregiverSearchField, {
        target: {
          value: "cook",
          id: "ccSearchCaregiver",
        },
      })
    })
    expect(getByText(caregivers[0], /cook tom/)).toBeDefined()
    expect(getByText(caregivers[1], /cook Clint/)).toBeDefined()
    expect(getByText(caregivers[2], /cook Cathy/i)).toBeDefined()
    expect(getByText(caregivers[3], /eon Jhon/)).not.toBeInTheDocument()
  })

  it("Verify alphabetical sorting when switching back from other", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(
        <ChatContext.Provider value={ChatContextData}>
          <CaregiversList />
        </ChatContext.Provider>
      )
    })
    const { container, getByRole, debug, getAllByTestId } = wrapper
    const selectionBox = container.querySelector(".sort-selection-dropdown")
    expect(selectionBox).toBeDefined()

    // sorting by burden score
    await waitFor(() => {
      fireEvent.mouseDown(getByRole("button"))
    })
    let listbox = within(getByRole("listbox"))
    await waitFor(() => {
      fireEvent.mouseDown(listbox.getByText("alphabetically"))
    })
    await waitFor(() => {
      fireEvent.click(listbox.getByText("burden"))
    })
    await waitFor(() => {
      fireEvent.mouseDown(getByRole("button"))
    })
    listbox = within(getByRole("listbox"))
    await waitFor(() => {
      fireEvent.mouseDown(listbox.getByText("burden"))
    })
    await waitFor(() => {
      fireEvent.click(listbox.getByText("alphabetically"))
    })
    const caregivers = getAllByTestId("caregiverscard")
    expect(getByText(caregivers[0], /cook Cathy/)).toBeDefined()
    expect(getByText(caregivers[1], /cook Clint/)).toBeDefined()
    expect(getByText(caregivers[2], /eon jessy/)).toBeDefined()
    expect(getByText(caregivers[3], /eon Jhon/)).toBeDefined()
    expect(getByText(caregivers[4], /Tim smith/)).toBeDefined()
    expect(getByText(caregivers[5], /cook tom/)).toBeDefined()
  })
  it("Verify sorting by last contact date", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(
        <ChatContext.Provider value={ChatContextData}>
          <CaregiversList />
        </ChatContext.Provider>
      )
    })
    const { container, getByRole, debug, getAllByTestId } = wrapper
    const selectionBox = container.querySelector(".sort-selection-dropdown")
    expect(selectionBox).toBeDefined()

    // sorting by burden score
    await waitFor(() => {
      fireEvent.mouseDown(getByRole("button"))
    })
    let listbox = within(getByRole("listbox"))
    await waitFor(() => {
      fireEvent.mouseDown(listbox.getByText("alphabetically"))
    })
    await waitFor(() => {
      fireEvent.click(listbox.getByText("lastcontact"))
    })
    const caregivers = getAllByTestId("caregiverscard")
    expect(getByText(caregivers[0], /cook Cathy/)).toBeDefined()
    expect(getByText(caregivers[4], /cook Clint/)).toBeDefined()
    expect(getByText(caregivers[1], /eon jessy/)).toBeDefined()
    expect(getByText(caregivers[2], /eon Jhon/)).toBeDefined()
    expect(getByText(caregivers[3], /Tim smith/)).toBeDefined()
    expect(getByText(caregivers[5], /cook tom/)).toBeDefined()
  })
})
