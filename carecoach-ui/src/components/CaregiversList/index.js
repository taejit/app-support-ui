/* eslint-disable */
import { Box, Grid } from "@material-ui/core"
import { useTranslation } from "react-i18next"
import React, { useContext, useState, useEffect } from "react"
import { useHistory } from "react-router-dom"
import { Auth } from "aws-amplify"
import SnackbarMessage from "../SnackbarMessage"
import * as CaregiversApiService from "../../services/Caregivers.service"
import * as CarecaochApiService from "../../services/CarecaochServices"
import Caregiver from "../Caregiver"
import Carerecipient from "../Carerecipient"
import Loading from "../Loading"
import { ChatContext } from "store/ChatContext"
import dynamicSorting from "../../utils/dynamicSorting"
import SearchAndSort from "./SearchAndSort"
import "./index.scss"

const Caregivers = () => {
  const { t } = useTranslation()
  const [caregiversList, setCaregiversList] = useState([])
  const [completeCaregiversList, setCompleteCaregiversList] = useState([])
  const [showLoading, setShowLoading] = useState(true)
  const [showSearchEmpty, setShowSearchEmpty] = useState(false)

  const history = useHistory()
  const [showSnackbarMessage, setShowSnackbarMessage] = React.useState({
    open: false,
    message: "",
    success: false,
  })
  const { getChannelInfo, chatClient } = useContext(ChatContext)

  const GetCaregiversCallsData = async (uuid) => {
    const callData = await CarecaochApiService.GetCarecoachCaregiversCalls(uuid)
    if (callData.status === 200) {
      return callData.data
    }
    return []
  }

  const getCaregivers = async () => {
    setShowLoading(true)
    const currentUser = await Auth.currentUserInfo()
    if (!currentUser) {
      setShowSnackbarMessage({
        open: true,
        message: "Unable to fetch Caregivers list.",
        success: false,
      })
      setShowLoading(false)
      return
    }
    const caregiversResp = await CaregiversApiService.GetCaregiversList(
      currentUser.attributes["custom:uuid"]
    )
    const callsData = await GetCaregiversCallsData(
      currentUser.attributes["custom:uuid"]
    )
    const withContactInfo = await Promise.all(
      caregiversResp.data.map(async (caregiver) => {
        const channelData = await getChannelInfo(caregiver.uuid)
        caregiver.hasUnReadMessages = channelData.unreadMessages || false
        caregiver.lastMessageDate = channelData.lastMessageDate
        caregiver.lastContactedDate = null
        caregiver.upcomingCallAt = null
        let callData = callsData.filter(
          (data) => data.caregiver_uuid === caregiver.uuid
        )
        if (callData && callData.length) {
          caregiver.upcomingCallAt = callData[0].upcomingCallAt
          caregiver.lastContactedDate = callData[0].lastContactedDate
        }
        return caregiver
      })
    )
    // sorted alphabetically on last name
    const lastNameSorted = withContactInfo.sort(
      dynamicSorting("lastName", "asc")
    )
    setCaregiversList([...lastNameSorted])
    setCompleteCaregiversList([...lastNameSorted])
    setShowLoading(false)
  }

  const redirectToProfile = async (caregiverObj) => {
    history.push({
      pathname: `/dashboard/caregivers/${caregiverObj.uuid}`,
      caregiver: caregiverObj,
    })
  }

  useEffect(() => {
    if (chatClient) {
      getCaregivers()
    }
  }, [chatClient])

  if (showLoading) {
    return (
      <Box className="loadingIcon">
        <Loading />
      </Box>
    )
  }

  return (
    <>
      <SnackbarMessage {...showSnackbarMessage} />
      <Grid container className="containerCss">
        <SearchAndSort
          caregiversList={caregiversList}
          setCaregiversList={setCaregiversList}
          completeCaregiversList={completeCaregiversList}
          setShowSearchEmpty={setShowSearchEmpty}
        />
        {!!caregiversList.length && (
          <Grid item xs={12}>
            <Box
              width="100%"
              pt="56px"
              pr="8px"
              pl="24px"
              display="flex"
              flexDirection="row"
              flexWrap="wrap"
            >
              {caregiversList.map((caregiver) => {
                return (
                  <Box
                    data-testid="caregiverscard"
                    key={caregiver.uuid}
                    width="20%"
                    mb="36px"
                    className="caregiverTemplate"
                    id="ccCaregiverCard"
                    onClick={() => redirectToProfile(caregiver)}
                  >
                    <Box mr="36px">
                      <Caregiver caregiverObj={caregiver} variant={true} />
                      {/* <Carerecipient caregiverObj={caregiver} /> */}
                    </Box>
                  </Box>
                )
              })}
            </Box>
          </Grid>
        )}
        {!caregiversList.length && (
          <Box
            display="flex"
            height="100%"
            width="100%"
            alignItems="center"
            justifyContent="center"
            className="empty-search-box"
          >
            <Grid
              container
              spacing={0}
              direction="column"
              alignItems="center"
              justify="center"
            >
              <Grid item xs={4}>
                <Box
                  display="flex"
                  alignItems="center"
                  justifyContent="center"
                  fontWeight="500"
                  flexDirection="column"
                >
                  {!showSearchEmpty ? (
                    <img
                      alt="no-caregivers"
                      src="./images/nocaregivers/image-banners-default-small.png"
                      srcSet="./images/nocaregivers/image-banners-default-small@2x.png 2x,
         ./images/nocaregivers/image-banners-default-small@3x.png 3x"
                    />
                  ) : (
                    <img
                      alt="no-caregivers"
                      src="./images/nolocation/icons-illustrative-not-found.png"
                      srcSet="./images/nolocation/icons-illustrative-not-found@2x.png 2x,
             ./images/nolocation/icons-illustrative-not-found@3x.png 3x"
                    />
                  )}
                  <Box fontSize={18} pt="24px" color="darkBlue">
                    {!showSearchEmpty
                      ? t("no-caregivers-text")
                      : t("no-caregivers-search-text")}
                  </Box>
                  <Box
                    fontSize={14}
                    pt="14px"
                    textAlign="center"
                    color="nightShade"
                  >
                    {!showSearchEmpty
                      ? t("no-caregivers-text-subscript")
                      : t("no-caregivers-search-text-subscript")}
                  </Box>
                </Box>
              </Grid>
            </Grid>
          </Box>
        )}
      </Grid>
    </>
  )
}

export default Caregivers
