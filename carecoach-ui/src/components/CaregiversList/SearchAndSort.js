import React, { useState } from "react"
import { useTranslation } from "react-i18next"
import {
  Box,
  TextField,
  Grid,
  InputAdornment,
  Select,
  MenuItem,
} from "@material-ui/core"
import {
  Alphabetically,
  LastContact,
  DateRegistered,
  Burden,
} from "../../utils/constant"
import { ReactComponent as SearchIcon } from "../../Assets/Caregivers/search-icon.svg"
import { ReactComponent as ClearSearchIcon } from "../../Assets/Caregivers/search-cross-icon.svg"
import { ReactComponent as ChevronDown } from "../../Assets/chevron-down.svg"
import dynamicSorting from "../../utils/dynamicSorting"

const SearchAndSort = ({
  setShowSearchEmpty,
  completeCaregiversList,
  setCaregiversList,
}) => {
  const { t } = useTranslation()
  const [searchTxtValue, setSearchTxtValue] = useState("")
  const [sortCriteria, setSortCriteria] = useState(Alphabetically)

  const getSortedData = (sortCriteria, cgList) => {
    let sorted = []
    switch (sortCriteria) {
      case Alphabetically:
        sorted = cgList.sort(dynamicSorting("lastName", "asc"))
        break
      case LastContact:
        sorted = getSortedByLastcontact(cgList)
        break
      case DateRegistered:
        sorted = cgList.sort(
          (cg1, cg2) =>
            new Date(cg2.registeredDate) - new Date(cg1.registeredDate)
        )
        break
      case Burden:
        sorted = getSortedByBurdenScore(cgList)
        break
    }

    return [...sorted]
  }

  const getSortedByBurdenScore = (cgList) => {
    const indeterminate = cgList
      .filter((cg) => !cg.burdenScore)
      .sort(dynamicSorting("lastName", "asc"))

    const determinate = cgList
      .filter((cg) => cg.burdenScore)
      .sort(dynamicSorting("burdenScore", "desc"))

    return [...determinate, ...indeterminate]
  }

  const handleSortingChange = (sortCriteria) => {
    setSortCriteria(sortCriteria)
    let filteredList = completeCaregiversList
    if (searchTxtValue) {
      filteredList = getFilteredList(getLowerCasedSearchValue(searchTxtValue))
    }
    setCaregiversList(getSortedData(sortCriteria, filteredList))
  }

  const getMostRecentDate = (date1, date2) => {
    if (!date1 || !date2) {
      return date2 ? date2 : date1
    }
    const dateObj1 = new Date(date1).valueOf()
    const dateObj2 = new Date(date2).valueOf()
    return dateObj1 > dateObj2 ? dateObj1 : dateObj2
  }

  const getSortedByLastcontact = (cgList) => {
    const notContactedAtAll = cgList.filter(
      (cg) => !(cg.lastContactedDate || cg.lastMessageDate)
    )

    const lastContacted = cgList
      .filter((cg) => cg.lastContactedDate || cg.lastMessageDate)
      .sort((cg1, cg2) => {
        const recentCg1Contacted = getMostRecentDate(
          cg1.lastMessageDate,
          cg1.lastContactedDate
        )
        const recentCg2Contacted = getMostRecentDate(
          cg2.lastMessageDate,
          cg2.lastContactedDate
        )
        return new Date(recentCg2Contacted) - new Date(recentCg1Contacted)
      })

    return [...notContactedAtAll, ...lastContacted]
  }

  const getFilterConditionStr = (caregiver, name, lastName, searchVal) => {
    return (
      caregiver[name].toString().toLowerCase().includes(searchVal) ||
      `${caregiver[name]}${caregiver.lastName}`
        .toString()
        .toLowerCase()
        .includes(searchVal)
    )
  }

  const getFilteredList = (lowercasedValue) => {
    const filteredData = completeCaregiversList.filter((item) => {
      const conditionsStr =
        (item.conditions && item.conditions.join(",").toLowerCase()) || ""
      return (
        getFilterConditionStr(item, "firstName", "lastName", lowercasedValue) ||
        getFilterConditionStr(item, "lastName", "firstName", lowercasedValue) ||
        getFilterConditionStr(item, "lastName", "nickName", lowercasedValue) ||
        getFilterConditionStr(item, "nickName", "lastName", lowercasedValue) ||
        conditionsStr.includes(lowercasedValue)
      )
    })

    return filteredData || []
  }

  const getLowerCasedSearchValue = (searchValue) => {
    // filter records by search text=
    if (searchValue) {
      return searchValue.toLowerCase().split(" ").join("")
    }
    return ""
  }

  const searchCaregivers = async (searchValue) => {
    // include column list from filter
    setSearchTxtValue(searchValue)

    const lowercasedValue = getLowerCasedSearchValue(searchValue)
    setCaregiversList(
      getSortedData(sortCriteria, getFilteredList(lowercasedValue))
    )
    setShowSearchEmpty(true)
  }

  const onSearchClear = () => {
    setCaregiversList(getSortedData(sortCriteria, completeCaregiversList))
    setShowSearchEmpty(false)
    setSearchTxtValue("")
  }

  return (
    <Grid item xs={12}>
      <Box
        width="100%"
        display="flex"
        flexDirection="row"
        alignItems="center"
        justifyContent="space-between"
        pt="72px"
        pr="72px"
        pl="72px"
        pb="0em"
      >
        <Box fontSize={42} fontWeight="fontWeightBold" className="primaryText">
          {t("my-caregivers-text")}
        </Box>
        <Box width="50%" fontSize={16} fontWeight="500" className="searchField">
          <Select
            fullWidth
            value={sortCriteria}
            onChange={(e) => handleSortingChange(e.target.value)}
            className="sort-selection-dropdown"
            IconComponent={ChevronDown}
          >
            <MenuItem value={Alphabetically}>{t("alphabetically")}</MenuItem>
            <MenuItem value={LastContact}>{t("lastcontact")}</MenuItem>
            <MenuItem value={DateRegistered}>{t("dateregistered")}</MenuItem>
            <MenuItem value={Burden}>{t("burden")}</MenuItem>
          </Select>
          <TextField
            id="ccSearchCaregiver"
            name="searchCaregiver"
            margin="dense"
            autoComplete="off"
            fullWidth
            placeholder={t("search-caregiver-text")}
            value={searchTxtValue}
            onChange={(e) => searchCaregivers(e.target.value)}
            InputProps={{
              endAdornment: (
                <InputAdornment>
                  {!searchTxtValue && (
                    <SearchIcon
                      id="ccSearchIcon"
                      src="./images/caregivers/search-icon.svg"
                      alt="search-icon"
                    />
                  )}
                  {searchTxtValue && (
                    <Box
                      onClick={() => onSearchClear()}
                      className="cursor-pointer"
                      id="ccClearSearchIcon"
                    >
                      <ClearSearchIcon
                        src="./images/caregivers/search-cross-icon.svg"
                        alt="search-icon"
                      />
                    </Box>
                  )}
                </InputAdornment>
              ),
            }}
          />
        </Box>
      </Box>
    </Grid>
  )
}

export default SearchAndSort
