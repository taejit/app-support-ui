import React, { useState, useEffect } from "react"
import PropTypes from "prop-types"
import { Drawer, Box } from "@material-ui/core"
import { ReactComponent as CrossMark } from "../../Assets/cross.svg"
import { ReactComponent as BackArrow } from "../../Assets/CarePlans/arrow-back.svg"
import useStyles from "./styles"

const RightDrawer = ({
  open,
  onClose,
  children,
  hideCrossClose,
  showBackArrow,
  backArrowClick,
}) => {
  const classes = useStyles()

  const [openDrawer, setOpenDrawer] = useState(open)
  const toggleDrawer = (toggleOpen) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return
    }
    setOpenDrawer(toggleOpen)
    if (onClose) {
      onClose()
    }
  }

  useEffect(() => {
    setOpenDrawer(open)
  }, [open])

  return (
    <Drawer
      className={classes.root}
      anchor="right"
      open={openDrawer}
      onClose={toggleDrawer(false)}
    >
      {!hideCrossClose && (
        <Box onClick={toggleDrawer(false)}>
          <CrossMark className={classes.crossmark} />
        </Box>
      )}
      {showBackArrow && (
        <BackArrow
          data-testid="backarrow"
          onClick={backArrowClick}
          className={classes.backArrow}
        />
      )}
      {children}
    </Drawer>
  )
}

RightDrawer.defaultProps = {
  onClose: () => {},
  hideCrossClose: false,
  showBackArrow: false,
}

RightDrawer.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func,
  children: PropTypes.node.isRequired,
  hideCrossClose: PropTypes.bool,
  showBackArrow: PropTypes.bool,
}

export default RightDrawer
