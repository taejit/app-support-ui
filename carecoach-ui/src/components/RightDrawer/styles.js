import { makeStyles } from "@material-ui/core"

export default makeStyles(() => ({
  root: {
    "& .MuiDrawer-paper": {
      width: "456px",
      padding: "36px 48px 0px 48px",
    },
    backdropFilter: "blur(6px)",
  },
  crossmark: {
    float: "right",
    cursor: "pointer",
  },
  backArrow: {
    float: "left",
    cursor: "pointer",
    marginBottom: `28px`,
  },
}))
