import "../Welcome/Welcome.scss"
import "./WhatAboutYou.scss"
import React, { useState, useEffect } from "react"
import { Box, Button, Grid, TextField, Typography } from "@material-ui/core"
import { useFormik } from "formik"
import { useTranslation } from "react-i18next"
import { Auth } from "aws-amplify"
import DateFnsUtils from "@date-io/date-fns"
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers"
import { useHistory } from "react-router-dom"
import * as Yup from "yup"
import { GetCarecoachProfile } from "../../services/CarecaochServices"
import { DateFormat } from "../../utils/constant"
import onboardingApiService from "../../services/Onboarding.service"
import SnackbarMessage from "../SnackbarMessage"
import { ReactComponent as CalendarIcon } from "../../Assets/calendar.svg"
import { ReactComponent as ErrorIcon } from "../../Assets/CarePlans/error-triangle.svg"

const WhatAboutYou = () => {
  const { t } = useTranslation()
  const [inputField, setInputField] = useState({})
  const [cognitoUser, setCognitoUser] = useState({})
  const history = useHistory()
  const [showSnackbarMessage, setShowSnackbarMessage] = React.useState({
    open: false,
    message: "",
    success: false,
  })
  const [isOpen, setIsOpen] = useState(false)

  const showError = () => {
    setShowSnackbarMessage({
      open: true,
      message: "Could not update profile.",
      success: false,
    })
  }

  const formik = useFormik({
    initialValues: {
      specialities: inputField.specialities || "",
      jobStartDate: inputField.jobStartDate || new Date(),
      about: inputField.about || "",
      calendlyUsername: inputField["custom:calendly_username"] || "",
    },
    validationSchema: Yup.object({
      specialities: Yup.string().required(t("specialities-required")),
      jobStartDate: Yup.date().required(t("job-start-date")).nullable(),
      about: Yup.string().required(t("about-required")),
    }),
    onSubmit: async (values) => {
      try {
        let cognitoCareCoach
        if (
          values.jobStartDate &&
          new Date(values.jobStartDate).toString() !== "Invalid Date"
        ) {
          cognitoCareCoach = {
            "custom:job_start_date": new Date(values.jobStartDate)
              .toISOString()
              .split("T")[0],
            "custom:credentials": values.specialities,
            "custom:bio": values.about,
            "custom:calendly_username":
              cognitoUser.attributes["custom:calendly_username"] || "",
          }
        } else if (values.jobStartDate === null) {
          cognitoCareCoach = {
            "custom:job_start_date": "",
            "custom:credentials": values.specialities,
            "custom:bio": values.about,
            "custom:calendly_username":
              cognitoUser.attributes["custom:calendly_username"] || "",
          }
        } else {
          return
        }
        await Auth.updateUserAttributes(cognitoUser, cognitoCareCoach)
        const careCoachProfile = {
          firstName: cognitoUser.attributes.given_name,
          lastName: cognitoUser.attributes.family_name,
          nickName: `${cognitoUser.attributes.nickname}`,
          email: cognitoUser.attributes.email,
          phone: cognitoUser.attributes.phone_number,
          location: cognitoUser.attributes["custom:location"] || "",
          pronouns:
            cognitoUser.attributes["custom:pronoun"] === "other"
              ? cognitoUser.attributes["custom:pronoun_other"] || ""
              : cognitoUser.attributes["custom:pronoun"] || "",
          isActive: true,
          jobStartDate: new Date(values.jobStartDate)
            .toISOString()
            .split("T")[0],
          specialities: values.specialities,
          about: values.about,
          calendlyUsername:
            cognitoUser.attributes["custom:calendly_username"] || "",
          cognitoId: cognitoUser.attributes.sub,
        }
        const syncResponse = await onboardingApiService.syncCareCoach(
          careCoachProfile,
          inputField.uuid
        )
        if (syncResponse && syncResponse.status === 202) {
          history.push("/dashboard")
        } else {
          // show fail message
          showError()
        }
      } catch (error) {
        showError()
      }
    },
  })

  const SetCareCoachBio = async () => {
    const currentUser = await Auth.currentAuthenticatedUser()
    const { data } = await GetCarecoachProfile(
      currentUser.attributes["custom:uuid"]
    )
    setCognitoUser(currentUser)
    setInputField(data)
    formik.setFieldValue("specialities", data.specialities || "")
    formik.setFieldValue("jobStartDate", data.jobStartDate || null)
    formik.setFieldValue("about", data.about || "")
  }

  useEffect(() => {
    SetCareCoachBio()
  }, [])

  return (
    <div>
      <SnackbarMessage {...showSnackbarMessage} />
      <Grid container className="base-grid">
        <Grid item className="image-grid">
          <div className="image-main">
            <div className="what-about-you">
              <img
                alt={t("Onboarding")}
                className="what-about-you-logo"
                src="./Onboarding.svg"
              />
              <span className="what-about-you-text">
                {t("introduceyourself")}
              </span>
            </div>
          </div>
        </Grid>
        <Grid
          item
          xs={8}
          container
          direction="row"
          justify="center"
          alignItems="center"
        >
          <Grid item xs={9} container direction="row">
            <form id="whataboutyou" onSubmit={formik.handleSubmit}>
              <Box>
                <Typography component="div" className="what-about-you-heading">
                  {t("whataboutyou")}
                </Typography>
                <Typography component="div" className="what-about-you-tagline">
                  {t("whataboutyousubheading1")}
                </Typography>
                <Typography component="div" className="what-about-you-tagline">
                  {t("whataboutyousubheading2")}
                </Typography>
              </Box>
              <Box width="100%" pt="6%">
                <TextField
                  id="ccSpecialities"
                  name="specialities"
                  margin="dense"
                  fullWidth
                  label={t("specialities")}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.specialities}
                  error={
                    formik.touched.specialities && formik.errors.specialities
                  }
                  InputProps={{
                    endAdornment:
                      formik.touched.specialities &&
                      formik.errors.specialities ? (
                        <ErrorIcon
                          width="24"
                          height="24"
                          className={"endAdornmentSpecialities"}
                        />
                      ) : null,
                  }}
                />
                <div className={"helperText"}>
                  {formik.touched.specialities && formik.errors.specialities ? (
                    <div className={"text-danger"}>
                      {formik.errors.specialities}
                    </div>
                  ) : (
                    t("credentials")
                  )}
                </div>
              </Box>
              <Box pt="6%" width="100%" className="date-picker-hover">
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    className="datePicker"
                    disableToolbar
                    disableFuture
                    autoOk
                    variant="inline"
                    format={DateFormat}
                    fullWidth
                    margin="dense"
                    id="ccJobStartDate"
                    label={t("jobstartdate")}
                    InputProps={{
                      readOnly: true,
                      onFocus: () => {
                        setIsOpen(true)
                      },
                    }}
                    KeyboardButtonProps={{
                      onFocus: () => {
                        setIsOpen(true)
                      },
                    }}
                    PopoverProps={{
                      disableRestoreFocus: true,
                      onClose: () => {
                        setIsOpen(false)
                      },
                    }}
                    open={isOpen}
                    value={
                      formik.values.jobStartDate
                        ? formik.values.jobStartDate
                        : null
                    }
                    onChange={(value) => {
                      setIsOpen(false)
                      formik.setFieldValue("jobStartDate", value)
                    }}
                    keyboardIcon={
                      formik.touched.jobStartDate &&
                      formik.errors.jobStartDate ? (
                        <ErrorIcon width="24" height="24" />
                      ) : (
                        <CalendarIcon onClick={() => setIsOpen(true)} />
                      )
                    }
                    className={
                      formik.touched.jobStartDate && formik.errors.jobStartDate
                        ? "errorField"
                        : ""
                    }
                  />
                </MuiPickersUtilsProvider>
                <div className={"helperText"}>
                  {formik.touched.jobStartDate && formik.errors.jobStartDate ? (
                    <div className={"text-danger"}>
                      {formik.errors.jobStartDate}
                    </div>
                  ) : (
                    t("datepicker")
                  )}
                </div>
              </Box>
              <Box pt="4%" width="100%">
                <TextField
                  id="ccAbout"
                  margin="dense"
                  name="about"
                  multiline
                  fullWidth
                  inputProps={{ maxLength: 300 }}
                  label={t("aboutstatement")}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.about}
                  error={formik.touched.about && formik.errors.about}
                  InputProps={{
                    endAdornment:
                      formik.touched.about && formik.errors.about ? (
                        <ErrorIcon
                          width="24"
                          height="24"
                          className={"endAdornmentAbout"}
                        />
                      ) : null,
                  }}
                />
                <Typography
                  component="div"
                  className="about-below"
                ></Typography>
                <div className={"helperText"}>
                  {formik.touched.about && formik.errors.about ? (
                    <div className={"text-danger"}>{formik.errors.about}</div>
                  ) : (
                    <>
                      {t("aboutbelow")}
                      <br />
                      {formik.values.about.length} / {t("maxlength")}
                    </>
                  )}
                </div>
              </Box>
              <Box
                pt="22%"
                width="100%"
                display="flex"
                flexDirection="row"
                alignItems="center"
                justifyContent="space-between"
              >
                <Box display="flex" flexDirection="row" alignItems="center">
                  <h1 className="Border">
                    <img
                      src="./icons-16-px-check.svg"
                      alt={t("tick")}
                      className="Icons16pxCheck"
                    />
                  </h1>
                  {/* <div className="pagination">1</div> */}
                  <div className="pagination-connector">&#8212;</div>
                  <div className="pagination-active">2</div>
                  <div className="step-1">STEP 2 OF 2</div>
                </Box>
                <Box className="btn-next">
                  <Button
                    id="ccFinishBtn"
                    type="submit"
                    name="finish"
                    disabled={!formik.dirty}
                  >
                    {t("finish")}
                  </Button>
                </Box>
              </Box>
            </form>
          </Grid>
        </Grid>
      </Grid>
    </div>
  )
}

export default WhatAboutYou
