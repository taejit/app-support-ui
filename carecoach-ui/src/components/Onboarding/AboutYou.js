/* eslint no-shadow:0, no-console: 0 */
import "../Welcome/Welcome.scss"
import "./Onboarding.scss"
import React, { useState, useEffect } from "react"
import { useHistory } from "react-router-dom"
import {
  Box,
  Button,
  Grid,
  MenuItem,
  Select,
  TextField,
  Typography,
  FormControl,
  InputLabel,
} from "@material-ui/core"
import { useFormik } from "formik"
import * as Yup from "yup"
import { useTranslation } from "react-i18next"
import { Auth } from "aws-amplify"
import FormatPhoneNumber from "../../utils/FormatPhoneNumber"
import LocationSearch from "../LocationSearch"
import OnboardingCareCoachAvatar from "../OnboardingCareCoachAvatar"
import onboardingApiService from "../../services/Onboarding.service"
import cognitoPhoneNumberFormat from "../../utils/cognitoPhoneNumberFormat"
import { ReactComponent as ErrorIcon } from "../../Assets/CarePlans/error-triangle.svg"
import useStyles from "./styles"
import getLocationDisplayText from "../../utils/getLocationDisplayText"

const AboutYou = () => {
  const classes = useStyles()
  const history = useHistory()
  const { t } = useTranslation()
  const [inputField, setInputField] = useState({})
  const [careCoachUuid, setcareCoachUuid] = useState(null)

  const [phone, setPhone] = useState("")
  const [customTextBox, setCustomTextBox] = useState("none")
  const [displayCustomPronoun, setdisplayCustomPronoun] = useState(false)

  const [isProfileUploaded, SetProfileUploaded] = useState(false)
  const [profileError, SetProfileError] = useState("")
  const [locationError, setLocationError] = useState(false)

  const pronounOptions = ["He, Him", "She, Her", "They, Them"]

  const formik = useFormik({
    initialValues: {
      firstName: inputField.given_name || "",
      lastName: inputField.family_name || "",
      pronouns:
        inputField["custom:pronoun"] &&
        (pronounOptions.includes(inputField["custom:pronoun"])
          ? inputField["custom:pronoun"]
          : "Other"),
      customPronoun: inputField["custom:pronoun"] || "",
      email: inputField.email || "",
      phone: inputField.phone_number || "",
      location: inputField["custom:location"] || "",
      calendlyUsername: inputField["custom:calendly_username"] || "",
    },
    enableReinitialize: true,
    validationSchema: Yup.object({
      firstName: Yup.string().required(t("edit-profile-first-name-required")),
      lastName: Yup.string().required(t("edit-profile-last-name-required")),
      pronouns: Yup.string().required(t("edit-profile-pronouns-required")),
      email: Yup.string()
        .email(t("invalidemail"))
        .required(t("edit-profile-email-required")),
      phone: Yup.string().required(t("edit-profile-phone-required")),
      location: Yup.string().required(t("edit-profile-location-required")),
      customPronoun: Yup.string().when("pronouns", (pronounsValue) => {
        if (pronounsValue == "Other") {
          return Yup.string().required(t("custompronoun-required")).nullable()
        } else {
          return Yup.string()
        }
      }),
    }),
    onSubmit: async (values) => {
      if (isProfileUploaded) {
        const currentUser = await Auth.currentAuthenticatedUser()
        const cognitoCareCoach = {
          given_name: values.firstName,
          family_name: values.lastName,
          email: values.email,
          phone_number: values.phone.includes("+1")
            ? values.phone
            : cognitoPhoneNumberFormat(phone),
          "custom:location": values.location || "",
          "custom:pronoun": pronounOptions.includes(values.pronouns)
            ? values.pronouns
            : values.customPronoun,
          "custom:calendly_username": values.calendlyUsername || "",
        }
        setLocationError(false)
        Auth.updateUserAttributes(currentUser, cognitoCareCoach)
          .then(() => {
            const careCoachProfile = {
              firstName: values.firstName,
              lastName: values.lastName,
              nickName: `${values.firstName} ${values.lastName}`,
              email: values.email,
              phone: values.phone.includes("+1")
                ? values.phone
                : cognitoPhoneNumberFormat(phone),
              location: values.location || "",
              pronouns: pronounOptions.includes(values.pronouns)
                ? values.pronouns
                : values.customPronoun,
              jobStartDate:
                currentUser.attributes["custom:job_start_date"] || "",
              specialities: currentUser.attributes["custom:credentials"] || "",
              about: currentUser.attributes["custom:bio"] || "",
              isActive: true,
              calendlyUsername:
                currentUser.attributes["custom:calendly_username"] || "",
              cognitoId: currentUser.attributes.sub,
            }
            onboardingApiService
              .syncCareCoach(careCoachProfile, careCoachUuid)
              .then((apiResponse) => {
                if (apiResponse && apiResponse.status === 202) {
                  // eslint-disable-next-line react/prop-types
                  history.push({
                    pathname: "/what-about-you",
                    state: "Changes Saved",
                  })
                }
              })
          })
          .catch((error) => {
            console.log(error)
          })
      } else {
        SetProfileError("Error")
      }
    },
  })

  const changeSelect = (value) => {
    formik.setFieldValue("pronouns", value, true)
    if (value === "Other") {
      setCustomTextBox("block")
      setdisplayCustomPronoun(true)
      formik.setFieldValue("customPronoun", "", true)
    } else {
      setCustomTextBox("none")
      setdisplayCustomPronoun(false)
    }
  }

  const changeLocationValue = (location) => {
    setLocationError(!location)
    if (location && location.description) {
      const changedLocationValue = getLocationDisplayText(location.description)
      formik.setFieldValue("location", changedLocationValue)
    }
  }

  function showPronoun(pronounValue) {
    if (pronounOptions.includes(pronounValue)) {
      formik.setFieldValue("pronouns", pronounValue)
    } else {
      formik.setFieldValue("pronouns", "Other", true)
      formik.setFieldValue("customProunoun", pronounValue, true)
      setdisplayCustomPronoun(true)
      setCustomTextBox("block")
    }
  }

  useEffect(() => {
    function getUserInfo() {
      return Auth.currentUserInfo()
    }
    getUserInfo().then((userInfo) => {
      setInputField(userInfo.attributes)
      if (typeof userInfo.attributes["custom:pronoun"] !== "undefined") {
        showPronoun(userInfo.attributes["custom:pronoun"])
      }
      setcareCoachUuid(userInfo.attributes["custom:uuid"])
    })
  }, [])

  const normalizeInput = (value) => {
    const tempInputfield = inputField
    tempInputfield.phone = value
    setInputField(tempInputfield)
    formik.setFieldValue("phone", tempInputfield.phone, true)
    const result = FormatPhoneNumber(value)
    setPhone(result)
  }

  const handleProfile = (profVal) => {
    if (profVal && profVal?.status === "uploadSuccess") {
      SetProfileUploaded(true)
      SetProfileError("")
    } else {
      SetProfileUploaded(false)
      SetProfileError("Error")
    }
  }

  const checkFieldError = (value) => {
    return formik.touched[value] && formik.errors[value]
  }

  // short circuit in JS
  const ErrorIconFunc = (value) => {
    return (
      checkFieldError(value) && (
        <div className={"errorIcon"}>
          <ErrorIcon width="24" height="24" />
        </div>
      )
    )
  }

  const ErrorTextField = (value) => {
    return (
      <div className={"helperText"}>
        {checkFieldError(value) ? (
          <div className={"text-danger"}>{formik.errors[value]}</div>
        ) : null}
      </div>
    )
  }

  return (
    <div>
      <Grid container className="about-you-base-grid">
        <Grid item xs={4} className="about-you-image-grid">
          <div className="about-you-image-main">
            <div className="about-you-img">
              <img
                alt="welcome"
                className="about-you-logo"
                src="./Onboarding.svg"
              />
              <span className="about-you-text">{t("aboutyouintro")}</span>
            </div>
          </div>
        </Grid>
        <Grid
          item
          xs={4}
          container
          direction="row"
          justify="center"
          alignItems="center"
        >
          <Grid item xs={9} container direction="row">
            <form
              id="aboutyou"
              onSubmit={formik.handleSubmit}
              data-testid="aboutyouform"
            >
              <Box>
                <Typography component="div" className="about-you-heading">
                  {t("aboutyou")}
                </Typography>
                <Typography component="div" className="about-you-tagline">
                  {t("aboutyousecondarytext")}
                </Typography>
              </Box>
              <Box width="100%" pt="6%">
                <TextField
                  id="ccFirstName"
                  name="firstName"
                  margin="dense"
                  fullWidth
                  label={t("firstname")}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.firstName}
                  error={checkFieldError("firstName")}
                  InputProps={{
                    endAdornment: ErrorIconFunc("firstName"),
                  }}
                />
                {ErrorTextField("firstName")}
              </Box>
              <Box pt="4%" width="100%">
                <TextField
                  id="ccLastName"
                  name="lastName"
                  margin="dense"
                  fullWidth
                  label={t("lastname")}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.lastName}
                  error={checkFieldError("lastName")}
                  InputProps={{
                    endAdornment: ErrorIconFunc("lastName"),
                  }}
                />
                {ErrorTextField("lastName")}
              </Box>
              <Box pt="4%" width="100%">
                <FormControl fullWidth error={checkFieldError("pronouns")}>
                  <InputLabel htmlFor="pronouns">{t("pronouns")}</InputLabel>
                  <Select
                    inputProps={{
                      "data-testid": "ccPronouns",
                      id: "ccPronouns",
                    }}
                    name="pronouns"
                    className="pronouns-select"
                    selected={formik.values.pronouns}
                    value={formik.values.pronouns || showPronoun}
                    onChange={(e) => changeSelect(e.target.value)}
                    onBlur={formik.handleBlur}
                    endAdornment={
                      <div className={"selecthelperText"}>
                        {ErrorIconFunc("pronouns")}
                      </div>
                    }
                  >
                    <MenuItem value="He, Him">He, Him</MenuItem>
                    <MenuItem value="She, Her">She, Her</MenuItem>
                    <MenuItem value="They, Them">They, Them</MenuItem>
                    <MenuItem value="Other">Other</MenuItem>
                  </Select>
                </FormControl>
                {ErrorTextField("pronouns")}
              </Box>

              {displayCustomPronoun && (
                <Box pt="4%" width="100%" display={customTextBox}>
                  <TextField
                    id="ccCustomPronoun"
                    margin="dense"
                    name="customPronoun"
                    fullWidth
                    label={t("pronountextbox")}
                    onChange={formik.handleChange}
                    value={formik.values.customPronoun}
                    error={checkFieldError("customPronoun")}
                    InputProps={{
                      endAdornment: ErrorIconFunc("customPronoun"),
                    }}
                    {...formik.getFieldProps("customPronoun")}
                  />
                  {ErrorTextField("customPronoun")}
                </Box>
              )}
              <Box pt="4%" width="100%">
                <TextField
                  id="ccEmail"
                  margin="dense"
                  name="email"
                  fullWidth
                  label={t("email")}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.email}
                  error={checkFieldError("email")}
                  InputProps={{
                    endAdornment: ErrorIconFunc("email"),
                  }}
                />
                {ErrorTextField("email")}
              </Box>
              <Box pt="4%" width="100%">
                <TextField
                  id="ccPhone"
                  margin="dense"
                  name="phone"
                  fullWidth
                  InputLabelProps={{ shrink: true }}
                  label={t("mobilephone")}
                  onChange={(e) => normalizeInput(e.target.value)}
                  onBlur={formik.handleBlur}
                  value={phone || FormatPhoneNumber(inputField.phone_number)}
                  disabled="true"
                  error={checkFieldError("phone")}
                  InputProps={{
                    endAdornment: ErrorIconFunc("phone"),
                    classes: { underline: classes.underline },
                  }}
                />
                {ErrorTextField("phone")}
              </Box>
              <Box pt="4%" width="100%">
                <LocationSearch
                  updateLocation={changeLocationValue}
                  selectedLocation={inputField["custom:location"] || ""}
                  error={checkFieldError("location") || locationError}
                />
                <div className={"helperText"}>
                  {checkFieldError("location") || locationError ? (
                    <div className={"text-danger"}>
                      {t("edit-profile-location-required")}
                    </div>
                  ) : null}
                </div>
              </Box>
              <Box
                pt="22%"
                width="100%"
                display="flex"
                flexDirection="row"
                alignItems="center"
                justifyContent="space-between"
                paddingBottom="72px"
              >
                <Box display="flex" flexDirection="row" alignItems="center">
                  <div className="pagination-active">1</div>
                  <div className="pagination-connector">&#8212;</div>
                  <div className="pagination">2</div>
                  <div className="step-1">STEP 1 OF 2</div>
                </Box>
                <Box className="about-you-btn-next">
                  <Button
                    id="ccNextButton"
                    type="submit"
                    name="next"
                    className="about-you-nextBtn"
                  >
                    {t("next")}
                  </Button>
                </Box>
              </Box>
            </form>
          </Grid>
        </Grid>
        <Grid
          container
          item
          xs={4}
          direction="row"
          justify="flex-start"
          alignItems="flex-start"
        >
          <Grid item xs={12}>
            <OnboardingCareCoachAvatar
              displayOrEdit={false}
              handleProfile={handleProfile}
              ProfError={profileError}
            />
          </Grid>
        </Grid>
      </Grid>
    </div>
  )
}

export default AboutYou
