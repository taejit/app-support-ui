import React from "react"
import { fireEvent, render, screen, waitFor } from "@testing-library/react"
import { createMemoryHistory } from "history"
import { Router } from "react-router-dom"
import { Auth } from "aws-amplify"
import AboutYou from "./AboutYou"
import OnboardingApiService from "../../services/Onboarding.service"
import LocationSearch from "../LocationSearch/index"

describe("AboutYou component testing", () => {
  beforeEach(() => {
    jest.spyOn(Auth, "currentAuthenticatedUser").mockResolvedValue(
      Promise.resolve({
        attributes: {
          email: "bramsai@intraedge.com",
          email_verified: true,
          phone_number: "+12254789321",
          phone_number_verified: true,
          sub: "38041359-614f-4c44-9a32-23c3c80506c7",
          family_name: "kiran",
          given_name: "ramsai",
          "custom:location": "Hyderabad Telangana",
          "custom:pronoun": "he/his",
          "custom:uuid": "test-uuid",
          "custom:job_start_date": new Date("2021-01-30"),
          "custom:bio": "bio",
          "custom:credentials": "test-credentials",
        },
        username: "acc2ea0f-3a1f-49b9-8b19-7f555ece71d6",
      })
    )
    jest.spyOn(Auth, "updateUserAttributes").mockResolvedValue({})
    jest.spyOn(Auth, "currentUserInfo").mockResolvedValue(
      Promise.resolve({
        attributes: {
          email: "bramsai@intraedge.com",
          email_verified: true,
          phone_number: "+12254789321",
          phone_number_verified: true,
          sub: "38041359-614f-4c44-9a32-23c3c80506c7",
          family_name: "kiran",
          given_name: "ramsai",
          "custom:location": "Hyderabad Telangana",
          "custom:pronoun": "he/his",
          "custom:uuid": "test-uuid",
          "custom:job_start_date": new Date("2021-01-30"),
          "custom:bio": "bio",
          "custom:credentials": "test-credentials",
        },
        username: "acc2ea0f-3a1f-49b9-8b19-7f555ece71d6",
      })
    )
    jest
      .spyOn(OnboardingApiService, "getCareCoachProfileImage")
      .mockResolvedValue({
        data: {
          presignedUrl: "testImgUrl",
        },
      })
    jest
      .spyOn(OnboardingApiService, "checkProfileImageUrl")
      .mockResolvedValue({})

    jest.spyOn(OnboardingApiService, "syncCareCoach").mockResolvedValue({
      status: 202,
    })
  })
  it("Should display aboutyou page", async () => {
    const history = createMemoryHistory()
    history.push = jest.fn()
    history.push("/aboutyou")
    await waitFor(async () => {
      render(
        <Router history={history}>
          <AboutYou />
        </Router>
      )
    })
    expect(history.push).toHaveBeenCalled()
  })

  it("check firstName", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<AboutYou />)
    })
    const { container, queryByText } = wrapper

    const wrapperFirstName = container.querySelector("#ccFirstName")

    await waitFor(() => {
      fireEvent.change(wrapperFirstName, {
        target: {
          value: "",
          name: "firstName",
        },
      })
      fireEvent.blur(wrapperFirstName, {
        target: {
          value: "",
          name: "firstName",
        },
      })
    })
    expect(queryByText(/^required$/)).toBeDefined()
  })

  it("check lastName", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<AboutYou />)
    })
    const { container, queryByText } = wrapper

    const wrapperLastName = container.querySelector("#ccLastName")

    await waitFor(() => {
      fireEvent.change(wrapperLastName, {
        target: {
          value: "",
          name: "lastName",
        },
      })
      fireEvent.blur(wrapperLastName, {
        target: {
          value: "",
          name: "lastName",
        },
      })
    })
    expect(queryByText(/^required$/)).toBeDefined()
  })

  it("check email", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<AboutYou />)
    })
    const { container, queryByText } = wrapper

    const wrapperEmail = container.querySelector("#ccEmail")

    await waitFor(() => {
      fireEvent.change(wrapperEmail, {
        target: {
          value: "",
          name: "email",
        },
      })
      fireEvent.blur(wrapperEmail, {
        target: {
          value: "",
          name: "email",
        },
      })
    })
    expect(queryByText(/^required$/)).toBeDefined()
  })

  it("check phone", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<AboutYou />)
    })
    const { container, queryByText } = wrapper

    const wrapperPhone = container.querySelector("#ccPhone")

    await waitFor(() => {
      fireEvent.change(wrapperPhone, {
        target: {
          value: "",
          name: "phone",
        },
      })
      fireEvent.blur(wrapperPhone, {
        target: {
          value: "",
          name: "phone",
        },
      })
    })
    expect(queryByText(/^required$/)).toBeDefined()
  })

  it("get profile image url rejects with error", async () => {
    jest
      .spyOn(OnboardingApiService, "getCareCoachProfileImage")
      .mockRejectedValue({
        response: {
          status: 400,
        },
      })
    let wrapper
    await waitFor(() => {
      wrapper = render(<AboutYou />)
    })
    const { container } = wrapper
    const getErrorText = container.querySelector("#getError")
    expect(getErrorText).toBeDefined()
  })

  it("check profile image url sends 404 status", async () => {
    jest.spyOn(OnboardingApiService, "checkProfileImageUrl").mockRejectedValue({
      response: {
        status: 404,
      },
    })
    let wrapper
    await waitFor(() => {
      wrapper = render(<AboutYou />)
    })
    const { container } = wrapper
    const defaultText = container.querySelector("#defaultText")
    expect(defaultText).toBeDefined()
  })

  it("update profile image success", async () => {
    jest
      .spyOn(OnboardingApiService, "getPresignedUrlForCareCoachProfileImage")
      .mockResolvedValue({
        data: {
          url: "testPresignedUrl",
        },
      })
    jest
      .spyOn(OnboardingApiService, "uploadImageThroughPresignedUrl")
      .mockResolvedValue({ status: 200 })
    let wrapper
    await waitFor(() => {
      wrapper = render(<AboutYou />)
    })
    const { container } = wrapper
    const defaultText = container.querySelector("#ccUploadButton")
    expect(defaultText).toBeDefined()
    const uploadBtn = container.querySelector("#profile-image-button-file")
    expect(uploadBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.change(uploadBtn, {
        target: {
          files: [{ name: "samplefile.png", size: 999 }],
        },
      })
    })
    expect(
      OnboardingApiService.getPresignedUrlForCareCoachProfileImage
    ).toHaveBeenCalled()
    expect(
      OnboardingApiService.uploadImageThroughPresignedUrl
    ).toHaveBeenCalled()
  })

  it("update profile image: extension error", async () => {
    jest
      .spyOn(OnboardingApiService, "getPresignedUrlForCareCoachProfileImage")
      .mockResolvedValue({
        data: {
          url: "testPresignedUrl",
        },
      })
    jest
      .spyOn(OnboardingApiService, "uploadImageThroughPresignedUrl")
      .mockResolvedValue({})
    let wrapper
    await waitFor(() => {
      wrapper = render(<AboutYou />)
    })
    const { container } = wrapper
    const defaultText = container.querySelector("#ccUploadButton")
    expect(defaultText).toBeDefined()
    const uploadBtn = container.querySelector("#profile-image-button-file")
    expect(uploadBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.change(uploadBtn, {
        target: {
          files: [{ name: "samplefile.png1" }],
        },
      })
    })
    const extErrorText = container.querySelector("#extError")
    expect(extErrorText).toBeDefined()
  })

  it("update profile image: size error", async () => {
    jest
      .spyOn(OnboardingApiService, "getPresignedUrlForCareCoachProfileImage")
      .mockResolvedValue({
        data: {
          url: "testPresignedUrl",
        },
      })
    jest
      .spyOn(OnboardingApiService, "uploadImageThroughPresignedUrl")
      .mockResolvedValue({})
    let wrapper
    await waitFor(() => {
      wrapper = render(<AboutYou />)
    })
    const { container } = wrapper
    const defaultText = container.querySelector("#ccUploadButton")
    expect(defaultText).toBeDefined()
    const uploadBtn = container.querySelector("#profile-image-button-file")
    expect(uploadBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.change(uploadBtn, {
        target: {
          files: [{ name: "samplefile.png", size: 999999999 }],
        },
      })
    })
    const sizeErrorText = container.querySelector("#sizeError")
    expect(sizeErrorText).toBeDefined()
  })

  it("<LocationSearch />", async () => {
    const updateLocation = ""
    const selectedLocation = "Hyderabad Telanagana India"
    const { getByRole, debug } = render(
      <LocationSearch
        updateLocation={updateLocation}
        selectedLocation={selectedLocation}
      />
    )
    const input = getByRole("textbox")

    await waitFor(() => {
      fireEvent.change(input, { target: { value: "search" } })
    })
    expect(input).toHaveValue("search")
    jest.advanceTimersByTime(600)
    expect(selectedLocation).toBe("Hyderabad Telanagana India")
    debug()
  })

  it("clicks submit button", async () => {
    render(<AboutYou />)
    screen.getByRole("button", { name: /next/i })
  })

  it("form submit when clicking on nextbutton", async () => {
    jest
      .spyOn(OnboardingApiService, "getPresignedUrlForCareCoachProfileImage")
      .mockResolvedValue({
        data: {
          url: "testPresignedUrl",
        },
      })
    jest
      .spyOn(OnboardingApiService, "uploadImageThroughPresignedUrl")
      .mockResolvedValue({ status: 200 })

    const history = createMemoryHistory()
    history.push = jest.fn()

    let wrapper
    await waitFor(async () => {
      wrapper = render(
        <Router history={history}>
          <AboutYou />
        </Router>
      )
    })
    const { queryByText, container } = wrapper

    const defaultText = container.querySelector("#ccUploadButton")
    expect(defaultText).toBeDefined()
    const uploadBtn = container.querySelector("#profile-image-button-file")
    expect(uploadBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.change(uploadBtn, {
        target: {
          files: [{ name: "samplefile.png", size: 999 }],
        },
      })
    })

    const submitBtn = container.querySelector("#aboutyou")
    expect(container.querySelector("#ccFirstName").value).toEqual("ramsai")
    expect(container.querySelector("#ccLastName").value).toEqual("kiran")
    expect(container.querySelector("#ccPronouns").value).toEqual("Other")
    expect(container.querySelector("#ccEmail").value).toEqual(
      "bramsai@intraedge.com"
    )
    expect(container.querySelector("#ccPhone").value).toEqual("(225) 478-9321")
    expect(container.querySelector("#google-map-demo").value).toEqual(
      "Hyderabad Telangana"
    )

    expect(
      OnboardingApiService.getPresignedUrlForCareCoachProfileImage
    ).toHaveBeenCalled()
    expect(
      OnboardingApiService.uploadImageThroughPresignedUrl
    ).toHaveBeenCalled()

    await waitFor(() => {
      fireEvent.click(submitBtn)
      fireEvent.submit(submitBtn)
    })
    expect(queryByText(/^next$/)).toBeDefined()
  })

  it("form submit not clickable", async () => {
    const history = createMemoryHistory()
    history.push = jest.fn()

    let wrapper
    await waitFor(async () => {
      wrapper = render(
        <Router history={history}>
          <AboutYou />
        </Router>
      )
    })
    const { queryByText, container } = wrapper

    const submitBtn = container.querySelector("#aboutyou")
    expect(container.querySelector("#ccFirstName").value).toEqual("ramsai")
    expect(container.querySelector("#ccLastName").value).toEqual("kiran")
    expect(container.querySelector("#ccPronouns").value).toEqual("Other")
    expect(container.querySelector("#ccEmail").value).toEqual(
      "bramsai@intraedge.com"
    )
    expect(container.querySelector("#ccPhone").value).toEqual("(225) 478-9321")
    expect(container.querySelector("#google-map-demo").value).toEqual(
      "Hyderabad Telangana"
    )

    await waitFor(() => {
      fireEvent.click(submitBtn)
      fireEvent.submit(submitBtn)
    })
    expect(queryByText(/^next$/)).toBeDefined()
  })

  it("show pronouns - She, Her", async () => {
    jest.spyOn(Auth, "currentUserInfo").mockResolvedValue(
      Promise.resolve({
        attributes: {
          email: "bramsai@intraedge.com",
          email_verified: true,
          phone_number: "+12254789321",
          phone_number_verified: true,
          sub: "38041359-614f-4c44-9a32-23c3c80506c7",
          family_name: "kiran",
          given_name: "ramsai",
          "custom:location": "Hyderabad Telangana",
          "custom:pronoun": "She, Her",
          "custom:uuid": "test-uuid",
          "custom:job_start_date": new Date("2021-01-30"),
          "custom:bio": "bio",
          "custom:credentials": "test-credentials",
        },
        username: "acc2ea0f-3a1f-49b9-8b19-7f555ece71d6",
      })
    )
    const history = createMemoryHistory()
    history.push = jest.fn()

    let wrapper
    await waitFor(async () => {
      wrapper = render(
        <Router history={history}>
          <AboutYou />
        </Router>
      )
    })
    const { container } = wrapper
    expect(container.querySelector("#ccPronouns").value).toEqual("She, Her")
  })

  it("show pronouns - custom other", async () => {
    jest.spyOn(Auth, "currentUserInfo").mockResolvedValue(
      Promise.resolve({
        attributes: {
          email: "bramsai@intraedge.com",
          email_verified: true,
          phone_number: "+12254789322",
          phone_number_verified: true,
          sub: "38041359-614f-4c44-9a32-23c3c80506c7",
          family_name: "kiran",
          given_name: "ramsai",
          "custom:location": "Hyderabad Telangana",
          "custom:pronoun": "other value",
          "custom:uuid": "test-uuid",
          "custom:job_start_date": new Date("2021-01-30"),
          "custom:bio": "bio",
          "custom:credentials": "test-credentials",
        },
        username: "acc2ea0f-3a1f-49b9-8b19-7f555ece71d6",
      })
    )
    const history = createMemoryHistory()
    history.push = jest.fn()

    let wrapper
    await waitFor(async () => {
      wrapper = render(
        <Router history={history}>
          <AboutYou />
        </Router>
      )
    })
    const { container } = wrapper
    expect(container.querySelector("#ccPronouns").value).toEqual("Other")
  })
})
