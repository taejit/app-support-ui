import { makeStyles } from "@material-ui/core/styles"

export default makeStyles((theme) => ({
  underline: {
    "&:hover": {
      "&:before": {
        borderBottom: ["1px solid", "!important"],
      },
    },
    "&:before": {
      borderBottom: ["1px solid", "!important"],
    },
  },
}))
