import React from "react"
import { fireEvent, render, screen, waitFor } from "@testing-library/react"
import { createMemoryHistory } from "history"
import { Router } from "react-router-dom"
import { Auth } from "aws-amplify"
import WhatAboutYou from "./WhatAboutYou"
import OnboardingApiService from "../../services/Onboarding.service"
import * as CarecaochServices from "../../services/CarecaochServices"

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "./whataboutyou",
    profileData: {
      uuid: "c1260a8b-28f3-4b94-88ff-9bdb3da94f37",
      firstName: "John",
      lastName: "Coach",
      email: "user@example.com",
      phone: "6021234567",
      pronouns: "he/his",
      isActive: true,
      jobTitle: "Nurse",
      jobStartDate: "2020-12-31",
      location: "Locations tring",
      specialities: "String values",
      about: "A long description for the bio of the care coach in rich text",
      profileImage: "https://signedurl-profile-example.com",
      createdAt: "2020-12-31T07:53:20.908Z",
      updatedAt: "2020-12-31T07:53:20.908Z",
    },
  }),
}))

describe("WhatAboutYou component testing", () => {
  beforeEach(() => {
    jest.spyOn(Auth, "currentAuthenticatedUser").mockResolvedValue({
      attributes: {
        email: "bramsai@intraedge.com",
        email_verified: true,
        phone_number: "+12254789321",
        phone_number_verified: true,
        sub: "38041359-614f-4c44-9a32-23c3c80506c7",
        family_name: "kiran",
        given_name: "ramsai",
        location: "Hyderabad Telangana Hyd",
        "custom:uuid": "38041359-614f-4c44-9a32-23c3c8050327",
        "custom:specialities": "MSW",
      },
    })

    jest.spyOn(Auth, "updateUserAttributes").mockResolvedValue({})

    jest.spyOn(Auth, "currentUserInfo").mockResolvedValue(
      Promise.resolve({
        attributes: {
          email: "bramsai@intraedge.com",
          email_verified: true,
          phone_number: "+12254789321",
          phone_number_verified: true,
          sub: "38041359-614f-4c44-9a32-23c3c80506c7",
          family_name: "kiran",
          given_name: "ramsai",
          "custom:location": "Hyderabad Telangana Hyd",
          "custom:pronoun": "he/his",
          "custom:uuid": "38041359-614f-4c44-9a32-23c3c8050327",
          "custom:specialities": "MSW",
        },
        username: "acc2ea0f-3a1f-49b9-8b19-7f555ece71d6",
      })
    )
    jest.spyOn(CarecaochServices, "GetCarecoachProfile").mockResolvedValue(
      Promise.resolve({
        data: {
          specialities: "MSW",
          jobStartDate: new Date("2021-01-30"),
          about: "Developer",
        },
      })
    )
    jest.spyOn(OnboardingApiService, "syncCareCoach").mockResolvedValue({
      status: 202,
    })
  })

  it("Should display whataboutyou page", async () => {
    const history = createMemoryHistory()
    history.push = jest.fn()
    history.push("/what-about-you")
    await waitFor(async () => {
      render(
        <Router history={history}>
          <WhatAboutYou />
        </Router>
      )
    })
    expect(history.push).toHaveBeenCalled()
  })

  it("check specialities", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<WhatAboutYou />)
    })
    const { container, queryByText } = wrapper

    const wrapperSpecialities = container.querySelector("#ccSpecialities")

    await waitFor(() => {
      fireEvent.change(wrapperSpecialities, {
        target: {
          value: "123",
          name: "specialities",
        },
      })
      fireEvent.blur(wrapperSpecialities, {
        target: {
          value: "",
          name: "specialities",
        },
      })
    })
    expect(wrapperSpecialities.value).toBe("123")
    expect(queryByText(/^required$/)).toBeDefined()
  })

  it("check JobStartDate", async () => {
    const inst = new Date()

    let wrapper
    await waitFor(() => {
      wrapper = render(<WhatAboutYou />)
    })
    const { container, queryByText } = wrapper

    const wrapperJobStartDate = container.querySelector("#ccJobStartDate")

    expect(wrapperJobStartDate.value).toEqual("January 30th, 2021")
    const setIsOpen = jest.fn()

    await waitFor(() => {
      fireEvent.change(
        wrapperJobStartDate,
        {
          target: {
            value: inst,
            name: "jobStartDate",
            InputProps: {
              readOnly: true,
              onFocus: () => {
                setIsOpen()
              },
            },
          },
        },
        setIsOpen()
      )
      fireEvent.blur(wrapperJobStartDate, {
        target: {
          value: inst,
          name: "jobStartDate",
        },
      })
    })
    expect(wrapperJobStartDate.value).toBeDefined()
    expect(queryByText(/^required$/)).toBeDefined()
    expect(setIsOpen).toHaveBeenCalledTimes(1)
  })

  it("check JobStartDate with keyboard input", async () => {
    const inst = new Date()

    let wrapper
    await waitFor(() => {
      wrapper = render(<WhatAboutYou />)
    })
    const { container, queryByText } = wrapper

    const wrapperJobStartDate = container.querySelector("#ccJobStartDate")

    expect(wrapperJobStartDate.value).toEqual("January 30th, 2021")
    const setIsOpen = jest.fn()

    await waitFor(() => {
      fireEvent.keyPress(
        wrapperJobStartDate,
        {
          target: {
            value: inst,
            name: "jobStartDate",
            InputProps: {
              readOnly: true,
              onFocus: () => {
                setIsOpen()
              },
            },
          },
        },
        setIsOpen()
      )
      fireEvent.blur(wrapperJobStartDate, {
        target: {
          value: inst,
          name: "jobStartDate",
        },
      })
    })
    expect(setIsOpen).toHaveBeenCalled()
    expect(wrapperJobStartDate.value).toBeDefined()
    expect(queryByText(/^required$/)).toBeDefined()
  })

  it("check About", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<WhatAboutYou />)
    })
    const { container, queryByText } = wrapper

    const wrapperCheckAbout = container.querySelector("#ccAbout")

    await waitFor(() => {
      fireEvent.change(wrapperCheckAbout, {
        target: {
          value: "",
          name: "About",
        },
      })
      fireEvent.blur(wrapperCheckAbout, {
        target: {
          value: "",
          name: "About",
        },
      })
    })
    expect(wrapperCheckAbout.value).toBe("Developer")
    expect(queryByText(/^required$/)).toBeDefined()
  })

  it("check form", async () => {
    const history = createMemoryHistory()

    let wrapper
    await waitFor(async () => {
      wrapper = render(
        <Router history={history}>
          <WhatAboutYou />
        </Router>
      )
    })

    history.push = jest.fn()

    const { container, queryByText } = wrapper

    const wrapperFormAboutyou = container.querySelector("#whataboutyou")
    expect(wrapperFormAboutyou).toBeTruthy()

    await waitFor(() => {
      fireEvent.submit(wrapperFormAboutyou)
    })

    expect(queryByText(/^required$/)).toBeDefined()
    expect(history.push).toHaveBeenCalled()

    expect(queryByText(/^Finish$/)).toBeDefined()
  })

  it("clicks submit button", async () => {
    render(<WhatAboutYou />)
    screen.getByRole("button", { name: /finish/i })
  })

  it("form submit when clicking on nextbutton", async () => {
    const history = createMemoryHistory()
    history.push = jest.fn()

    let wrapper
    await waitFor(async () => {
      wrapper = render(
        <Router history={history}>
          <WhatAboutYou />
        </Router>
      )
    })
    const { queryByText, container } = wrapper
    const rightClick = { button: 2 }
    const submitBtn = container.querySelector("#ccFinishBtn")
    expect(container.querySelector("#ccSpecialities").value).toEqual("MSW")
    expect(container.querySelector("#ccJobStartDate").value).toEqual(
      "January 30th, 2021"
    )
    expect(container.querySelector("#ccAbout").value).toEqual("Developer")
    await waitFor(() => {
      fireEvent.click(submitBtn, rightClick)
    })
    expect(queryByText(/^Finish$/)).toBeDefined()
  })

  it("if SyncCareCoah fails: error on sync care coach", async () => {
    jest.spyOn(OnboardingApiService, "syncCareCoach").mockResolvedValue({
      status: 400,
    })
    let wrapper
    await waitFor(() => {
      wrapper = render(<WhatAboutYou />)
    })
    const { container } = wrapper
    const submitBtn = container.querySelector("#ccFinishBtn")
    await waitFor(() => {
      fireEvent.submit(submitBtn)
    })
    expect(Auth.currentAuthenticatedUser).toHaveBeenCalled()
  })

  it("Should click on submit button: error on saving to changes", async () => {
    jest.spyOn(Auth, "updateUserAttributes").mockResolvedValue("FAILED")

    let wrapper
    await waitFor(() => {
      wrapper = render(<WhatAboutYou />)
    })
    const { container } = wrapper
    const submitBtn = container.querySelector("#ccFinishBtn")
    await waitFor(() => {
      fireEvent.submit(submitBtn)
    })
    expect(Auth.currentAuthenticatedUser).toHaveBeenCalled()
  })
})
