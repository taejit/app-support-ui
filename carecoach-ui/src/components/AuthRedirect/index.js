import React from "react"
import { Auth } from "aws-amplify"
import { CognitoJwtVerifier } from "aws-cognito-jwt-verifier"
import PreOnboardingCheck from "../PreOnboardingCheck"
import Forbidden from "../Forbidden"

/* eslint camelcase:0 */
class AuthRedirect extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showPreOnboardingCheck: false,
      showForbidden: false,
    }
    this.verify = new CognitoJwtVerifier()
  }

  async componentDidMount() {
    const currentSession = await Auth.currentSession().catch(() => {})
    if (currentSession) {
      const isClaimValid = await this.verifyTokenClaim(
        currentSession.accessToken.jwtToken
      )
      // if authenticated user is carecoach then let in
      if (isClaimValid) {
        this.setShowHome()
      } else {
        this.setState({ showForbidden: true })
      }
    } else {
      Auth.federatedSignIn()
    }
  }

  setShowHome() {
    this.setState({ showPreOnboardingCheck: true })
  }

  async verifyTokenClaim(access_token) {
    try {
      const result = await this.verify.checkJwt(
        access_token,
        window._env_.Aws_Region,
        window._env_.Cognito_Pool_Id
      )

      const claim = result ? JSON.parse(result).data : null
      const groups = claim ? claim["cognito:groups"] : null
      return groups && groups.includes("carecoach")
    } catch (error) {
      return false
    }
  }

  render() {
    const { showForbidden, showPreOnboardingCheck } = this.state
    return (
      <>
        {showForbidden && <Forbidden />}
        {showPreOnboardingCheck && <PreOnboardingCheck />}
      </>
    )
  }
}

export default AuthRedirect
