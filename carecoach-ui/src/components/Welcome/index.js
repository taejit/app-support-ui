import "./Welcome.scss"
import React from "react"
import { useTranslation } from "react-i18next"
import { useHistory } from "react-router-dom"

function App() {
  const { t } = useTranslation()
  const history = useHistory()
  const getStarted = () => {
    history.push("/aboutyou")
  }
  return (
    <div className="App">
      <img
        alt="welcome"
        src="./images/welcome/image-banners-desktop-large.png"
        srcSet="./images/welcome/image-banners-desktop-large@2x.png 2x,
        ./images/welcome/image-banners-desktop-large@3x.png 3x"
        className="welcome-img"
      ></img>
      <div>
        <span id="welcometext" className="welcome-text">
          {t("welcometext")}
        </span>

        <p id="welcomemsg" className="welcome-message">
          {t("welcomemsg1")}
          <br />
          {t("welcomemsg2")}
        </p>
        <button
          id="getstarted"
          type="button"
          className="get-started-btn"
          onClick={getStarted}
          data-amplify-analytics-on="click"
          data-amplify-analytics-name="click"
          data-amplify-analytics-attrs="page:welcome,button:get-started"
        >
          {t("getstarted")}
        </button>
      </div>
    </div>
  )
}

export default App
