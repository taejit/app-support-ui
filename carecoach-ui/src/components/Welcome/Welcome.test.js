import React from "react"
import { render, waitFor, fireEvent } from "@testing-library/react"
import { createMemoryHistory } from "history"
import { Router } from "react-router-dom"
import App from "."

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "./aboutyou",
  }),
}))

test("renders welcome page", async () => {
  const history = createMemoryHistory()
  history.push = jest.fn()
  let wrapper
  await waitFor(() => {
    // wrapper = render(<App />)
    wrapper = render(
      <Router history={history}>
        <App />
      </Router>
    )
  })
  const { container } = wrapper
  const linkElement = container.querySelector(".get-started-btn")
  expect(linkElement).toBeDefined()
  await waitFor(async () => {
    fireEvent.click(linkElement)
  })
  expect(history.push).toHaveBeenCalled()
})
