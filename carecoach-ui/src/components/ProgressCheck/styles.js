import { makeStyles } from "@material-ui/core"
export default makeStyles((theme) => ({
  addoptionsoverride: {
    height: `100%`,
    display: `flex`,
    flexDirection: `column`,
    "& #title": {
      marginTop: `12px`,
    },
    "& #subscript": {
      marginBottom: 20,
    },
  },
  overviewtitle: {
    fontSize: `18px`,
    fontWeight: `500`,
    lineHeight: `1.33`,
    color: theme.palette.darkBlue,
    marginBottom: 10,
  },
  progressBlock: {
    marginBottom: 30,
  },
  progressItem: {
    paddingLeft: 0,
    paddingRight: 0,
    marginBottom: 10,
  },
  progressItemicon: {
    background: theme.palette.cloud,
    textAlign: "center",
    minWidth: 48,
    height: 48,
    margin: "0 18px 8px 0",
    padding: "13px 0 14px",
    borderRadius: 12,
    fontSize: 14,
    fontWeight: 500,
    color: theme.palette.twilight,
  },
  progressTitle: {
    fontSize: 16,
    fontWeight: 500,
    lineHeight: 1.5,
    marginTop: 0,
    color: theme.palette.twilight,
  },
  progressSubTitle: {
    fontSize: 18,
    fontWeight: 500,
    lineHeight: 1.33,
    color: theme.palette.darkBlue,
    marginTop: 8,
  },
  progressTxt: {
    marginTop: 0,
    marginBottom: 0,
  },
  score: {
    fontSize: 16,
    fontWeight: 500,
    color: theme.palette.twilight,
    "& span": {
      fontSize: 22,
      fontWeight: 600,
      color: theme.palette.darkBlue,
    },
  },
  goBackDiv: {
    display: "flex",
    marginTop: "auto",
    justifyContent: "center",
    paddingBottom: 40,
  },
  progressMain: {
    display: "flex",
    flexDirection: "column",
    flex: 1,
  },
  progressList: {
    overflow: "hidden auto",
    maxHeight: 500,
  },
}))
