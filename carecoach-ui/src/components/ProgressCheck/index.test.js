import React from "react"
import { fireEvent, render, screen, waitFor } from "@testing-library/react"
import ProgressCheck from "."

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useParams: () => ({ caregiversUuid: "abc-123-hdfhj-1234" }),
}))

describe("ProgressCheck component", () => {
  it("Loading options page", () => {
    render(<ProgressCheck {...{ open: true }} />)
    expect(screen.getByText(/^view-progress-check$/)).toBeDefined()
    expect(screen.getByText(/^progress-answers$/)).toBeDefined()
    expect(screen.getByText(/^progress-score$/)).toBeDefined()
  })

  it("Progress Check with MockData", async () => {
    const mockData = {
      createdAt: "2021-05-13T18:50:07.899Z",
      overAllScore: 13,
      responses: [
        {
          answer: "Answer to the mini survey1 question",
          answerIdentifier: "A_HEALTH_ID",
          question: "Sample mini survey question1 goes here.",
          questionIdentifier: "Q_HEALTH_ID",
          score: 1,
        },
        {
          answer: "Answer to the mini survey2 question",
          answerIdentifier: "A_HEALTH_ID",
          question: "Sample mini survey question 2goes here.",
          questionIdentifier: "Q_HEALTH_ID",
          score: 2,
        },
      ],
    }
    render(<ProgressCheck {...mockData} />)
    expect(screen.getByText(/^view-progress-check$/)).toBeDefined()

    const getQuestion = screen.getByText(
      /^Answer to the mini survey1 question$/
    )

    expect(getQuestion).toBeDefined()

    const getScore = screen.getByText(/^13$/)

    expect(getScore).toBeDefined()
  })

  it("Progress Check with MockData less score", async () => {
    const mockData = {
      createdAt: "2021-05-13T18:50:07.899Z",
      overAllScore: 3,
      responses: [
        {
          answer: "Answer to the mini survey1 question",
          answerIdentifier: "A_HEALTH_ID",
          question: "Sample mini survey question1 goes here.",
          questionIdentifier: "Q_HEALTH_ID",
          score: 1,
        },
        {
          answer: "Answer to the mini survey2 question",
          answerIdentifier: "A_HEALTH_ID",
          question: "Sample mini survey question 2goes here.",
          questionIdentifier: "Q_HEALTH_ID",
          score: 2,
        },
      ],
    }
    render(<ProgressCheck {...mockData} />)
    expect(screen.getByText(/^view-progress-check$/)).toBeDefined()

    const getQuestion = screen.getByText(
      /^Answer to the mini survey1 question$/
    )

    expect(getQuestion).toBeDefined()

    const getScore = screen.getByText(/^3$/)

    expect(getScore).toBeDefined()
  })

  it("Progress Check with MockData high score", async () => {
    const mockData = {
      createdAt: "2021-05-13T18:50:07.899Z",
      overAllScore: 30,
      responses: [
        {
          answer: "Answer to the mini survey1 question",
          answerIdentifier: "A_HEALTH_ID",
          question: "Sample mini survey question1 goes here.",
          questionIdentifier: "Q_HEALTH_ID",
          score: 1,
        },
        {
          answer: "Answer to the mini survey2 question",
          answerIdentifier: "A_HEALTH_ID",
          question: "Sample mini survey question 2goes here.",
          questionIdentifier: "Q_HEALTH_ID",
          score: 2,
        },
      ],
    }
    render(<ProgressCheck {...mockData} />)
    expect(screen.getByText(/^view-progress-check$/)).toBeDefined()

    const getQuestion = screen.getByText(
      /^Answer to the mini survey1 question$/
    )

    expect(getQuestion).toBeDefined()

    const getScore = screen.getByText(/^30$/)

    expect(getScore).toBeDefined()
  })

  it("Progress Check click on Go back", async () => {
    const mockData = {
      createdAt: "2021-05-13T18:50:07.899Z",
      overAllScore: 13,
      responses: [
        {
          answer: "Answer to the mini survey1 question",
          answerIdentifier: "A_HEALTH_ID",
          question: "Sample mini survey question1 goes here.",
          questionIdentifier: "Q_HEALTH_ID",
          score: 1,
        },
        {
          answer: "Answer to the mini survey2 question",
          answerIdentifier: "A_HEALTH_ID",
          question: "Sample mini survey question 2goes here.",
          questionIdentifier: "Q_HEALTH_ID",
          score: 2,
        },
      ],
    }
    render(<ProgressCheck {...mockData} />)
    expect(screen.getByText(/^general-form-go-back$/)).toBeDefined()

    const goback = screen.getByText(/^general-form-go-back$/)
    await waitFor(() => {
      fireEvent.click(goback)
    })
  })
})
