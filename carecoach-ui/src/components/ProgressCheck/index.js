import React from "react"
import { useTranslation } from "react-i18next"
import RightDrawer from "../RightDrawer"
import { ReactComponent as Trophy } from "../../Assets/trophy.svg"
import { Box, Typography, Button } from "@material-ui/core"
import useStyles from "./styles"
import DrawerIconStyle from "../CommonStyles/DrawerIcon"
import DrawerTitle from "../CommonStyles/DrawerTitle"
import FormStyles from "../CommonStyles/FormikFormStyles"
import { ListItem, ListItemAvatar, ListItemText } from "@material-ui/core"
import moment from "moment"

const ProgressCheck = (props) => {
  const { t } = useTranslation()

  const classes = useStyles()
  const iconStyle = DrawerIconStyle()
  const drawerTitle = DrawerTitle()
  const formStyles = FormStyles()

  const getScoreText = (score) => {
    if (score < 5) return "light"
    else if (score < 15) return "mild"
    else return "heavy"
  }

  const getBurdenText = (type) => {
    return t(`${getScoreText(props.overAllScore)}-burden-${type}`)
  }

  const ProgressItem = (item) => {
    return (
      <ListItem alignItems="flex-start" className={classes.progressItem}>
        <ListItemAvatar className={classes.progressItemicon}>
          {`Q${item.qNo}`}
        </ListItemAvatar>
        <ListItemText
          classes={{
            root: classes.progressTxt,
            primary: classes.progressTitle,
            secondary: classes.progressSubTitle,
          }}
          primary={item.question}
          secondary={item.answer}
        />
      </ListItem>
    )
  }

  return (
    <RightDrawer
      open={true}
      onClose={props.onClose}
      hideCrossClose={false}
      showBackArrow={false}
    >
      <Box className={classes.addoptionsoverride}>
        <div>
          <Trophy className={iconStyle.icon} />
        </div>

        <Typography
          id="title"
          component="div"
          className={drawerTitle.secondaryTitle}
        >
          {t("view-progress-check")}
        </Typography>
        <Typography
          id="subscript"
          component="div"
          className={drawerTitle.subscript}
        >
          {moment(new Date(props.createdAt)).format("MMMM Do, YYYY")} at{" "}
          {moment(new Date(props.createdAt)).format("h:mma")}
        </Typography>
        <Box className={classes.progressMain}>
          <Box className={classes.progressBlock}>
            <Typography
              id="overviewsection"
              component="div"
              className={classes.overviewtitle}
            >
              {t("progress-answers")}
            </Typography>
            <Box className={classes.progressList} id="progressList">
              {props.responses &&
                props.responses.map((item, index) => {
                  return <ProgressItem {...item} qNo={index + 1} key={index} />
                })}
            </Box>
          </Box>
          <Box className={classes.progressBlock} style={{ marginTop: "auto" }}>
            <Typography
              id="overviewsection"
              component="div"
              className={classes.overviewtitle}
            >
              {t("progress-score")}
            </Typography>
            <Typography
              id="burdenScore"
              component="div"
              className={classes.score}
            >
              <span>{props.overAllScore}</span>
              {"/ 30  •  "}
              {getBurdenText("text")}
            </Typography>
          </Box>
        </Box>
        <Box className={classes.goBackDiv}>
          <Button
            className={formStyles.cancelbtn}
            onClick={props.backArrowClick}
          >
            {t("general-form-go-back")}
          </Button>
        </Box>
      </Box>
    </RightDrawer>
  )
}

export default ProgressCheck
