/* eslint-disable */
import { Card, CardContent, Box } from "@material-ui/core"
import React from "react"
import PropTypes from "prop-types"
import { getFormatedFullName } from "../../utils/names.util"

const Carerecipient = ({ caregiverObj }) => {
  const getPreferredCommunication = (type) => {
    switch (type) {
      case "CALLS":
        return "Prefers calls"
      case "MESSAGING":
        return "Prefers messages"
      default:
        return "Prefers calls"
    }
  }

  const formatConditions = (conditions) => {
    let conditionsValue = ""
    if (conditions && conditions.length) {
      conditions.every((condition, index) => {
        if (conditionsValue.length >= 20) {
          conditionsValue = `${conditionsValue}, (+${conditions.length - index
            })`
          return false
        }
        conditionsValue = conditionsValue
          ? `${conditionsValue}, ${condition}`
          : `${condition}`
        return true
      })
    }
    return conditionsValue
  }

  return (
    <Card className="borderRadiusTopZero customRecipientCard">
      <CardContent className="customCaregiverCardContent">
        <Box
          height="116px"
          display="flex"
          flexDirection="column"
          alignItems="center"
          justifyContent="center"
          pl="16px"
          fontSize={12}
          fontWeight="fontWeightMedium"
          className="primaryText"
        >
          {/* <Avatar src="caregiverObj.profileImg" /> */}
          <Box
            width="100%"
            display="flex"
            alignItems="center"
            justifyContent="flex-start"
            pb="12px"
          >
            <img
              src="./images/caregivers/recipient-name.svg"
              alt="recipient-name"
              width="16"
              height="16"
            />
            <Box pl="8px" className="capitalize">
              {caregiverObj.recipientNickName
                ? getFormatedFullName(caregiverObj.recipientNickName)
                : getFormatedFullName(caregiverObj.recipientFirstName)}{" "}
              ({caregiverObj.relationshipToRecipient})
            </Box>
          </Box>
          <Box
            width="100%"
            display="flex"
            alignItems="center"
            justifyContent="flex-start"
            pb="12px"
          >
            <img
              src="./images/caregivers/recipient-conditions.svg"
              alt="recipient-conditions"
              width="16"
              height="16"
            />
            <Box pl="8px">{formatConditions(caregiverObj.conditions)}</Box>
          </Box>
          <Box
            width="100%"
            display="flex"
            alignItems="center"
            justifyContent="flex-start"
          >
            <img
              src={
                caregiverObj.preferredCommunication &&
                  !caregiverObj.preferredCommunication.includes("CALLS")
                  ? "./images/caregivers/messaging-communication.svg"
                  : "./images/caregivers/call-communication.svg"
              }
              alt="recipient-communication"
              width="16"
              height="16"
            />
            <Box pl="8px">
              {getPreferredCommunication(caregiverObj.preferredCommunication)}
            </Box>
          </Box>
        </Box>
      </CardContent>
    </Card>
  )
}

Carerecipient.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  caregiverObj: PropTypes.object.isRequired,
}

export default Carerecipient
