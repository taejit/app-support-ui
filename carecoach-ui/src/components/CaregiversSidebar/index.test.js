import React from "react"
import renderer from "react-test-renderer"
import CaregiversProfileContext from "../../Contexts/CaregiversProfileContext"
import CaregiversSidebar from "."

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "/careplan",
  }),
}))

describe("Caregivers Sidebar menu", () => {
  it("Display Sidebar menu", () => {
    const caregiverProfile = {
      uuid: "c1260a8b-28f3-4b94-88ff-9bdb3da94f37",
      firstName: "John",
      lastName: "Coach",
      nickName: "Charlie",
      email: "example@lantern.care",
      phone: "+16021234567",
      employerName: "ABC Inc",
      pronouns: "He/him",
      location: "Gilbert, Arizona",
      role: "Primary Caregiver",
      timeSpentInRole: "Less than 6 months",
      recipient: {
        uuid: "c1260a8b-28f3-4b94-88ff-9bdb3da94f37",
        firstName: "John",
        lastName: "Coach",
        nickName: "Charlie",
        pronouns: "He/him",
        dateOfBirth: "2020-12-31",
        relationship: "He/him",
        location: "Gilbert, Arizona",
        livingArrangement: "Lives indepedently",
        livingArrangementDescription: "Lives among the wolves",
        conditions: ["Aging", "Diabetes", "Dementia", "Other1", "Other2"],
      },
    }

    const tree = renderer
      .create(
        <CaregiversProfileContext.Provider value={{ caregiverProfile }}>
          <CaregiversSidebar />
        </CaregiversProfileContext.Provider>
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})
