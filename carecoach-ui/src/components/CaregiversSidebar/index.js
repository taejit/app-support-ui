import React, { useContext, useState } from "react"
import clsx from "clsx"
import { useTranslation } from "react-i18next"
import { useLocation } from "react-router-dom"
import { Drawer, Box, Link, Button } from "@material-ui/core"
import { ReactComponent as CarePlanIcon } from "../../Assets/care-plan.svg"
import { ReactComponent as TrophyIcon } from "../../Assets/trophy.svg"
import { ReactComponent as StickyNoteIcon } from "../../Assets/sticky-note.svg"
import { ReactComponent as UserIcon } from "../../Assets/user.svg"
import Caregiver from "../Caregiver"
import CaregiversContext from "../../Contexts/CaregiversProfileContext"
import useStyles from "./styles"
import { PopupContext } from "store/PopupContext"
import { ChatContext } from "store/ChatContext"

const MESSAGING_SUFFIX = "messaging"

const CaregiversSidebar = () => {
  const classes = useStyles()
  const { t } = useTranslation()
  const location = useLocation()
  const { pathname } = location
  const [caregiverProfile] = useState(
    useContext(CaregiversContext).caregiverProfile
  )

  const caregiversUuid = caregiverProfile.uuid
  const { chatClient } = useContext(ChatContext)

  const { setMiniMessagingPanel, setMiniMessagingChannel } = useContext(
    PopupContext
  )
  const getFullPathname = (route) => {
    return `/dashboard/caregivers/${caregiversUuid}/${route}`
  }
  const menuList = [
    {
      path: getFullPathname(`careplan`),
      iconComponent: <CarePlanIcon />,
      label: t("caregivers-sidebar-menu-care-plan"),
    },
    {
      path: getFullPathname(`progress`),
      iconComponent: <TrophyIcon />,
      label: t("caregivers-sidebar-menu-progress"),
    },
    {
      path: getFullPathname(`notes`),
      iconComponent: <StickyNoteIcon />,
      label: t("caregivers-sidebar-menu-notes"),
    },
    {
      path: getFullPathname(`profile`),
      iconComponent: <UserIcon className={classes.menuprofileicon} />,
      label: t("caregivers-sidebar-menu-profile"),
    },
  ]
  const handleSendClick = () => {
    const channelName = `${caregiversUuid}-${MESSAGING_SUFFIX}`
    const channel = chatClient.channel("messaging", channelName)
    setMiniMessagingChannel(channel)
    setMiniMessagingPanel(true)
  }
  return (
    <div className={classes.root}>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
        anchor="left"
      >
        <Box className={classes.menuconatiner}>
          <Caregiver caregiverObj={caregiverProfile} />
          {menuList.map(({ path, iconComponent, label }) => {
            return (
              <Box
                key={label}
                className={clsx(classes.menurow, pathname === path && "active")}
              >
                <Box className={classes.activeverticalbar} />
                <Link href={`#${path}`} className={classes.menu}>
                  <Box className={classes.menuicon}>{iconComponent}</Box>
                  <Box className={classes.menutext}>{label}</Box>
                </Link>
              </Box>
            )
          })}
          <Box className={classes.footer}>
            <Button
              component="div"
              variant="outlined"
              className={classes.sendamessagebtn}
              onClick={handleSendClick}
            >
              {t("caregivers-sidebar-menu-send-a-message")}
            </Button>
          </Box>
        </Box>
      </Drawer>
    </div>
  )
}

export default CaregiversSidebar
