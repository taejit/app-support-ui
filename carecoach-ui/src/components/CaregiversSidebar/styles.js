import { makeStyles } from "@material-ui/core/styles"

const drawerWidth = 276
const styles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
    position: "relative",
    border: "none",
    borderRight: `1.5px solid ${theme.palette.cloud}`,
  },
  menuconatiner: {
    display: "flex",
    flexDirection: "column",
    height: "100%",
    "& .customCaregiverCard": {
      margin: "24px",
      borderRadius: "12px",
    },
  },
  menurow: {
    display: "flex",
    width: "100%",
    justifyContent: "flex-start",
    "&.active>div": {
      backgroundColor: theme.palette.darkBlue,
    },
    "&.active>a g": {
      stroke: theme.palette.darkBlue,
    },
    "&.active>a div:nth-child(2)": {
      color: theme.palette.darkBlue,
    },
    "&:hover>a g": {
      stroke: theme.palette.darkBlue,
    },
    "&:hover>a div:nth-child(2)": {
      color: theme.palette.darkBlue,
    },
  },
  menu: {
    "&:hover": {
      textDecoration: "none",
    },
    marginLeft: "24px",
    display: "flex",
    paddingTop: "10px",
    paddingBottom: "10px",
    alignItems: "center",
  },
  menuicon: {
    "& g": {
      stroke: theme.palette.twilight,
    },
    lineHeight: "1",
  },
  menutext: {
    "&.active": {
      color: theme.palette.darkBlue,
    },
    fontSize: "16px",
    fontWeight: "500",
    color: theme.palette.twilight,
    lineHeight: "1.31",
    paddingLeft: "10px",
  },
  menuprofileicon: {
    ".active & g": {
      stroke: theme.palette.darkBlue,
    },
    "& g": {
      stroke: theme.palette.twilight,
    },
  },
  activeverticalbar: {
    height: "100%",
    padding: "2px",
    backgroundColor: theme.palette.white,
  },
  footer: {
    display: "flex",
    justifyContent: "center",
    margin: "36px",
    marginTop: "auto",
  },
  sendamessagebtn: {
    fontSize: "14px",
    borderRadius: "25px",
    padding: "5px 41px",
    border: `solid 1.5px ${theme.palette.morningGlory}`,
    color: theme.palette.morningGlory,
  },
}))

export default styles
