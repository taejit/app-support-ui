import React, { useState, useEffect, useRef } from "react"
import { useTranslation } from "react-i18next"
import { Auth } from "aws-amplify"
import { Box, Grid, Typography } from "@material-ui/core"
import moment from "moment"
import { useHistory } from "react-router-dom"

import useStyles from "./styles"
import SnackbarMessage from "../SnackbarMessage"
import Loading from "../Loading"
import { ReactComponent as CircleCheckIcon } from "../../Assets/Dashboard/no-data-check.svg"
import { ReactComponent as QuoteIcon } from "../../Assets/Dashboard/group-copy.svg"
import * as DashboardApiService from "../../services/Dashboard.service"
import {
  GetCaregiversList,
  GetAllQuotes,
} from "../../services/Caregivers.service"

const Home = () => {
  const { t } = useTranslation()
  const classes = useStyles()
  const history = useHistory()
  const [careCoach, setCareCoach] = useState(null)
  const [schedules, setSchedules] = useState(null)
  const [showLoading, setShowLoading] = useState(true)
  const [showSnackbarMessage, setShowSnackbarMessage] = React.useState({
    open: false,
    message: "",
    success: false,
  })

  const [quoteOfTheDay, setQuoteOfTheDay] = useState(null)
  const [scrollPosition, setPosition] = useState(0)
  const [scrollBottom, setScrollBottom] = useState(600)
  const buttonRef = useRef()

  useEffect(() => {
    const currentDay = moment().format("D")
    GetAllQuotes().then((result) => {
      const data = result.items.filter((item) => item.fields.day == currentDay)
      setQuoteOfTheDay(data[0].fields)
    })
  }, [])

  const getDateTemplate = (schedule) => {
    const currentDate = moment()
    const startDate = moment(schedule.start_time)
    const endDate = moment(schedule.end_time)
    if (startDate.get("date") === currentDate.get("date")) {
      if (moment.duration(startDate.diff(currentDate)).asMinutes() <= 5) {
        return ["Happening now", true]
      } else {
        return [
          `${startDate.format("h:mm")} - ${endDate.format("h:mm a")}`,
          false,
        ]
      }
    } else {
      return [
        `${startDate.format("MMM D")} from ${startDate.format(
          "h:mm"
        )} - ${endDate.format("h:mm a")}`,
        false,
      ]
    }
  }

  const formatSchedules = (schedulesData, caregiversList) => {
    let finalSchedules = []
    schedulesData = schedulesData?.slice(0, 20)
    schedulesData &&
      schedulesData.forEach((schedule) => {
        let dateFormatObj = getDateTemplate(schedule)
        schedule.displayTime = dateFormatObj[0]
        schedule.isCurrentSchedule = dateFormatObj[1]
        let caregiver = caregiversList.find(
          (caregiverData) => caregiverData.uuid === schedule.caregiver_uuid
        )
        if (caregiver) {
          schedule.caregiverIntials = `${
            caregiver.firstName[0] && caregiver.firstName[0].toUpperCase()
          }${caregiver.lastName[0] && caregiver.lastName[0].toUpperCase()}`
          schedule.caregiverName = `${caregiver.firstName} 
          ${caregiver.lastName}`
          finalSchedules.push(schedule)
        }
      })
    finalSchedules.length && setSchedules(finalSchedules || null)
  }

  const getCarecoach = async () => {
    const currentUser = await Auth.currentUserInfo()
    if (!currentUser) {
      setShowLoading(false)
      setShowSnackbarMessage({
        open: true,
        message: "Unable to fetch carecoach.",
        success: false,
      })
      return
    }
    const caregiversList = await GetCaregiversList(
      currentUser.attributes["custom:uuid"]
    )
    const schedulesResp = await DashboardApiService.GetScheduleCalls(
      currentUser.attributes["custom:uuid"]
    )
    setShowLoading(false)
    formatSchedules(schedulesResp?.data, caregiversList?.data)
    setCareCoach(currentUser.attributes)
  }

  const goToVideoCall = (schedule) => {
    if (schedule.isCurrentSchedule)
      history.push({
        pathname: `/${schedule.carecoach_uuid}/call/${schedule.uuid}`,
        scheduleData: schedule,
      })
  }

  useEffect(() => {
    getCarecoach()
  }, [])

  const onScroll = () => {
    const scrollTop = buttonRef.current.scrollTop
    setPosition(scrollTop)
    setScrollBottom(
      buttonRef.current.scrollHeight - buttonRef.current.scrollTop - 600
    )
  }

  return (
    <>
      <SnackbarMessage {...showSnackbarMessage} />
      {showLoading && (
        <Box className={classes.loadingIcon}>
          <Loading />
        </Box>
      )}
      {!showLoading && (
        <Box className={classes.root}>
          <Box className={classes.header}>
            <Typography className={classes.greetingHeader}>
              {t("dashboard-hey")}, {careCoach?.given_name}!
            </Typography>
          </Box>
          <Box className={classes.homeBlock}>
            <Box className={classes.homeLeftblock}>
              {!schedules && (
                <Grid className={classes.noDataDiv}>
                  <Grid item xs={4} align="center">
                    <CircleCheckIcon />
                    <Typography className={classes.noSchedulesText}>
                      {t("dashboard-no-data-text")}
                    </Typography>
                    <Typography className={classes.noSchedulesSubscript}>
                      {t("dashboard-no-data-description")}
                    </Typography>
                  </Grid>
                </Grid>
              )}
              {schedules && (
                <>
                  <Box>
                    <Typography className={classes.dashboardBlockTitle}>
                      {t("dashboard-upcoming-calls")}
                    </Typography>
                  </Box>
                  <div
                    className={classes.homeLeftblockInnerdiv}
                    onScroll={onScroll}
                    ref={buttonRef}
                  >
                    {scrollPosition > 0 ? (
                      <div className={classes.backdropShadowTop}></div>
                    ) : null}
                    {schedules.map((schedule) => {
                      return (
                        <Grid key={schedule.uuid}>
                          <Grid
                            item
                            data-testid="scheduleCard"
                            className={
                              schedule.isCurrentSchedule
                                ? `${classes.scheduleCard} ${classes.clickEffect} `
                                : `${classes.scheduleCard} `
                            }
                            onClick={() => goToVideoCall(schedule)}
                          >
                            <Box className={classes.callTitle}>
                              <Typography className={classes.caregiverName}>
                                {schedule.caregiverName}
                              </Typography>
                              <Typography className={classes.scheduleTitle}>
                                {schedule.topic}
                              </Typography>
                            </Box>
                            <Box
                              className={
                                schedule.isCurrentSchedule
                                  ? classes.currentSchedule
                                  : classes.scheduleTime
                              }
                            >
                              {schedule.displayTime}
                            </Box>
                          </Grid>
                        </Grid>
                      )
                    })}
                    {scrollBottom > 0 ? (
                      <div className={classes.backdropShadowBottom}></div>
                    ) : null}
                  </div>
                </>
              )}
            </Box>
            <Box className={classes.homeRightblock}>
              <Box>
                <Typography className={classes.dashboardBlockTitle}>
                  {t("daily-quote")}
                </Typography>
              </Box>
              <Box className={classes.dailyQuote}>
                <Box className={classes.dailyQuoteinr}>
                  {quoteOfTheDay && (
                    <Box className={classes.dailyQuotedata}>
                      <div>
                        <QuoteIcon />
                      </div>
                      <div className={classes.dailyQuoteContent}>
                        {quoteOfTheDay.quote}
                      </div>
                      <div className={classes.dailyQuoteAuthor}>
                        {quoteOfTheDay.author}
                      </div>
                      <div className={classes.dailyQuoteAuthordes}>
                        {quoteOfTheDay.credential}
                      </div>
                    </Box>
                  )}
                </Box>
              </Box>
            </Box>
          </Box>
        </Box>
      )}
    </>
  )
}

export default Home
