import React from "react"
import { Auth } from "aws-amplify"
import { render, waitFor, fireEvent, screen } from "@testing-library/react"
import { createMemoryHistory } from "history"
import { Router } from "react-router-dom"

import * as DashboardApiService from "../../services/Dashboard.service"
import Dashboard from "."
import * as CaregiversApiService from "../../services/Caregivers.service"

describe("Dashboard test", () => {
  beforeEach(() => {
    jest.spyOn(DashboardApiService, "GetScheduleCalls").mockResolvedValue(
      Promise.resolve({
        data: [
          {
            uuid: "1",
            caregiver_uuid: "d6717250-0948-4b31-94d6-ee6bc244259f",
            name: "Nikola Tesla",
            title: "End of life",
            startTime: "2021-04-08T12:05:53Z",
            endTime: "2021-04-08T12:20:53Z",
          },
          {
            uuid: "2",
            caregiver_uuid: "777f7e96-76b9-449b-a141-58016fe23eae",
            name: "Asim Searah",
            title: "Legalese",
            startTime: "2021-04-08T07:36:53Z",
            endTime: "2021-04-08T07:51:53Z",
          },
          {
            uuid: "3",
            caregiver_uuid: "a2bda6b7-1838-4a51-949a-8c86c169cada",
            name: "Annette Olzon",
            title: "Inurance is Annoying",
            startTime: "2021-04-10T02:36:53Z",
            endTime: "2021-04-10T02:51:53Z",
          },
          {
            uuid: "4",
            caregiver_uuid: "95683eac-353c-4cff-8fec-85004b8e8abf",
            name: "Ashley James",
            title: "Nutrition",
            startTime: "2021-04-11T11:36:53Z",
            endTime: "2021-04-11T11:51:53Z",
          },
          {
            uuid: "5",
            caregiver_uuid: "95683eac-353c-4cff-8fec-85004b8e8ab2",
            name: "Kristen Brown",
            title: "Memory Activities",
            startTime: "2021-04-11T08:30:53Z",
            endTime: "2021-04-11T09:00:53Z",
          },
        ],
      })
    )
    jest.spyOn(Auth, "currentUserInfo").mockResolvedValue(
      Promise.resolve({
        attributes: {
          email: "email@intraedge.com",
          email_verified: true,
          phone_number: "+12254789321",
          phone_number_verified: true,
          sub: "sub",
          family_name: "family",
          given_name: "given",
          "custom:location": "Hyderabad Telangana Hyd",
          "custom:uuid": "test uuid",
        },
        username: "username",
      })
    )
    jest.spyOn(CaregiversApiService, "GetCaregiversList").mockResolvedValue({
      data: [
        {
          uuid: "d6717250-0948-4b31-94d6-ee6bc244259f",
          firstName: "Ajitav",
          lastName: "C",
          nickName: "Ajit",
        },
        {
          uuid: "777f7e96-76b9-449b-a141-58016fe23eae",
          firstName: "Ankit",
          lastName: "Yadav",
          nickName: "anku",
        },
        {
          uuid: "a2bda6b7-1838-4a51-949a-8c86c169cada",
          firstName: "Jayesh",
          lastName: "Tikkas",
          nickName: "Jayu",
        },
        {
          uuid: "95683eac-353c-4cff-8fec-85004b8e8abf",
          firstName: "Test",
          lastName: "User",
          nickName: "Test - User",
        },
      ],
    })
    jest.spyOn(CaregiversApiService, "GetAllQuotes").mockResolvedValue({
      items: [
        {
          fields: {
            quote:
              "To care for those who once cared for us is one of the highest honors.",
            day: 20,
            author: "Tia Walker",
            credential: "Author",
          },
        },
        {
          fields: {
            quote: "The simple act of caring is heroic.",
            day: 21,
            author: "Edward Albert",
            credential: "Actor",
          },
        },
        {
          fields: {
            quote:
              "You are braver than you believe, and stronger than you seem, and smarter than you think.\n",
            day: 31,
            author: "Christopher Robin",
          },
        },
        {
          fields: {
            quote:
              "Challenges are part of life. Overcoming them makes you a stronger person.",
            day: 30,
            author: "Lailah Gifty Akita",
            credential: "Author",
          },
        },
        {
          fields: {
            quote:
              "None of us can go back and start a new beginning, but all of us can start a new day and make a new ending.",
            day: 29,
            author: "Lisa Lieberman-Wang",
            credential: "Author",
          },
        },
        {
          fields: {
            quote:
              "They may forget what you said, but they will not forget how you made them feel.",
            day: 28,
            author: "Carl W. Buechner",
            credential: "Author",
          },
        },
        {
          fields: {
            quote:
              "Sometimes it helps to know that I can’t do it all. One step at a time is all that’s possible, even when those steps are taken on the run.",
            day: 27,
            author: "Anne W. Schaef",
            credential: "Author",
          },
        },
        {
          fields: {
            quote:
              "Life’s challenges are not supposed to paralyze you; they’re supposed to help you discover who you are.",
            day: 26,
            author: "Bernice Johnson Reagon",
            credential: "Musician",
          },
        },
        {
          fields: {
            quote:
              "Doctors diagnose, nurses heal, and caregivers make sense of it all.",
            day: 25,
            author: "Brett H. Lewis",
            credential: "Author",
          },
        },
        {
          fields: {
            quote:
              "Laughter and tears are both responses to frustration and exhaustion. I myself prefer to laugh, since there is less cleaning up to do afterward.",
            day: 24,
            author: "Kurt Vonnegut",
            credential: "Writer",
          },
        },
        {
          fields: {
            quote:
              "One person caring about another represents life's greatest value.",
            day: 22,
            author: "Jim Rohn",
            credential: "Author",
          },
        },
        {
          fields: {
            quote:
              "Never believe that a few caring people can't change the world. For, indeed, that's all who ever have.",
            day: 19,
            author: "Margaret Mead",
            credential: "Anthropologist",
          },
        },
        {
          fields: {
            quote:
              "Being deeply loved by someone gives you strength, while loving someone deeply gives you courage.",
            day: 18,
            author: "Lao Tzu",
            credential: "Philosopher",
          },
        },
        {
          fields: {
            quote:
              "Care is a state in which something does matter; it is the source of human tenderness.",
            day: 17,
            author: "Rollo May",
            credential: "Psychologist",
          },
        },
        {
          fields: {
            quote:
              "Caregiving is a constant learning experience. Most of us are untrained and learning as we go. Remain positive and stay on your path.",
            day: 16,
            author: "Vivian Frazier",
          },
        },
        {
          fields: {
            quote:
              "A smile is the light in your window that tells others that there is a caring, sharing person inside.",
            day: 23,
            author: "Denis Waitley",
            credential: "Author",
          },
        },
        {
          fields: {
            quote:
              "Happiness is an attitude. We either make ourselves miserable or happy and strong. The amount of work is the same.",
            day: 15,
            author: "Francesca Riegler",
            credential: "Author",
          },
        },
        {
          fields: {
            quote:
              "Caregiving often calls us to lean into love we didn’t know possible.",
            day: 14,
            author: "Tia Walker",
            credential: "Author",
          },
        },
        {
          fields: {
            quote:
              "It isn't what we say or think that defines us, but what we do.",
            day: 13,
            author: "Jane Austen",
            credential: "Author",
          },
        },
        {
          fields: {
            quote:
              "You give but little when you give of your possessions. It is when you give of yourself that you truly give.",
            day: 12,
            author: "Khalil Gibran",
            credential: "Writer",
          },
        },
        {
          fields: {
            quote:
              "Some days there won’t be a song in your heart. Sing anyway.",
            day: 11,
            author: "Emory Austin",
            credential: "Author",
          },
        },
        {
          fields: {
            quote:
              "Don't judge people. You never know what kind of battle they are fighting.",
            day: 10,
            author: "Unknown",
          },
        },
        {
          fields: {
            quote:
              "Be determined to handle any challenge in a way that will make you grow.",
            day: 9,
            author: "Les Brown",
            credential: "Motivational speaker",
          },
        },
        {
          fields: {
            quote: "If you can’t change your fate, change your attitude.",
            day: 8,
            author: "Amy Tan",
            credential: "Author",
          },
        },
        {
          fields: {
            quote: "Put one foot in front of the other, no matter what.",
            day: 5,
            author: "Eleanor Brownn",
            credential: "Novelist",
          },
        },
        {
          fields: {
            quote:
              "Compassion brings us to a stop, and for a moment we rise above ourselves.",
            day: 7,
            author: "Mason Cooley",
            credential: "Writer",
          },
        },
        {
          fields: {
            quote:
              "It is not the load that breaks you down. It’s the way you carry it.",
            day: 6,
            author: "Lena Horne",
            credential: "Singer",
          },
        },
        {
          fields: {
            quote:
              "Worry never robs tomorrow of its sorrow, it only robs today of its joy.",
            day: 4,
            author: "Leo Buscaglia",
            credential: "Author",
          },
        },
        {
          fields: {
            quote:
              "Take care of your body. It’s the only place you have to live.",
            day: 3,
            author: "Jim Rohn",
            credential: "Author",
          },
        },
        {
          fields: {
            quote:
              "A good laugh and a long sleep are the two best cures for anything.",
            day: 1,
            author: "Irish Proverb",
          },
        },
        {
          fields: {
            quote:
              "Sometimes our work as caregivers is not for the faint of heart. But you will never know what you're made of until you step into the fire. Step bravely.",
            day: 2,
            author: "Deborah A. Beasley",
            credential: "Author",
          },
        },
      ],
    })
  })
  afterEach(() => {
    jest.resetAllMocks()
  })
  it("Verify component show all data", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<Dashboard />)
    })
    const { queryByText } = wrapper
    expect(queryByText(/^my-caregivers-text$/)).toBeDefined()
  })
  it("Show components no data img if no data from getschedulecalls", async () => {
    jest.spyOn(DashboardApiService, "GetScheduleCalls").mockResolvedValue({})
    let wrapper
    await waitFor(() => {
      wrapper = render(<Dashboard />)
    })
    const { container } = wrapper
    const caregiverNoDataImgId = container.querySelector("#caregiverImg")
    expect(caregiverNoDataImgId).toBeDefined()
  })
  it("Go to video call on click of schedule card", async () => {
    jest.spyOn(DashboardApiService, "GetScheduleCalls").mockResolvedValue({
      data: [
        {
          uuid: "1",
          caregiver_uuid: "d6717250-0948-4b31-94d6-ee6bc244259f",
          name: "Nikola Tesla",
          title: "End of life",
          startTime: "2021-04-08T12:05:53Z",
          endTime: "2021-04-08T12:20:53Z",
        },
      ],
    })
    const history = createMemoryHistory()
    history.push = jest.fn()
    await waitFor(() => {
      render(
        <Router history={history}>
          <Dashboard />
        </Router>
      )
    })
    // const { container } = wrapper
    // const scheduleCardId = container.querySelector("#scheduleCard")
    const scheduleCardId = screen.getByTestId("scheduleCard")
    expect(scheduleCardId).toBeDefined()
    await waitFor(() => {
      fireEvent.click(scheduleCardId)
    })
  })
  it("Show components no data img if no data from auth", async () => {
    jest.spyOn(Auth, "currentUserInfo").mockResolvedValue(Promise.resolve(null))
    let wrapper
    await waitFor(() => {
      wrapper = render(<Dashboard />)
    })
    const { container } = wrapper
    const caregiverNoDataImgId = container.querySelector("#caregiverImg")
    expect(caregiverNoDataImgId).toBeDefined()
  })
})
