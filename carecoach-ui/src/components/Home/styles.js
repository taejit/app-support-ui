import { makeStyles } from "@material-ui/core/styles"

export default makeStyles((theme) => ({
  root: {
    display: "flex",
    padding: "72px 72px 0px 72px",
    backgroundColor: theme.palette.cloud,
    width: "100%",
    minHeight: `calc(100vh - 86px)`,
    flexDirection: "column",
    paddingBottom: "32px",
  },
  header: {
    display: "flex",
    flexDirection: "column",
  },
  greetingHeader: {
    fontSize: "36px",
    fontWeight: "bold",
    color: theme.palette.darkBlue,
  },
  greetingDescription: {
    fontSize: "16px",
    fontWeight: 500,
    color: theme.palette.twilight,
  },
  loadingIcon: {
    position: "absolute",
    top: "50%",
    left: "55%",
  },
  noDataDiv: {
    width: "100%",
    height: "60%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  noSchedulesText: {
    fontSize: "18px",
    paddingTop: "8px",
    fontWeight: "500",
    textAlign: "center",
    color: theme.palette.darkBlue,
  },
  noSchedulesSubscript: {
    fontSize: "14px",
    paddingTop: "12px",
    fontWeight: "500",
    textAlign: "center",
    color: theme.palette.nightShade,
  },
  dashboardBlockTitle: {
    fontSize: "18px",
    paddingTop: "30px",
    fontWeight: "500",
    color: theme.palette.darkBlue,
    marginBottom: "20px",
  },
  scheduleCard: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: theme.palette.white,
    padding: "15px 24px",
    borderRadius: "6px",
    marginBottom: "12px",
  },
  clickEffect: {
    cursor: "pointer",
    "&:hover": {
      boxShadow: "0 4px 18px -4px rgba(24, 24, 43, 0.32)",
    },
    "&:focus": {
      boxShadow: "0 4px 18px -4px rgba(24, 24, 43, 0.32)",
    },
  },
  caregiverName: {
    fontSize: "16px",
    fontWeight: 500,
    color: theme.palette.darkBlue,
    lineHeight: 1.3,
    wordBreak: "break-all",
  },
  scheduleTitle: {
    fontSize: "12px",
    fontWeight: 500,
    color: theme.palette.twilight,
    paddingTop: "2px",
    lineHeight: 1.3,
    wordBreak: "break-all",
  },
  scheduleTime: {
    fontSize: "14px",
    fontWeight: 500,
    textAlign: "right",
    color: theme.palette.darkBlue,
  },
  currentSchedule: {
    fontSize: "14px",
    fontWeight: 600,
    textAlign: "right",
    color: theme.palette.valid,
  },
  callTitle: {
    width: "65%",
    "@media (max-width: 1440px)": {
      width: "55%",
    },
  },
  homeBlock: {
    display: "flex",
    flexDirection: "row",
  },
  homeLeftblock: {
    width: "80%",
  },
  homeLeftblockInnerdiv: {
    width: "95%",
    height: "600px",
    overflowX: "hidden",
    overflowY: "auto",
    paddingRight: 17,
    boxSizing: "content-box",
    "&::-webkit-scrollbar": {
      display: "none",
    },
  },

  homeRightblock: {
    maxWidth: 276,
    width: "100%",
  },

  backdropShadowBottom: {
    backgroundImage:
      "linear-gradient(to bottom, rgba(231, 236, 238, 0), #e7ecee)",
    width: "100%",
    position: "sticky",
    height: 40,
    bottom: "-1px",
  },

  backdropShadowTop: {
    backgroundImage: "linear-gradient(to top, rgba(231, 236, 238, 0), #e7ecee)",
    width: "100%",
    position: "sticky",
    height: 40,
    top: "-1px",
  },

  homeLeftblockInnerdivscroll: {},

  dailyQuote: {
    background: "url('./images/dailyQuote.png') no-repeat",
    height: "100vh",
    maxHeight: "600px",
    width: "100%",
    borderRadius: 10,
    backgroundSize: "cover",
  },
  dailyQuoteinr: {
    height: "100%",
    borderRadius: 10,
    padding: 24,
    color: theme.palette.white,
    fontSize: 18,
    fontWeight: 500,
    position: "relative",
  },
  dailyQuotedata: {
    marginTop: "120%",
    position: "absolute",
    padding: "10px 0",
    width: "calc(100% - 35px)",
    fontSize: 18,
  },
  dailyQuoteContent: {
    fontWeight: 500,
    padding: "12px 0",
  },
  dailyQuoteAuthor: {
    fontSize: 16,
    color: theme.palette.lanternGold,
  },
  dailyQuoteAuthordes: {
    color: theme.palette.whiteShade,
    fontSize: 14,
  },
  backdropShadow: {
    backgroundImage:
      "linear-gradient(to bottom, rgba(231, 236, 238, 0), #e7ecee)",
    width: "100%",
    position: "sticky",
    height: 40,
    bottom: 0,
  },
}))
