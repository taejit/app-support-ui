import React, { useState } from "react"
import RightDrawer from "../RightDrawer"
import AddEditMilestoneForm from "../AddEditMilestoneForm"
import { ValidMilestoneAddEditSteps as ValidSteps } from "../../utils/constant"

const AddEditMilestone = (props) => {
  const [currentStep, setCurrentStep] = useState(ValidSteps.milestone)
  const [hideCrossClose, setHideCrossClose] = useState(false)
  const [showBackArrow, setshowBackArrow] = useState(false)
  const goToStep = (step, hideCross, showArrow) => {
    setCurrentStep(step)
    setHideCrossClose(hideCross)
    setshowBackArrow(showArrow)
  }
  return (
    <RightDrawer
      open={props.open}
      onClose={props.onClose}
      hideCrossClose={hideCrossClose}
      showBackArrow={showBackArrow}
      backArrowClick={() => goToStep(ValidSteps.milestone, true, true)}
      // ToDo uncomment once we have step 2
      //   {
      //   if (currentStep === 2) {
      //     return goToStep(1, false, false)
      //   } else if (currentStep === 3) {
      //     return goToStep(2, true, true)
      //   }
      // }}
    >
      <AddEditMilestoneForm
        {...props}
        currentStep={currentStep}
        goToStep={goToStep}
      />
    </RightDrawer>
  )
}

export default AddEditMilestone
