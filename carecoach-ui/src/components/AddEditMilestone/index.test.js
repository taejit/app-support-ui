import React from "react"
import { render, waitFor, screen, fireEvent } from "@testing-library/react"
import AddEditMilestone from "."

const milestone = {
  id: "test-id",
  name: "name",
  rationale: "rationale",
  description: "description",
  open: true,
  onClose: jest.fn(),
  onEditTaskSave: jest.fn(),
  tasks: [
    {
      id: "task-id",
      name: "name",
      link: "link",
    },
  ],
}

const taskDeleteConfirmData = {
  taskDeleteConfirmed: false,
  setTaskDeleteConfirmed: jest.fn(),
}

const undoTaskDeletion = {
  undoTaskDelete: false,
  setUndoTaskDelete: jest.fn(),
}
describe("AddEditMilestone component", () => {
  it("Load component without error", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(
        <AddEditMilestone
          open
          onClose={jest.fn()}
          tasks={[]}
          taskDeleteConfirmData={{
            taskDeleteConfirmed: true,
            setTaskDeleteConfirmed: jest.fn(),
          }}
          undoTaskDeletion={undoTaskDeletion}
        />
      )
    })
    expect(wrapper).toBeDefined()
  })

  it("form validation for name", async () => {
    await waitFor(() => {
      render(
        <AddEditMilestone
          open
          onClose={jest.fn()}
          tasks={[]}
          taskDeleteConfirmData={taskDeleteConfirmData}
          undoTaskDeletion={undoTaskDeletion}
        />
      )
    })
    const name = screen.getByLabelText(/^edit-milestone-name$/)
    await waitFor(() => {
      fireEvent.change(name, {
        target: {
          value: "",
          id: "milestonename",
        },
      })
      fireEvent.blur(name, {
        target: {
          value: "",
          id: "milestonename",
        },
      })
    })
    expect(
      screen.getByText(
        /^edit-milestone-name edit-milestone-validation-message$/
      )
    ).toBeDefined()
  })
  it("form validation for name edit", async () => {
    const milestonedata = {
      name: "Milestone name",
      rationale: "Rationale",
      description: "description",
      open: true,
      onClose: jest.fn(),
      tasks: [
        {
          name: "Example task",
          link: "This is link",
          id: "random",
        },
        {
          name: "Eample name",
          id: "random even",
        },
      ],
    }
    await waitFor(() => {
      render(
        <AddEditMilestone
          {...milestonedata}
          taskDeleteConfirmData={taskDeleteConfirmData}
          undoTaskDeletion={undoTaskDeletion}
        />
      )
    })
    const name = screen.getByLabelText(/^edit-milestone-name$/)
    await waitFor(() => {
      fireEvent.change(name, {
        target: {
          value: "",
          id: "milestonename",
        },
      })
      fireEvent.blur(name, {
        target: {
          value: "",
          id: "milestonename",
        },
      })
    })
    expect(
      screen.getByText(
        /^edit-milestone-name edit-milestone-validation-message$/
      )
    ).toBeDefined()
  })
  it("form validation for rationale", async () => {
    await waitFor(() => {
      render(
        <AddEditMilestone
          open
          onClose={jest.fn()}
          tasks={[]}
          taskDeleteConfirmData={taskDeleteConfirmData}
          undoTaskDeletion={undoTaskDeletion}
        />
      )
    })
    const rationale = screen.getByLabelText(/^edit-milestone-rationale$/)
    await waitFor(() => {
      fireEvent.change(rationale, {
        target: {
          value: "",
          id: "milestonerationale",
        },
      })
      fireEvent.blur(rationale, {
        target: {
          value: "",
          id: "milestonerationale",
        },
      })
    })
    expect(
      screen.getByText(
        /^edit-milestone-rationale edit-milestone-validation-message$/
      )
    ).toBeDefined()
  })
  it("form validation for description", async () => {
    await waitFor(() => {
      render(
        <AddEditMilestone
          open
          onClose={jest.fn()}
          tasks={[]}
          taskDeleteConfirmData={taskDeleteConfirmData}
          undoTaskDeletion={undoTaskDeletion}
        />
      )
    })
    const description = screen.getByLabelText(/^edit-milestone-description$/)
    await waitFor(() => {
      fireEvent.change(description, {
        target: {
          value: "",
          id: "milestonedescription",
        },
      })
      fireEvent.blur(description, {
        target: {
          value: "",
          id: "milestonedescription",
        },
      })
    })
    expect(
      screen.getByText(
        /^edit-milestone-description edit-milestone-validation-message$/
      )
    ).toBeDefined()
  })
  it("Overview section collapsible", async () => {
    await waitFor(() => {
      render(
        <AddEditMilestone
          open
          onClose={jest.fn()}
          tasks={[]}
          taskDeleteConfirmData={taskDeleteConfirmData}
          undoTaskDeletion={undoTaskDeletion}
        />
      )
    })
    const overview = screen.getByText(/^edit-milestone-overview$/)
    await waitFor(() => {
      fireEvent.click(overview)
    })
    expect(screen.getByLabelText(/^edit-milestone-description$/)).toBeDefined()
  })
  it("Navigate to add new task options page", async () => {
    await waitFor(async () => {
      render(
        <AddEditMilestone
          {...milestone}
          taskDeleteConfirmData={taskDeleteConfirmData}
          undoTaskDeletion={undoTaskDeletion}
        />
      )
    })
    const addNewTaskBtn = screen.getByRole("button", {
      name: /^general-form-add-new$/,
    })
    expect(addNewTaskBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.click(addNewTaskBtn)
    })
  })
  it("Navigate to add new task page upon click of create from scratch", async () => {
    await waitFor(async () => {
      render(
        <AddEditMilestone
          {...milestone}
          taskDeleteConfirmData={taskDeleteConfirmData}
        />
      )
    })
    const addNewTaskBtn = screen.getByRole("button", {
      name: /^general-form-add-new$/,
    })
    expect(addNewTaskBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.click(addNewTaskBtn)
    })
    // TO DO uncomment once step 2 is added
    // const addNewTaskScratchBtn = screen.getByText(/^add-from-scratch$/)
    // expect(addNewTaskScratchBtn).toBeDefined()
    // await waitFor(() => {
    //   fireEvent.click(addNewTaskScratchBtn)
    // })
    expect(
      screen.getByText(/^edit-task-drawer-title-add-scratch-mode$/)
    ).toBeDefined()
  })
  it("Navigate to add new task options page and go back upon click of back arrow", async () => {
    await waitFor(async () => {
      render(
        <AddEditMilestone
          {...milestone}
          taskDeleteConfirmData={taskDeleteConfirmData}
          undoTaskDeletion={undoTaskDeletion}
        />
      )
    })
    const addNewTaskBtn = screen.getByRole("button", {
      name: /^general-form-add-new$/,
    })
    expect(addNewTaskBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.click(addNewTaskBtn)
    })
    const backarrowBtn = screen.getByTestId(/^backarrow$/)
    expect(backarrowBtn).toBeDefined()
    // TO DO uncomment once step 2 is added
    // await waitFor(() => {
    //   fireEvent.click(backarrowBtn)
    // })
    // expect(
    //   screen.getByRole("button", {
    //     name: /^general-form-add-new$/,
    //   })
    // ).toBeDefined()
  }, 10000)
  it("Navigate to add new task page upon click of create from scratch and go back to add task options pafe", async () => {
    await waitFor(async () => {
      render(
        <AddEditMilestone
          {...milestone}
          taskDeleteConfirmData={taskDeleteConfirmData}
          undoTaskDeletion={undoTaskDeletion}
        />
      )
    })
    const addNewTaskBtn = screen.getByRole("button", {
      name: /^general-form-add-new$/,
    })
    expect(addNewTaskBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.click(addNewTaskBtn)
    })
    // TO DO uncomment once step 2 is added
    // const addNewTaskScratchBtn = screen.getByText(/^add-from-scratch$/)
    // expect(addNewTaskScratchBtn).toBeDefined()
    // await waitFor(() => {
    //   fireEvent.click(addNewTaskScratchBtn)
    // })
    expect(
      screen.getByText(/^edit-task-drawer-title-add-scratch-mode$/)
    ).toBeDefined()
    const backarrowBtn = screen.getByTestId(/^backarrow$/)
    expect(backarrowBtn).toBeDefined()
    // await waitFor(() => {
    //   fireEvent.click(backarrowBtn)
    // })
    // expect(screen.getByText(/^add-from-scratch$/)).toBeDefined()
  })
  it("Navigate to add new task page from scratch and add new task", async () => {
    await waitFor(async () => {
      render(
        <AddEditMilestone
          {...milestone}
          taskDeleteConfirmData={taskDeleteConfirmData}
          undoTaskDeletion={undoTaskDeletion}
        />
      )
    })
    const addNewTaskBtn = screen.getByRole("button", {
      name: /^general-form-add-new$/,
    })
    expect(addNewTaskBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.click(addNewTaskBtn)
    })
    // TO DO uncomment once step 2 is added
    // const addNewTaskScratchBtn = screen.getByText(/^add-from-scratch$/)
    // expect(addNewTaskScratchBtn).toBeDefined()
    // await waitFor(() => {
    //   fireEvent.click(addNewTaskScratchBtn)
    // })
    const name = screen.getByLabelText(/^edit-task-name$/)
    await waitFor(() => {
      fireEvent.change(name, {
        target: {
          value: "new name",
          id: "taskname",
        },
      })
      fireEvent.blur(name, {
        target: {
          value: "new name",
          id: "taskname",
        },
      })
    })
    const addNewTaskFormBtn = screen.getByText(/^general-form-add-text$/)
    expect(addNewTaskFormBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.click(addNewTaskFormBtn)
    })
  })
  it("due date validation", async () => {
    await waitFor(async () => {
      render(
        <AddEditMilestone
          {...{ duedate: "2021-09-03", ...milestone }}
          taskDeleteConfirmData={taskDeleteConfirmData}
          undoTaskDeletion={undoTaskDeletion}
        />
      )
    })
    const duedate = screen.getByLabelText(/^edit-milestone-duedate$/)
    expect(duedate).toBeDefined()
    const clearbutton = screen.getByTestId("clearduedate")
    expect(clearbutton).toBeDefined()
    await waitFor(() => {
      fireEvent.click(clearbutton)
    })
    expect(duedate.value).toEqual("")
  })

  it("Navigate to edit task page", async () => {
    await waitFor(async () => {
      render(
        <AddEditMilestone
          {...milestone}
          taskDeleteConfirmData={taskDeleteConfirmData}
          undoTaskDeletion={undoTaskDeletion}
        />
      )
    })
    const editTaskBtn = screen.getByTestId("editTask")
    expect(editTaskBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.click(editTaskBtn)
    })
    const editTaskTitle = screen.getByText(/^edit-task-drawer-title-edit-mode$/)

    expect(editTaskTitle).toBeDefined()

    const saveChnagesBtn = screen.getByRole("button", {
      name: /^general-form-save-changes-text$/,
    })
    expect(saveChnagesBtn.closest("button")).toHaveAttribute("disabled")

    const taskName = screen.getByLabelText(/^edit-task-name$/)
    await waitFor(() => {
      fireEvent.change(taskName, {
        target: {
          value: "New name",
          name: "name",
        },
      })
      fireEvent.blur(taskName, {
        target: {
          value: "value",
          name: "name",
        },
      })
    })
    const taskLink = screen.getByLabelText(/^edit-task-link$/)
    await waitFor(() => {
      fireEvent.change(taskLink, {
        target: {
          value: "https://www.google.com",
          name: "link",
        },
      })
      fireEvent.blur(taskLink, {
        target: {
          name: "link",
        },
      })
    })

    await waitFor(() => {
      expect(saveChnagesBtn.closest("button").disabled).toBeFalsy()
    })

    await waitFor(() => {
      fireEvent.click(saveChnagesBtn.closest("button"))
    })
    const updatedText = screen.getByText("New name")
    expect(updatedText).toBeDefined()
  })

  it("clicks on remove button in edit task page", async () => {
    await waitFor(async () => {
      render(
        <AddEditMilestone
          {...milestone}
          taskDeleteConfirmData={taskDeleteConfirmData}
          undoTaskDeletion={{
            undoTaskDelete: false,
            setUndoTaskDelete: jest.fn(),
          }}
        />
      )
    })
    const editTaskBtn = screen.getByTestId("editTask")
    expect(editTaskBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.click(editTaskBtn)
    })
    const editTaskTitle = screen.getByText(/^edit-task-drawer-title-edit-mode$/)

    expect(editTaskTitle).toBeDefined()

    const saveChnagesBtn = screen.getByRole("button", {
      name: /^general-form-save-changes-text$/,
    })
    expect(saveChnagesBtn.closest("button")).toHaveAttribute("disabled")

    const removeBtn = screen.getByRole("button", {
      name: /^general-form-remove-text$/,
    })
    expect(removeBtn.closest("button")).toBeDefined()

    const taskName = screen.getByLabelText(/^edit-task-name$/)
    await waitFor(() => {
      fireEvent.change(taskName, {
        target: {
          value: "New name",
          name: "name",
        },
      })
      fireEvent.blur(taskName, {
        target: {
          value: "value",
          name: "name",
        },
      })
    })
    const taskLink = screen.getByLabelText(/^edit-task-link$/)
    await waitFor(() => {
      fireEvent.change(taskLink, {
        target: {
          value: "https://www.google.com",
          name: "link",
        },
      })
      fireEvent.blur(taskLink, {
        target: {
          name: "link",
        },
      })
    })

    await waitFor(() => {
      expect(saveChnagesBtn.closest("button").disabled).toBeFalsy()
    })
    await waitFor(() => {
      fireEvent.click(removeBtn.closest("button"))
    })
    const confirmButton = screen.getByRole("button", {
      name: /^general-form-remove-text$/,
    })
    expect(confirmButton).toBeDefined()
    await waitFor(() => {
      fireEvent.click(confirmButton)
    })
    const undoButton = screen.getByTestId("undoBtn")
    expect(undoButton).toBeDefined()
    await waitFor(() => {
      fireEvent.click(undoButton)
    })
  })
})
