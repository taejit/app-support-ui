import React from "react"
import renderer from "react-test-renderer"
import CollapsibleDisplay from "."

describe("CollapsibleDisplay component", () => {
  it("Render with count", () => {
    const data = {
      title: "sometitle",
      count: 2,
    }
    const tree = renderer.create(<CollapsibleDisplay {...data} />).toJSON()
    expect(tree).toMatchSnapshot()
  })
  it("Render without count", () => {
    const data = {
      title: "sometitle",
    }
    const tree = renderer.create(<CollapsibleDisplay {...data} />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
