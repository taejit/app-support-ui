import { makeStyles } from "@material-ui/core"

export default makeStyles((theme) => ({
  root: {
    display: `inline-flex`,
    flexDirection: `column`,
    width: `100%`,
  },
  titlerow: {
    width: `100%`,
    paddingTop: `24px`,
    paddingBottom: `32px`,
    flexDirection: `row`,
    display: `inline-flex`,
    alignItems: `center`,
  },
  titlecontainer: {
    display: `inline-flex`,
    marginRight: `auto`,
  },
  title: {
    fontSize: `18px`,
    fontWeight: `500`,
    lineHeight: `1.33`,
    color: theme.palette.darkBlue,
  },
  countintitle: {
    fontSize: `12px`,
    fontWeight: `500`,
    lineHeight: `1.33`,
    color: theme.palette.nightShade,
  },
  icons: {
    marginLeft: `auto`,
  },
  chevrondownicon: {
    height: `16px`,
    width: `16px`,
    marginLeft: `16px`,
  },
  chevronuptransform: {
    transform: `rotate(180deg)`,
  },
  titleContainer: {
    marginRight: "145px",
  },
  titleRow: {
    width: `100%`,
    paddingTop: `24px`,
    paddingBottom: `28px`,
    flexDirection: `row`,
    display: `inline-flex`,
    alignItems: `center`,
  },
}))
