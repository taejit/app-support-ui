import React, { useState } from "react"
import clsx from "clsx"
import { Collapse, Typography, Box } from "@material-ui/core"
import { ReactComponent as ChevronDownIcon } from "../../Assets/chevron-down.svg"
import useStyles from "./styles"

const CollapsibleDisplay = ({
  open,
  title,
  children,
  count,
  titleContainer,
  titleRow,
  collapsableIcon,
}) => {
  const classes = useStyles()
  const [collapseOpen, setOpen] = useState(open || true)

  return (
    <>
      <Box className={classes.root}>
        <Box
          className={!titleRow ? classes.titlerow : classes.titleRow}
          onClick={() => setOpen(!collapseOpen)}
        >
          <Box
            className={
              !titleContainer ? classes.titlecontainer : classes.titleContainer
            }
          >
            <Typography component="div" className={classes.title}>
              {title}
            </Typography>
          </Box>
          {count > 0 && (
            <>
              <Box
                className={
                  !collapsableIcon ? classes.icons : classes.collapsableIcon
                }
              >
                <Typography component="div" className={classes.countintitle}>
                  {count}
                </Typography>
              </Box>
              <ChevronDownIcon
                className={
                  collapseOpen
                    ? classes.chevrondownicon
                    : clsx(classes.chevrondownicon, classes.chevronuptransform)
                }
              />
            </>
          )}
        </Box>
        <Collapse in={collapseOpen} timeout="auto">
          {children}
        </Collapse>
      </Box>
    </>
  )
}

export default CollapsibleDisplay
