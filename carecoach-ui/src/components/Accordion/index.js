import React from "react"
import Accordion from "@material-ui/core/Accordion"
import AccordionDetails from "@material-ui/core/AccordionDetails"
import AccordionSummary from "@material-ui/core/AccordionSummary"
import AccordionActions from "@material-ui/core/AccordionActions"
import Typography from "@material-ui/core/Typography"
import ExpandMoreIcon from "@material-ui/icons/ExpandMore"
import MuiAccordionSummary from "@material-ui/core/AccordionSummary"
import MuiAccordionDetails from "@material-ui/core/AccordionDetails"
import MuiAccordion from "@material-ui/core/Accordion"
import { withStyles } from "@material-ui/core/styles"
import useStyles from "./styles"

const DetailedAccordion = (props) => {
  const classes = useStyles()
  const Accordion = withStyles({
    root: {
      backgroundColor: "#e7ecee",
      boxShadow: "none !important",
      width: "299px",
    },
  })(MuiAccordion)
  const AccordionSummary = withStyles({
    root: {
      marginLeft: "55px",
      minHeight: "0px !important",
      height: "30px !important",
    },
    expandIcon: {
      marginRight: "0px",
    },
  })(MuiAccordionSummary)
  const AccordionDetails = withStyles({
    root: {
      backgroundColor: "#e7ecee",
      display: "flex",
      flexDirection: "column",
    },
  })(MuiAccordionDetails)

  return (
    <div>
      <Accordion defaultExpanded>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1c-content"
          id="panel1c-header"
        >
          <div className={classes.title}>{props.title}</div>
          <Typography className={classes.secondaryHeading}>
            {props.count}
          </Typography>
        </AccordionSummary>
        <AccordionDetails>{props.children}</AccordionDetails>
      </Accordion>
    </div>
  )
}
export default DetailedAccordion
