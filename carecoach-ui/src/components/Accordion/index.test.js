import React from "react"
import renderer from "react-test-renderer"
import { render, waitFor } from "@testing-library/react"
import DetailedAccordion from "."

describe("Accordion", () => {
  it("test render", () => {
    const tree = renderer.create(<DetailedAccordion />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
