import { makeStyles } from "@material-ui/core/styles"

export default makeStyles((theme) => ({
  heading: {
    fontSize: theme.typography.pxToRem(15),
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
    marginLeft: "155px",
  },
  icon: {
    verticalAlign: "bottom",
    height: 20,
    width: 20,
  },
  title: {
    marginLeft: "100px",
  },
}))
