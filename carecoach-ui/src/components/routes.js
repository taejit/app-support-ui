import React from "react"

const Routes = [
  {
    path: "/welcome",
    name: "Welcome",
    component: React.lazy(() => import("./Welcome")),
    exact: true,
  },
  {
    path: "/aboutyou",
    name: "AboutYou",
    component: React.lazy(() => import("./Onboarding/AboutYou.js")),
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    component: React.lazy(() => import("./Dashboard")),
  },
  {
    path: "/what-about-you",
    name: "WhatAboutYou",
    component: React.lazy(() => import("./Onboarding/WhatAboutYou.js")),
    exact: true,
  },
  {
    path: "/careplansent/:caregiversUuid/:caregiversName",
    name: "Care Plan Sent",
    component: React.lazy(() => import("./CarePlanSent")),
  },
  {
    path: "/:carecoachUuid/call/:scheduleUuid",
    name: "Caregiver video call",
    component: React.lazy(() => import("./VideoCall")),
  },
]

export default Routes
