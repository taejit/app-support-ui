import React from "react"
import renderer from "react-test-renderer"
import Dashboard from "."
import { render, waitFor } from "@testing-library/react"
import { Auth } from "aws-amplify"
import ProfileImgService from "../../services/avatar.service"
import AvtarContext from "../../Contexts/AvtarContext"

jest.mock("../Header", () => {
  return {
    __esModule: true,
    default: () => {
      return <div />
    },
  }
})
jest.mock("../Sidebar", () => {
  return {
    __esModule: true,
    default: () => {
      return <div />
    },
  }
})

jest.mock("../DashboardRoutes", () => {
  return {
    __esModule: true,
    default: () => {
      return <div />
    },
  }
})

describe("Dashboard testing", () => {
  beforeEach(() => {
    jest.spyOn(Auth, "currentAuthenticatedUser").mockResolvedValue({
      attributes: {
        email: "bramsai@intraedge.com",
        email_verified: true,
        phone_number: "+12254789321",
        phone_number_verified: true,
        sub: "38041359-614f-4c44-9a32-23c3c80506c7",
        family_name: "kiran",
        given_name: "ramsai",
        location: "Hyderabad Telangana Hyd",
        "custom:uuid": "38041359-614f-4c44-9a32-23c3c8050327",
        "custom:specialities": "MSW",
      },
    })

    jest.spyOn(Auth, "currentUserInfo").mockResolvedValue(
      Promise.resolve({
        attributes: {
          email: "bramsai@intraedge.com",
          email_verified: true,
          phone_number: "+12254789321",
          phone_number_verified: true,
          sub: "38041359-614f-4c44-9a32-23c3c80506c7",
          family_name: "kiran",
          given_name: "ramsai",
          "custom:location": "Hyderabad Telangana Hyd",
          "custom:pronoun": "he/his",
          "custom:uuid": "38041359-614f-4c44-9a32-23c3c8050327",
          "custom:specialities": "MSW",
        },

        username: "acc2ea0f-3a1f-49b9-8b19-7f555ece71d6",
      })
    )

    jest.spyOn(ProfileImgService, "GetProfileImage").mockResolvedValue(
      Promise.resolve({
        data: {
          profileImageUrl: null,
          status: "defaultText",
          isLoading: false,
        },
      })
    )
  })

  it("Dashboard loading", async () => {
    const AvtarImageData = {
      profileImageUrl: null,
      status: "defaultText",
      isLoading: false,
    }

    let wrapper

    await waitFor(() => {
      wrapper = render(
        <AvtarContext.Provider value={{ AvtarImageData }}>
          <Dashboard />
        </AvtarContext.Provider>
      )
    })

    expect(wrapper).toBeTruthy()
  })

  it("checks Dashboard loading unasync", () => {
    const tree = renderer.create(<Dashboard />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
