import React, { useEffect, useState } from "react"
import { Box, Grid } from "@material-ui/core"
import { Auth } from "aws-amplify"
import Header from "../Header"
import Sidebar from "../Sidebar"
import DashboardRoutes from "../DashboardRoutes"
import useStyles from "./styles"
import { Notifications } from "../Messaging/Notifications"
import { MiniList, MiniMessenger } from "../Messaging/Mini"
import { AppDataProvider } from "../AppDataProvider"
import AvtarContext from "../../Contexts/AvtarContext"
import ProfileImgService from "../../services/avatar.service"
import CaregiversContext from "../../Contexts/CaregiversProfileContext"

const RightSide = () => {
  return (
    <div>
      <Notifications />
      <MiniList />
      <MiniMessenger />
    </div>
  )
}

const Dashboard = () => {
  const classes = useStyles()

  const [AvtarImageData, setAvtarImageData] = useState({
    url: null,
    textType: "defaultText",
    isLoading: true,
  })

  const [caregiverProfile, setCaregiverProfile] = useState({})

  const updateProfileImageDetails = (url, textType, isLoading) => {
    setAvtarImageData({
      url: url || null,
      textType: "defaultText",
      isLoading,
    })
  }

  const getAvatarImage = async (uuid) => {
    const imageData = await ProfileImgService.GetProfileImage(uuid)
    updateProfileImageDetails(
      imageData.profileImageUrl,
      imageData.status,
      imageData.isLoading
    )
  }

  useEffect(() => {
    function getUserInfo() {
      return Auth.currentUserInfo()
    }

    getUserInfo().then((userInfo) => {
      if (userInfo) {
        const customUuid = userInfo.attributes["custom:uuid"]
        getAvatarImage(customUuid)
      }
    })
  }, [])

  return (
    <AppDataProvider>
      <AvtarContext.Provider value={{ AvtarImageData, setAvtarImageData }}>
        <CaregiversContext.Provider
          value={{ caregiverProfile, setCaregiverProfile }}
        >
          <Grid container>
            <Grid item className={classes.root} xs={12}>
              <Sidebar />
              <Box width="100%">
                <Header />
                <DashboardRoutes initialRedirect="/dashboard/home" />
              </Box>
            </Grid>
          </Grid>
          <RightSide />
        </CaregiversContext.Provider>
      </AvtarContext.Provider>
    </AppDataProvider>
  )
}

export default Dashboard
