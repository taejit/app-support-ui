import { makeStyles } from "@material-ui/core/styles"

export default makeStyles(() => ({
  root: {
    display: "flex",
    width: "100%",
  },
  dashboardroot: {
    height: `calc(100% - 84px)`,
  },
  mainContainer: {
    height: `calc(100% - 84px)`,
  },
}))
