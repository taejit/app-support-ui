import { makeStyles } from "@material-ui/core/styles"

export default makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  caregiversplayarea: {
    backgroundColor: theme.palette.cloud,
    width: "100%",
    overflowY: "auto",
    minHeight: `calc(100vh - 84px)`,
  },
}))
