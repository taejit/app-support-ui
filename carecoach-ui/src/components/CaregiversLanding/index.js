import React, { useState, useEffect, useContext } from "react"
import { useParams, useLocation } from "react-router-dom"
import { Auth } from "aws-amplify"
import { Box } from "@material-ui/core"
import CaregiversSidebar from "../CaregiversSidebar"
import Loading from "../Loading"
import CaregiversRoutes from "../CaregiversRoutes"
import CaregiversContext from "../../Contexts/CaregiversProfileContext"
import { GetCaregiversProfile } from "../../services/Caregivers.service"
import useStyles from "./styles"

const CaregiversLanding = () => {
  const classes = useStyles()
  const { caregiverUuid } = useParams()
  const [loading, setLoading] = useState(true)
  const pathLocation = useLocation()
  const caregiverPathData = pathLocation.caregiver || {}
  const { setCaregiverProfile } = useContext(CaregiversContext)
  useEffect(() => {
    async function getCaregiversProfile() {
      const currentUser = await Auth.currentUserInfo()
      if (currentUser) {
        const profile = await GetCaregiversProfile(
          currentUser.attributes["custom:uuid"],
          caregiverUuid
        )
        if (profile.status === 200) {
          const caregiverProfile = Object.assign(profile.data, {
            hasUnReadMessages: caregiverPathData.hasUnReadMessages || false,
            lastMessageDate: caregiverPathData.lastMessageDate,
            lastContactedDate: caregiverPathData.lastContactedDate,
            upcomingCallAt: caregiverPathData.upcomingCallAt,
          })
          setCaregiverProfile(caregiverProfile)
          setLoading(false)
        }
      }
    }
    getCaregiversProfile()
  }, [caregiverUuid])

  if (loading) {
    return <Loading />
  }

  return (
    <Box display="flex">
      {!loading && <CaregiversSidebar />}
      <Box className={classes.caregiversplayarea}>
        <CaregiversRoutes
          initialRedirect={`/dashboard/caregivers/${caregiverUuid}/careplan`}
        />
      </Box>
    </Box>
  )
}

export default CaregiversLanding
