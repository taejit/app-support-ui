import React from "react"
import renderer from "react-test-renderer"
import CaregiversLanding from "."

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "/careplan",
  }),
  useParams: () => ({ caregiverUuid: "abc-123-hdfhj-1234" }),
}))

describe("Caregiver landing page", () => {
  it("test render", () => {
    const tree = renderer.create(<CaregiversLanding />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
