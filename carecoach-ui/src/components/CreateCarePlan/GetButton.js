import React from "react"
import PropTypes from "prop-types"
import clsx from "clsx"
import FilledButton from "../CommonStyles/FilledButton"
import { Button } from "@material-ui/core"
import useStyles from "./styles"

const GetButton = ({ btnText, btnOnClick, styles, id, disabled }) => {
  const filledBtnClasses = FilledButton()
  const classes = useStyles()

  return (
    <Button
      id={id}
      disabled={disabled || false}
      onClick={btnOnClick}
      className={clsx(filledBtnClasses.actionbtn, classes.finishButton, styles)}
    >
      {btnText}
    </Button>
  )
}

GetButton.propTypes = {
  btnText: PropTypes.string.isRequired,
  btnOnClick: PropTypes.func.isRequired,
}

export default GetButton
