import { Box, Button, Typography } from "@material-ui/core"
import React, { useContext, useState, useEffect } from "react"
import { useTranslation } from "react-i18next"
import { useParams, useHistory } from "react-router-dom"
import { Auth } from "aws-amplify"
import find from "lodash/find"
import useStyles from "./styles"
import GetButton from "./GetButton"
import ListMilestones from "../ListMilestones"
import CaregiversCareplanStepInfo from "../CaregiversCarePlan/CareplanStepInfo"
import {
  GetAllCarePlanTemplate,
  GetAllCarePlanItem,
  SaveCarePlan,
} from "../../services/CarePlan.service"
import CarePlanTemplate from "../../Contexts/CarePlanTemplates"
import TemplatesSearch from "../CaregiversCarePlan/TemplatesSearch"
import { ReactComponent as ClearSlectedTemplateIcon } from "../../Assets/CarePlans/cross-icon.svg"
import HorizontalLinearStepper from "../Stepper"
import { ReactComponent as NoCarePlan } from "../../Assets/CarePlans/care-plan-no-data.svg"
import SnackbarMessage from "../SnackbarMessage"
import getMilestoneObj from "../../utils/getMilestoneObj"
import ErrorPopup from "../ErrorPopup"
import CaregiversContext from "../../Contexts/CaregiversProfileContext"

const CreateCarePlan = () => {
  const [currentUser, setCurrentUser] = useState(1)
  const [currentStep, setCurrentStep] = useState(1)
  const [carePlanTemplates, setCarePlanTemplates] = useState({})
  const classes = useStyles()
  const [templates, setTemplates] = useState([])
  const [selectedTemplate, setSelectedTemplate] = useState(null)
  const [addNewMilestone, setAddNewMilestone] = useState(false)
  const [backStepAlert, SetBackStepAlert] = useState(false)
  const [backStepNum, SetbackStepNum] = useState("")
  const { caregiverUuid } = useParams()
  const { t } = useTranslation()
  const history = useHistory()
  const [snackbarMessage, setSnackbarMessage] = useState({
    open: false,
    message: "",
    onClose: () =>
      setSnackbarMessage({
        ...snackbarMessage,
        open: false,
        children: null,
      }),
  })
  const [careplanReset, SetcareplanReset] = useState(false)
  const { caregiverProfile } = useContext(CaregiversContext)

  useEffect(() => {
    async function getCarePlanTemplates() {
      const loggedInUser = await Auth.currentUserInfo()
      if (loggedInUser) {
        setCurrentUser(loggedInUser)
      }
      const allCarePlanTemplates = await GetAllCarePlanTemplate()
      const lanternTemplates = allCarePlanTemplates.items.map((item) => {
        return {
          name: item.fields.name,
          isCommon: item.fields.isCommon || false,
          id: item.sys.id,
        }
      })
      setTemplates(lanternTemplates)
      setCarePlanTemplates(allCarePlanTemplates)
    }
    getCarePlanTemplates()
    SetcareplanReset(false)
  }, [caregiverUuid, careplanReset])

  const undoBtn = (item) => {
    return (
      <>
        <Typography
          component="div"
          className={classes.undobtn}
          onClick={() => onTemplateSelection(item)}
          id="undoBtn"
        >
          {t("caregivers-delete-undo")}
        </Typography>
      </>
    )
  }

  const onTemplateSelection = (item) => {
    let data = templates
    let showSnackbar = false
    data.forEach((template, index) => {
      if (template.id === item.id) {
        data[index].isSelected = !data[index].isSelected
        if (data[index].isSelected) showSnackbar = true
      }
    })
    setTemplates([...data])
    setSelectedTemplate(data.filter((template) => template.isSelected))
    if (currentStep > 1) {
      // show snackbar
      setSnackbarMessage({
        ...snackbarMessage,
        open: false,
        message: "",
      })
      setTimeout(() => {
        showSnackbar &&
          setSnackbarMessage({
            ...snackbarMessage,
            open: true,
            message: `${item.name} added`,
            ...{ children: undoBtn(item) },
          })
      })
    }
  }

  const getCarePlanMilestone = (carePlanId) => {
    return find(carePlanTemplates.items, (item) => {
      return item.sys.id === carePlanId
    }).fields.milestones
  }

  const onApplyTemplate = async () => {
    let selectedMilestones = []
    selectedTemplate?.forEach((template) => {
      selectedMilestones = [
        ...selectedMilestones,
        ...(template.id && getCarePlanMilestone(template.id)),
      ]
    })
    const allIds = (selectedMilestones || []).map(
      (milestone) => milestone.sys.id
    )
    // get the whole entry of milestone from contentful
    const allMilestones = await GetAllCarePlanItem()
    const withSelection = allMilestones.items.map((milestone) => {
      milestone.fields.isSelected = allIds.includes(milestone.sys.id)
      return milestone
    })
    const selectedCarePlanMilestone = getMilestoneObj(withSelection)
    setTemplates([...selectedCarePlanMilestone])
    // navigate to step 2
    setCurrentStep(2)
  }

  const saveCarePlan = () => {
    setCurrentStep(3)
  }

  const getSelectedTemplate = () => {
    if (!selectedTemplate || !selectedTemplate.length) {
      return (
        <Typography className={classes.noTemplateText}>
          {t("no-template-selected")}
        </Typography>
      )
    }
    return (
      <>
        {selectedTemplate.map((template) => {
          return (
            <Button
              variant="outlined"
              className={classes.selectedTemplateBtn}
              onClick={() => onTemplateSelection(template)}
            >
              {template?.name}

              <ClearSlectedTemplateIcon className={classes.clearIcon} />
            </Button>
          )
        })}
      </>
    )
  }

  const handleFinish = async () => {
    const carePlan = {
      isDraft: false,
      items: templates.filter((milestone) => {
        delete milestone.id
        if (milestone.duedate) milestone.dueDate = milestone.duedate
        milestone.tasks.forEach((task) => {
          delete task.id
        })
        return milestone.isSelected
      }),
    }
    const carePlanSave = await SaveCarePlan(
      currentUser.attributes["custom:uuid"],
      caregiverProfile.uuid,
      carePlan
    )
    if (carePlanSave?.status === 201) {
      history.push(
        `/careplansent/${caregiverProfile.uuid}/${caregiverProfile.firstName}`
      )
    } else {
      setSnackbarMessage({
        ...snackbarMessage,
        open: true,
        message: `Unable to save Care Plan. Please try again later.`,
      })
    }
  }

  const getContinueBtn = {
    Step1: () => {
      if (!selectedTemplate || !selectedTemplate.length) {
        return (
          <GetButton
            btnText={t("continue-from-scratch")}
            btnOnClick={onApplyTemplate}
            styles={classes.scratchButton}
          />
        )
      }
      return (
        <GetButton
          id="apply-btn"
          btnText={t("apply-template")}
          btnOnClick={onApplyTemplate}
          styles={classes.applytemplate}
        />
      )
    },
    Step2: () => (
      <>
        <Button
          id="addnewmilestone"
          className={classes.addnewmilestonebutton}
          onClick={() => setAddNewMilestone(true)}
        >
          {t("careplan-step-2-add-new-milestone")}
        </Button>
        <GetButton
          id="continue-btn"
          disabled={
            templates.filter((milestone) => milestone.isSelected).length === 0
          }
          btnText={t("careplan-step-2-continue-btn")}
          btnOnClick={saveCarePlan}
        />
      </>
    ),
    Step3: () => (
      <div className={classes.fullBtn}>
        <GetButton
          id="finish-btn"
          btnText={t("careplan-step-3-btn")}
          btnOnClick={handleFinish}
        />
      </div>
    ),
  }

  const handleStepBack = (status) => {
    if (status == "yes") {
      setCurrentStep(backStepNum)
      // reset everything here
      SetcareplanReset(true)
      setSelectedTemplate(null)
      setTemplates([])
      setCarePlanTemplates({})
    }
    SetBackStepAlert(false)
  }

  const handleClickedSteper = (stepClick) => {
    if (
      currentStep !== stepClick &&
      currentStep > stepClick &&
      stepClick != 2
    ) {
      SetBackStepAlert(true)
      SetbackStepNum(stepClick)
    } else if (currentStep > stepClick) {
      setCurrentStep(stepClick)
      SetBackStepAlert(false)
    }
  }

  const onClose = () => {
    SetBackStepAlert(false)
  }

  const selectedTemplatesCheck = templates.filter(
    (singleTemplate) => singleTemplate.isSelected
  )

  const getNickName = (caregiver) => {
    return caregiver.nickName ? caregiver.nickName : caregiver.firstName
  }

  return (
    <>
      {snackbarMessage.open && <SnackbarMessage {...snackbarMessage} />}
      <CarePlanTemplate.Provider
        value={{
          carePlanTemplates,
          templates,
          setTemplates,
          addNewMilestone,
          setAddNewMilestone,
        }}
      >
        <Box
          className={classes.mainContainer}
          id="drawer-container"
          style={{ position: "relative" }}
        >
          <ErrorPopup
            open={backStepAlert}
            title={t(`careplan-backstep-title`)}
            description_1={t(`careplan-backstep-description-1`)}
            description_2={t(`careplan-backstep-description-2`)}
            description_3={t(`careplan-backstep-description-3`)}
            handleActions={handleStepBack}
            direction={"left"}
            leftButtonText={t("nevermind-button-text-error-drawer")}
            rightButtonText={"Continue"}
          />
          <Box className={classes.leftPanel}>
            <Box color="darkBlue" width={1}>
              <HorizontalLinearStepper
                currentStep={currentStep}
                clickedStepper={handleClickedSteper}
              />
            </Box>
            <Box className={classes.stepInfo}>
              <CaregiversCareplanStepInfo
                title={t(`careplan-step-${currentStep}-title`)}
                description={
                  currentStep != 3
                    ? t(`careplan-step-${currentStep}-description`)
                    : t(`careplan-step-3-description1`) +
                      getNickName(caregiverProfile) +
                      t(`careplan-step-3-description2`) +
                      getNickName(caregiverProfile) +
                      t(`careplan-step-3-description3`)
                }
              />
            </Box>
            {currentStep !== 3 && (
              <>
                <Box className={classes.searchTemplate}>
                  <TemplatesSearch
                    currentStep={currentStep}
                    templateList={templates}
                    searchPlaceholder={t(
                      `search-placeholder-on-step-${currentStep}`
                    )}
                    onTemplateSelection={(args) => onTemplateSelection(args)}
                  />
                </Box>
              </>
            )}
            <Box className={classes.footerelements}>
              {currentStep === 1 && (
                <Box className={classes.selectedTemplateDiv}>
                  {getSelectedTemplate()}
                </Box>
              )}
              <Box className={classes.scratchButtonBackground}>
                {getContinueBtn[`Step${currentStep}`]()}
              </Box>
            </Box>
          </Box>
          <Box className={classes.rightPanel}>
            <div
              className={backStepAlert ? classes.transLayer : ""}
              onClick={onClose}
              onKeyDown={onClose}
              role="button"
              tabIndex={0}
              id="backStepLayer"
            ></div>
            {templates.length > 0 &&
            currentStep > 1 &&
            (selectedTemplatesCheck.length > 0 || addNewMilestone) ? (
              <>
                <ListMilestones restrictOnclick={currentStep == 3} />
                <Box className={classes.footer}>
                  <Typography component="div" className={classes.footermessage}>
                    {currentStep == 3
                      ? t("careplan-step-3-milestone-message")
                      : t("careplan-step-2-milestone-message")}
                  </Typography>
                </Box>
              </>
            ) : (
              <Box className={classes.noCarePlanBox}>
                <Box className={classes.noCareplanIcon}>
                  <NoCarePlan />
                </Box>
                <Box className={classes.noCareplanDisplayText}>
                  <span class="capitalize">
                    {caregiverProfile.nickName
                      ? caregiverProfile.nickName
                      : caregiverProfile.firstName}
                  </span>
                  {t("no-careplan-sub-text")}
                </Box>
              </Box>
            )}
          </Box>
        </Box>
      </CarePlanTemplate.Provider>
    </>
  )
}
export default CreateCarePlan
