import React from "react"
import { render, waitFor, fireEvent, screen } from "@testing-library/react"
import { Auth } from "aws-amplify"
import CreateCarePlan from "./index"
import * as CareplanService from "../../services/CarePlan.service"

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "/careplan",
  }),
  useParams: () => ({ caregiverUuid: "abc-123-hdfhj-1234" }),
}))

describe("create care plan testing", () => {
  beforeEach(() => {
    jest.spyOn(CareplanService, "GetAllCarePlanTemplate").mockReturnValue({
      items: [
        {
          fields: {
            isCommon: true,
            name: "template",
            milestones: [
              {
                sys: {
                  id: "test-id",
                },
              },
            ],
          },
          sys: {
            id: "test-id",
          },
        },
      ],
    })
    jest.spyOn(CareplanService, "GetAllCarePlanItem").mockReturnValue({
      items: [
        {
          fields: {
            isCommon: true,
            name: "template",
            description: {
              content: [
                {
                  content: [
                    {
                      value: "template description",
                    },
                  ],
                },
              ],
            },
            rationale: {
              data: {},
              content: [
                {
                  data: {},
                  content: [
                    {
                      value: "content",
                      data: {},
                    },
                  ],
                },
              ],
            },
          },
          sys: {
            id: "test-id",
          },
        },
      ],
    })
    jest.spyOn(Auth, "currentUserInfo").mockResolvedValue(
      Promise.resolve({
        attributes: {
          email: "bramsai@intraedge.com",
          email_verified: true,
          phone_number: "+12254789321",
          phone_number_verified: true,
          sub: "38041359-614f-4c44-9a32-23c3c80506c7",
          family_name: "kiran",
          given_name: "ramsai",
          "custom:location": "Hyderabad Telangana Hyd",
          "custom:pronoun": "he/his",
        },
        username: "acc2ea0f-3a1f-49b9-8b19-7f555ece71d6",
      })
    )
    jest.spyOn(CareplanService, "SaveCarePlan").mockReturnValue({})
  })
  it("Should display CreateCarePlan page", async () => {
    let wrapper
    await waitFor(async () => {
      wrapper = render(<CreateCarePlan />)
    })
    const { queryByText } = wrapper
    expect(queryByText(/^continue-from-scratch$/)).toBeDefined()
  })
  it("Should call handleclick to get selected item", async () => {
    let wrapper
    await waitFor(async () => {
      wrapper = render(<CreateCarePlan />)
    })
    const { container, queryByText } = wrapper
    const template = container.querySelector("#itemBox .MuiListItem-button")
    expect(template).toBeDefined()
    await waitFor(() => {
      fireEvent.click(template)
    })
    expect(queryByText(/^continue-from-scratch$/)).toBeDefined()
  })
  it("Should call handleclick to get selected item and then milestone", async () => {
    let wrapper
    await waitFor(async () => {
      wrapper = render(<CreateCarePlan />)
    })
    const { container, queryByText } = wrapper
    const template = container.querySelector("#itemBox .MuiListItem-button")
    expect(template).toBeDefined()
    await waitFor(() => {
      fireEvent.click(template)
    })
    const applyBtn = container.querySelector("#apply-btn")
    expect(applyBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.click(applyBtn)
    })
    expect(queryByText(/^finish$/)).toBeDefined()
  })

  it("Should call handleclick to get selected item, get milestones and select a milestone", async () => {
    let wrapper
    await waitFor(async () => {
      wrapper = render(<CreateCarePlan />)
    })
    const { container, queryByText } = wrapper
    const template = container.querySelector("#itemBox .MuiListItem-button")
    expect(template).toBeDefined()
    await waitFor(() => {
      fireEvent.click(template)
    })
    const applyBtn = container.querySelector("#apply-btn")
    expect(applyBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.click(applyBtn)
    })
    const milestone = container.querySelector("#itemBox .MuiListItem-button")
    expect(milestone).toBeDefined()
    await waitFor(() => {
      fireEvent.click(milestone)
    })
    expect(queryByText(/^continue$/)).toBeDefined()
  })

  it("Should save careplan", async () => {
    let wrapper
    await waitFor(async () => {
      wrapper = render(<CreateCarePlan />)
    })
    const { container } = wrapper
    const template = container.querySelector("#itemBox .MuiListItem-button")
    expect(template).toBeDefined()
    await waitFor(() => {
      fireEvent.click(template)
    })
    const applyBtn = container.querySelector("#apply-btn")
    expect(applyBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.click(applyBtn)
    })
    const milestone = container.querySelector("#itemBox .MuiListItem-button")
    expect(milestone).toBeDefined()
    await waitFor(() => {
      fireEvent.click(milestone)
    })
    const continueBtn = container.querySelector("#continue-btn")
    expect(continueBtn).toBeDisabled()
    await waitFor(() => {
      fireEvent.click(continueBtn)
    })
  })

  it("Add new milestone", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<CreateCarePlan />)
    })
    const { container } = wrapper
    const template = container.querySelector("#itemBox .MuiListItem-button")
    expect(template).toBeDefined()
    await waitFor(() => {
      fireEvent.click(template)
    })
    const applyBtn = container.querySelector("#apply-btn")
    expect(applyBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.click(applyBtn)
    })
    const addMilestone = container.querySelector("#addnewmilestone")
    await waitFor(() => {
      fireEvent.click(addMilestone)
    })

    const name = screen.getByLabelText(/^edit-milestone-name$/)
    const rationale = screen.getByLabelText(/^edit-milestone-rationale$/)
    const description = screen.getByLabelText(/^edit-milestone-description$/)

    await waitFor(() => {
      fireEvent.change(name, {
        target: {
          value: "new name",
          id: "milestonename",
        },
      })
      fireEvent.blur(name, {
        target: {
          value: "new name",
          id: "milestonename",
        },
      })
      fireEvent.change(rationale, {
        target: {
          value: "new rationale",
          id: "milestonerationale",
        },
      })
      fireEvent.blur(rationale, {
        target: {
          value: "new rationale",
          id: "milestonerationale",
        },
      })
      fireEvent.change(description, {
        target: {
          value: "new description",
          id: "milestonedescription",
        },
      })
      fireEvent.blur(description, {
        target: {
          value: "new description",
          id: "milestonedescription",
        },
      })
    })
    const milestoneSave = screen.getByRole("button", {
      name: /^general-form-add-text$/,
    })
    await waitFor(() => {
      fireEvent.click(milestoneSave)
    })
    const milestone = container.querySelector("#milestone-2")
    expect(milestone).toBeDefined()
  }, 10000)

  it("Do not enable finish button unless one milestone selected", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<CreateCarePlan />)
    })
    const { container, queryByText } = wrapper
    const continueFromScratch = queryByText(/^continue-from-scratch$/).closest(
      "button"
    )

    await waitFor(() => {
      fireEvent.click(continueFromScratch)
    })

    const continueBtn = container.querySelector("#continue-btn")
    expect(continueBtn).toBeDisabled()
  })
  it("back Step in Create careplan", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<CreateCarePlan />)
    })
    const { container, queryByText } = wrapper
    const continueFromScratch = queryByText(/^continue-from-scratch$/).closest(
      "button"
    )
    await waitFor(() => {
      fireEvent.click(continueFromScratch)
    })

    const continueBtn = container.querySelector("#continue-btn")
    expect(continueBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.click(continueBtn)
    })

    expect(/^careplan-step-3-description1$/).toBeDefined()
    expect(/^careplan-step-3-description2$/).toBeDefined()
    expect(/^careplan-step-3-description3$/).toBeDefined()
    expect(/^finish$/).toBeDefined()

    const step_0 = container.querySelector("#step_0")
    expect(step_0).toBeDefined()
    await waitFor(() => {
      fireEvent.click(step_0)
    })

    const careplanBackstepTitle = screen.getByText(/^careplan-backstep-title$/)
    expect(careplanBackstepTitle).toBeDefined()

    const stepbackCtnbtn = container.querySelector("#stepbackCtnbtn")

    await waitFor(() => {
      fireEvent.click(stepbackCtnbtn)
    })
  })
})
