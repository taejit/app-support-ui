import { makeStyles } from "@material-ui/core/styles"

export default makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  header: {
    "& .MuiToolbar-regular": {
      height: "84px",
    },
    borderBottom: `solid 1px ${theme.palette.cloud}`,
  },
  menuButton: {
    marginRight: theme.spacing(2),
    display: "none",
  },
  title: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
    fontSize: "16px",
    fontWeight: "500",
    color: theme.palette.darkBlue,
  },
  menucontainer: {
    borderRadius: "4px",
  },
  actionmenu: {
    fontSize: "15px",
    fontWeight: "500",
    color: theme.palette.darkBlue,
  },
  menuitem: {
    width: "328px",
  },
  profileicon: {
    minWidth: `48px`,
    padding: "12px",
    marginRight: "12px",
    borderRadius: "12px",
    justifyContent: "center",
    backgroundColor: theme.palette.darkBlue,
  },
  logouticon: {
    minWidth: `48px`,
    padding: "12px",
    marginRight: "12px",
    borderRadius: "12px",
    justifyContent: "center",
    backgroundColor: theme.palette.rose,
  },
  inputRoot: {
    color: "inherit",
  },
  sectionDesktop: {
    display: "none",
    height: "100%",
    alignItems: "center",
    [theme.breakpoints.up("md")]: {
      display: "flex",
    },
  },
  sectionMobile: {
    display: "flex",
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  },
  profilemenucontainer: {
    cursor: `pointer`,
    borderRadius: `4px`,
    display: "flex",
    height: "56px",
    alignItems: "center",
    padding: `6px 12px`,
    "& > div.profile-img-div-2": {
      marginRight: `12px`,
      width: "3.5em",
      height: "3.5em",
      "& div.profile-img-div": {
        width: "3em",
        height: "3em",
      },
    },
    "&:hover": {
      // theme.palette.dove is applied in rgba format below to get the opacity in color
      background: `rgba(190, 195, 197, 0.16)`,
    },
  },
  chevronicon: {
    fill: theme.palette.darkBlue,
  },
}))
