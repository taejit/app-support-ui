import React from "react"
import { render, waitFor } from "@testing-library/react"
import { Auth } from "aws-amplify"
import Header from "./index"
import CaregiversProfileContext from "../../Contexts/CaregiversProfileContext"

jest.mock("../CareCoachAvatar", () => {
  return {
    __esModule: true,
    default: () => {
      return <div />
    },
  }
})

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "/dashboard/caregivers/test-caregiver-uuid/careplan",
  }),
  matchPath: jest.fn().mockReturnThis({
    path: "/dashboard/caregivers/:caregiverUuid/careplan",
    url: "/dashboard/caregivers/test-caregiver-uuid/careplan",
  }),
}))

describe("Header component testing", () => {
  beforeEach(() => {
    jest.spyOn(Auth, "currentAuthenticatedUser").mockResolvedValue(
      Promise.resolve({
        attributes: {
          email: "bramsai@intraedge.com",
          email_verified: true,
          phone_number: "+12254789321",
          phone_number_verified: true,
          sub: "38041359-614f-4c44-9a32-23c3c80506c7",
          family_name: "kiran",
          given_name: "ramsai",
          "custom:location": "Hyderabad Telangana",
          "custom:pronoun": "he/his",
          "custom:uuid": "test-uuid",
          "custom:job_start_date": new Date("2021-01-30"),
          "custom:bio": "bio",
          "custom:credentials": "test-credentials",
        },
        username: "acc2ea0f-3a1f-49b9-8b19-7f555ece71d6",
      })
    )
    jest.spyOn(Auth, "updateUserAttributes").mockResolvedValue({})
    jest.spyOn(Auth, "currentUserInfo").mockResolvedValue(
      Promise.resolve({
        attributes: {
          email: "bramsai@intraedge.com",
          email_verified: true,
          phone_number: "+12254789321",
          phone_number_verified: true,
          sub: "38041359-614f-4c44-9a32-23c3c80506c7",
          family_name: "kiran",
          given_name: "ramsai",
          "custom:location": "Hyderabad Telangana",
          "custom:pronoun": "he/his",
          "custom:uuid": "test-uuid",
          "custom:job_start_date": new Date("2021-01-30"),
          "custom:bio": "bio",
          "custom:credentials": "test-credentials",
        },
        username: "acc2ea0f-3a1f-49b9-8b19-7f555ece71d6",
      })
    )
  })
  it("Should display Header page", async () => {
    const caregiverProfile = {
      uuid: "c1260a8b-28f3-4b94-88ff-9bdb3da94f37",
      firstName: "John",
      lastName: "Coach",
      nickName: "Charlie",
      email: "example@lantern.care",
      phone: "+16021234567",
      employerName: "ABC Inc",
      pronouns: "he/him",
      location: "Gilbert, Arizona",
      role: "Primary Caregiver",
      timeSpentInRole: "Less than 6 months",
      recipient: {
        uuid: "c1260a8b-28f3-4b94-88ff-9bdb3da94f37",
        firstName: "John",
        lastName: "Coach",
        nickName: "Charlie",
        pronouns: "He/him",
        dateOfBirth: "2020-12-31",
        relationship: "he/him",
        location: "Gilbert, Arizona",
        livingArrangement: "Lives indepedently",
        livingArrangementDescription: "Lives among the wolves",
        conditions: ["Aging", "Diabetes", "Dementia", "Other1", "Other2"],
      },
    }
    let wrapper
    await waitFor(async () => {
      wrapper = render(
        <CaregiversProfileContext.Provider value={{ caregiverProfile }}>
          <Header />
        </CaregiversProfileContext.Provider>
      )
    })
    const { queryByText } = wrapper
    expect(queryByText(/^profile$/)).toBeDefined()
    jest.resetAllMocks()
  })
})
