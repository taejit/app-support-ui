import React, { useEffect, useContext } from "react"
import { useTranslation } from "react-i18next"
import { useHistory, useLocation, matchPath } from "react-router-dom"
import { useIdleTimer } from "react-idle-timer"
import { Auth } from "aws-amplify"
import cloneDeep from "lodash/cloneDeep"
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  MenuItem,
  Menu,
  ListItemIcon,
  Box,
} from "@material-ui/core"
import KeyboardArrowDownOutlinedIcon from "@material-ui/icons/KeyboardArrowDownOutlined"
import KeyboardArrowUpOutlinedIcon from "@material-ui/icons/KeyboardArrowUpOutlined"
import MenuIcon from "@material-ui/icons/Menu"
import { ReactComponent as SignOutIcon } from "../../Assets/logout.svg"
import { ReactComponent as UserIcon } from "../../Assets/user.svg"
import useStyles from "./styles"
import CareCoachAvatar from "../CareCoachAvatar"
import Logout from "../Logout"
import { GetCarecoachProfile } from "../../services/CarecaochServices"
import { NotificationIcon } from "components/Messaging/Notifications"
import { MessagingIcon } from "components/Messaging/MessagingIcon"
import caregiverRoutes from "./routes.breadCrumb"
import CaregiversContext from "../../Contexts/CaregiversProfileContext"
import Breadcrumb from "../BreadCrumb"
import { getFormatedName } from "../../utils/names.util"

export default function PrimarySearchAppBar({ navigation }) {
  const classes = useStyles()
  const { t } = useTranslation()
  const history = useHistory()
  const [profileMenuName, setProfileMenuName] = React.useState("")
  const [anchorEl, setAnchorEl] = React.useState(null)
  const [, setMobileMoreAnchorEl] = React.useState(null)
  const [breadCrumbData, setBreadCrumbData] = React.useState(null)
  const { caregiverProfile } = useContext(CaregiversContext)
  const pathLocation = useLocation()

  const isMenuOpen = Boolean(anchorEl)

  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget)
  }

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null)
  }

  const handleMenuClose = () => {
    setAnchorEl(null)
    handleMobileMenuClose()
  }

  const handleProfileClick = () => {
    history.push("/dashboard/view")
    handleMenuClose()
  }

  useIdleTimer({
    timeout: 1000 * 60 * 15, // 15 minutes, supplied in millisecond
    onIdle: Logout,
    debounce: 500,
  })

  useEffect(() => {
    async function getCareCoachProfile() {
      const currentUser = await Auth.currentUserInfo()
      const { data } = await GetCarecoachProfile(
        currentUser.attributes["custom:uuid"]
      )
      data && setProfileMenuName(data.firstName)
    }
    getCareCoachProfile()
  })

  useEffect(() => {
    setBreadCrumbData(null)
    let match = null
    const breadCrumbRoutes = cloneDeep(caregiverRoutes)
    breadCrumbRoutes.every((route) => {
      if (matchPath(pathLocation.pathname, route)) {
        match = route
        return false
      }
      return true
    })
    const caregiverName = caregiverProfile.nickName
      ? getFormatedName(
          caregiverProfile.nickName || "",
          caregiverProfile.lastName || ""
        )
      : getFormatedName(
          caregiverProfile.firstName || "",
          caregiverProfile.lastName || ""
        )
    if (match) {
      match.breadCrumb.navigationLinks.forEach((navLink) => {
        navLink.link = navLink.link?.replace(
          ":caregiversUuid",
          caregiverProfile.uuid
        )
        navLink.title = navLink.showCaregiverName
          ? caregiverName
          : navLink.title
      })
      setBreadCrumbData(match.breadCrumb)
    }
  }, [pathLocation, caregiverProfile])

  const renderMenu = (
    <Menu
      className={classes.menucontainer}
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id="primary-search-account-menu"
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleProfileClick} className={classes.menuitem}>
        <ListItemIcon className={classes.profileicon}>
          <UserIcon />
        </ListItemIcon>
        <Typography variant="inherit" className={classes.actionmenu}>
          {t("profile")}
        </Typography>
      </MenuItem>
      <MenuItem onClick={Logout}>
        <ListItemIcon className={classes.logouticon}>
          <SignOutIcon />
        </ListItemIcon>
        <Typography variant="inherit" className={classes.actionmenu}>
          {t("signout")}
        </Typography>
      </MenuItem>
    </Menu>
  )

  return (
    <div className={classes.grow}>
      <AppBar
        color="transparent"
        elevation={0}
        position="static"
        className={classes.header}
      >
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="open drawer"
          >
            <MenuIcon />
          </IconButton>
          <div className={classes.grow}>
            {breadCrumbData && (
              <Breadcrumb breadCrumbData={breadCrumbData}></Breadcrumb>
            )}
          </div>
          <div className={classes.sectionDesktop}>
            <MessagingIcon />
            <NotificationIcon />
          </div>
          <Box
            onClick={handleProfileMenuOpen}
            className={classes.profilemenucontainer}
          >
            <CareCoachAvatar displayOrEdit />
            <Typography className={classes.title} variant="h6" noWrap>
              {profileMenuName}
            </Typography>
            {anchorEl ? (
              <KeyboardArrowUpOutlinedIcon className={classes.chevronicon} />
            ) : (
              <KeyboardArrowDownOutlinedIcon className={classes.chevronicon} />
            )}
          </Box>
        </Toolbar>
      </AppBar>
      {renderMenu}
    </div>
  )
}
