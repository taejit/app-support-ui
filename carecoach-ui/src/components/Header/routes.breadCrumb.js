const Routes = [
  {
    path: "/dashboard/caregivers/:caregiversUuid/careplan",
    name: "Care Plan",
    exact: true,
    strict: false,
    breadCrumb: {
      title: "Care Plan",
      navigationLinks: [
        {
          link: "/dashboard/caregivers",
          title: "My Caregivers",
        },
        {
          link: null,
          title: null,
          showCaregiverName: true,
        },
        {
          link: null,
          title: "Care Plan",
        },
      ],
    },
  },
  {
    path: "/dashboard/caregivers/:caregiverUuid/careplan/create",
    name: "Create Care Plan",
    exact: true,
    strict: false,
    breadCrumb: {
      title: "Create a Care Plan",
      navigationLinks: [
        {
          link: "/dashboard/caregivers",
          title: "My Caregivers",
        },
        {
          link: "/dashboard/caregivers/:caregiversUuid",
          title: null,
          showCaregiverName: true,
        },
        {
          link: `/dashboard/caregivers/:caregiversUuid`,
          title: "Care Plan",
        },
        {
          link: null,
          title: "Create",
        },
      ],
    },
  },
  {
    path: "/dashboard/caregivers/:caregiversUuid/notes",
    name: "Notes",
    breadCrumb: {
      title: "Notes",
      navigationLinks: [
        {
          link: "/dashboard/caregivers",
          title: "My Caregivers",
        },
        {
          link: "/dashboard/caregivers/:caregiversUuid",
          title: null,
          showCaregiverName: true,
        },
        {
          link: null,
          title: "Notes",
        },
      ],
    },
  },
  {
    path: "/dashboard/caregivers/:caregiversUuid/profile",
    name: "Profile",
    breadCrumb: {
      title: "Profile",
      navigationLinks: [
        {
          link: "/dashboard/caregivers",
          title: "My Caregivers",
        },
        {
          link: "/dashboard/caregivers/:caregiversUuid",
          title: null,
          showCaregiverName: true,
        },
        {
          link: null,
          title: "Profile",
        },
      ],
    },
  },
  {
    path: "/dashboard/caregivers/:caregiversUuid/progress",
    name: "Mini Surveys",
    breadCrumb: {
      title: "Progress",
      navigationLinks: [
        {
          link: "/dashboard/caregivers",
          title: "My Caregivers",
        },
        {
          link: "/dashboard/caregivers/:caregiversUuid",
          title: null,
          showCaregiverName: true,
        },
        {
          link: null,
          title: "Progress",
        },
      ],
    },
  },
  {
    path: "/dashboard/edit",
    name: "Edit My Profile",
    breadCrumb: {
      title: "Edit My Profile",
      navigationLinks: [
        {
          link: "/dashboard/view",
          title: "My Profile",
        },
        {
          link: null,
          title: "Edit",
        },
      ],
    },
  },
]

export default Routes
