import React from "react"
import { useTranslation } from "react-i18next"
import clsx from "clsx"
import { Grid, Typography, Button } from "@material-ui/core"
import OtlinedButton from "../CommonStyles/OutlineButton"
import FormStyles from "../CommonStyles/FormikFormStyles"

const FormikFormFooter = ({
  errorMessage,
  editMode,
  addModeSuccessLabel,
  hasChanged,
  onClose,
  isFormValid,
  onClickSuccess,
  onClickRemove,
  backArrowClick,
  showGoBack,
}) => {
  const { t } = useTranslation()
  const formStyles = FormStyles()
  const outlineBtnStyle = OtlinedButton()

  return (
    <Grid container className={formStyles.footer}>
      {errorMessage && (
        <Grid item className={formStyles.message}>
          <Typography component="h4" className={formStyles.error}>
            {errorMessage}
          </Typography>
        </Grid>
      )}
      {editMode && !hasChanged && (
        <Grid className={formStyles.nochangemagegrid}>
          <Typography component="h4" className={formStyles.nochangemessage}>
            {t("general-form-no-changes-have-been-made")}
          </Typography>
        </Grid>
      )}
      <Grid item className={formStyles.buttonplacement}>
        {!showGoBack && (
          <Button
            id="cancel"
            onClick={onClose}
            className={formStyles.cancelbtn}
          >
            {t("general-form-cancel-text")}
          </Button>
        )}
        {showGoBack && (
          <Button
            id="backBtn"
            onClick={backArrowClick}
            className={formStyles.cancelbtn}
          >
            {t("general-form-go-back")}
          </Button>
        )}
        <Button
          disabled={!isFormValid || !hasChanged}
          id="submitform"
          onClick={onClickSuccess}
          className={formStyles.successbtn}
        >
          {editMode
            ? t("general-form-save-changes-text")
            : addModeSuccessLabel || t("general-form-add-text")}
        </Button>
      </Grid>

      {editMode && (
        <>
          <Grid className={formStyles.border} />
          <Grid className={formStyles.removebtngrid}>
            <Button
              id="remove"
              onClick={onClickRemove}
              className={clsx(
                outlineBtnStyle.dangeractionbtn,
                formStyles.removebtn
              )}
            >
              {t("general-form-remove-text")}
            </Button>
          </Grid>
        </>
      )}
    </Grid>
  )
}

export default FormikFormFooter
