import React from "react"
import { render, waitFor } from "@testing-library/react"
import TemplateItem from "./index"

describe("TemplateItem component testing", () => {
  it("Should display TemplateItem page", async () => {
    let wrapper
    await waitFor(async () => {
      wrapper = render(<TemplateItem item={{ name: "testName" }} />)
    })
    const { container } = wrapper
    const templateId = container.querySelector("#itemBox")
    expect(templateId).toBeDefined()
  })
})
