import { makeStyles } from "@material-ui/core"

export default makeStyles((theme) => ({
  listItemBox: {
    "& .MuiListItem-button:hover": {
      backgroundColor: theme.palette.cloud,
    },
  },
  defaultItem: {
    borderRadius: "4px",
    backgroundColor: theme.palette.cloud,
    marginBottom: "12px",
    fontSize: "15px",
    fontWeight: 500,
    paddingTop: "13px",
    paddingBottom: "13px",
    paddingRight: "15px",
    color: theme.palette.darkBlue,
    wordBreak: "break-all",
  },
  selectedItem: {
    borderRadius: "4px",
    backgroundColor: `${theme.palette.morningGlory} !important`,
    opacity: 1,
    marginBottom: "12px",
    fontSize: "15px",
    fontWeight: 500,
    paddingTop: "13px",
    paddingBottom: "13px",
    paddingRight: "15px",
    color: theme.palette.white,
    wordBreak: "break-all",
  },
  checkMarkIcon: {
    position: "absolute",
    right: "16px",
  },
}))
