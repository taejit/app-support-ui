import { Box, ListItem } from "@material-ui/core"
import React from "react"
import PropTypes from "prop-types"
import useStyles from "./styles"
import { ReactComponent as CheckMarkIcon } from "../../Assets/CarePlans/template-checkmark.svg"

const TemplateItem = ({ item, handleClick }) => {
  const classes = useStyles()

  return (
    <>
      <Box className={classes.listItemBox} id="itemBox">
        <ListItem
          button
          className={
            item?.isSelected ? classes.selectedItem : classes.defaultItem
          }
          selected={item?.isSelected}
          onClick={() => handleClick(item)}
        >
          {item.name}
          {item?.isSelected && (
            <CheckMarkIcon className={classes.checkMarkIcon} />
          )}
        </ListItem>
      </Box>
    </>
  )
}

TemplateItem.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  item: PropTypes.object.isRequired,
  handleClick: PropTypes.func.isRequired,
}

export default TemplateItem
