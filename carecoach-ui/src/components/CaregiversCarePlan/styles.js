import { makeStyles } from "@material-ui/core/styles"

export default makeStyles((theme) => ({
  noCareplan: {
    marginTop: "8%",
  },
  itemscontainer: () => ({
    [`&:last-child > div > div > div:nth-child(2)`]: {
      minHeight: `47px`,
      maxHeight: `80px`,
      height: `40px`,
      borderBottom: `2px solid ${theme.palette.twilight}`,
      borderBottomLeftRadius: `33px`,
    },
    [`& > div > div > div:first-child`]: {
      backgroundColor: theme.palette.twilight,
      marginLeft: `-18px`,
    },
    [`& > div > div > div:nth-child(2)`]: {
      height: `100%`,
      maxHeight: `160px`,
      minHeight: `140px`,
      borderLeft: `2px solid ${theme.palette.twilight}`,
      position: `absolute`,
      marginLeft: `-16px`,
    },
  }),
  collpsedisplay: {
    height: `100%`,
  },
  loadingIcon: {
    position: "absolute",
    top: "50%",
    left: "65%",
  },

  customButton: {
    textTransform: "none",
    borderRadius: "25px",
    padding: "3px 20px",
    border: `solid 1.5px ${theme.palette.morningGlory}`,
    color: theme.palette.morningGlory,
  },

  createButton: {
    "&:hover": {
      backgroundColor: theme.palette.lanternGold,
      border: "none",
    },

    textTransform: "none",
    backgroundColor: theme.palette.lanternGold,
    borderRadius: "25px",
    padding: "8px 40px 8px 40px",
    border: "none",
    marginTop: "42px",
    fontSize: "16px",
    color: theme.palette.darkBlue,
  },

  noCareplanText: {
    fontSize: "18px",
    paddingTop: "8px",
    fontWeight: "500",
    textAlign: "center",
    color: theme.palette.darkBlue,
  },

  noCareplanTextSubscript: {
    fontSize: "14px",
    paddingTop: "12px",
    fontWeight: "500",
    textAlign: "center",
    color: theme.palette.nightShade,
  },
  upcomingText: {
    width: "97px",
    height: "24px",
    margin: "0 45px 0 57 !important",
    fontFamily: "Montserrat",
    fontSize: "18px",
    fontWeight: "500",
    lineHeight: "1.33",
    color: theme.palette.darkBlue,
  },
  upcomingItems: {
    margin: "16px 0px 12px 14px",
    fontSize: "12px",
    fontWeight: 500,
    lineHeight: 1.5,
    color: theme.palette.twinight,
  },
  duedate: {
    width: "86px",
    height: "21px",
    fontSize: "14px",
    fontWeight: "500",
    lineHeight: "1.5",
    color: theme.palette.darkBlue,
  },
  duedateerror: {
    fontWeight: "600",
    lineHeight: "1.5",
    color: theme.palette.darkError,
  },
  titleContainer: {
    display: `inline-flex`,
    marginRight: `10px`,
  },
  undobtn: {
    cursor: "pointer",
    fontSize: "12px",
    fontWeight: "600",
    lineHeight: "1.5",
    padding: "14px",
    width: "68px",
    borderTopRightRadius: "4px",
    borderBottomRightRadius: "4px",
    color: theme.palette.white,
    backgroundColor: theme.palette.brightBlue,
  },
}))
