import React from "react"
import { render, waitFor, fireEvent } from "@testing-library/react"
import TemplatesSearch from "./index"
import CarePlanTemplateContext from "../../../Contexts/CarePlanTemplates"

const templates = [
  { name: "testName", isCommon: true },
  { name: "testName2", isCommon: false },
]

describe("TemplatesSearch component testing", () => {
  it("Should display TemplatesSearch page", async () => {
    let wrapper
    await waitFor(async () => {
      wrapper = render(<TemplatesSearch searchPlaceholder="test-placeholder" />)
    })
    const { queryByText } = wrapper
    expect(queryByText(/^common-text$/)).toBeDefined()
  })

  it("Search templates", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(
        <CarePlanTemplateContext.Provider value={{ templates }}>
          <TemplatesSearch searchPlaceholder="test-placeholder" />
        </CarePlanTemplateContext.Provider>
      )
    })
    const { container } = wrapper
    const caregiverSearchField = container.querySelector("#ccSearchTemplate")
    const caregiverClearSearchBtn = container.querySelector(
      "#ccClearSearchIcon"
    )
    await waitFor(() => {
      fireEvent.change(caregiverSearchField, {
        target: {
          value: "cat",
          id: "ccSearchTemplate",
        },
      })
      fireEvent.blur(caregiverSearchField, {
        target: {
          value: "cat",
          id: "ccSearchTemplate",
        },
      })
    })
    expect(caregiverClearSearchBtn).toBeDefined()
  })
})
