import { Box, TextField, InputAdornment, Typography } from "@material-ui/core"
import React, { useContext, useState, useEffect } from "react"
import PropTypes from "prop-types"
import { useTranslation } from "react-i18next"

import useCommonStyles from "../../CommonStyles/SearchField"
import { ReactComponent as SearchIcon } from "../../../Assets/Caregivers/search-icon.svg"
import { ReactComponent as ClearSearchIcon } from "../../../Assets/Caregivers/search-cross-icon.svg"
import TemplateItem from "../../TemplateItem"
import carePlanTemplates from "../../../Contexts/CarePlanTemplates"

const TemplatesSearch = ({
  currentStep,
  searchPlaceholder,
  onTemplateSelection,
  nothingFoundDescription,
}) => {
  const commonClasses = useCommonStyles()
  const { t } = useTranslation()
  const [commonList, setCommonList] = useState([])
  const [displayList, setDisplayList] = useState([])
  const [searchTxtValue, setSearchTxtValue] = useState("")
  const { templates } = useContext(carePlanTemplates)

  const search = async (searchValue) => {
    setSearchTxtValue(searchValue)
    // filter records by search text=
    const lowercasedValue = searchValue.toLowerCase().split(" ").join("")
    if (!lowercasedValue) {
      setDisplayList([])
      setSearchTxtValue("")
      return
    }

    const filteredData = templates.filter((item) =>
      item.name.toString().toLowerCase().includes(lowercasedValue)
    )
    setDisplayList(filteredData)
  }

  const renderList = () => {
    if (searchTxtValue && displayList.length) {
      return (
        <Box className={commonClasses.searchList}>
          {displayList.map((list) => (
            <TemplateItem
              item={list}
              key={list.name}
              handleClick={onTemplateSelection}
            />
          ))}
        </Box>
      )
    }
    if (searchTxtValue && !displayList.length) {
      return (
        <Box className={commonClasses.templateNotFound}>
          <img
            src="./images/careplan/template-not-found.png"
            alt="message-icon"
            width="84"
            height="84"
          />
          <Typography className={commonClasses.nothingFound}>
            {t("no-search-text")}
          </Typography>
          <Typography className={commonClasses.nothingFoundDescription}>
            {nothingFoundDescription || t("no-search-text-subscript")}
          </Typography>
        </Box>
      )
    }
    return (
      <>
        <Box className={commonClasses.commonText}>{t("common-text")}</Box>
        <Box>
          {commonList.map((list) => (
            <TemplateItem
              item={list}
              key={list.name}
              handleClick={onTemplateSelection}
            />
          ))}
        </Box>
      </>
    )
  }

  useEffect(() => {
    setCommonList(templates.filter((template) => template.isCommon).slice(0, 4))
  }, [templates])

  // this is to force the render upon step change
  useEffect(() => {
    setSearchTxtValue("")
  }, [currentStep])

  return (
    <>
      <Box className={commonClasses.searchField}>
        <TextField
          id="ccSearchTemplate"
          name="searchTemplate"
          margin="dense"
          autoComplete="off"
          fullWidth
          placeholder={searchPlaceholder}
          value={searchTxtValue}
          onChange={(e) => search(e.target.value)}
          InputProps={{
            endAdornment: (
              <InputAdornment>
                {!searchTxtValue && (
                  <SearchIcon
                    id="ccSearchIcon"
                    src="./images/caregivers/search-icon.svg"
                    alt="search-icon"
                  />
                )}
                {searchTxtValue && (
                  <Box
                    onClick={() => search("")}
                    className="cursor-pointer"
                    id="ccClearSearchIcon"
                  >
                    <ClearSearchIcon
                      src="./images/caregivers/search-cross-icon.svg"
                      alt="search-icon"
                    />
                  </Box>
                )}
              </InputAdornment>
            ),
          }}
        />
      </Box>
      {renderList()}
    </>
  )
}

TemplatesSearch.propTypes = {
  currentStep: PropTypes.number.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  searchPlaceholder: PropTypes.string.isRequired,
  onTemplateSelection: PropTypes.func.isRequired,
}

export default TemplatesSearch
