import { Button, Box, Grid, Typography } from "@material-ui/core"
import React, { useEffect, useState, useContext } from "react"
import { useTranslation } from "react-i18next"
import { useHistory } from "react-router-dom"
import clsx from "clsx"
import CaregiversContext from "../../Contexts/CaregiversProfileContext"
import Loading from "../Loading"
import * as CarePlansApiService from "../../services/CarePlan.service"
import { ReactComponent as NoCarePlan } from "../../Assets/CarePlans/care-plan-no-data.svg"
import CollapsibleDisplay from "../CollapsibleDisplay"
import EditCarePlan from "../EditCarePlan"
import useStyles from "./styles"
import MilestoneCard from "../MilestoneCard"
import { getUpcomingDueDate } from "../../utils/dateformatting"
import {
  UpdateCareGiversMilestone,
  DeleteCareGiversMilestone,
} from "../../services/Caregivers.service"
import { Auth } from "aws-amplify"
import AddEditMilestone from "../AddEditMilestone"
import SnackbarComponent from "../SnackbarMessage"
import getMilestonesInOrder from "../../utils/getMilestonesInOrder"
import ErrorPopup from "../ErrorPopup"

const CaregiversCarePlan = () => {
  const [snackbarMessage, setSnackbarMessage] = useState({
    open: false,
    message: "",
    onClose: () => {
      setSnackbarMessage({
        ...snackbarMessage,
        open: false,
        children: null,
      })
    },
  })
  const [profile] = useState(useContext(CaregiversContext).caregiverProfile)
  const { t } = useTranslation()
  const [carePlan, setCarePlan] = useState()
  const [showLoading, setShowLoading] = useState(true)
  const [editCarePlan, setEditCarePlan] = useState({
    open: false,
    onClose: (status) => {
      if (typeof status !== "number") {
        setEditCarePlan({
          ...editCarePlan,
          open: false,
        })
        return
      }
      if (status === 201) {
        setSnackbarMessage({
          ...snackbarMessage,
          open: true,
          messageState: "info",
          message: t("careplan-update-successful"),
        })
        setEditCarePlan({
          ...editCarePlan,
          open: false,
        })
        getCarePlan()
      } else {
        setSnackbarMessage({
          ...snackbarMessage,
          open: true,
          messageState: `error`,
          message: t("unable-to-update-careplan"),
        })
      }
    },
  })

  const [upcomingItems, setUpcomingItems] = useState([])
  const [completedItems, setCompletedItems] = useState([])
  const [confirmDelete, setConfirmDelete] = useState(false)
  const [editMilestoneData, setEditMilestoneData] = useState(null)
  const [currentActivity, setcurrentActivity] = useState(null)
  const { caregiverProfile } = useContext(CaregiversContext)

  const history = useHistory()
  const classes = useStyles()

  const accordionData = (carePlanData) => {
    let upcomingItems = []
    let completedItems = []
    if (carePlanData.items && carePlanData.items.length > 0) {
      carePlanData.items.forEach((item) => {
        const completedTasks = item.tasks.filter((task) => task.isCompleted)
        let tasks = []
        item.tasks.forEach((task) => {
          tasks.push({ ...task, id: task.uuid })
        })
        if (completedTasks.length === item.tasks.length) {
          completedItems.push({
            ...item,
            id: item.uuid,
            tasks: tasks,
            duedate: item.dueDate,
          })
        } else {
          upcomingItems.push({
            ...item,
            id: item.uuid,
            tasks: tasks,
            duedate: item.dueDate,
          })
        }
        setCompletedItems(getMilestonesInOrder(completedItems))
        setUpcomingItems(getMilestonesInOrder(upcomingItems))
      })
    } else {
      setCompletedItems([])
      setUpcomingItems([])
    }
  }

  const getCarePlan = async () => {
    setShowLoading(true)
    const caregiversResp = await CarePlansApiService.GetCarePlan(profile.uuid)
    if (caregiversResp?.status === 200) {
      setCarePlan(caregiversResp.data && caregiversResp.data.items.length > 0)
      accordionData(caregiversResp.data)
    }
    setShowLoading(false)
  }
  const createCarePlan = () => {
    history.push(`/dashboard/caregivers/${profile.uuid}/careplan/create`)
  }

  const editMilestone = (props, selectedFor) => {
    setcurrentActivity(selectedFor)
    setEditMilestoneData(props)
  }

  const onEditClose = () => {
    setEditMilestoneData(null)
  }

  const onRemove = (id) => {
    setConfirmDelete(true)
  }

  const onEditSave = async (milestoneObj) => {
    const currentUser = await Auth.currentUserInfo()
    if (currentUser) {
      if (milestoneObj.id) {
        delete milestoneObj.id
        if (milestoneObj.duedate) {
          milestoneObj.dueDate = milestoneObj.duedate
          delete milestoneObj.duedate
        } else {
          milestoneObj.dueDate = ""
          delete milestoneObj.duedate
        }
        milestoneObj.tasks.forEach((task) => {
          delete task.id
        })
        const milestoneEdited = await UpdateCareGiversMilestone(
          currentUser.attributes["custom:uuid"],
          caregiverProfile.uuid,
          editMilestoneData.uuid,
          milestoneObj
        )
        if (milestoneEdited?.status === 200) {
          getCarePlan()
          setSnackbarMessage({
            ...snackbarMessage,
            open: true,
            messageState: "info",
            message: `"${milestoneObj.name}" ${t("general-form-updated")}.`,
          })
        } else {
          setSnackbarMessage({
            ...snackbarMessage,
            open: true,
            messageState: "error",
            message: `Unable to save Care Plan. Please try again later.`,
          })
        }
      }
    }
  }

  useEffect(() => {
    getCarePlan()
  }, [])

  if (showLoading) {
    return (
      <Box className={classes.loadingIcon}>
        <Loading />
      </Box>
    )
  }

  // for upcoming Items
  const upcomingTitle = (
    <Typography className={classes.upcomingText}>
      {t("upcoming-title")}
    </Typography>
  )

  const upcomingItemDetail = upcomingItems.map((item) => {
    const rationale = item.rationale
    const description = item.description
    const name = item.name
    const today = new Date()
    const duedate = (
      <Typography
        className={
          new Date(item.dueDate) > today
            ? classes.duedate
            : clsx(classes.duedate, classes.duedateerror)
        }
      >
        {item.dueDate
          ? "Due " + `${getUpcomingDueDate(item.dueDate).replace(",", "")}`
          : " "}
      </Typography>
    )

    return (
      <Box
        width="100%"
        pb="0em"
        id="singleCard"
        onClick={() => editMilestone(item, "upcoming")}
        className={classes.itemscontainer}
      >
        <Box className={classes.upcomingItems}>
          <MilestoneCard
            {...{
              name,
              rationale,
              description,
              duedate: item.dueDate,
              duedatestring: duedate,
              numberOfTask: `${
                item.tasks.filter((task) => task.isCompleted).length
              } of ${item.tasks.length}`,
            }}
          />
        </Box>
      </Box>
    )
  })

  // for completed items
  const completedTitle = (
    <Typography className={classes.upcomingText}>
      {t("completed-title")}
    </Typography>
  )

  const completedItemDetail = completedItems.map((item) => {
    const rationale = item.rationale
    const description = item.description
    const name = item.name
    return (
      <Box
        width="100%"
        pb="0em"
        id="singleCard"
        onClick={() => editMilestone(item, "completed")}
        className={classes.itemscontainer}
      >
        <Box className={classes.upcomingItems}>
          <MilestoneCard
            {...{
              name,
              rationale,
              description,
              numberOfTask: item.tasks?.length || 0,
            }}
          />
        </Box>
      </Box>
    )
  })

  const onEditTaskSave = (taskObj, allTasks) => {
    if (taskObj.isCompleted === false) {
      // show confirmation message of task getting added
      setSnackbarMessage({
        ...snackbarMessage,
        open: true,
        messageState: "info",
        message: `"${taskObj.name}" ${t("caregivers-add-note-added-success")}.`,
      })
    } else {
      // show confirmation message of task getting updated
      setSnackbarMessage({
        ...snackbarMessage,
        open: true,
        messageState: "info",
        message: `"${taskObj.name}" ${t(
          "caregivers-add-note-updated-success"
        )}.`,
      })
    }
  }

  const onEditTaskClose = () => {
    setEditMilestoneData(null)
  }

  const onRemoveConfirm = async (action) => {
    if (action == "no") {
      setConfirmDelete(false)
    } else {
      let id = editMilestoneData?.uuid //used safe navigation operator
      let milestoneName = editMilestoneData?.name
      setConfirmDelete(false)
      setEditMilestoneData(null)
      if (!id || !currentActivity) {
        return
      }
      let removedUId =
        currentActivity == "upcoming"
          ? upcomingItems.filter((item) => item.uuid == id)[0].uuid
          : completedItems.filter((item) => item.uuid == id)[0].uuid

      const currentUser = await Auth.currentUserInfo()
      if (currentUser) {
        if (removedUId) {
          const milestoneRemoved = await DeleteCareGiversMilestone(
            currentUser.attributes["custom:uuid"],
            caregiverProfile.uuid,
            removedUId
          )
          if (milestoneRemoved?.status === 200) {
            getCarePlan()
            setSnackbarMessage({
              ...snackbarMessage,
              open: true,
              messageState: "info",
              message: `"${milestoneName}" removed.`,
            })
          } else {
            setSnackbarMessage({
              ...snackbarMessage,
              open: true,
              messageState: "error",
              message: `Unable to Delete Milestone. Please try again later.`,
            })
          }
        }
      }
    }
  }
  const onClose = () => {
    setConfirmDelete(false)
  }

  const ShowErorPopup = (prop) => {
    return (
      <ErrorPopup
        open={confirmDelete}
        title={
          `${t("general-form-remove-text")} "` + editMilestoneData.name + `?"`
        }
        description_1={t("confirm-remove-description")}
        handleActions={onRemoveConfirm}
        standalone={true}
        direction={"right"}
        showBackArrow={true}
        onClose={onClose}
        hideCrossClose={true}
        leftButtonText={t("nevermind-button-text-error-drawer")}
        rightButtonText={t("general-form-remove-text")}
      />
    )
  }

  return (
    <>
      {snackbarMessage.open ? <SnackbarComponent {...snackbarMessage} /> : null}
      {editMilestoneData && (
        <AddEditMilestone
          {...editMilestoneData}
          open={!!editMilestoneData}
          onClose={onEditClose}
          onSave={onEditSave}
          onRemove={onRemove}
          onEditTaskSave={onEditTaskSave}
          onEditTaskClose={onEditTaskClose}
        />
      )}
      {confirmDelete && ShowErorPopup()}
      {editCarePlan.open && <EditCarePlan {...editCarePlan} />}
      <Grid container>
        <Grid item xs={12}>
          <Box
            display="flex"
            flexDirection="row"
            alignItems="center"
            justifyContent="space-between"
            pt="72px"
            pr="72px"
            pl="72px"
            pb="0em"
          >
            <Box
              fontSize={36}
              fontWeight="fontWeightBold"
              color="darkBlue"
              className="capitalize"
            >
              {profile.nickName ? profile.nickName : profile.firstName}
              {t("care-plan-header")}
            </Box>
            <Box fontSize={14}>
              {!carePlan && (
                <Button
                  variant="outlined"
                  className={classes.customButton}
                  id="createBtn"
                  onClick={() => createCarePlan()}
                >
                  {t("create")}
                </Button>
              )}
              {carePlan && (
                <Button
                  variant="outlined"
                  className={classes.customButton}
                  id="editcareplanaddmilestone"
                  onClick={() =>
                    setEditCarePlan({
                      ...editCarePlan,
                      open: true,
                    })
                  }
                >
                  {t("add-milestones")}
                </Button>
              )}
            </Box>
          </Box>
        </Grid>
        {!carePlan && (
          <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justify="center"
            className={classes.noCareplan}
          >
            <Grid item xs={4} align="center">
              <NoCarePlan />
              <Box className={classes.noCareplanText}>
                {t("no-careplans-text")}
              </Box>
              <Box className={classes.noCareplanTextSubscript}>
                {t("no-careplans-text-subscript")}
              </Box>
              <Button
                variant="outlined"
                className={classes.createButton}
                id="createCarePlan"
                onClick={() => createCarePlan()}
              >
                {t("create-care-plan")}
              </Button>
            </Grid>
          </Grid>
        )}
        {carePlan && (
          <Grid item xs={12}>
            <Box px="72px" pb="100px">
              {upcomingItems.length > 0 && (
                <CollapsibleDisplay
                  title={upcomingTitle}
                  count={upcomingItems.length}
                  children={upcomingItemDetail}
                  titleContainer={"titleContainer"}
                  titleRow={"titleRow"}
                  collapsableIcon={"collapsableIcon"}
                />
              )}
              {completedItems.length > 0 && (
                <CollapsibleDisplay
                  title={completedTitle}
                  count={completedItems.length}
                  children={completedItemDetail}
                  titleContainer={"titleContainer"}
                  titleRow={"titleRow"}
                  collapsableIcon={"collapsableIcon"}
                />
              )}
            </Box>
          </Grid>
        )}
      </Grid>
    </>
  )
}

export default CaregiversCarePlan
