import React from "react"
import renderer from "react-test-renderer"
import { fireEvent, render, waitFor, screen } from "@testing-library/react"
import CarePlan from "."
import * as CarePlansApiService from "../../services/CarePlan.service"
import * as CaregiversService from "./../../services/Caregivers.service"

jest.mock("aws-amplify", () => {
  return {
    __esModule: true,
    Auth: {
      currentUserInfo: () =>
        Promise.resolve({
          attributes: {
            "custom:uuid": "acc2ea0f-3a1f-49b9-8b19",
          },
          username: "acc2ea0f-3a1f-49b9-8b19-7f555ece71d6",
        }),
    },
  }
})

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useParams: () => ({ caregiversUuid: "abc-123-hdfhj-1234" }),
}))

describe("Care plan page", () => {
  beforeEach(() => {
    jest.spyOn(CarePlansApiService, "GetCarePlan").mockResolvedValue({
      status: 200,
      data: {
        isDraft: true,
        items: [
          {
            name: "Cancel Driving Licence",
            rationale: "Describe what is the rationale behind it. ",
            description:
              "Describe the item properly. Lorem ipsum is placeholder text commonly used ",
            dueDate: "2021-02-27",
            tasks: [
              {
                name: '"Read why you need to cancel DL"',
                link: '"https://loremipsum.io/"',
                isCompleted: true,
              },
            ],
            updatedAt: "2021-04-08T07:49:41.713Z",
            uuid: "aa121917-22f7-47f6-b296-f97adb59633b",
          },
        ],
      },
    })
    jest
      .spyOn(CaregiversService, "UpdateCareGiversMilestone")
      .mockResolvedValue({
        status: 200,
        data: {},
      })
    jest
      .spyOn(CaregiversService, "DeleteCareGiversMilestone")
      .mockResolvedValue({
        status: 200,
        data: {},
      })
  })

  it("test render", () => {
    const tree = renderer.create(<CarePlan />).toJSON()
    expect(tree).toMatchSnapshot()
  })
  it("Verify component show care plan text", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<CarePlan />)
    })
    const { queryByText } = wrapper
    expect(queryByText(/^care-plan-header$/)).toBeDefined()
  })
  it("Edit care plan by adding milestone", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<CarePlan />)
    })
    const { queryByText } = wrapper
    const addMilestone = queryByText(/^add-milestones$/)
    await waitFor(() => {
      fireEvent.click(addMilestone)
    })
    expect(screen.getByText(/^add-new-task-subscript$/)).toBeDefined()
  })

  it("Verify For Upcoming and Completed Milestones", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<CarePlan />)
    })
    const { container, queryByText } = wrapper

    const createBtn = container.querySelector("#createBtn")
    expect(createBtn).toBeDefined()

    expect(queryByText(/^upcoming-title$/)).toBeDefined()
    expect(queryByText(/^completed-title$/)).toBeDefined()
  })

  it("Edit Popup Upcoming Popup Test", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<CarePlan />)
    })
    const { container, queryByText } = wrapper
    const singleCard = container.querySelector("#singleCard")

    await waitFor(() => {
      fireEvent.click(singleCard)
    })

    const name = screen.getByLabelText(/^edit-milestone-name$/)
    expect(name).toBeDefined()
  })

  it("Edit Popup Upcoming Popup Remove Button Click", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<CarePlan />)
    })
    const { container } = wrapper
    const singleCard = container.querySelector("#singleCard")

    await waitFor(() => {
      fireEvent.click(singleCard)
    })

    const removeBtn = screen.getByRole("button", {
      name: /^general-form-remove-text$/,
    })
    expect(removeBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.click(removeBtn)
    })

    // expect(CaregiversService.DeleteCareGiversMilestone).toHaveBeenCalled()
    expect(screen.getByText(/^confirm-remove-description$/)).toBeDefined()

    const neverMindBtn = screen.getByRole("button", {
      name: /^nevermind-button-text-error-drawer$/,
    })

    expect(neverMindBtn).toBeDefined()
  })

  it("Edit Popup Upcoming Popup Close Button Click", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<CarePlan />)
    })
    const { container } = wrapper
    const singleCard = container.querySelector("#singleCard")

    await waitFor(() => {
      fireEvent.click(singleCard)
    })

    const cancelBtn = screen.getByRole("button", {
      name: /^general-form-cancel-text$/,
    })
    expect(cancelBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.click(cancelBtn)
    })
  })

  it("Edit Popup Upcoming Popup add new task options", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<CarePlan />)
    })
    const { container } = wrapper
    const singleCard = container.querySelector("#singleCard")
    await waitFor(() => {
      fireEvent.click(singleCard)
    })

    const addNewTaskBtn = screen.getByRole("button", {
      name: /^general-form-add-new$/,
    })
    expect(addNewTaskBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.click(addNewTaskBtn)
    })
    const name = screen.getByLabelText(/^edit-task-name$/)
    await waitFor(() => {
      fireEvent.change(name, {
        target: {
          value: "new name",
          id: "taskname",
        },
      })
      fireEvent.blur(name, {
        target: {
          value: "new name",
          id: "taskname",
        },
      })
    })
    const addNewTaskFormBtn = screen.getByText(/^general-form-add-text$/)
    expect(addNewTaskFormBtn).toBeDefined()
  })
})
