import { makeStyles } from "@material-ui/core/styles"

export default makeStyles((theme) => ({
  title: {
    fontSize: "22px",
    fontWeight: 600,
    color: theme.palette.darkBlue,
  },
  description: {
    fontSize: "14px",
    fontWeight: 500,
    paddingTop: "16px",
    color: theme.palette.nightShade,
  },
}))
