import { Typography } from "@material-ui/core"
import React from "react"
import PropTypes from "prop-types"
import useStyles from "./styles"

const CaregiversCareplanStepInfo = ({ title, description }) => {
  const classes = useStyles()
  return (
    <>
      <Typography className={classes.title} variant="h6">
        {title}
      </Typography>
      <Typography className={classes.description}>{description}</Typography>
    </>
  )
}

CaregiversCareplanStepInfo.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
}

export default CaregiversCareplanStepInfo
