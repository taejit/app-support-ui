import React from "react"
import { render, waitFor } from "@testing-library/react"
import CaregiversCareplanStepInfo from "./index"

describe("CaregiversCareplanStepInfo component testing", () => {
  it("Should display CaregiversCareplanStepInfo page", async () => {
    let wrapper
    await waitFor(async () => {
      wrapper = render(<CaregiversCareplanStepInfo />)
    })
    const { queryByText } = wrapper
    expect(queryByText(/^profile$/)).toBeDefined()
  })
})
