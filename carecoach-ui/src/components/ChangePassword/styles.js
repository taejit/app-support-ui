import { makeStyles } from "@material-ui/core/styles"

const styles = makeStyles((theme) => ({
  root: {
    "& .MuiDrawer-paper": {
      width: "456px",
      padding: "34px",
    },
  },
  form: {
    flex: "1 0 auto",
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  error: {
    color: "red",
  },
  success: {
    color: "green",
  },
  message: {
    paddingLeft: "0",
    paddingTop: "16px",
    paddingBottom: "16px",
  },
  grid: {
    paddingTop: "16px",
  },
  changepasswordbtn: {
    "&:hover": {
      backgroundColor: theme.palette.lanternGold,
    },
    fontSize: "16px",
    fontWeight: "500",
    color: theme.palette.darkBlue,
    borderRadius: "25px",
    backgroundColor: theme.palette.lanternGold,
    padding: "10px 40px 10px 40px",
    "& :disabled": {
      opacity: "0.5",
    },
  },
  footer: {
    marginTop: "auto",
  },
  buttonplacement: {
    display: "flex",
    justifyContent: "space-between",
    width: "100%",
  },
  cancelbtn: {
    color: theme.palette.morningGlory,
    fontSize: "15px",
    fontWeight: "600",
    padding: "0",
  },
  editmyprofiletext: {
    fontSize: "27px",
    fontWeight: "bold",
    color: theme.palette.darkBlue,
    height: "34px",
    letterSpacing: "normal",
    marginBottom: "36px",
  },
  usericon: {
    height: "48px",
    width: "48px",
    "& g": {
      stroke: theme.palette.darkBlue,
    },
  },
  editprofilepicturecontainer: {
    "& .upload-inprogress": {
      "& div:first-child": {
        paddingTop: "1%",
      },
    },
  },
  donebuttonposition: {
    width: "100%",
    height: "fit-content",
    textAlign: "center",
    backgroundColor: theme.palette.lanternGold,
    borderRadius: "25px",
  },
  donebutton: {
    "& :hover": {
      backgroundColor: theme.palette.lanternGold,
    },
    backgroundColor: theme.palette.lanternGold,
    borderRadius: "25px",
    width: "100%",
    height: "fit-content",
    fontSize: "16px",
    fontWeight: "500",
    color: theme.palette.darkBlue,
    textTransform: "none",
  },
}))

export default styles
