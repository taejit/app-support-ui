import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { useTranslation } from "react-i18next"
import { useFormik } from "formik"
import * as Yup from "yup"
import { Auth } from "aws-amplify"
import { Grid, Typography, Button, Drawer, TextField } from "@material-ui/core"
import { ReactComponent as KeysIcon } from "../../Assets/keys.svg"
import useStyles from "./styles"

const ChangePassword = ({ open, onClose }) => {
  const classes = useStyles()
  const { t } = useTranslation()
  const [openDrawer, setOpenDrawer] = useState(open)
  const toggleDrawer = (toggleOpen) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return
    }
    setOpenDrawer(toggleOpen)
    if (onClose) {
      onClose()
    }
  }

  const [errorMsg, setErrorMsg] = useState("")

  useEffect(() => {
    setOpenDrawer(open)
  }, [open])

  const [showError, setShowError] = useState(false)
  const [showSuccess, setShowSuccess] = useState(false)
  const formik = useFormik({
    initialValues: {
      currentPassword: "",
      newPassword: "",
    },
    validateOnChange: true,
    validationSchema: Yup.object({
      currentPassword: Yup.string()
        .required(t("change-password-current-password-required"))
        .nullable()
        .min(6, t("change-password-min-characters"))
        .max(99, t("change-password-max-characters")),
      newPassword: Yup.string()
        .required(t("change-password-new-password-required"))
        .nullable()
        .min(8, t("change-password-min-characters"))
        .max(99, t("change-password-max-characters"))
        .matches(
          RegExp("(.*[a-z].*)"),
          t("change-password-lowercase-character")
        )
        .matches(
          RegExp("(.*[A-Z].*)"),
          t("change-password-uppercase-characters")
        )
        .matches(RegExp("(.*\\d.*)"), t("change-password-number"))
        /* eslint-disable no-useless-escape */
        .matches(
          /^(.*[=+\-^$*.\[\]{}()?"!@#%&/\\,><':;|_~`].*)$/,
          t("change-password-special-characters")
        ),
    }),
    onSubmit: async (values) => {
      if (values.currentPassword === values.newPassword) {
        setShowError(true)
        setErrorMsg(t("password-shouldnt-same-error-message"))
      } else {
        const currentUser = await Auth.currentAuthenticatedUser()
        if (currentUser) {
          const data = await Auth.changePassword(
            currentUser,
            values.currentPassword,
            values.newPassword
          ).catch((error) => {
            if (
              (error &&
                error.message ==
                  "1 validation error detected: Value at 'previousPassword' failed to satisfy constraint: Member must have length greater than or equal to 6") ||
              error.message == "Incorrect username or password."
            ) {
              setErrorMsg(t("current-password-wrong-error-message"))
            } else {
              setErrorMsg(t("change-password-error-message"))
            }
            setShowError(true)
            setShowSuccess(false)
          })
          if (data === "SUCCESS") {
            formik.resetForm()
            setShowSuccess(true)
            setShowError(false)
            setErrorMsg("")
          }
          formik.setSubmitting(false)
        }
      }
    },
  })

  return (
    <Drawer
      className={classes.root}
      anchor="right"
      open={openDrawer}
      onClose={toggleDrawer(false)}
    >
      <form onSubmit={formik.handleSubmit} className={classes.form}>
        <Grid container>
          <Grid item xs={12}>
            <KeysIcon className={classes.usericon} />
            <Typography component="div" className={classes.editmyprofiletext}>
              {t("change-password-title")}
            </Typography>
          </Grid>
          <Grid item xs={12} className={classes.grid}>
            <TextField
              fullWidth
              id="currentPassword"
              label={t("change-password-current-password")}
              type="password"
              {...formik.getFieldProps("currentPassword")}
            />
            {formik.touched.currentPassword &&
              formik.errors.currentPassword && (
                <div className={classes.error}>
                  {formik.errors.currentPassword}
                </div>
              )}
          </Grid>
          <Grid item xs={12} className={classes.grid}>
            <TextField
              fullWidth
              id="newPassword"
              label={t("change-password-new-password")}
              type="password"
              helperText={t("change-password-new-password-criteria")}
              {...formik.getFieldProps("newPassword")}
            />
            {formik.touched.newPassword && formik.errors.newPassword && (
              <div className={classes.error}>{formik.errors.newPassword}</div>
            )}
          </Grid>
        </Grid>

        <Grid container className={classes.footer}>
          <Grid item className={classes.message}>
            {showError && (
              <Typography
                component="h4"
                className={classes.error}
                id="errorMsg"
              >
                {errorMsg && t(errorMsg)}
              </Typography>
            )}
            {showSuccess && (
              <Typography component="h4" className={classes.success}>
                {t("change-password-success-message")}
              </Typography>
            )}
          </Grid>
          <Grid item className={classes.buttonplacement}>
            <Button onClick={toggleDrawer(false)} className={classes.cancelbtn}>
              {t("change-password-cancel")}
            </Button>
            <Button
              type="submit"
              className={classes.changepasswordbtn}
              disabled={!formik.isValid || !formik.dirty}
              id="changePasswordsubmit"
            >
              {t("change-password-change-password")}
            </Button>
          </Grid>
        </Grid>
      </form>
    </Drawer>
  )
}

ChangePassword.defaultProps = {
  onClose: () => {},
}

ChangePassword.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func,
}

export default ChangePassword
