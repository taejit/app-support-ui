import React from "react"
import { render, waitFor, fireEvent, screen } from "@testing-library/react"
import { Auth } from "aws-amplify"
import ChangePassword from "."

describe("ChangePassword test", () => {
  beforeEach(() => {
    jest.spyOn(Auth, "currentAuthenticatedUser").mockResolvedValue(
      Promise.resolve({
        attributes: {
          email: "bramsai@intraedge.com",
          email_verified: true,
          phone_number: "+12254789321",
          phone_number_verified: true,
          sub: "38041359-614f-4c44-9a32-23c3c80506c7",
          family_name: "kiran",
          given_name: "ramsai",
          "custom:location": "Hyderabad Telangana",
          "custom:pronoun": "he/his",
          "custom:uuid": "test-uuid",
          "custom:job_start_date": new Date("2021-01-30"),
          "custom:bio": "bio",
          "custom:credentials": "test-credentials",
        },
        username: "acc2ea0f-3a1f-49b9-8b19-7f555ece71d6",
      })
    )
    jest.spyOn(Auth, "changePassword").mockRejectedValue({
      __type: "NotAuthorizedException",
      message: "Incorrect username or password.",
    })
  })
  it("Verify component", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<ChangePassword open={true} onClose={() => {}} />)
    })
    const { queryByText, container } = wrapper
    expect(
      screen.queryByText(/^view-profile-change-my-password$/)
    ).toBeDefined()
    expect(
      screen.queryByText(/^change-password-current-password$/)
    ).toBeDefined()
    expect(screen.queryByText(/^change-password-new-password$/)).toBeDefined()

    const changePassword = screen.getByRole("button", {
      name: /^change-password-change-password$/,
    })
    expect(changePassword).toHaveAttribute("disabled")
  })

  it("Verify component Error Msges password shouldnt match", async () => {
    render(<ChangePassword open onClose={jest.fn()} />)

    const currentPasswordIn = screen.getByLabelText(
      /^change-password-current-password$/
    )

    await waitFor(() => {
      fireEvent.change(currentPasswordIn, {
        target: {
          value: "Test@1234",
          id: "currentPassword",
        },
      })
    })

    const newPasswordIn = screen.getByLabelText(
      /^change-password-new-password$/
    )

    await waitFor(() => {
      fireEvent.change(newPasswordIn, {
        target: {
          value: "Test@123623!",
          id: "newPassword",
        },
      })
    })

    const changePwdSubmit = screen.getByRole("button", {
      name: /^change-password-change-password$/,
    })
    await waitFor(() => {
      fireEvent.submit(changePwdSubmit)
    })

    expect(Auth.currentAuthenticatedUser).toHaveBeenCalled()
  })

  it("Verify component Error Msges password shouldnt match, throw error", async () => {
    render(<ChangePassword open onClose={jest.fn()} />)

    const currentPasswordIn = screen.getByLabelText(
      /^change-password-current-password$/
    )

    await waitFor(() => {
      fireEvent.change(currentPasswordIn, {
        target: {
          value: "Test@1234",
          id: "currentPassword",
        },
      })
    })

    const newPasswordIn = screen.getByLabelText(
      /^change-password-new-password$/
    )

    await waitFor(() => {
      fireEvent.change(newPasswordIn, {
        target: {
          value: "Test@1234",
          id: "newPassword",
        },
      })
    })

    const changePwdSubmit = screen.getByRole("button", {
      name: /^change-password-change-password$/,
    })
    await waitFor(() => {
      fireEvent.submit(changePwdSubmit)
    })

    expect(Auth.currentAuthenticatedUser).toHaveBeenCalledTimes(0)
  })

  it("Verify component Error Msges", async () => {
    let wrapper
    wrapper = render(<ChangePassword open onClose={jest.fn()} />)
    const { queryByText, debug } = wrapper

    const currentPasswordIn = screen.getByLabelText(
      /^change-password-current-password$/
    )

    await waitFor(() => {
      fireEvent.change(currentPasswordIn, {
        target: {
          value: "Test@1234876",
          id: "currentPassword",
        },
      })
    })

    const newPasswordIn = screen.getByLabelText(
      /^change-password-new-password$/
    )

    await waitFor(() => {
      fireEvent.change(newPasswordIn, {
        target: {
          value: "Test@123623!",
          id: "newPassword",
        },
      })
    })

    const changePasswordBtn = screen.getByRole("button", {
      name: /^change-password-change-password$/,
    })

    await waitFor(() => {
      fireEvent.submit(changePasswordBtn)
    })

    expect(Auth.currentAuthenticatedUser).toHaveBeenCalled()

    expect(Auth.changePassword).toHaveBeenCalled()

    expect(queryByText(/^current-password-wrong-error-message$/)).toBeDefined()
  })

  it("Verify component Error Msges - couldnot post cal", async () => {
    jest.spyOn(Auth, "changePassword").mockRejectedValue({})

    let wrapper
    wrapper = render(<ChangePassword open onClose={jest.fn()} />)
    const { queryByText, debug } = wrapper

    const currentPasswordIn = screen.getByLabelText(
      /^change-password-current-password$/
    )

    await waitFor(() => {
      fireEvent.change(currentPasswordIn, {
        target: {
          value: "Test@1234876",
          id: "currentPassword",
        },
      })
    })

    const newPasswordIn = screen.getByLabelText(
      /^change-password-new-password$/
    )

    await waitFor(() => {
      fireEvent.change(newPasswordIn, {
        target: {
          value: "Test@123623!",
          id: "newPassword",
        },
      })
    })

    const changePasswordBtn = screen.getByRole("button", {
      name: /^change-password-change-password$/,
    })

    await waitFor(() => {
      fireEvent.submit(changePasswordBtn)
    })

    expect(Auth.currentAuthenticatedUser).toHaveBeenCalled()

    expect(Auth.changePassword).toHaveBeenCalled()

    expect(queryByText(/^current-password-wrong-error-message$/)).toBeDefined()
  })

  it(" Does not throw  Error Msges", async () => {
    jest.spyOn(Auth, "changePassword").mockResolvedValue("SUCCESS")
    let wrapper
    wrapper = render(<ChangePassword open onClose={jest.fn()} />)
    const { queryByText, debug } = wrapper

    const currentPasswordIn = screen.getByLabelText(
      /^change-password-current-password$/
    )

    await waitFor(() => {
      fireEvent.change(currentPasswordIn, {
        target: {
          value: "Test@1234876",
          id: "currentPassword",
        },
      })
    })

    const newPasswordIn = screen.getByLabelText(
      /^change-password-new-password$/
    )

    await waitFor(() => {
      fireEvent.change(newPasswordIn, {
        target: {
          value: "Test@123623!",
          id: "newPassword",
        },
      })
    })

    const changePasswordBtn = screen.getByRole("button", {
      name: /^change-password-change-password$/,
    })

    await waitFor(() => {
      fireEvent.submit(changePasswordBtn)
    })

    expect(Auth.currentAuthenticatedUser).toHaveBeenCalled()

    expect(Auth.changePassword).toHaveBeenCalled()
  })
})
