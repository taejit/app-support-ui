import React from "react"
import { render, screen, waitFor } from "@testing-library/react"
import { createMemoryHistory } from "history"
import { Router } from "react-router-dom"
import Home from "."

describe("Home component testing", () => {
  it("Should display welcome page", async () => {
    const history = createMemoryHistory()
    history.push("/welcome")
    await waitFor(async () => {
      render(
        <Router history={history}>
          <Home initialRedirect="/welcome" />
        </Router>
      )
    })
    expect(screen.getByText(/welcometext/)).toBeInTheDocument()
  })
})
