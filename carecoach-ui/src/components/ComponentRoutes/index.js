/* eslint react/prop-types: 0 */
import React, { Suspense } from "react"
import { Redirect, Route, Switch } from "react-router-dom"
import Loading from "../Loading"

// routes config
import routes from "../routes"

const ComponentRoutes = ({ initialRedirect }) => {
  return (
    <Suspense fallback={<Loading />}>
      <Switch>
        {routes.map((route) => {
          return (
            route.component && (
              <Route
                key={route.name}
                path={route.path}
                exact={route.exact}
                name={route.name}
                render={() => <route.component />}
              />
            )
          )
        })}
        <Redirect to={initialRedirect} />
      </Switch>
    </Suspense>
  )
}

export default React.memo(ComponentRoutes)
