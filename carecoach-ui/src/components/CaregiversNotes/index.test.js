import React from "react"
import { render, waitFor, fireEvent } from "@testing-library/react"
import CaregiversProfileContext from "../../Contexts/CaregiversProfileContext"
import * as CaregiversService from "../../services/Caregivers.service"
import CaregiversNotes from "."

jest.mock("aws-amplify", () => {
  return {
    __esModule: true,
    Auth: {
      currentUserInfo: () =>
        Promise.resolve({
          attributes: {
            email: "bramsai@intraedge.com",
            email_verified: true,
            phone_number: "+12254789321",
            phone_number_verified: true,
            sub: "38041359-614f-4c44-9a32-23c3c80506c7",
            family_name: "kiran",
            given_name: "ramsai",
            "custom:location": "Hyderabad Telangana Hyd",
            "custom:pronoun": "he/his",
          },
          username: "acc2ea0f-3a1f-49b9-8b19-7f555ece71d6",
        }),
    },
  }
})

describe("Notes display test", () => {
  const caregiverProfile = {
    uuid: "c1260a8b-28f3-4b94-88ff-9bdb3da94f37",
    firstName: "John",
    lastName: "Coach",
    nickName: "Charlie",
    email: "example@lantern.care",
    phone: "+16021234567",
    employerName: "ABC Inc",
    pronouns: "he/him",
    location: "Gilbert, Arizona",
    role: "Primary Caregiver",
    timeSpentInRole: "Less than 6 months",
    recipient: {
      uuid: "c1260a8b-28f3-4b94-88ff-9bdb3da94f37",
      firstName: "John",
      lastName: "Coach",
      nickName: "Charlie",
      pronouns: "He/him",
      dateOfBirth: "2020-12-31",
      relationship: "he/him",
      location: "Gilbert, Arizona",
      livingArrangement: "Lives indepedently",
      livingArrangementDescription: "Lives among the wolves",
      conditions: ["Aging", "Diabetes", "Dementia", "Other1", "Other2"],
    },
  }

  const notesData = [
    {
      title: "Some title",
      description: "some description",
      updatedAt: "2021-03-03T14:42:50.759Z",
    },
    {
      title: "Some title",
      description: "some description",
      updatedAt: "2021-04-03T14:42:50.759Z",
    },
    {
      title: "Some title",
      description: "some description",
      updatedAt: "2021-05-03T14:42:50.759Z",
    },
  ]

  it("Load the notes", async () => {
    CaregiversService.GetCaregiversNotes = () =>
      Promise.resolve({
        status: 200,
        data: notesData,
      })

    let wrapper
    await waitFor(() => {
      wrapper = render(
        <CaregiversProfileContext.Provider value={{ caregiverProfile }}>
          <CaregiversNotes />
        </CaregiversProfileContext.Provider>
      )
    })

    const { queryAllByText } = wrapper
    expect(queryAllByText(notesData[0].title)).toHaveLength(3)
  })

  it("Click on add button should open right drawer", async () => {
    CaregiversService.GetCaregiversNotes = () =>
      Promise.resolve({
        status: 200,
        data: notesData,
      })
    let wrapper
    await waitFor(() => {
      wrapper = render(
        <CaregiversProfileContext.Provider value={{ caregiverProfile }}>
          <CaregiversNotes />
        </CaregiversProfileContext.Provider>
      )
    })
    const { container, queryByText } = wrapper
    const addBtn = container.querySelector("#addnotebtn")
    await waitFor(() => {
      fireEvent.click(addBtn)
    })
    expect(queryByText(/^caregivers-add-note$/)).toBeDefined()
  })
})
