import { makeStyles } from "@material-ui/core/styles"

export default makeStyles((theme) => ({
  notescontainer: {
    paddingBottom: "96px",
  },
  pagetitle: {
    marginBottom: "56px",
  },
  titlerow: {
    display: "flex",
    alignItems: "center",
    paddingRight: "72px",
  },
  addbtn: {
    height: "36px",
    width: "76px",
    marginLeft: "auto",
    marginTop: "42px",
  },
  notesdisplay: {
    display: "flex",
    flexWrap: "nowrap",
    paddingRight: "64px",
    paddingLeft: "64px",
  },
  nonotes: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
  },
  jotthatdown: {
    margin: "12px",
    fontSize: "18px",
    fontWeight: "500",
    lineHeight: "1.33",
    letterSpacing: "normal",
    color: theme.palette.darkBlue,
  },
  nonotesmessage: {
    textAlign: "center",
    fontSize: "14px",
    fontWeight: "500",
    lineHeight: "1.5",
    letterSpacing: "normal",
    color: theme.palette.nightShade,
  },
  addanote: {
    margin: "36px",
    padding: "12px 42px",
  },
  nonotesimage: {
    width: "96px",
  },
  undobtn: {
    cursor: "pointer",
    fontSize: "12px",
    fontWeight: "600",
    lineHeight: "1.5",
    padding: "14px",
    width: "68px",
    borderTopRightRadius: "4px",
    borderBottomRightRadius: "4px",
    color: theme.palette.white,
    backgroundColor: theme.palette.brightBlue,
  },
}))
