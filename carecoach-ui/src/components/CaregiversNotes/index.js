import React, { useContext, useEffect, useState } from "react"
import { useTranslation } from "react-i18next"
import { Auth } from "aws-amplify"
import clsx from "clsx"
import { Box, Typography, Button } from "@material-ui/core"
import {
  GetCaregiversNotes,
  EditCaregiversNote,
} from "../../services/Caregivers.service"
import AddEditNote from "../AddEditNote"
import NoteCard from "../NoteCard"
import CaregiversContext from "../../Contexts/CaregiversProfileContext"
import CaregiversPageTitle from "../CommonStyles/CaregiversPageTitle"
import OutlineButton from "../CommonStyles/OutlineButton"
import FilledButton from "../CommonStyles/FilledButton"
import SnackbarMessage from "../SnackbarMessage"
import Loading from "../Loading"
import useStyles from "./styles"
import { ReactComponent as NoNotes } from "../../Assets/no-notes.svg"

const CaregiversNotes = () => {
  const [loading, setLoading] = useState(true)
  const [editDetails, setEditDetails] = useState({})
  const [snackbarMessage, setSnackbarMessage] = useState({
    open: false,
    message: "",
    onClose: () =>
      setSnackbarMessage({
        ...snackbarMessage,
        open: false,
        children: null,
      }),
  })

  const [openAddNote, setOpenAddNote] = useState(false)
  const { caregiverProfile } = useContext(CaregiversContext)
  const [notes, setNotes] = useState([])
  const { t } = useTranslation()

  const pageTitleClasses = CaregiversPageTitle()
  const btnClasses = OutlineButton()
  const filledBtnClasses = FilledButton()
  const classes = useStyles()

  const addNote = () => {
    setOpenAddNote(true)
  }

  const getCaregiversNotes = async () => {
    const currentUser = await Auth.currentUserInfo()
    if (currentUser) {
      const notesList = await GetCaregiversNotes(
        currentUser.attributes["custom:uuid"],
        caregiverProfile.uuid
      )
      if (notesList && notesList.status === 200) {
        setNotes(notesList.data)
      }
      setLoading(false)
    }
  }

  const undoNoteRemove = async () => {
    const currentUser = await Auth.currentUserInfo()
    if (currentUser) {
      const removeUndoResp = await EditCaregiversNote(
        currentUser.attributes["custom:uuid"],
        caregiverProfile.uuid,
        editDetails.uuid,
        {
          isActive: true,
        }
      )

      if (removeUndoResp && removeUndoResp.status === 200) {
        // after deleting, get fresh list of notes
        getCaregiversNotes()
      }
    }
  }

  const undoBtn = (
    <Typography
      component="div"
      className={classes.undobtn}
      onClick={() => undoNoteRemove()}
    >
      {t("caregivers-delete-undo")}
    </Typography>
  )

  const onNoteDrawerClose = (msg, undo) => {
    setOpenAddNote(false)
    if (typeof msg === "string") {
      // show snackbar
      setSnackbarMessage({
        ...snackbarMessage,
        open: true,
        message: msg,
        ...(undo && { children: undoBtn }),
      })
      // get the new list of notes
      getCaregiversNotes()
    }
    setEditDetails({
      title: "",
      description: "",
      createdAt: "",
      nextStep: "",
      isActive: true,
      uuid: "",
    })
  }

  useEffect(() => {
    getCaregiversNotes()
  }, [])

  const getNewContainer = (elements, index) => {
    return (
      <Box key={`${index}container`} className={classes.notesdisplay}>
        {elements}
      </Box>
    )
  }

  const editNote = (noteDetails) => {
    setEditDetails(noteDetails)
    setOpenAddNote(true)
  }

  const getDisplayNotes = () => {
    return notes
      .reduce((acc, cur, index) => {
        const newNote = (
          <NoteCard key={`${cur.updatedAt}`} {...cur} onClick={editNote} />
        )
        if (index % 3) {
          acc[acc.length - 1].push(newNote)
        } else {
          acc.push([newNote])
        }
        return acc
      }, [])
      .map((cardGroup, index) => {
        return getNewContainer(cardGroup, index)
      })
  }

  if (loading) {
    return <Loading />
  }

  return (
    <>
      {snackbarMessage.open && <SnackbarMessage {...snackbarMessage} />}
      {openAddNote && (
        <AddEditNote
          {...editDetails}
          open={openAddNote}
          onClose={onNoteDrawerClose}
        />
      )}
      <Box className={classes.notescontainer}>
        <Box className={classes.titlerow}>
          <Typography
            className={clsx(pageTitleClasses.pagetitle, classes.pagetitle)}
            variant="h6"
            id="notestitle"
            component="div"
          >
            {t("caregivers-notes-your-notes-on-caregiver")}{" "}
            <span className="capitalize">
              {caregiverProfile.nickName
                ? caregiverProfile.nickName
                : caregiverProfile.firstName}
            </span>
          </Typography>
          <Button
            onClick={addNote}
            component="div"
            variant="outlined"
            id="addnotebtn"
            className={clsx(btnClasses.actionbtn, classes.addbtn)}
          >
            {t("caregivers-notes-add-button-text")}
          </Button>
        </Box>
        {notes.length > 0 ? (
          <>{getDisplayNotes()}</>
        ) : (
          <Box className={classes.nonotes}>
            <img
              alt={t("caregivers-notes-no-notes-image")}
              src="./images/notes/icons-96-px-sticky-note.png"
              srcset="./images/notes/icons-96-px-sticky-note@2x.png 2x,
     ./images/notes/icons-96-px-sticky-note@3x.png 3x"
              className={classes.nonotesimage}
            />
            <Typography
              className={classes.jotthatdown}
              variant="h6"
              id="jotthatdown"
              component="div"
            >
              {t("caregivers-notes-jot-that-down")}
            </Typography>

            <Typography
              className={classes.nonotesmessage}
              variant="h6"
              id="nonotesmessage"
              component={Box}
            >
              {t("caregivers-notes-no-notes-line1")}
              <br />
              {t("caregivers-notes-no-notes-line2")}
              <br />
              {t("caregivers-notes-no-notes-line3")}
            </Typography>
            <Button
              onClick={addNote}
              component="div"
              variant="filled"
              className={clsx(filledBtnClasses.actionbtn, classes.addanote)}
            >
              {t("caregivers-notes-add-a-note")}
            </Button>
          </Box>
        )}
      </Box>
    </>
  )
}

export default CaregiversNotes
