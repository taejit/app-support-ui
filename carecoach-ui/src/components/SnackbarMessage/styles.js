import { makeStyles } from "@material-ui/core"

export default makeStyles((theme) => ({
  root: {
    "&>div": {
      borderRadius: "0",
      backgroundColor: theme.palette.darkBlue,
    },
    "& .MuiSnackbarContent-root": {
      padding: "0",
    },
    "& .MuiSnackbarContent-message": {
      padding: "13px 10px",
    },
  },
  strip: {
    height: "44px",
    color: theme.palette.white,
    borderRadius: "4px",
    boxShadow: "0 6px 12px -6px rgba(24, 24, 43, 0.21)",
    fontSize: "12px",
    fontWeight: "500",
    padding: "7px 16px 7px 16px",
  },
  verticalstrip: {
    width: "4px",
    padding: "2px",
    height: "46px",
    borderTopLeftRadius: "2px",
    borderBottomLeftRadius: "2px",
  },
  info: {
    backgroundColor: theme.palette.brightBlue,
  },
  error: {
    backgroundColor: theme.palette.darkError,
  },
}))
