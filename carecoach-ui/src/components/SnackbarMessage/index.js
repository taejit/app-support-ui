import React from "react"
import PropTypes from "prop-types"
import clsx from "clsx"
import Box from "@material-ui/core/Box"
import Snackbar from "@material-ui/core/Snackbar"
import SnackbarContent from "@material-ui/core/SnackbarContent"
import useStyles from "./styles"

export default function SnackbarMessage({
  open,
  message,
  onClose,
  children,
  messageState,
}) {
  const [openSnackbar, setOpenSnackbar] = React.useState(open)
  const onCloseSnackbar = () => {
    setOpenSnackbar(false)
    if (onClose) {
      onClose()
    }
  }

  const classes = useStyles()
  const vertical = "bottom"
  const horizontal = "center"
  return (
    <>
      <Snackbar
        autoHideDuration={5000}
        className={classes.root}
        anchorOrigin={{ vertical, horizontal }}
        open={openSnackbar}
        onClose={onCloseSnackbar}
        key={vertical + horizontal}
      >
        <>
          <Box
            component="span"
            className={clsx(classes.verticalstrip, classes[messageState])}
          />
          <SnackbarContent
            message={message}
            action={children}
            className={classes.snackbarcontent}
          />
        </>
      </Snackbar>
    </>
  )
}

SnackbarMessage.defaultProps = {
  open: false,
  message: null,
  onClose: () => {},
  children: null,
}
SnackbarMessage.propTypes = {
  open: PropTypes.bool,
  message: PropTypes.string,
  onClose: PropTypes.func,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
}
