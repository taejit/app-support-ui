import React from "react"
import renderer from "react-test-renderer"
import SnackbarMessage from "."

describe("Snackbar Message test", () => {
  it("Snackbar component render", () => {
    jest.useFakeTimers()
    const tree = renderer.create(<SnackbarMessage />).toJSON()
    jest.runAllTimers()
    expect(tree).toMatchSnapshot()
  })
})
