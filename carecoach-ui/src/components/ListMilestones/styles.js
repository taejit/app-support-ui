import { makeStyles } from "@material-ui/core"

export default makeStyles((theme) => ({
  duedate: {
    width: "86px",
    height: "21px",
    fontSize: "14px",
    fontWeight: "500",
    lineHeight: "1.5",
    color: theme.palette.darkBlue,
  },
  undobtn: {
    cursor: "pointer",
    fontSize: "12px",
    fontWeight: "600",
    lineHeight: "1.5",
    padding: "14px",
    width: "68px",
    borderTopRightRadius: "4px",
    borderBottomRightRadius: "4px",
    color: theme.palette.white,
    backgroundColor: theme.palette.brightBlue,
  },
}))
