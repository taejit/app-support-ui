import React from "react"
import renderer from "react-test-renderer"
import ListMilestone from "."
import CarePlanTemplateContext from "../../Contexts/CarePlanTemplates"
import { render, waitFor, fireEvent, screen } from "@testing-library/react"

const templates = [
  {
    name: "Milestone 1: Do this",
    rationale: "This is rationale",
    description: "This is description",
    tasks: [],
    isSelected: false,
    id: "milestone-1",
  },
  {
    name: "Milestone 1: Do this",
    rationale: "This is rationale",
    description: "This is description",
    tasks: [],
    isSelected: true,
    id: "milestone-2",
  },
]

describe("ListMilestone component", () => {
  it("Should not display anything when list is undefined", () => {
    const tree = renderer
      .create(
        <CarePlanTemplateContext.Provider value={{ undefined }}>
          <ListMilestone />
        </CarePlanTemplateContext.Provider>
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })

  it("No milestone display when list empty", () => {
    const templates = []
    const tree = renderer
      .create(
        <CarePlanTemplateContext.Provider value={{ templates }}>
          <ListMilestone />
        </CarePlanTemplateContext.Provider>
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })

  it("Display milestones when present", () => {
    const tree = renderer
      .create(
        <CarePlanTemplateContext.Provider value={{ templates }}>
          <ListMilestone />
        </CarePlanTemplateContext.Provider>
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })

  it("Click on milestone to edit", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(
        <CarePlanTemplateContext.Provider
          value={{
            templates,
            setTemplates: jest.fn(),
            setAddNewMilestone: jest.fn(),
          }}
        >
          <ListMilestone />
        </CarePlanTemplateContext.Provider>
      )
    })
    const { container } = wrapper
    const milestone = container.querySelector("#milestone-2")
    await waitFor(() => {
      fireEvent.click(milestone)
    })
    expect(screen.getByText(/^edit-milestone-name$/)).toBeDefined()
  })

  it("Click on milestone to edit", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(
        <CarePlanTemplateContext.Provider
          value={{
            templates,
            setTemplates: jest.fn(),
            setAddNewMilestone: jest.fn(),
          }}
        >
          <ListMilestone />
        </CarePlanTemplateContext.Provider>
      )
    })
    const { container } = wrapper
    const milestone = container.querySelector("#milestone-2")
    await waitFor(() => {
      fireEvent.click(milestone)
    })
    const name = screen.getByLabelText(/^edit-milestone-name$/)
    await waitFor(() => {
      fireEvent.change(name, {
        target: {
          value: "new name",
          id: "milestonename",
        },
      })
      fireEvent.blur(name, {
        target: {
          value: "new name",
          id: "milestonename",
        },
      })
    })
    const milestoneSave = screen.getByRole("button", {
      name: /^general-form-save-changes-text$/,
    })
    await waitFor(() => {
      fireEvent.click(milestoneSave)
    })
    expect(milestone).toBeDefined()
  })

  it("Click on milestone to remove", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(
        <CarePlanTemplateContext.Provider
          value={{
            templates,
            setTemplates: jest.fn(),
            setAddNewMilestone: jest.fn(),
          }}
        >
          <ListMilestone />
        </CarePlanTemplateContext.Provider>
      )
    })
    const { container } = wrapper
    const milestone = container.querySelector("#milestone-2")
    await waitFor(() => {
      fireEvent.click(milestone)
    })
    const milestoneRemove = screen.getByRole("button", {
      name: /^general-form-remove-text$/,
    })
    await waitFor(() => {
      fireEvent.click(milestoneRemove)
    })
    expect(milestone).toBeDefined()
  })
})
