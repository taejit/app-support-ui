import React, { useContext, useState, useEffect } from "react"
import { useTranslation } from "react-i18next"
import MilestoneCard from "../MilestoneCard"
import AddEditMilestone from "../AddEditMilestone"
import carePlanTemplates from "../../Contexts/CarePlanTemplates"
import SnackbarMessage from "../SnackbarMessage"
import { getMilestoneDueDate } from "../../utils/dateformatting"
import { Typography } from "@material-ui/core"
import useStyles from "./styles"
import getMilestonesInOrder from "../../utils/getMilestonesInOrder"
import ErrorPopup from "components/ErrorPopup"

const ListMilestones = ({ restrictOnclick = false }) => {
  const { t } = useTranslation()
  const classes = useStyles()
  const [editMilestoneData, setEditMilestoneData] = useState(null)
  const [milestones, setMilestones] = useState([])
  const [snackbarMessage, setSnackbarMessage] = useState({
    open: false,
    message: "",
    onClose: () =>
      setSnackbarMessage({
        ...snackbarMessage,
        open: false,
        children: null,
      }),
  })
  const {
    templates,
    setTemplates,
    addNewMilestone,
    setAddNewMilestone,
  } = useContext(carePlanTemplates)
  const [confirmDelete, setConfirmDelete] = useState(false)

  const editMilestone = (props) => {
    setEditMilestoneData(props)
  }

  const onEditClose = () => {
    setEditMilestoneData(null)
    setAddNewMilestone(false)
  }

  const onRemove = (id) => {
    setConfirmDelete(true)
  }

  useEffect(() => {
    const selectedTemplates = (templates || []).filter(
      (template) => template.isSelected
    )
    setMilestones(getMilestonesInOrder(selectedTemplates))

    if (addNewMilestone) {
      editMilestone({
        name: null,
        description: null,
        rationale: null,
        duedate: null,
        tasks: [],
        isSelected: true,
      })
    }
  }, [templates, addNewMilestone])

  const onEditSave = (milestoneObj) => {
    if (!milestoneObj.id) {
      milestoneObj.id = `new-${new Date().toLocaleString()}`
      milestoneObj.isSelected = true
      templates.push(milestoneObj)
      setAddNewMilestone(false)
      setTemplates([...templates])
      return
    }
    templates.forEach((milestone) => {
      if (milestone.id === milestoneObj.id) {
        milestone.name = milestoneObj.name
        milestone.description = milestoneObj.description
        milestone.rationale = milestoneObj.rationale
        milestone.duedate = milestoneObj.duedate || null
        milestone.tasks = milestoneObj.tasks
      }
    })
    setTemplates([...templates])
    // show confirmation message of milestone getting updated
    setTimeout(() => {
      setSnackbarMessage({
        ...snackbarMessage,
        open: true,
        messageState: "info",
        message: `"${milestoneObj.name}" ${t("general-form-updated")}.`,
      })
    })
  }

  const onEditTaskSave = (taskObj) => {
    if (taskObj.isCompleted === false) {
      // show confirmation message of task getting added
      setSnackbarMessage({
        ...snackbarMessage,
        open: true,
        messageState: "info",
        message: `"${taskObj.name}" ${t("caregivers-add-note-added-success")}.`,
      })
    } else {
      // show confirmation message of task getting updated
      setSnackbarMessage({
        ...snackbarMessage,
        open: true,
        messageState: "info",
        message: `"${taskObj.name}" ${t(
          "caregivers-add-note-updated-success"
        )}.`,
      })
    }
  }

  const onEditTaskClose = () => {
    setEditMilestoneData(null)
    setAddNewMilestone(false)
  }

  const onRemoveConfirm = async (action) => {
    if (action == "no") {
      setConfirmDelete(false)
    } else {
      let id = editMilestoneData?.id
      let milestoneName = editMilestoneData?.name
      setConfirmDelete(false)
      setEditMilestoneData(null)
      templates.forEach((template) => {
        if (template.id === id) {
          template.isSelected = false
        }
      })
      setTemplates([...templates])
      setSnackbarMessage({
        ...snackbarMessage,
        open: true,
        messageState: "info",
        message: `"${milestoneName}" removed.`,
      })
    }
  }
  const onClose = () => {
    setConfirmDelete(false)
  }

  const ShowErrorPopup = (prop) => {
    return (
      <ErrorPopup
        open={confirmDelete}
        title={
          `${t("general-form-remove-text")} "` + editMilestoneData.name + `?"`
        }
        description_1={t("confirm-remove-description")}
        handleActions={onRemoveConfirm}
        standalone={true}
        direction={"Right"}
        showBackArrow={true}
        onClose={onClose}
        hideCrossClose={true}
        leftButtonText={t("nevermind-button-text-error-drawer")}
        rightButtonText={t("general-form-remove-text")}
      />
    )
  }
  return (
    <>
      {snackbarMessage.open && <SnackbarMessage {...snackbarMessage} />}
      {editMilestoneData && (
        <AddEditMilestone
          {...editMilestoneData}
          open={!!editMilestoneData}
          onClose={onEditClose}
          onSave={onEditSave}
          onRemove={onRemove}
          onEditTaskSave={onEditTaskSave}
          onEditTaskClose={onEditTaskClose}
        />
      )}
      {confirmDelete && ShowErrorPopup()}
      {milestones.map((milestone) => {
        const rationale = milestone.rationale
        const description = milestone.description
        const name = milestone.name
        return (
          <MilestoneCard
            key={milestone.id}
            {...{
              id: milestone.id,
              name,
              rationale,
              description,
              duedate: milestone.duedate,
              duedatestring: (
                <Typography className={classes.duedate}>
                  {getMilestoneDueDate(milestone.duedate)}
                </Typography>
              ),
              tasks: milestone.tasks,
              numberOfTask: milestone.tasks?.length || 0,
              onClick: restrictOnclick ? null : editMilestone,
            }}
          />
        )
      })}
    </>
  )
}

export default ListMilestones
