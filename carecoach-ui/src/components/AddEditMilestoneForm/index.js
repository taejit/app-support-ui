import React, { useState, useEffect } from "react"
import PropTypes from "prop-types"
import { useTranslation } from "react-i18next"
import isEqual from "lodash/isEqual"
import { useFormik } from "formik"
import * as Yup from "yup"
import {
  TextField,
  Grid,
  Typography,
  Collapse,
  IconButton,
} from "@material-ui/core"
import DateFnsUtils from "@date-io/date-fns"
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers"
import { ReactComponent as CrossMark } from "../../Assets/cross.svg"
import MilestoneTasksDisplay from "../MilestoneTasksDisplay"
import AddOptions from "../AddOptions"
import AddEditTask from "../AddEditTask"

import {
  DateFormat,
  ValidMilestoneAddEditSteps as ValidSteps,
} from "../../utils/constant"
import { ReactComponent as CarePlanIcon } from "../../Assets/care-plan.svg"
import { ReactComponent as CalendarIcon } from "../../Assets/calendar.svg"
import FormStyles from "../CommonStyles/FormikFormStyles"
import DrawerIconStyle from "../CommonStyles/DrawerIcon"
import DrawerTitle from "../CommonStyles/DrawerTitle"
import FormFooter from "../FormikFormFooter"
import useStyles from "./styles"
import ErrorPopup from "../ErrorPopup"
import findIndex from "lodash/findIndex"
import SnackbarComponent from "../SnackbarMessage"

const AddEditMilestoneForm = (props) => {
  const [snackbarMessage, setSnackbarMessage] = useState({
    open: false,
    message: "",
    onClose: () => {
      setSnackbarMessage({
        ...snackbarMessage,
        open: false,
        children: null,
      })
    },
  })
  const { t } = useTranslation()
  const iconStyle = DrawerIconStyle()
  const drawerTitle = DrawerTitle()
  const formStyles = FormStyles()
  const [hasChanged, setHasChanged] = useState(false)
  const [collapseOpen, setCollapseOpen] = useState(true)
  const [calendarOpen, setCalendarOpen] = useState(false)
  const currentStep = props.currentStep
  const classes = useStyles()
  const [editTaskData, setEditTaskData] = useState(null)
  const [enableNoTaskError, setEnableNoTaskError] = useState(false)
  const [confirmDelete, setConfirmDelete] = useState(false)
  const [errorPopUpData, setErrorPopupData] = useState({})

  const { rationale, description, name, duedate, tasks } = props
  const editMode = !!name
  const formik = useFormik({
    initialValues: {
      name: name,
      description: description,
      rationale: rationale,
      duedate: duedate,
      tasks: tasks,
    },
    validateOnChange: true,
    validationSchema: Yup.object({
      name: Yup.string()
        .nullable()
        .min(1)
        .max(60)
        .required(
          `${t("edit-milestone-name")} ${t(
            "edit-milestone-validation-message"
          )}`
        ),
      rationale: Yup.string()
        .nullable()
        .min(1)
        .max(90)
        .required(
          `${t("edit-milestone-rationale")} ${t(
            "edit-milestone-validation-message"
          )}`
        ),
      description: Yup.string()
        .nullable()
        .min(1)
        .max(120)
        .required(
          `${t("edit-milestone-description")} ${t(
            "edit-milestone-validation-message"
          )}`
        ),
      duedate: Yup.date().nullable(),
    }),
    onSubmit: async (values) => {
      if (formik.values.tasks.length) {
        props.onSave({ ...values, id: props.id })
        props.onClose()
        setEnableNoTaskError(false)
      } else {
        setEnableNoTaskError(true)
      }
    },
  })

  const removeMilestone = () => {
    props.onRemove(props, "milestone")
  }

  useEffect(() => {
    setHasChanged(!isEqual(formik.values, formik.initialValues))
  }, [formik.values, formik.initialValues])

  const taskAction = (taskdetails) => {
    const copyTasks = formik.values.tasks.map((item) => ({ ...item }))
    for (const task of copyTasks) {
      if (task.id === taskdetails.id) {
        task.isCompleted = !task.isCompleted
        break
      }
    }
    formik.setFieldValue("tasks", [...copyTasks])
  }

  const deleteTask = (task) => {
    setErrorPopupData(task)
    setConfirmDelete(true)
  }

  const onRemoveConfirm = async (action) => {
    if (action === "no") {
      setConfirmDelete(false)
    } else {
      const taskCopy = [...formik.values.tasks]
      const indexTask = findIndex(taskCopy, {
        id: errorPopUpData.id,
      })
      if (indexTask !== -1) {
        taskCopy.splice(indexTask, 1)
      }
      // the below tasks update is just too much time to refect on formik
      // which caused the issue of duplicate undo of deleted task
      // tried multiple setTimeout but nothing works untill time is too high
      // so to navigate the issue passing the updated taskCopy as param to methods to search and undo delete from
      await formik.setFieldValue("tasks", [...taskCopy])
      setConfirmDelete(false)
      goToStep(ValidSteps.milestone, true, true, false)
      setSnackbarMessage({
        ...snackbarMessage,
        open: true,
        messageState: "info",
        message: `"${errorPopUpData.name}" removed.`,
        ...{ children: undoBtn(taskCopy) },
      })
    }
  }

  const undoDeleteTask = (remainingtask) => {
    const task = errorPopUpData
    const tasksList = remainingtask
    const index = findIndex(tasksList, {
      id: task.id,
    })
    if (index === -1) {
      tasksList.push(task)
      formik.setFieldValue("tasks", [...tasksList])
    }
  }

  const undoBtn = (remainingtask) => (
    <Typography
      component="div"
      data-testid="undoBtn"
      onClick={() => undoDeleteTask(remainingtask)}
      className={classes.undobtn}
      id="undoBtn"
    >
      {t("caregivers-delete-undo")}
    </Typography>
  )

  const editTask = (task) => {
    setEditTaskData(task)
    goToStep(ValidSteps.task, true, true, false)
  }

  const goToStep = (step, hideCross, showArrow, isNewTask) => {
    if (isNewTask) {
      setEditTaskData({
        name: "",
        link: "",
      })
    }
    props.goToStep(step, hideCross, showArrow)
  }

  const onEditTaskSave = (taskObj) => {
    let copyTasks = formik.values.tasks.map((item, key) => ({ ...item }))

    if (!taskObj.id) {
      taskObj.id = `new-${new Date().toLocaleString()}`
      taskObj.isCompleted = false
      copyTasks.push(taskObj)
    } else {
      copyTasks.map((data, index) => {
        if (data.id === taskObj.id) {
          copyTasks[index] = taskObj
        }
      })
    }
    formik.setFieldValue("tasks", [...copyTasks])
    props.onEditTaskSave(taskObj, copyTasks)
    setEditTaskData(null)
    goToStep(ValidSteps.milestone, false, false)
  }

  const clearDueDate = () => {
    formik.setFieldValue("duedate", null)
  }

  const onClose = () => {
    setConfirmDelete(false)
  }

  const ShowErorPopup = (prop) => {
    return (
      <ErrorPopup
        open={confirmDelete}
        title={
          `${t("general-form-remove-text")} "` + errorPopUpData.name + `?"`
        }
        description_1={t("confirm-remove-description")}
        handleActions={(param) => onRemoveConfirm(param)}
        standalone={true}
        direction={"right"}
        showBackArrow={true}
        onClose={onClose}
        hideCrossClose={true}
        leftButtonText={t("nevermind-button-text-error-drawer")}
        rightButtonText={t("general-form-remove-text")}
      />
    )
  }

  return (
    <>
      {snackbarMessage.open ? <SnackbarComponent {...snackbarMessage} /> : null}
      {currentStep === ValidSteps.milestone && (
        <form onSubmit={formik.handleSubmit} className={formStyles.form}>
          <Grid container className={classes.container}>
            <Grid item xs={12}>
              {!props.hideTitleIcon && (
                <CarePlanIcon className={iconStyle.icon} />
              )}
              <Typography
                id="editmilestonetitle"
                component="div"
                className={drawerTitle.title}
              >
                {editMode
                  ? t("edit-milestone-drawer-title-edit-mode")
                  : props.addModeTitle ||
                    t("edit-milestone-drawer-title-add-mode")}
              </Typography>
            </Grid>
            <Grid
              item
              xs={12}
              onClick={() => setCollapseOpen(!collapseOpen)}
              className={formStyles.grid}
            >
              <Typography
                id="overviewsection"
                component="div"
                className={classes.overviewtitle}
              >
                {t("edit-milestone-overview")}
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Collapse in={collapseOpen} timeout="auto">
                <Grid item xs={12} className={formStyles.grid}>
                  <TextField
                    fullWidth
                    multiline
                    rowsMax={2}
                    className={formStyles.text}
                    inputProps={{ maxLength: 60 }}
                    id="milestonename"
                    label={t("edit-milestone-name")}
                    type="text"
                    {...formik.getFieldProps("name")}
                  />
                  {formik.touched.name && formik.errors.name && (
                    <div className={formStyles.error}>{formik.errors.name}</div>
                  )}
                </Grid>
                <Grid item xs={12} className={formStyles.grid}>
                  <TextField
                    fullWidth
                    multiline
                    rowsMax={3}
                    className={formStyles.text}
                    inputProps={{ maxLength: 90 }}
                    id="milestonerationale"
                    label={t("edit-milestone-rationale")}
                    type="text"
                    {...formik.getFieldProps("rationale")}
                  />
                  {formik.touched.rationale && formik.errors.rationale && (
                    <div className={formStyles.error}>
                      {formik.errors.rationale}
                    </div>
                  )}
                </Grid>
                <Grid item xs={12} className={formStyles.grid}>
                  <TextField
                    fullWidth
                    multiline
                    rowsMax={10}
                    className={formStyles.text}
                    inputProps={{ maxLength: 120 }}
                    id="milestonedescription"
                    label={t("edit-milestone-description")}
                    type="text"
                    {...formik.getFieldProps("description")}
                  />
                  {formik.touched.description && formik.errors.description && (
                    <div className={formStyles.error}>
                      {formik.errors.description}
                    </div>
                  )}
                </Grid>
                <Grid item xs={12} className={formStyles.grid}>
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <>
                      <KeyboardDatePicker
                        className={formStyles.datepicker}
                        disableToolbar
                        autoOk
                        disablePast
                        variant="inline"
                        format={DateFormat}
                        minDateMessage={null}
                        invalidDateMessage={null}
                        margin="normal"
                        id="milestoneduedate"
                        InputProps={{
                          readOnly: true,
                          onFocus: () => {
                            setCalendarOpen(true)
                          },
                        }}
                        KeyboardButtonProps={{
                          onFocus: () => {
                            setCalendarOpen(true)
                          },
                        }}
                        PopoverProps={{
                          disableRestoreFocus: true,
                          onClose: () => {
                            setCalendarOpen(false)
                          },
                        }}
                        open={calendarOpen}
                        label={t("edit-milestone-duedate")}
                        value={
                          formik.values.duedate ? formik.values.duedate : null
                        }
                        onChange={(value) => {
                          formik.setFieldValue("duedate", value)
                          setCalendarOpen(false)
                        }}
                        keyboardIcon={
                          <CalendarIcon onClick={() => setCalendarOpen(true)} />
                        }
                      />
                      {formik.values.duedate && (
                        <IconButton
                          data-testid="clearduedate"
                          id="clearduedate"
                          className={classes.clearduedate}
                          onClick={() => clearDueDate()}
                        >
                          <CrossMark className={classes.clearduedateicon} />
                        </IconButton>
                      )}
                      {formik.touched.duedate && formik.errors.duedate && (
                        <div className={classes.error}>
                          {formik.errors.duedate}
                        </div>
                      )}
                    </>
                  </MuiPickersUtilsProvider>
                </Grid>
              </Collapse>
            </Grid>
          </Grid>

          <MilestoneTasksDisplay
            tasks={formik.values.tasks}
            taskAction={taskAction}
            editTask={editTask}
            addNewTaskOptions={() =>
              goToStep(ValidSteps.task, true, true, true)
            }
            enableNoTaskError={enableNoTaskError}
          />

          <FormFooter
            {...{
              editMode,
              hasChanged,
              backArrowClick: props.backArrowClick,
              showGoBack: props.showGoBack,
              onClose: props.onClose,
              isFormValid: formik.isValid,
              onClickRemove: removeMilestone,
              onClickSuccess: () => formik.submitForm(),
            }}
          />
        </form>
      )}
      {currentStep === ValidSteps.addtaskoptions && (
        <AddOptions
          title={t("add-new-task-title")}
          subscript={t("add-new-task-subscript")}
          description={t("add-new-task-description")}
          searchOption1Text={t("search-lantern-resources")}
          searchOption2Text={t("add-from-scratch")}
          goBackText={t("general-form-go-back")}
          backArrowClick={() => goToStep(ValidSteps.milestone, false, false)}
          addNewTaskFromScratch={() =>
            goToStep(ValidSteps.task, true, true, true)
          }
        />
      )}
      {confirmDelete && ShowErorPopup()}
      {currentStep === ValidSteps.task && (
        <AddEditTask
          {...editTaskData}
          open={!!editTaskData}
          onSave={onEditTaskSave}
          onDelete={deleteTask}
          backArrowClick={() => goToStep(ValidSteps.milestone, false, false)}
        />
      )}
    </>
  )
}

AddEditMilestoneForm.defaultProps = {
  showGoBack: false,
  rationale: "",
  description: "",
  name: "",
  duedate: null,
  tasks: [],
  addModeTitle: null,
  currentStep: null,
  hideTitleIcon: false,
  onSave: () => {
    //intentional
  },
  onRemove: () => {
    //intentional
  },
  goToStep: () => {
    //intentional
  },
  onEditTaskSave: () => {
    //intentional
  },
  onClose: () => {
    //intentional
  },
  backArrowClick: () => {
    //intentional
  },
}

AddEditMilestoneForm.propTypes = {
  showGoBack: PropTypes.bool,
  rationale: PropTypes.string,
  description: PropTypes.string,
  name: PropTypes.string,
  duedate: PropTypes.string,
  tasks: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      link: PropTypes.string,
    })
  ),
  addModeTitle: PropTypes.string,
  currentStep: PropTypes.string,
  onSave: PropTypes.func,
  onRemove: PropTypes.func,
  goToStep: PropTypes.func,
  onEditTaskSave: PropTypes.func,
  hideTitleIcon: PropTypes.bool,
  onClose: PropTypes.func,
  backArrowClick: PropTypes.func,
}

export default AddEditMilestoneForm
