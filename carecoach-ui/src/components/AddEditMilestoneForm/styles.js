import { makeStyles } from "@material-ui/core"

export default makeStyles((theme) => ({
  container: {
    flexShrink: 0,
  },
  overviewtitle: {
    fontSize: `18px`,
    fontWeight: `500`,
    lineHeight: `1.33`,
    color: theme.palette.darkBlue,
  },
  clearduedate: {
    float: `right`,
    top: `-46px`,
    left: `-35px`,
  },
  clearduedateicon: {
    width: "24px",
    height: "24px",
  },
  undobtn: {
    cursor: "pointer",
    fontSize: "12px",
    fontWeight: "600",
    lineHeight: "1.5",
    padding: "14px",
    width: "68px",
    borderTopRightRadius: "4px",
    borderBottomRightRadius: "4px",
    color: theme.palette.white,
    backgroundColor: theme.palette.brightBlue,
  },
}))
