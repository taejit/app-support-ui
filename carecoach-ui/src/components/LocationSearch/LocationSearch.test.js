import React from "react"
import { render, fireEvent, waitFor } from "@testing-library/react"
import LocationSearch from "."

describe("component is loading or not", () => {
  it("mounts without crashing", () => {
    render(<LocationSearch />)
  })

  it("should call auto complete", async () => {
    const { getByRole } = render(<LocationSearch />)
    const autocomplete = getByRole("textbox")

    // click into the component
    autocomplete.focus()

    await waitFor(() => {
      // type "a"
      fireEvent.change(document.activeElement, {
        target: { value: "Arkansas" },
      })

      // arrow down to first option
      fireEvent.keyDown(document.activeElement, { key: "ArrowDown" })

      // select element
      fireEvent.keyDown(document.activeElement, { key: "Enter" })
    })

    expect(autocomplete.value).toEqual("Arkansas")
    // expect(someChangeHandler).toHaveBeenCalledTimes(1)
  })

  it("Should call fetch api in init", () => {
    window.google = {
      maps: {
        places: {
          AutocompleteService: jest.fn().mockReturnThis({
            getPlacePredictions: jest.fn(),
          }),
        },
      },
    }
    const { getByRole } = render(<LocationSearch />)
    const autocomplete = getByRole("textbox")
    expect(autocomplete).toBeDefined()
  })
})
