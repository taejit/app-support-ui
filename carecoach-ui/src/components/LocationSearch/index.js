import React, { useEffect } from "react"
import TextField from "@material-ui/core/TextField"
import Autocomplete from "@material-ui/lab/Autocomplete"
// import Typography from "@material-ui/core/Typography"
import parse from "autosuggest-highlight/parse"
import throttle from "lodash/throttle"
import { Box, Divider, List, ListItem } from "@material-ui/core"
import "./LocationSearch.scss"
import { useTranslation } from "react-i18next"
import getLocationDisplayText from "../../utils/getLocationDisplayText"
import { ReactComponent as ErrorIcon } from "../../Assets/CarePlans/error-triangle.svg"

function loadScript(src, position, id) {
  if (!position) {
    return
  }

  const script = document.createElement("script")
  script.setAttribute("async", "")
  script.setAttribute("id", id)
  script.src = src
  position.appendChild(script)
}

const autocompleteService = { current: null }

export default function GoogleMaps(props) {
  const { t } = useTranslation()
  // eslint-disable-next-line react/prop-types
  const [value, setValue] = React.useState(null)
  const [inputValue, setInputValue] = React.useState("")
  const [options, setOptions] = React.useState([])
  const loaded = React.useRef(false)
  const { selectedLocation, updateLocation, error } = props

  if (typeof window !== "undefined" && !loaded.current) {
    if (!document.querySelector("#google-maps")) {
      loadScript(
        `https://maps.googleapis.com/maps/api/js?key=${window._env_.Google_Places_Api_Key}&libraries=places`,
        document.querySelector("head"),
        "google-maps"
      )
    }

    loaded.current = true
  }

  const fetch = React.useMemo(
    () =>
      throttle((request, callback) => {
        request.types = ["(cities)"]
        autocompleteService.current.getPlacePredictions(request, callback)
      }, 200),
    []
  )

  const noOptions = (
    <div>
      <Box
        display="flex"
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
      >
        <Box>
          <img
            alt={t("locationPopupImageAlt")}
            src="./images/locationsearch/image-banners-default-small.png"
            srcset="./images/locationsearch/image-banners-default-small@2x.png 2x,
     ./images/locationsearch/image-banners-default-small@3x.png 3x"
            className="nooptions-img"
          />
        </Box>
        <Box pt="5%">
          <div className="nooptions-text" align="center">
            <div>{t("locationPopupTextOne")}</div>
            {t("locationPopupTextTwo")}
          </div>
        </Box>
      </Box>
    </div>
  )
  React.useEffect(() => {
    let active = true

    if (!autocompleteService.current && window.google) {
      autocompleteService.current = new window.google.maps.places.AutocompleteService()
    }
    if (!autocompleteService.current) {
      return undefined
    }
    if (inputValue === "") {
      setOptions(value ? [value] : [])
      return undefined
    }
    fetch({ input: inputValue }, (results) => {
      if (active) {
        let newOptions = []

        if (value) {
          newOptions = [value]
        }

        if (results) {
          newOptions = [...newOptions, ...results]
        }

        setOptions(newOptions)
      }
    })

    return () => {
      active = false
    }
  }, [value, inputValue, fetch])

  useEffect(() => {
    setValue(selectedLocation)
  }, [selectedLocation])

  return (
    <Autocomplete
      id="google-map-demo"
      data-testid="ccLocation"
      getOptionLabel={(option) =>
        typeof option === "string" ? option : option.description
      }
      classes={{ inputRoot: error ? "removeUnncessary" : "" }}
      filterOptions={(x) => x}
      options={options}
      autoComplete
      includeInputInList
      filterSelectedOptions
      value={value}
      noOptionsText={noOptions}
      onChange={(event, newValue) => {
        setOptions(newValue ? [newValue, ...options] : options)
        if (newValue && newValue.description) {
          setValue(getLocationDisplayText(newValue.description))
        } else {
          setValue(newValue)
        }
        updateLocation(newValue)
      }}
      onInputChange={(event, newInputValue) => {
        setInputValue(newInputValue)
      }}
      renderInput={(params) => {
        return (
          <TextField
            {...params}
            id="ccLocation"
            label={t("location")}
            margin="dense"
            error={error ? error : false}
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <React.Fragment>
                  {error ? (
                    <div className={"errorIcon"}>
                      <ErrorIcon width="24" height="24" />
                    </div>
                  ) : null}
                  {params.InputProps.endAdornment}
                </React.Fragment>
              ),
            }}
            fullWidth
          />
        )
      }}
      renderOption={(option) => {
        const matches =
          option.structured_formatting &&
          option.structured_formatting.main_text_matched_substrings
        const parts =
          option.description &&
          parse(
            getLocationDisplayText(option.description),
            matches.map((match) => [match.offset, match.offset + match.length])
          )
        return (
          <>
            {parts && (
              <List
                component="nav"
                aria-label="mailbox folders"
                style={{ width: "100%", maxWidth: 360, height: "50%" }}
              >
                <ListItem button>
                  {parts.map((part, index) => (
                    <span
                      // eslint-disable-next-line react/no-array-index-key
                      key={index}
                      style={{ fontWeight: part.highlight ? 700 : 400 }}
                    >
                      {part.text}
                    </span>
                  ))}
                </ListItem>
                <Divider />
              </List>
            )}
          </>
        )
      }}
    />
  )
}
