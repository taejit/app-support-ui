import { makeStyles } from "@material-ui/core"

export default makeStyles((theme) => ({
  root: {
    width: `100%`,
    marginTop: `12px`,
    marginBottom: `12px`,
  },
  addnewtask: {
    display: `flex`,
    alignItems: `center`,
    padding: `9px`,
    paddingLeft: `5px`,
  },
  addnewtaskbtn: {
    fontSize: `12px`,
    fontWeight: `600`,
    lineHeight: `1.5`,
    color: theme.palette.morningGlory,
  },
}))
