import React from "react"
import renderer from "react-test-renderer"
import { render, waitFor, fireEvent } from "@testing-library/react"
import MilestoneTasksDisplay from "."
import CarePlanTemplateContext from "../../Contexts/CarePlanTemplates"

const templates = [
  {
    id: "milestone-1",
    name: "testName",
    isCommon: true,
    tasks: [
      {
        id: "test id",
        name: "taskname",
        link: "no link",
        isCompleted: false,
      },
    ],
  },
  {
    id: "milestone-2",
    name: "testName2",
    isCommon: false,
    tasks: [
      {
        id: "test id",
        name: "taskname",
        link: "no link",
        isCompleted: false,
      },
    ],
  },
]

describe("EachTask component", () => {
  it("Component rendering, incomplete task", () => {
    const data = {
      tasks: [
        {
          name: "taskname",
          link: "no link",
          isCompleted: false,
        },
      ],
    }
    const tree = renderer.create(<MilestoneTasksDisplay {...data} />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it("Component rendering, check task", async () => {
    const data = {
      milestoneId: "milestone-1",
      tasks: [
        {
          id: "test id",
          name: "taskname",
          link: "no link",
          isCompleted: false,
        },
      ],
      taskAction: jest.fn(),
    }
    let wrapper
    await waitFor(() => {
      wrapper = render(
        <CarePlanTemplateContext.Provider
          value={{ templates, setTemplates: jest.fn() }}
        >
          <MilestoneTasksDisplay {...data} />
        </CarePlanTemplateContext.Provider>
      )
    })
    const { container } = wrapper
    const taskCheckBox = container.querySelector("#task-checkbox")
    await waitFor(() => {
      fireEvent.click(taskCheckBox)
    })
    expect(taskCheckBox).toBeDefined()
  })

  it("Component rendering, edit task", async () => {
    const data = {
      milestoneId: "milestone-1",
      tasks: [
        {
          id: "test id",
          name: "taskname",
          link: "no link",
          isCompleted: false,
        },
      ],
      taskAction: jest.fn(),
      editTask: jest.fn(),
    }
    let wrapper
    await waitFor(() => {
      wrapper = render(
        <CarePlanTemplateContext.Provider
          value={{ templates, setTemplates: jest.fn() }}
        >
          <MilestoneTasksDisplay {...data} />
        </CarePlanTemplateContext.Provider>
      )
    })
    const { container } = wrapper
    const editTaskBtn = container.querySelector("#edit-task")
    await waitFor(() => {
      fireEvent.click(editTaskBtn)
    })
    expect(editTaskBtn).toBeDefined()
  })
})
