import React from "react"
import { useTranslation } from "react-i18next"
import { Box, Button, Typography } from "@material-ui/core"
import EachTask from "../EachTask"
import CollapsibleDisplay from "../CollapsibleDisplay"
import { ReactComponent as PlusIcon } from "../../Assets/plus.svg"
import useStyles from "./styles"
import { ReactComponent as NoTaskCheckMark } from "../../Assets/CarePlans/tasks-circle-checkmark.svg"
import { ReactComponent as NoTaskErrorIcon } from "../../Assets/CarePlans/error-triangle.svg"
import DrawerTitle from "../CommonStyles/DrawerTitle"

const MilestoneTasksDisplay = (props) => {
  const classes = useStyles()
  const { t } = useTranslation()
  const drawerTitle = DrawerTitle()

  const data = {
    title: t("edit-milestone-tasks"),
    count: (props.tasks || []).length,
  }

  const getNoTasksTemplate = () => {
    if (!props.enableNoTaskError) {
      return (
        <Box
          className={drawerTitle.noOptionsDiv}
          onClick={() => props.addNewTaskOptions()}
        >
          <NoTaskCheckMark />
          <Typography className={drawerTitle.noOptionsText}>
            {t("no-tasks-list-text")}
          </Typography>
        </Box>
      )
    }
    return (
      <Box className={drawerTitle.noOptionsErrorDiv}>
        <NoTaskErrorIcon />
        <Typography className={drawerTitle.noOptionsErrorText}>
          {t("no-tasks-list-error-text")}
        </Typography>
      </Box>
    )
  }

  return (
    <Box className={classes.root}>
      <CollapsibleDisplay {...data}>
        {!props.tasks.length && getNoTasksTemplate()}
        {(props.tasks || []).map((task) => {
          return (
            <EachTask
              key={task.name}
              {...task}
              onCheckboxClick={(args) => props.taskAction(args)}
              onEdit={(args) => props.editTask(args)}
            />
          )
        })}
        <Box id="add-new-task" className={classes.addnewtask}>
          <PlusIcon
            className={classes.plusicon}
            onClick={() => props.addNewTaskOptions()}
          />
          <Button
            id="addnewtask"
            className={classes.addnewtaskbtn}
            onClick={() => props.addNewTaskOptions()}
          >
            {t("general-form-add-new")}
          </Button>
        </Box>
      </CollapsibleDisplay>
    </Box>
  )
}

export default MilestoneTasksDisplay
