import React from "react"

const Routes = [
  {
    path: "/dashboard/caregivers/:caregiversUuid/careplan",
    name: "Care Plan",
    component: React.lazy(() => import("../CaregiversCarePlan")),
  },
  {
    path: "/dashboard/caregivers/:caregiversUuid/notes",
    name: "Notes",
    component: React.lazy(() => import("../CaregiversNotes")),
  },
  {
    path: "/dashboard/caregivers/:caregiversUuid/profile",
    name: "Profile",
    component: React.lazy(() => import("../CaregiversProfile")),
  },
  {
    path: "/dashboard/caregivers/:caregiversUuid/progress",
    name: "Mini Surveys",
    component: React.lazy(() => import("../MiniSurveys")),
  },
]

export default Routes
