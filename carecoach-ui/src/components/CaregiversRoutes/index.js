import React, { Suspense } from "react"
import PropTypes from "prop-types"
import { Redirect, Route, Switch } from "react-router-dom"
import Loading from "../Loading"

// routes config
import routes from "./routes"

const CaregiversRoutes = ({ initialRedirect }) => {
  return (
    <Suspense fallback={<Loading />}>
      <Switch>
        {routes.map((route) => {
          return (
            route.component && (
              <Route
                key={route.name}
                path={route.path}
                exact={route.exact}
                name={route.name}
                render={() => <route.component />}
              />
            )
          )
        })}
        <Redirect to={initialRedirect} />
      </Switch>
    </Suspense>
  )
}

CaregiversRoutes.propTypes = {
  initialRedirect: PropTypes.string.isRequired,
}

export default React.memo(CaregiversRoutes)
