import React, { useEffect } from "react"
import { ChatContext, useChat } from "store/ChatContext"
import { Auth } from "aws-amplify"
import { publish } from "utils/PubSub"
import services from "services"

const MESSAGING_SUFFIX = "messaging"
const NOTIFICATIONS_SUFFIX = "notification"

const { initClientWithUserProvider, closeClientGracefully } = services.stream()
const { getMessagingToken } = services.api.users()

const initchannel = async (
  client,
  channelName,
  setNewMessages,
  setMessagesList,
  onNewMessages = () => {
    // this is intentional
  }
) => {
  if (!client) return Promise.resolve()

  return new Promise((resolve) => {
    const channel = client.channel("messaging", channelName)

    const updateHooks = () => {
      setNewMessages(channel?.state.unreadCount > 0)
      setMessagesList({
        loadTime: new Date().getMilliseconds(),
        messages: channel?.state.messages,
      })
    }
    channel?.state.clearMessages()
    channel?.watch().then(() => {
      updateHooks()

      channel?.on("message.new", (event) => {
        updateHooks()
        onNewMessages(event)
      })

      channel?.on("message.read", () => {
        setNewMessages(channel?.state.unreadCount > 0)
      })

      channel?.on("message.updated", () => {
        updateHooks()
      })

      resolve(channel)
    })
  })
}

export const ChatDataProvider = (props) => {
  const data = useChat()
  const {
    tryAgain,
    chatClient,
    messagingChannel,
    notificationChannel,
    setChatClient,
    setMessagingChannel,
    setNotificationChannel,
    setNewNotifications,
    setNewMessages,
    setNotificationMessages,
    setIsConnecting,
  } = data

  const closeClient = async () => {
    if (messagingChannel) {
      messagingChannel.stopWatching()
      messagingChannel.off()
    }

    if (notificationChannel) {
      notificationChannel.stopWatching()
      notificationChannel.off()
    }

    if (chatClient) {
      await chatClient.disconnectUser()
    }

    setChatClient(undefined)
    setMessagingChannel(undefined)
    setNotificationChannel(undefined)
    setIsConnecting(false)
    setNewNotifications(undefined)
    setNewMessages(undefined)
    setNotificationMessages(undefined)

    closeClientGracefully()
  }

  const connect = async (retryCount) => {
    const authInfo = await Auth.currentUserInfo()

    const { attributes: { "custom:uuid": caregiverUuid } = {} } = authInfo || {}

    if (caregiverUuid) {
      initClientWithUserProvider(caregiverUuid, getMessagingToken, retryCount)
        .then(async (client) => {
          if (client) {
            const messagingChannelName = `${caregiverUuid}-${MESSAGING_SUFFIX}`
            const notificationChannelName = `${caregiverUuid}-${NOTIFICATIONS_SUFFIX}`

            setChatClient(client)

            const mChannel = await initchannel(
              client,
              messagingChannelName,
              setNewMessages,
              () => {}
            )
            const nChannel = await initchannel(
              client,
              notificationChannelName,
              setNewNotifications,
              setNotificationMessages,
              (event) => {
                publish("notification.new", event.message)
              }
            )

            setMessagingChannel(mChannel)

            setNotificationChannel(nChannel)

            setIsConnecting(false)
          }
        })
        .catch((error) => {
          console.error("CHAT CLIENT NOT CONNECTED", error)
          setIsConnecting(false)
        })
    }
  }

  useEffect(() => {
    connect(tryAgain)
    return () => {
      closeClient()
    }
  }, [tryAgain])

  return (
    <ChatContext.Provider value={data}>{props.children}</ChatContext.Provider>
  )
}
