import React from "react"
import { PopupContext, usePopup } from "store/PopupContext"

export const PopupDataProvider = (props) => {
  const data = usePopup()

  return (
    <PopupContext.Provider value={data}>{props.children}</PopupContext.Provider>
  )
}
