import React from "react"
import { ChatDataProvider } from "./ChatDataProvider"
import { PopupDataProvider } from "./PopupDataProvider"
import { NotificationProcessor } from "components/Messaging/Notifications"

export const AppDataProvider = ({ children }) => {
  return (
    <ChatDataProvider>
      <NotificationProcessor />
      <PopupDataProvider>{children}</PopupDataProvider>
    </ChatDataProvider>
  )
}
