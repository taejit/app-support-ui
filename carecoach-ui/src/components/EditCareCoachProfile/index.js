import isEqual from "lodash/isEqual"
import React, { useEffect, useState } from "react"
import { useTranslation } from "react-i18next"
import { useFormik } from "formik"
import { useHistory, useLocation } from "react-router-dom"
import * as Yup from "yup"
import { Auth } from "aws-amplify"
import {
  Grid,
  Box,
  Button,
  TextField,
  Typography,
  Table,
  TableBody,
  TableRow,
  TableContainer,
  TableCell,
  InputLabel,
  Select,
  MenuItem,
} from "@material-ui/core"
import "date-fns"
import DateFnsUtils from "@date-io/date-fns"
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers"
import useStyles from "./styles"
import { PhoneRegExp, DateFormat } from "../../utils/constant"
import { getDateFormatForDB } from "../../utils/dateformatting"
import FormatPhoneNumber from "../../utils/FormatPhoneNumber"
import cognitoPhoneNumberFormat from "../../utils/cognitoPhoneNumberFormat"
import SnackbarMessage from "../SnackbarMessage"
import EditAvatar from "../EditAvatar"
import CareCoachAvatar from "../CareCoachAvatar"
import { ReactComponent as PenIcon } from "../../Assets/pen.svg"
import onboardingApiService from "../../services/Onboarding.service"
import LocationSearch from "../LocationSearch"
import { UpdateCarecoachBioProfile } from "../../services/CarecaochServices"
import getLocationDisplayText from "../../utils/getLocationDisplayText"
import { ReactComponent as CalendarIcon } from "../../Assets/calendar.svg"
import { ReactComponent as ErrorIcon } from "../../Assets/CarePlans/error-triangle.svg"
import FormControl from "@material-ui/core/FormControl"
import clsx from "clsx"

const EditCareCoachProfile = () => {
  const [editavatar, setEditAvatar] = useState(false)
  const pathLocation = useLocation()
  const profileData = pathLocation.profileData || {}
  const classes = useStyles()
  const { t } = useTranslation()
  const history = useHistory()
  const [showNoChangeMessage, setShowNoChangeMessage] = useState(true)
  const [showSnackbarMessage, setShowSnackbarMessage] = React.useState({
    open: false,
    message: "",
    success: false,
  })
  const [isOpen, setIsOpen] = React.useState(false)
  const [locationError, setLocationError] = useState(false)

  const getCognitoData = (editValues) => {
    return {
      given_name: editValues.firstName,
      family_name: editValues.lastName,
      email: editValues.email,
      phone_number: editValues.phone,
      "custom:pronoun": editValues.pronouns,
      "custom:location": editValues.location,
      "custom:calendly_username": editValues.calendlyUsername,
    }
  }

  const redirectToView = () => {
    history.push({
      pathname: "/dashboard/view",
      success: true,
    })
  }

  const showError = () => {
    setShowSnackbarMessage({
      open: true,
      message: "Could not update profile.",
      success: false,
    })
  }
  const pronounOptions = ["He, Him", "She, Her", "They, Them"]

  const formik = useFormik({
    initialValues: {
      firstName: profileData.firstName,
      lastName: profileData.lastName,
      pronouns:
        profileData.pronouns &&
        (pronounOptions.includes(profileData.pronouns)
          ? profileData.pronouns
          : "Other"),
      email: profileData.email,
      location: profileData.location,
      specialities: profileData.specialities,
      profileImage: profileData.profileImage,
      phone: profileData.phone,
      jobStartDate: profileData.jobStartDate,
      about: profileData.about,
      calendlyUsername: profileData.calendlyUsername,
      custompronoun:
        (profileData.pronouns &&
          (!pronounOptions.includes(profileData.pronouns)
            ? profileData.pronouns
            : "")) ||
        "",
    },
    validateOnChange: true,
    validationSchema: Yup.object({
      firstName: Yup.string()
        .required(t("edit-profile-first-name-required"))
        .nullable(),
      lastName: Yup.string()
        .required(t("edit-profile-last-name-required"))
        .nullable(),
      pronouns: Yup.string()
        .required(t("edit-profile-pronouns-required"))
        .nullable(),
      email: Yup.string()
        .email("edit-profile-invalid-email")
        .required(t("edit-profile-email-required"))
        .nullable(),
      phone: Yup.string()
        .matches(PhoneRegExp, t("phonenumberisnotvalid"))
        .required(t("edit-profile-phone-required"))
        .nullable(),
      location: Yup.string()
        .required(t("edit-profile-location-required"))
        .nullable(),
      about: Yup.string()
        .max(300, t("edit-profile-max-300-characters-allowed"))
        .required(t("edit-profile-about-required"))
        .nullable(),
      specialities: Yup.string()
        .required(t("edit-profile-credential-required"))
        .nullable(),
      jobStartDate: Yup.string()
        .required(t("edit-profile-start-date-required"))
        .nullable(),
      custompronoun: Yup.string().when("pronouns", (pronounsValue) => {
        if (pronounsValue == "Other") {
          return Yup.string().required(t("custompronoun-required")).nullable()
        } else {
          return Yup.string()
        }
      }),
    }),
    onSubmit: async (values) => {
      const copyValue = { ...values }
      copyValue.phone = cognitoPhoneNumberFormat(copyValue.phone)
      if (copyValue.pronouns == "Other") {
        copyValue.pronouns = copyValue.custompronoun
      }
      const cognitoData = getCognitoData(copyValue)
      const currentUser = await Auth.currentAuthenticatedUser()
      setLocationError(false)

      Auth.updateUserAttributes(currentUser, cognitoData)
        .then((resp) => {
          if (resp === "SUCCESS") {
            const careCoachProfile = {
              firstName: values.firstName,
              lastName: values.lastName,
              nickName: `${values.firstName} ${values.lastName}`,
              email: values.email,
              phone: values.phone.includes("+1")
                ? values.phone
                : cognitoPhoneNumberFormat(values.phone),
              location: values.location || "",
              pronouns:
                values.pronouns == "Other"
                  ? values.custompronoun
                  : values.pronouns,
              isActive: true,
              jobStartDate: getDateFormatForDB(values.jobStartDate),
              specialities: values.specialities,
              about: values.about,
              calendlyUsername: values.calendlyUsername || "",
              custompronoun: values.custompronoun || "",
              cognitoId: currentUser.attributes.sub,
            }
            onboardingApiService
              .syncCareCoach(careCoachProfile, profileData.uuid)
              .then((apiResponse) => {
                if (apiResponse && apiResponse.status === 202) {
                  const careCoachBioData = {
                    jobStartDate: getDateFormatForDB(values.jobStartDate),
                    specialities: values.specialities,
                    about: values.about,
                    calendlyUsername: values.calendlyUsername || "",
                  }
                  UpdateCarecoachBioProfile(
                    careCoachBioData,
                    profileData.uuid
                  ).then((response) => {
                    if (response && response.status === 200) {
                      redirectToView()
                    } else {
                      showError()
                    }
                  })
                } else {
                  showError()
                }
              })
          } else {
            // show fail message
            showError()
          }
        })
        .catch(() => {
          // show fail message
          showError()
        })
    },
  })

  const { setFieldValue } = formik
  useEffect(() => {
    setFieldValue(
      "phone",
      formik.values.phone ? FormatPhoneNumber(formik.values.phone) : ""
    )
  }, [setFieldValue, formik.values.phone])

  useEffect(() => {
    const valuesCopy = { ...formik.values }
    valuesCopy.phone = cognitoPhoneNumberFormat(valuesCopy.phone)
    setShowNoChangeMessage(isEqual(formik.initialValues, valuesCopy))
  }, [formik.values])

  const goToViewProfile = () => {
    history.push("/dashboard/view")
  }

  const editAvatarOpenDrawer = () => {
    setEditAvatar(true)
  }

  const onEditAvatarDrawerClose = () => {
    setEditAvatar(false)
  }

  const changeLocationValue = (location) => {
    setLocationError(!location)
    if (location && location.description) {
      const changedLocationValue = getLocationDisplayText(location.description)
      formik.setFieldValue("location", changedLocationValue)
    }
  }

  const emailBlock = (prop) => {
    return (
      <TableCell>
        <TextField
          id="email"
          label={t("email")}
          type="email"
          error={checkFieldError("email")}
          InputProps={{
            endAdornment: ErrorIconFunc("email"),
          }}
          {...formik.getFieldProps("email")}
        />
        {ErrorTextField("email")}
      </TableCell>
    )
  }

  const locationBlock = (prop) => {
    return (
      <>
        <LocationSearch
          updateLocation={changeLocationValue}
          selectedLocation={formik.values.location}
          error={checkFieldError("location") || locationError}
          {...formik.getFieldProps("ccLocation")}
        />
        <div className={classes.helperTextLocation}>
          {checkFieldError("location") || locationError ? (
            <div className={classes.error}>
              {t("edit-profile-location-required")}
            </div>
          ) : null}
        </div>
      </>
    )
  }

  const checkFieldError = (value) => {
    return formik.touched[value] && formik.errors[value]
  }
  // short circuit in JS
  const ErrorIconFunc = (value) => {
    return (
      checkFieldError(value) && (
        <div className={classes.errorIcon}>
          <ErrorIcon width="24" height="24" />
        </div>
      )
    )
  }

  const ErrorTextField = (value) => {
    return (
      <div className={classes.helperText}>
        {checkFieldError(value) ? (
          <div className={classes.error}>{formik.errors[value]}</div>
        ) : null}
      </div>
    )
  }

  return (
    <>
      <SnackbarMessage {...showSnackbarMessage} />
      <EditAvatar open={editavatar} onClose={onEditAvatarDrawerClose} />
      <form onSubmit={formik.handleSubmit} className={classes.editprofileform}>
        <Grid container>
          <Grid item xs className={classes.root}>
            <Grid container spacing={4} className={classes.formcontainer}>
              <Grid container key="header">
                <Grid item>
                  <Typography
                    className={classes.editmyprofile}
                    variant="h1"
                    id="myprofiletitl"
                    component="div"
                  >
                    {t("editmyprofile")}
                  </Typography>
                </Grid>
                <Grid item xs={3} className={classes.avatardisplay}>
                  <Box onClick={editAvatarOpenDrawer}>
                    <CareCoachAvatar displayOrEdit />
                    <Box className={classes.peniconposition}>
                      <PenIcon className={classes.penicondisplay} />
                    </Box>
                  </Box>
                </Grid>
              </Grid>

              <Grid key="aboutyou" item xs={8}>
                <Typography
                  className={classes.title}
                  variant="h6"
                  id="aboutyoutitle"
                  component="div"
                >
                  {t("profileviewaboutyou")}
                </Typography>
                <TableContainer>
                  <Table
                    className={classes.table}
                    aria-label="customized table"
                  >
                    <TableBody>
                      <TableRow>
                        <TableCell>
                          <TextField
                            id="firstName"
                            label={t("care-coach-profile-first-name")}
                            type="text"
                            error={checkFieldError("firstName")}
                            InputProps={{
                              endAdornment: ErrorIconFunc("firstName"),
                            }}
                            {...formik.getFieldProps("firstName")}
                          />
                          {ErrorTextField("firstName")}
                        </TableCell>

                        <TableCell>
                          <TextField
                            id="lastName"
                            label={t("care-coach-profile-last-name")}
                            type="text"
                            error={checkFieldError("lastName")}
                            InputProps={{
                              endAdornment: ErrorIconFunc("lastName"),
                            }}
                            {...formik.getFieldProps("lastName")}
                          />
                          {ErrorTextField("lastName")}
                        </TableCell>
                      </TableRow>

                      <TableRow>
                        <TableCell>
                          <FormControl
                            className={classes.formControl}
                            error={checkFieldError("pronouns")}
                          >
                            <InputLabel
                              id="pronouns-input"
                              className={classes.pronounslabel}
                            >
                              {t("pronouns")}
                            </InputLabel>
                            <Select
                              className={classes.pronounsselect}
                              labelId="pronouns-input"
                              id="pronouns"
                              selected={formik.values.pronouns}
                              value={formik.values.pronouns || "Others"}
                              inputProps={{
                                id: "pronounSelectInput",
                              }}
                              {...formik.getFieldProps("pronouns")}
                            >
                              <MenuItem value="He, Him">
                                {t("pronounshehis")}
                              </MenuItem>
                              <MenuItem value="She, Her">
                                {t("pronounssheher")}
                              </MenuItem>
                              <MenuItem value="They, Them">
                                {t("pronounstheytheir")}
                              </MenuItem>
                              <MenuItem value="Other">
                                {t("pronounsother")}
                              </MenuItem>
                            </Select>
                          </FormControl>
                          {ErrorTextField("pronouns")}
                        </TableCell>

                        {formik.values.pronouns == "Other" ? (
                          <TableCell>
                            <TextField
                              id="custompronoun"
                              label={t("pronountextbox")}
                              type="text"
                              value={formik.values.pronouns || ""}
                              error={checkFieldError("custompronoun")}
                              InputProps={{
                                endAdornment: ErrorIconFunc("custompronoun"),
                              }}
                              {...formik.getFieldProps("custompronoun")}
                            />
                            {ErrorTextField("custompronoun")}
                          </TableCell>
                        ) : null}

                        {formik.values.pronouns !== "Other"
                          ? emailBlock()
                          : null}
                      </TableRow>

                      <TableRow>
                        {formik.values.pronouns == "Other"
                          ? emailBlock()
                          : null}

                        <TableCell>
                          <TextField
                            className={classes.mobilephone}
                            id="phone"
                            label={t("mobilephone")}
                            type="text"
                            error={checkFieldError("phone")}
                            InputProps={{
                              endAdornment: ErrorIconFunc("phone"),
                            }}
                            {...formik.getFieldProps("phone")}
                          />
                          {ErrorTextField("phone")}
                        </TableCell>

                        {formik.values.pronouns !== "Other" ? (
                          <TableCell>{locationBlock()}</TableCell>
                        ) : null}
                      </TableRow>

                      {formik.values.pronouns == "Other" ? (
                        <TableRow>
                          <TableCell colSpan={2}>{locationBlock()}</TableCell>
                        </TableRow>
                      ) : null}

                      <TableRow>
                        <TableCell colSpan={2}>
                          <TextField
                            id="aboutstatement"
                            label={t("aboutstatement")}
                            type="text"
                            multiline
                            inputProps={{ maxLength: 300 }}
                            rowsMax={3}
                            error={checkFieldError("about")}
                            InputProps={{
                              endAdornment: ErrorIconFunc("about"),
                            }}
                            {...formik.getFieldProps("about")}
                            className={
                              checkFieldError("about")
                                ? classes.multiLineInput
                                : ""
                            }
                          />
                          <Typography component="div" className={classes.about}>
                            {checkFieldError("about") ? (
                              <div className={classes.error}>
                                {formik.errors.about}
                              </div>
                            ) : (
                              <>
                                {t("aboutstatementhelpertext")} &#8212;{" "}
                                {t("wewillhandlethat")}
                                <br />
                                {formik.values.about
                                  ? formik.values.about.length
                                  : "0"}{" "}
                                / {t("maxlength")}
                              </>
                            )}
                          </Typography>
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                </TableContainer>
              </Grid>
              <Grid key="whatyoudo" item xs={4}>
                <Typography
                  className={classes.title}
                  variant="h6"
                  id="whatyoudotitle"
                  component="div"
                >
                  {t("whatyoudo")}
                </Typography>
                <TableContainer>
                  <Table
                    className={classes.table}
                    aria-label="customized table"
                  >
                    <TableBody>
                      <TableRow>
                        <TableCell>
                          <TextField
                            id="credentials"
                            label={t("care-coach-profile-credentials")}
                            type="text"
                            error={checkFieldError("specialities")}
                            InputProps={{
                              endAdornment: (
                                <div className={classes.credentialsBlock}>
                                  {ErrorIconFunc("specialities")}
                                </div>
                              ),
                            }}
                            // helperText={t("credentialsexampletext")}
                            {...formik.getFieldProps("specialities")}
                          />
                          <div className={classes.helperText}>
                            {checkFieldError("specialities") ? (
                              <div className={classes.error}>
                                {formik.errors.specialities}
                              </div>
                            ) : (
                              t("credentialsexampletext")
                            )}
                          </div>
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>
                          <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                              disableToolbar
                              autoOk
                              variant="inline"
                              disableFuture
                              format={DateFormat}
                              margin="normal"
                              id="jobstartdate"
                              InputProps={{
                                readOnly: true,
                                onFocus: () => {
                                  setIsOpen(true)
                                },
                              }}
                              KeyboardButtonProps={{
                                onFocus: () => {
                                  setIsOpen(true)
                                },
                              }}
                              PopoverProps={{
                                disableRestoreFocus: true,
                                onClose: () => {
                                  setIsOpen(false)
                                },
                              }}
                              open={isOpen}
                              label={t("edit-profile-job-start-date")}
                              value={
                                formik.values.jobStartDate
                                  ? formik.values.jobStartDate
                                  : null
                              }
                              onChange={(value) => {
                                setIsOpen(false)
                                formik.setFieldValue("jobStartDate", value)
                              }}
                              keyboardIcon={
                                checkFieldError("jobStartDate") ? (
                                  <ErrorIcon width="24" height="24" />
                                ) : (
                                  <CalendarIcon
                                    onClick={() => setIsOpen(true)}
                                  />
                                )
                              }
                              className={
                                checkFieldError("jobStartDate")
                                  ? clsx(
                                      classes.errorField,
                                      classes.jobstartDate
                                    )
                                  : classes.jobstartDate
                              }
                              style={{ marginTop: 0, padding: 0 }}
                            />
                          </MuiPickersUtilsProvider>
                          <div className={classes.helperText}>
                            {checkFieldError("jobStartDate") ? (
                              <div className={classes.error}>
                                {formik.errors.jobStartDate}
                              </div>
                            ) : (
                              t("edit-carecoach-profile-start-date")
                            )}
                          </div>
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                </TableContainer>
              </Grid>
              <Grid key="footeraction" item xs={12} className={classes.footer}>
                <Grid item xs={6}>
                  {showNoChangeMessage && (
                    <Typography
                      component="div"
                      className={classes.nochangemademsg}
                    >
                      {t("edit-profile-no-changes-have-been-made")}
                    </Typography>
                  )}
                </Grid>
                <Box component="div" className={classes.buttongroup}>
                  <Button
                    className={classes.cancelbutton}
                    id="editprofilecancel"
                    onClick={() => goToViewProfile()}
                  >
                    {t("cancel")}
                  </Button>
                  <Button
                    disabled={showNoChangeMessage}
                    variant="outlined"
                    className={classes.savechangesbutton}
                    id="editprofilesavechanges"
                    color="primary"
                    type="submit"
                  >
                    {t("savechanges")}
                  </Button>
                </Box>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </form>
    </>
  )
}

export default EditCareCoachProfile
