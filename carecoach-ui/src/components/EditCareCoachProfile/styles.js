import { makeStyles } from "@material-ui/core"

const styles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      width: "100%",
      "& input": {
        fontSize: "16px",
        fontWeight: "500",
        color: theme.palette.darkBlue,
      },
    },
    "& .MuiTableCell-root": {
      borderBottom: "none",
      width: `50%`,
      maxWidth: `50%`,
    },
  },
  editprofileform: {
    height: `calc(100vh - 84px)`,
    display: `flex`,
    flexGrow: 1,
    marginLeft: `72px`,
    marginRight: `72px`,
  },
  formcontainer: {
    height: `100%`,
  },
  error: {
    color: "#b83055",
    height: `0px`,
    marginTop: "-1px",
  },
  helperText: {
    height: `0px`,
    marginTop: 3,
    lineHeight: 1.66,
    fontSize: "0.75rem",
    color: "rgba(0, 0, 0, 0.54)",
  },
  helperTextLocation: {
    height: `0px`,
    marginTop: 0,
    lineHeight: 1.66,
    fontSize: "0.75rem",
    color: "rgba(0, 0, 0, 0.54)",
  },
  pronounsselect: {
    width: "100%",
    fontWeight: "500",
    "& .MuiSelect-icon": {
      top: "calc(50% - 21px)",
      marginRight: -5,
    },
  },
  pronounslabel: {
    fontSize: "12px",
    fontWeight: 500,
  },
  editmyprofile: {
    paddingTop: "100px",
    paddingLeft: "32px",
    fontSize: "42px",
    fontWeight: "bold",
    letterSpacing: "-0.35px",
    color: theme.palette.darkBlue,
    display: "inline-block",
  },
  title: {
    fontSize: "14px",
    lineHeight: "1.5",
    fontWeight: "600",
    color: theme.palette.nightShade,
    paddingLeft: "16px",
  },
  nochangemademsg: {
    fontSize: "15px",
    fontWeight: "300",
    lineHeight: "1.4",
    color: theme.palette.twilight,
    paddingLeft: "16px",
  },
  savechangesbutton: {
    "&:hover": {
      backgroundColor: theme.palette.lanternGold,
      border: "none",
    },
    textTransform: "none",
    backgroundColor: theme.palette.lanternGold,
    borderRadius: "25px",
    padding: "12px 42px",
    border: "none",
    marginLeft: "15px",
    color: theme.palette.darkBlue,
    "&.MuiButton-root.Mui-disabled": {
      color: theme.palette.darkBlue,
      opacity: 0.5,
    },
    "&.MuiButton-outlined.Mui-disabled": {
      border: `none`,
    },
  },
  cancelbutton: {
    textTransform: "none",
    fontSize: "15px",
    color: theme.palette.morningGlory,
    fontWeight: 600,
    lineHeight: 1.4,
  },
  footer: {
    height: `84px`,
    marginTop: `auto`,
    borderTop: `solid 1.5px ${theme.palette.cloud}`,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  buttongroup: {
    display: "flex",
    justifyContent: "flex-end",
  },
  avatardisplay: {
    display: "inline-block",
    paddingTop: "80px",
    "& .profile-img-div-2": {
      float: "right",
    },
  },
  penicondisplay: {
    backgroundColor: theme.palette.morningGlory,
    height: "32px",
    width: "32px",
    borderRadius: "50%",
    padding: "9px",
    "& svg": {
      float: "right",
    },
  },
  peniconposition: {
    float: "right",
    left: "90px",
    top: "60px",
    position: "relative",
  },
  about: {
    fontSize: "12px",
    color: theme.palette.twilight,
    marginTop: 5,
  },
  formControl: {
    width: "100%",
  },
  errorField: {
    "& .MuiInput-underline": {
      "&:before": {
        borderBottomColor: theme.palette.darkError,
      },
    },
    "& .MuiInputLabel-root": {
      color: theme.palette.darkError,
    },
  },
  credentialsBlock: {
    marginRight: "0px",
  },
  errorIcon: {
    color: "#db687a !important",
    paddingBottom: "12px",
  },
  multiLineInput: {
    "& .MuiInputBase-multiline": {
      paddingBottom: "2px !important",
    },
  },
  jobstartDate: {
    "& button": {
      paddingRight: 0,
      paddingBottom: "20px",
      "& svg": {
        marginBottom: 20,
      },
    },
  },
}))

export default styles
