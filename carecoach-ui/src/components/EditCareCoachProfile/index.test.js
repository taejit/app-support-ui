import React from "react"
import { render, waitFor, fireEvent } from "@testing-library/react"
import { Auth } from "aws-amplify"
import EditCareCoachProfile from "."
import OnboardingApiService from "../../services/Onboarding.service"
import * as CareCoachServices from "../../services/CarecaochServices"

jest.mock("../CareCoachAvatar", () => {
  return {
    __esModule: true,
    default: () => {
      return <div />
    },
  }
})

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "./edit",
    profileData: {
      uuid: "c1260a8b-28f3-4b94-88ff-9bdb3da94f37",
      firstName: "John",
      lastName: "Coach",
      email: "user@example.com",
      phone: "6021234567",
      pronouns: "he/his",
      isActive: true,
      jobTitle: "Nurse",
      jobStartDate: "2020-12-31",
      location: "Locations tring",
      specialities: "String values",
      about: "A long description for the bio of the care coach in rich text",
      profileImage: "https://signedurl-profile-example.com",
      createdAt: "2020-12-31T07:53:20.908Z",
      updatedAt: "2020-12-31T07:53:20.908Z",
    },
  }),
  useHistory: () => ({ push: () => {} }),
}))

describe("EditCareCoachProfile test", () => {
  beforeEach(() => {
    jest.spyOn(Auth, "currentAuthenticatedUser").mockResolvedValue(
      Promise.resolve({
        attributes: {
          email: "bramsai@intraedge.com",
          email_verified: true,
          phone_number: "+12254789321",
          phone_number_verified: true,
          sub: "38041359-614f-4c44-9a32-23c3c80506c7",
          family_name: "kiran",
          given_name: "ramsai",
          "custom:location": "Hyderabad Telangana",
          "custom:pronoun": "he/his",
          "custom:uuid": "test-uuid",
          "custom:job_start_date": new Date("2021-01-30"),
          "custom:bio": "bio",
          "custom:credentials": "test-credentials",
        },
        username: "acc2ea0f-3a1f-49b9-8b19-7f555ece71d6",
      })
    )
    jest.spyOn(Auth, "updateUserAttributes").mockResolvedValue("SUCCESS")

    jest.spyOn(Auth, "currentUserInfo").mockResolvedValue(
      Promise.resolve({
        attributes: {
          email: "bramsai@intraedge.com",
          email_verified: true,
          phone_number: "+12254789321",
          phone_number_verified: true,
          sub: "38041359-614f-4c44-9a32-23c3c80506c7",
          family_name: "kiran",
          given_name: "ramsai",
          "custom:location": "Hyderabad Telangana Hyd",
          "custom:pronoun": "He, His",
        },
        username: "acc2ea0f-3a1f-49b9-8b19-7f555ece71d6",
      })
    )
    jest.spyOn(OnboardingApiService, "syncCareCoach").mockResolvedValue({
      status: 202,
    })
    jest
      .spyOn(CareCoachServices, "UpdateCarecoachBioProfile")
      .mockResolvedValue({
        status: 202,
      })
  })
  it("Load component with profile data", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<EditCareCoachProfile />)
    })
    const { queryByText } = wrapper
    expect(queryByText(/^editmyprofile$/)).toBeDefined()
  })

  it("Check for validation of field", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<EditCareCoachProfile />)
    })
    const { container, queryByText } = wrapper
    const firstName = container.querySelector("#firstName")
    await waitFor(() => {
      fireEvent.change(firstName, {
        target: {
          value: "",
          id: "firstName",
        },
      })
      fireEvent.blur(firstName, {
        target: {
          value: "",
          id: "firstName",
        },
      })
    })
    expect(queryByText(/^required$/)).toBeDefined()
  })

  it("Validation of lastname field", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<EditCareCoachProfile />)
    })
    const { container, queryByText } = wrapper
    const lastName = container.querySelector("#lastName")
    await waitFor(() => {
      fireEvent.change(lastName, {
        target: {
          value: "",
          id: "lastName",
        },
      })
      fireEvent.blur(lastName, {
        target: {
          value: "",
          id: "lastName",
        },
      })
    })
    expect(queryByText(/^required$/)).toBeDefined()
  })
  it("Validation of email field", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<EditCareCoachProfile />)
    })
    const { container, queryByText } = wrapper
    const email = container.querySelector("#email")
    await waitFor(() => {
      fireEvent.change(email, {
        target: {
          value: "",
          id: "email",
        },
      })
      fireEvent.blur(email, {
        target: {
          value: "",
          id: "email",
        },
      })
    })
    expect(queryByText(/^required$/)).toBeDefined()
  })
  it("Validation of phone field", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<EditCareCoachProfile />)
    })
    const { container, queryByText } = wrapper
    const mobilephone = container.querySelector("#phone")
    await waitFor(() => {
      fireEvent.change(mobilephone, {
        target: {
          value: "",
          id: "mobilephone",
        },
      })
      fireEvent.blur(mobilephone, {
        target: {
          value: "",
          id: "mobilephone",
        },
      })
    })
    expect(queryByText(/^required$/)).toBeDefined()
  })
  it("Validation of about field", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<EditCareCoachProfile />)
    })
    const { container, queryByText } = wrapper
    const aboutstatement = container.querySelector("#aboutstatement")
    await waitFor(() => {
      fireEvent.change(aboutstatement, {
        target: {
          value: "",
          id: "aboutstatement",
        },
      })
      fireEvent.blur(aboutstatement, {
        target: {
          value: "",
          id: "aboutstatement",
        },
      })
    })
    expect(queryByText(/^required$/)).toBeDefined()
  })
  it("Validation of specialities field", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<EditCareCoachProfile />)
    })
    const { container, queryByText } = wrapper
    const specialities = container.querySelector("#credentials")
    await waitFor(() => {
      fireEvent.change(specialities, {
        target: {
          value: "",
          id: "specialities",
        },
      })
      fireEvent.blur(specialities, {
        target: {
          value: "",
          id: "specialities",
        },
      })
    })
    expect(queryByText(/^required$/)).toBeDefined()
  })
  it("Should click on profile edit submit button", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<EditCareCoachProfile />)
    })
    const { container } = wrapper
    const editprofilesaveBtn = container.querySelector(
      "#editprofilesavechanges"
    )
    await waitFor(() => {
      fireEvent.submit(editprofilesaveBtn)
    })
    expect(Auth.currentAuthenticatedUser).toHaveBeenCalled()
  })

  it("Should click on profile edit submit button: go to view page", async () => {
    jest
      .spyOn(CareCoachServices, "UpdateCarecoachBioProfile")
      .mockResolvedValue({
        status: 200,
      })
    let wrapper
    await waitFor(() => {
      wrapper = render(<EditCareCoachProfile />)
    })
    const { container } = wrapper
    const editprofilesaveBtn = container.querySelector(
      "#editprofilesavechanges"
    )
    await waitFor(() => {
      fireEvent.submit(editprofilesaveBtn)
    })
    expect(Auth.currentAuthenticatedUser).toHaveBeenCalled()
  })
  it("Should click on profile edit submit button: error on sync care coach", async () => {
    jest.spyOn(OnboardingApiService, "syncCareCoach").mockResolvedValue({
      status: 400,
    })
    let wrapper
    await waitFor(() => {
      wrapper = render(<EditCareCoachProfile />)
    })
    const { container } = wrapper
    const editprofilesaveBtn = container.querySelector(
      "#editprofilesavechanges"
    )
    await waitFor(() => {
      fireEvent.submit(editprofilesaveBtn)
    })
    expect(Auth.currentAuthenticatedUser).toHaveBeenCalled()
  })

  it("Should click on profile edit submit button: error on saving to carecoach", async () => {
    jest.spyOn(Auth, "updateUserAttributes").mockResolvedValue("FAILED")

    let wrapper
    await waitFor(() => {
      wrapper = render(<EditCareCoachProfile />)
    })
    const { container } = wrapper
    const editprofilesaveBtn = container.querySelector(
      "#editprofilesavechanges"
    )
    await waitFor(() => {
      fireEvent.submit(editprofilesaveBtn)
    })
    expect(Auth.currentAuthenticatedUser).toHaveBeenCalled()
  })

  it("check JobStartDate", async () => {
    const inst = new Date()
    let wrapper
    await waitFor(() => {
      wrapper = render(<EditCareCoachProfile />)
    })
    const { container, queryByText } = wrapper

    const wrapperJobStartDate = container.querySelector("#jobstartdate")

    expect(wrapperJobStartDate).toBeDefined()

    expect(wrapperJobStartDate.value).toEqual("December 31st, 2020")

    const setIsOpen = jest.fn()

    await waitFor(() => {
      fireEvent.change(
        wrapperJobStartDate,
        {
          target: {
            value: inst,
            name: "jobStartDate",
            InputProps: {
              readOnly: true,
              onFocus: () => {
                setIsOpen()
              },
            },
          },
        },
        setIsOpen()
      )
      fireEvent.blur(wrapperJobStartDate, {
        target: {
          value: inst,
          name: "jobStartDate",
        },
      })
    })
    expect(wrapperJobStartDate.value).toBeDefined()
    expect(queryByText(/^required$/)).toBeDefined()
    expect(setIsOpen).toHaveBeenCalledTimes(1)
  })

  it("check JobStartDate with keyboard input", async () => {
    const inst = new Date()

    let wrapper
    await waitFor(() => {
      wrapper = render(<EditCareCoachProfile />)
    })
    const { container, queryByText } = wrapper

    const wrapperJobStartDate = container.querySelector("#jobstartdate")

    expect(wrapperJobStartDate).toBeDefined()

    expect(wrapperJobStartDate.value).toEqual("December 31st, 2020")

    const setIsOpen = jest.fn()

    await waitFor(() => {
      fireEvent.keyPress(
        wrapperJobStartDate,
        {
          target: {
            value: inst,
            name: "jobStartDate",
          },
        },
        setIsOpen()
      )
      fireEvent.blur(wrapperJobStartDate, {
        target: {
          value: inst,
          name: "jobStartDate",
        },
      })
    })
    expect(setIsOpen).toHaveBeenCalled()
    expect(wrapperJobStartDate.value).toBeDefined()
    expect(queryByText(/^required$/)).toBeDefined()
  })
})
