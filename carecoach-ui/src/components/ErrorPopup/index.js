import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { useTranslation } from "react-i18next"
import { Button, Box } from "@material-ui/core"
import ErrorDrawer from "../ErrorDrawer"
import useStyles from "./styles"
import GetButton from "../CreateCarePlan/GetButton"
import clsx from "clsx"

const ErrorPopup = ({
  open,
  onClose,
  title,
  description_1,
  description_2,
  description_3,
  handleActions,
  direction,
  standalone,
  showBackArrow,
  hideCrossClose,
  leftButtonText,
  rightButtonText,
}) => {
  const classes = useStyles()
  const [openDrawer, setOpenDrawer] = useState(open)

  useEffect(() => {
    setOpenDrawer(open)
  }, [open])

  const getbackstepActions = () => {
    return (
      <>
        <Button
          id="nevermindBtn"
          className={classes.backstepNevermindBtn}
          onClick={() => handleActions("no")}
        >
          {leftButtonText}
        </Button>
        <GetButton
          id="stepbackCtnbtn"
          btnText={rightButtonText}
          btnOnClick={() => handleActions("yes")}
          styles={classes.backstepContinue}
        />
      </>
    )
  }

  return (
    <ErrorDrawer
      open={openDrawer}
      onClose={onClose}
      anchor={direction ? direction : "left"}
      standalone={standalone ? standalone : false}
      showBackArrow={showBackArrow ? showBackArrow : false}
      hideCrossClose={hideCrossClose ? hideCrossClose : true}
    >
      <Box
        className={clsx(
          classes.backStepimg,
          standalone ? classes.standaloneCss : classes.normalCss
        )}
      >
        <img
          alt="careplanerror"
          className="careplanerror-img"
          src="./images/careplan/careplan-error.png"
        />
      </Box>
      <Box className={classes.backSteptitle}>{title}</Box>
      <Box className={classes.backStepdesc}>
        {description_1}
        <br /> {description_2} <br /> {description_3}
      </Box>
      <Box className={classes.backStepactions}>{getbackstepActions()}</Box>
    </ErrorDrawer>
  )
}

ErrorPopup.defaultProps = {}

ErrorPopup.propTypes = {
  open: PropTypes.bool.isRequired,
}

export default ErrorPopup
