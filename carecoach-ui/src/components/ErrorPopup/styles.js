import { makeStyles } from "@material-ui/core/styles"

const styles = makeStyles((theme) => ({
  backstepContinue: {
    backgroundColor: "#dc6879 !important",
    color: "white !important",
    marginLeft: 20,
    "&:hover": {
      backgroundColor: "#dc6879 !important",
      color: "white !important",
    },
  },
  backStepimg: {
    textAlign: "center",
    "& img": {
      width: 96,
      height: 96,
      margin: "109px 0 0 0",
    },
  },
  backSteptitle: {
    margin: "20px 0 12px 0",
    fontSize: 18,
    fontWeight: 500,
    lineHeight: 1.3,
    color: theme.palette.darkBlue,
    wordBreak: "break-word",
    whiteSpace: "initial",
  },
  backStepdesc: {
    fontSize: 14,
    fontWeight: 500,
    lineHeight: 1.5,
    color: theme.palette.nightShade,
    minHeight: 100,
    textAlign: "center",
  },
  backstepNevermindBtn: {
    color: theme.palette.morningGlory,
    "&:hover": {
      backgroundColor: "white",
    },
  },
  backStepactions: {
    marginTop: "auto",
    display: "flex",
    justifyContent: "space-between",
    // marginBottom: 24,
    width: "85%",
  },
  standaloneCss: {
    // marginTop: 50,
  },
  normalCss: {
    marginTop: 10,
  },
}))

export default styles
