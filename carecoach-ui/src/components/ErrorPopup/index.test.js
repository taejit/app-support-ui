import React from "react"
import { render, waitFor, fireEvent } from "@testing-library/react"
import ErrorPopup from "./index"

describe("EditAvatar component testing", () => {
  it("Should display ErrorPopup page", async () => {
    let wrapper
    await waitFor(async () => {
      wrapper = render(<ErrorPopup open />)
    })
    const { queryByText } = wrapper
    expect(queryByText(/^Nevermind$/)).toBeDefined()
    expect(queryByText(/^Continue$/)).toBeDefined()
  })

  it("Should call onclose of ErrorPopup page", async () => {
    let wrapper
    await waitFor(async () => {
      wrapper = render(<ErrorPopup open />)
    })
    const { container } = wrapper
    const wrapperDrawer = container.querySelector("Drawer")
    expect(wrapperDrawer).toBeDefined()
  })

  it("Should Display the data from Props", async () => {
    let wrapper
    let Open = true
    let testTitle = "Test title"
    let testDescription_1 = "Test Description 1"
    let testDescription_2 = "Test Description 2"
    let testDescription_3 = "Test Description 3"
    await waitFor(() => {
      wrapper = render(
        <ErrorPopup
          open={Open}
          title={testTitle}
          description_1={testDescription_1}
          description_2={testDescription_2}
          description_3={testDescription_3}
        />
      )
    })
    const { queryByText } = wrapper
    expect(queryByText(testTitle)).toBeDefined()
    expect(queryByText(testDescription_1)).toBeDefined()
    expect(queryByText(testDescription_2)).toBeDefined()
    expect(queryByText(testDescription_3)).toBeDefined()
  })

  it("Should Call the Action on NeverMind Button of Error Popup", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<ErrorPopup open={true} handleActions={jest.fn()} />)
    })
    const { container } = wrapper
    const nevermindBtn = container.querySelector("#nevermindBtn")
    expect(nevermindBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.click(nevermindBtn)
    })
  })
  it("Should Call the Action on Continue Button of Error Popup", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<ErrorPopup open={true} handleActions={jest.fn()} />)
    })
    const { container } = wrapper
    const continueBtn = container.querySelector("#stepbackCtnbtn")
    expect(continueBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.click(continueBtn)
    })
  })
})
