import "./index.scss"
import React, { useState, useEffect } from "react"
import { useTranslation } from "react-i18next"
import PropTypes from "prop-types"
import { Auth } from "aws-amplify"
import {
  Avatar,
  CircularProgress,
  Box,
  Typography,
  FormLabel,
  Button,
} from "@material-ui/core"
import ProfileImgService from "../../services/avatar.service"

/**
 *
 * displayOrEdit => true => display
 * displayOrEdit => false => edit also
 */
const OnboardingCareCoachAvatar = ({
  displayOrEdit,
  handleProfile,
  ProfError,
}) => {
  const { t } = useTranslation()
  const [UuidCareCoach, setUuidCareCoach] = useState(null)
  const [profilePicDetails, setProfilePicDetails] = useState({
    url: null,
    textType: "defaultText",
    isLoading: true,
  })

  const updateProfilePicDetails = (url, textType, isLoading) => {
    setProfilePicDetails({
      url: url || null,
      textType,
      isLoading,
    })
  }
  const getProfilePicture = async (uuid) => {
    updateProfilePicDetails(null, "defaultText", true)
    const imageData = await ProfileImgService.GetProfileImage(uuid)
    updateProfilePicDetails(
      imageData.profileImageUrl,
      imageData.status,
      imageData.isLoading
    )
  }

  const processProfilePic = async (event) => {
    try {
      updateProfilePicDetails(null, "defaultText", true)
      const uploadResponse = await ProfileImgService.ProcessProfileImage(
        event,
        UuidCareCoach
      )
      if (uploadResponse.status === "uploadSuccess") {
        updateProfilePicDetails(profilePicDetails.url, "uploadSuccess", false)
        getProfilePicture(UuidCareCoach)
      } else {
        updateProfilePicDetails(
          uploadResponse.profileImageUrl || null,
          uploadResponse.status || "uploadError",
          uploadResponse.isLoading || false
        )
      }
      handleProfile(uploadResponse)
    } catch (error) {
      updateProfilePicDetails(null, "uploadError", false)
      handleProfile(null, "uploadError", false)
    }
  }

  const nullifyImageClickEvent = (event) => {
    // eslint-disable-next-line no-param-reassign
    event.target.value = null
  }

  useEffect(() => {
    function getUserInfo() {
      return Auth.currentUserInfo()
    }
    getUserInfo().then((userInfo) => {
      if (userInfo) {
        const customUuid = userInfo.attributes["custom:uuid"]
        setUuidCareCoach(customUuid)
        getProfilePicture(customUuid)
      }
    })
  }, [])

  const displayAvatar = (
    <div className="profile-img-div-2">
      <svg id="svg1" width="100%" height="100%">
        <foreignObject x="8%" y="8%" width="100%" height="100%">
          <div className="profile-img-div">
            {!profilePicDetails.isLoading && profilePicDetails.url && (
              <Avatar
                src={profilePicDetails.url}
                alt="profile-img"
                className="upload-avatar upload-img"
                id="ccProfilePic"
              />
            )}
            {!profilePicDetails.isLoading && !profilePicDetails.url && (
              <img
                alt="avatar-icon"
                className="upload-avatar-img"
                id="ccProfilePic"
                src={
                  profilePicDetails.textType.includes("Error")
                    ? "./assets/images/onboarding/avatar-error.svg"
                    : "./assets/images/onboarding/avatar-default.svg"
                }
              />
            )}
            {profilePicDetails.isLoading && (
              <div className="profile-img-div">
                <Avatar className="upload-avatar" id="ccProfilePic">
                  <CircularProgress size="24px" />
                </Avatar>
              </div>
            )}
          </div>
        </foreignObject>
        <circle
          cx="51%"
          cy="51%"
          r="46"
          fill="none"
          stroke={
            profilePicDetails.url && !profilePicDetails.isLoading
              ? "#a96da3"
              : "#ffffff"
          }
          strokeWidth="1.5"
          strokeDashoffset="20"
          strokeLinecap="round"
          strokeDasharray="5 3 95 7 18 118"
        />
      </svg>
    </div>
  )

  const getErrorText = (id, line1, line2) => {
    return (
      <span className="error-text" id={id}>
        {line1}
        <div>{line2}</div>
      </span>
    )
  }

  return (
    <>
      {displayOrEdit ? (
        displayAvatar
      ) : (
        <div
          className={
            profilePicDetails.isLoading ? "upload-inprogress" : "avatar-display"
          }
        >
          <Box
            display="flex"
            flexDirection="row"
            alignItems="center"
            justifyContent="flex-start"
            pt="32%"
          >
            {displayAvatar}
            <Typography component="div" className="upload-typography">
              {profilePicDetails.textType === "uploadSuccess" && (
                <span id="uploadComplete">
                  {t("profileImgUploadComplete1")}
                  <div>{t("profileImgUploadComplete2")}</div>
                </span>
              )}
              {profilePicDetails.textType === "sizeError" && (
                <>
                  {getErrorText(
                    "sizeError",
                    t("profileImgSizeError1"),
                    t("profileImgSizeError2")
                  )}
                </>
              )}
              {profilePicDetails.textType === "extError" && (
                <>
                  {getErrorText(
                    "extError",
                    t("profileImgInvalidError1"),
                    t("profileImgInvalidError2")
                  )}
                </>
              )}
              {profilePicDetails.textType === "getError" && (
                <>
                  {getErrorText(
                    "extError",
                    t("profileImgGetError1"),
                    t("profileImgGetError2")
                  )}
                </>
              )}
              {profilePicDetails.textType === "defaultText" && (
                <>
                  {displayOrEdit ? (
                    <span id="defaultText">
                      {t("editmyprofilepicturetext")}
                    </span>
                  ) : (
                    <span
                      id="defaultText"
                      className={ProfError ? "error-text" : ""}
                    >
                      {t("profileImgDefaultText1")}
                      <div>{t("profileImgDefaultText2")}</div>
                      <div>{t("profile_Img_DefaultText_3")}</div>
                    </span>
                  )}
                </>
              )}
              {profilePicDetails.textType === "uploadError" && (
                <>
                  {getErrorText(
                    "extError",
                    t("profileImgUploadError1"),
                    t("profileImgUploadError2")
                  )}
                </>
              )}
            </Typography>
          </Box>
          <Box
            display="flex"
            flexDirection="row"
            alignItems="center"
            justifyContent="flex-start"
            pt="4%"
            pl="0.5em"
          >
            <input
              accept="image/gif, image/jpg, image/jpeg, image/png"
              id="profile-image-button-file"
              type="file"
              style={{ display: "none" }}
              onClick={(event) => {
                nullifyImageClickEvent(event)
              }}
              onChange={(event) => {
                processProfilePic(event)
              }}
            />
            <FormLabel
              className="upload-form-label"
              htmlFor="profile-image-button-file"
            >
              <Button
                variant="outlined"
                className="upload-button"
                id="ccUploadButton"
                component="span"
              >
                {profilePicDetails.isLoading && (
                  <CircularProgress
                    size="16px"
                    className="upload-button-loader"
                  />
                )}
                {!profilePicDetails.url
                  ? t("profileImgUploadBtn")
                  : t("profileImgChangeBtn")}
              </Button>
            </FormLabel>
            <Typography variant="subtitle1" className="upload-filesize-text">
              {t("profileImgExtText")}
            </Typography>
          </Box>
        </div>
      )}
    </>
  )
}

OnboardingCareCoachAvatar.propTypes = {
  displayOrEdit: PropTypes.bool.isRequired,
}

export default OnboardingCareCoachAvatar
