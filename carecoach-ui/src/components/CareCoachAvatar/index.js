import "./index.scss"
import React, { useState, useContext, useEffect } from "react"
import { useTranslation } from "react-i18next"
import PropTypes from "prop-types"
import { Auth } from "aws-amplify"
import {
  Avatar,
  CircularProgress,
  Box,
  Typography,
  FormLabel,
  Button,
} from "@material-ui/core"
import ProfileImgService from "../../services/avatar.service"
import AvtarContext from "../../Contexts/AvtarContext"

/**
 *
 * displayOrEdit => true => display
 * displayOrEdit => false => edit also
 */
const CareCoachAvatar = ({ displayOrEdit }) => {
  const { t } = useTranslation()
  const [careCoachUuid, setCareCoachUuid] = useState(null)
  const { AvtarImageData, setAvtarImageData } = useContext(AvtarContext)

  const [profileImgDetails, setProfileImgDetails] = useState({
    url: null,
    textType: "defaultText",
    isLoading: true,
  })

  //  Below function sets profile data from context API

  useEffect(() => {
    setProfileImgDetails(AvtarImageData)
  }, [AvtarImageData])

  const updateProfileImageDetails = (url, textType, isLoading) => {
    setAvtarImageData({
      url: url || null,
      textType,
      isLoading,
    })
  }

  const getAvatarImage = async (uuid) => {
    const imageData = await ProfileImgService.GetProfileImage(uuid)
    updateProfileImageDetails(
      imageData.profileImageUrl,
      imageData.status,
      imageData.isLoading
    )
  }

  const processProfileImage = async (event) => {
    try {
      updateProfileImageDetails(null, "defaultText", true)
      const uploadResponse = await ProfileImgService.ProcessProfileImage(
        event,
        careCoachUuid
      )
      if (uploadResponse.status === "uploadSuccess") {
        getAvatarImage(careCoachUuid)
      } else {
        updateProfileImageDetails(
          uploadResponse.profileImageUrl || null,
          uploadResponse.status || "uploadError",
          uploadResponse.isLoading || false
        )
      }
    } catch (error) {
      updateProfileImageDetails(null, "uploadError", false)
    }
  }

  const nullifyImageClickEvent = (event) => {
    // eslint-disable-next-line no-param-reassign
    event.target.value = null
  }

  useEffect(() => {
    function getUserInfo() {
      return Auth.currentUserInfo()
    }
    getUserInfo().then((userInfo) => {
      if (userInfo) {
        const customUuid = userInfo.attributes["custom:uuid"]
        setCareCoachUuid(customUuid)
      }
    })
  }, [])

  const displayAvatar = (
    <div className="profile-img-div-2">
      <svg id="svg1" width="100%" height="100%">
        <foreignObject x="8%" y="8%" width="100%" height="100%">
          <div className="profile-img-div">
            {!profileImgDetails.isLoading && profileImgDetails.url && (
              <Avatar
                src={profileImgDetails.url}
                alt="profile-img"
                className="upload-avatar upload-img"
                id="ccProfilePic"
              />
            )}
            {!profileImgDetails.isLoading && !profileImgDetails.url && (
              <img
                alt="avatar-icon"
                className="upload-avatar-img"
                id="ccProfilePic"
                src={
                  profileImgDetails.textType === "Error"
                    ? "./assets/images/onboarding/avatar-error.svg"
                    : "./assets/images/onboarding/avatar-default.svg"
                }
              />
            )}
            {profileImgDetails.isLoading && (
              <div className="profile-img-div">
                <Avatar className="upload-avatar" id="ccProfilePic">
                  <CircularProgress size="24px" />
                </Avatar>
              </div>
            )}
          </div>
        </foreignObject>
        <circle
          cx="51%"
          cy="51%"
          r="46"
          fill="none"
          stroke={
            profileImgDetails.url && !profileImgDetails.isLoading
              ? "#a96da3"
              : "#ffffff"
          }
          strokeWidth="1.5"
          strokeDashoffset="20"
          strokeLinecap="round"
          strokeDasharray="5 3 95 7 18 118"
        />
      </svg>
    </div>
  )

  const getErrorText = (id, line1, line2) => {
    return (
      <span className="error-text" id={id}>
        {line1}
        <div>{line2}</div>
      </span>
    )
  }

  return (
    <>
      {displayOrEdit ? (
        displayAvatar
      ) : (
        <div
          className={
            profileImgDetails.isLoading ? "upload-inprogress" : "avatar-display"
          }
        >
          <Box
            display="flex"
            flexDirection="row"
            alignItems="center"
            justifyContent="flex-start"
            pt="32%"
          >
            {displayAvatar}
            <Typography component="div" className="upload-typography">
              {profileImgDetails.textType === "uploadSuccess" && (
                <span id="uploadComplete">
                  {t("profileImgUploadComplete1")}
                  <div>{t("profileImgUploadComplete2")}</div>
                </span>
              )}
              {profileImgDetails.textType === "sizeError" && (
                <>
                  {getErrorText(
                    "sizeError",
                    t("profileImgSizeError1"),
                    t("profileImgSizeError2")
                  )}
                </>
              )}
              {profileImgDetails.textType === "extError" && (
                <>
                  {getErrorText(
                    "extError",
                    t("profileImgInvalidError1"),
                    t("profileImgInvalidError2")
                  )}
                </>
              )}
              {profileImgDetails.textType === "getError" && (
                <>
                  {getErrorText(
                    "extError",
                    t("profileImgGetError1"),
                    t("profileImgGetError2")
                  )}
                </>
              )}
              {profileImgDetails.textType === "defaultText" && (
                <>
                  {displayOrEdit ? (
                    <span id="defaultText" className="error-text">
                      {t("editmyprofilepicturetext")}
                    </span>
                  ) : (
                    <span id="defaultText">
                      {t("profileImgDefaultText1")}
                      <div>{t("profileImgDefaultText2")}</div>
                      <div>{t("profile_Img_DefaultText_3")}</div>
                    </span>
                  )}
                </>
              )}
              {profileImgDetails.textType === "uploadError" && (
                <>
                  {getErrorText(
                    "extError",
                    t("profileImgUploadError1"),
                    t("profileImgUploadError2")
                  )}
                </>
              )}
            </Typography>
          </Box>
          <Box
            display="flex"
            flexDirection="row"
            alignItems="center"
            justifyContent="flex-start"
            pt="4%"
            pl="0.5em"
          >
            <input
              accept="image/gif, image/jpg, image/jpeg, image/png"
              id="profile-image-button-file"
              type="file"
              style={{ display: "none" }}
              onClick={(event) => {
                nullifyImageClickEvent(event)
              }}
              onChange={(event) => {
                processProfileImage(event)
              }}
            />
            <FormLabel
              className="upload-form-label"
              htmlFor="profile-image-button-file"
            >
              <Button
                variant="outlined"
                className="upload-button"
                id="ccUploadButton"
                component="span"
              >
                {profileImgDetails.isLoading && (
                  <CircularProgress
                    size="16px"
                    className="upload-button-loader"
                  />
                )}
                {!profileImgDetails.url
                  ? t("profileImgUploadBtn")
                  : t("profileImgChangeBtn")}
              </Button>
            </FormLabel>
            <Typography variant="subtitle1" className="upload-filesize-text">
              {t("profileImgExtText")}
            </Typography>
          </Box>
        </div>
      )}
    </>
  )
}

CareCoachAvatar.propTypes = {
  displayOrEdit: PropTypes.bool.isRequired,
}

export default CareCoachAvatar
