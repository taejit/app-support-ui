import React from "react"
import clsx from "clsx"
import { Box, Typography, Checkbox, Link } from "@material-ui/core"
import { ReactComponent as ChainLinkIcon } from "../../Assets/chainlink.svg"
import { ReactComponent as EditIcon } from "../../Assets/pen.svg"
import useStyles from "./styles"

const EachTask = (props) => {
  const { name, link, isCompleted, onCheckboxClick, onEdit } = props
  const classes = useStyles()

  return (
    <>
      <Box className={classes.tasklistcontainer}>
        <Box
          id="task-checkbox"
          className={classes.leftside}
          onClick={() => onCheckboxClick(props)}
        >
          <Checkbox
            checked={isCompleted}
            color="primary"
            className={classes.checkbox}
            classes={{
              root: classes.checkboxborder,
              colorPrimary: classes.checkboxcolor,
            }}
          />
          <Typography
            className={
              isCompleted
                ? clsx(classes.taskname, classes.strikethrough)
                : classes.taskname
            }
            component="span"
          >
            {name}
          </Typography>
        </Box>
        <Box className={classes.rightside}>
          {link && (
            <Link
              target={`_blank`}
              rel="noreferrer noopener"
              type="text"
              href={link}
              variant="body2"
              className={classes.link}
            >
              <ChainLinkIcon />
            </Link>
          )}

          <EditIcon
            id="edit-task"
            data-testid="editTask"
            className={classes.trashcan}
            onClick={() => onEdit(props)}
          />
        </Box>
      </Box>
    </>
  )
}

export default EachTask
