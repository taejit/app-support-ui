import React from "react"
import renderer from "react-test-renderer"
import { render, waitFor, fireEvent } from "@testing-library/react"
import EachTask from "."

describe("EachTask component", () => {
  it("Component rendering, incomplete task", () => {
    const data = {
      name: "taskname",
      link: "no link",
      isCompleted: false,
    }
    const tree = renderer.create(<EachTask {...data} />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it("Component rendering, complete task", () => {
    const data = {
      name: "taskname",
      link: "no link",
      isCompleted: true,
    }
    const tree = renderer.create(<EachTask {...data} />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it("Component rendering, check task", async () => {
    const data = {
      name: "taskname",
      link: "no link",
      isCompleted: true,
      onCheckboxClick: jest.fn(),
    }
    let wrapper
    await waitFor(() => {
      wrapper = render(<EachTask {...data} />)
    })
    const { container } = wrapper
    const taskCheckBox = container.querySelector("#task-checkbox")
    await waitFor(() => {
      fireEvent.click(taskCheckBox)
    })
    expect(taskCheckBox).toBeDefined()
  })
})
