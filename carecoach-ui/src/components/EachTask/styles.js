import { makeStyles } from "@material-ui/core"

export default makeStyles((theme) => ({
  tasklistcontainer: {
    width: `100%`,
    height: `auto`,
    display: `inline-flex`,
    marginBottom: `12px`,
    flexDirection: `row`,
    "& .MuiCheckbox-colorPrimary.Mui-checked": {
      color: theme.palette.morningGlory,
    },
    alignItems: "flex-start",
  },
  checkboxcolor: {
    color: theme.palette.morningGlory,
  },
  leftside: {
    cursor: `pointer`,
    display: `inline-flex`,
    marginRight: `auto`,
    alignItems: `center`,
    maxWidth: `260px`,
    wordBreak: "break-word",
  },
  rightside: {
    display: `inline-flex`,
    alignItems: `center`,
    marginLeft: `auto`,
  },
  taskname: {
    fontSize: `15px`,
    fontWeight: `500`,
    lineHeight: `1.4`,
    color: theme.palette.darkBlue,
    display: `flex`,
    alignSelf: `flex-start`,
    marginTop: `5px`,
  },
  strikethrough: {
    textDecoration: `line-through`,
  },
  checkboxborder: {
    color: theme.palette.dove,
  },
  checkbox: {
    padding: `0`,
    margin: `0 15px 0 0`,
    display: `flex`,
    alignSelf: `end`,
    "& svg": {
      height: `32px`,
      width: `32px`,
    },
  },
  link: {
    margin: `6px`,
    height: `16px`,
    width: `16px`,
    cursor: `pointer`,
    alignItems: `center`,
    display: `flex`,
  },
  trashcan: {
    cursor: `pointer`,
    height: `16px`,
    width: `16px`,
    margin: `6px 0 6px 6px`,
    "&>g>g": {
      stroke: theme.palette.morningGlory,
      strokeWidth: `2px`,
    },
  },
}))
