import React, { useEffect, useState } from "react"
import { Auth } from "aws-amplify"
import ComponentRoutes from "../ComponentRoutes"
import Loading from "../Loading"
import { GetCarecoachProfile } from "../../services/CarecaochServices"

const PreOnboardingCheck = () => {
  const [showLoading, setShowLoading] = useState(true)
  const [redirect, setRedirect] = useState(null)

  useEffect(() => {
    async function performCheck() {
      const currentUser = await Auth.currentUserInfo()
      const { data } = await GetCarecoachProfile(
        currentUser.attributes["custom:uuid"]
      )
      const profileComplete =
        data &&
        [
          "firstName",
          "lastName",
          "email",
          "phone",
          "pronouns",
          "location",
          "jobStartDate",
          "specialities",
          "about",
          "profileImage",
        ].every((item) => {
          return !!data[item]
        })
      setRedirect(profileComplete ? "/dashboard" : "/welcome")
      setShowLoading(false)
    }
    performCheck()
  }, [])

  return (
    <>
      {showLoading && <Loading />}
      {redirect && <ComponentRoutes initialRedirect={redirect} />}
    </>
  )
}

export default PreOnboardingCheck
