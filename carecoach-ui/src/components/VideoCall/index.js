import React, { useEffect } from "react"
import { Box, Button, Typography } from "@material-ui/core"
import { OTSession, OTPublisher, OTSubscriber, OTStreams } from "opentok-react"
import { useTranslation } from "react-i18next"
import { useHistory, useParams, useLocation } from "react-router-dom"
import moment from "moment"

import useStyles from "./styles"
import FormStyles from "../CommonStyles/FormikFormStyles"
import { ReactComponent as CameraOffIcon } from "../../Assets/VideoCall/camera-off.svg"
import { ReactComponent as CameraIcon } from "../../Assets/VideoCall/camera.svg"
import { ReactComponent as MicrophoneIcon } from "../../Assets/VideoCall/microphone.svg"
import { ReactComponent as MicrophoneMuteIcon } from "../../Assets/VideoCall/microphone-mute.svg"
import { ReactComponent as HangupIcon } from "../../Assets/VideoCall/phone-hang-up.svg"
import * as DashboardApiService from "../../services/Dashboard.service"
import SnackbarMessage from "../SnackbarMessage"
import Loading from "../Loading"

const VideoCall = () => {
  const { scheduleUuid, carecoachUuid } = useParams()
  const [publisher, setPublisher] = React.useState({
    audioFallbackEnabled: true,
    publishAudio: true,
    publishVideo: false,
    showControls: false,
  })
  const [subscriber, setSubscriber] = React.useState({
    error: null,
    audio: false,
    video: false,
    showControls: false,
  })
  const [sessionHelper, setSessionHelper] = React.useState(null)
  const [showConfirmation, setShowConfirmation] = React.useState(false)
  const [isSubscriberJoined, setIsSubscriberJoined] = React.useState(false)
  const [timer, setTimer] = React.useState({
    minutes: "00",
    seconds: "00",
  })
  const [timerInterval, setTimerInterval] = React.useState(null)
  const classes = useStyles()
  const formStyles = FormStyles()
  const { t } = useTranslation()
  const history = useHistory()
  const pathLocation = useLocation()
  const scheduleData = pathLocation.scheduleData || {}
  const [showSnackbarMessage, setShowSnackbarMessage] = React.useState({
    open: false,
    message: "",
    success: false,
  })
  const [showLoading, setShowLoading] = React.useState(true)
  let totalDurationInSec = 0

  const updateCallTimings = async (requestObj) => {
    await DashboardApiService.updateCarecoachVideoSchedule(
      carecoachUuid,
      scheduleUuid,
      requestObj
    )
  }

  const goToDashboard = () => {
    updateCallTimings({ carecoach_call_ending_time: moment.utc() })
    history.push("/dashboard/home")
  }

  const startTimer = () => {
    ++totalDurationInSec
    const secondsCondition = Math.floor(totalDurationInSec % 60) > 9
    let seconds = Math.floor(totalDurationInSec % 60)
    if (!secondsCondition) {
      seconds = `0${Math.floor(totalDurationInSec % 60)}`
    }
    let minutes = Math.floor(totalDurationInSec / 60)
    if (!secondsCondition) {
      minutes = `0${Math.floor(totalDurationInSec / 60)}`
    }
    setTimer({
      seconds,
      minutes,
    })
  }

  const sessionEvents = {
    onError: (error) => {
      console.log(error, "Session error")
    },
    streamCreated: (event) => {
      setIsSubscriberJoined(true)
      setSubscriber({
        error: null,
        audio: event.stream.hasAudio,
        video: event.stream.hasVideo,
        showControls: false,
      })
    },
    streamDestroyed: (event) => {
      setIsSubscriberJoined(false)
    },
    sessionDisconnected: (event) => {
      setIsSubscriberJoined(false)
      clearInterval(timerInterval)
    },
    connectionDestroyed: (event) => {
      setPublisher({
        audioFallbackEnabled: true,
        publishAudio: true,
        publishVideo: false,
        showControls: false,
      })
      setSubscriber({
        error: null,
        audio: false,
        video: false,
        showControls: false,
      })
    },
  }

  const subscriberEvents = {
    videoDisabled: (event) => {
      setSubscriber({
        ...subscriber,
        video: false,
      })
    },
    videoEnabled: (event) => {
      setSubscriber({
        ...subscriber,
        video: true,
      })
    },
    audioBlocked: () => {
      setSubscriber({
        ...subscriber,
        audio: false,
      })
    },
    audioUnblocked: () => {
      setSubscriber({
        ...subscriber,
        audio: true,
      })
    },
  }

  const createCallSession = async () => {
    setShowLoading(true)
    const sessionData = await DashboardApiService.GetOpenTokSession(
      carecoachUuid,
      scheduleUuid
    )
    setShowLoading(false)
    if (sessionData.data) {
      setSessionHelper(sessionData.data)
      setTimerInterval(setInterval(startTimer, 1000))
      updateCallTimings({ carecoach_call_joining_time: moment.utc() })
    } else {
      setTimeout(() => {
        setShowSnackbarMessage({
          open: true,
          message: "Unable to join the video call. Please try again later.",
          success: false,
        })
      })
    }
  }

  const getSubscriberTemplate = () => {
    if (!isSubscriberJoined) {
      return (
        <Box className={classes.waitingText}>
          {scheduleData.caregiverName} {t("video-call-wait-text")}
        </Box>
      )
    }
    if (isSubscriberJoined && !subscriber.video) {
      let className = classes.displayCaregiver
      if (subscriber.video) {
        className = classes.displayNone
      }
      return (
        <Box className={className}>
          <Box className={classes.caregiverInitials}>
            {scheduleData.caregiverIntials}
          </Box>
          <Box className={classes.caregiverName}>
            {scheduleData.caregiverName}
          </Box>
        </Box>
      )
    }
    return <></>
  }

  useEffect(() => {
    clearInterval(timerInterval)
    createCallSession()
    return function cleanup() {
      clearInterval(timerInterval)
      updateCallTimings({ carecoach_call_ending_time: moment.utc() })
    }
  }, [])

  return (
    <Box className={classes.videoBox}>
      {showSnackbarMessage.open ? (
        <SnackbarMessage {...showSnackbarMessage} />
      ) : null}
      {showLoading && (
        <Box className={classes.loadingIcon}>
          <Loading />
        </Box>
      )}
      {!showLoading && (
        <Box>
          {sessionHelper && (
            <>
              {getSubscriberTemplate()}
              <OTSession
                apiKey={window._env_.Tok_Box_API_Key}
                eventHandlers={sessionEvents}
                sessionId={sessionHelper.sessionId}
                token={sessionHelper.token}
              >
                <OTPublisher
                  className={!publisher.publishVideo ? classes.displayNone : ""}
                  properties={publisher}
                />
                <OTStreams>
                  <OTSubscriber
                    className={
                      !subscriber.video && isSubscriberJoined
                        ? classes.displayNone
                        : ""
                    }
                    properties={subscriber}
                    eventHandlers={subscriberEvents}
                  />
                </OTStreams>
              </OTSession>
              {!showConfirmation && (
                <Box className={classes.controlPanel}>
                  <Box className={classes.publisherControls}>
                    <Button
                      className={
                        publisher.publishAudio
                          ? classes.validIcon
                          : classes.inValidIcon
                      }
                      onClick={() =>
                        setPublisher({
                          ...publisher,
                          publishAudio: !publisher.publishAudio,
                        })
                      }
                    >
                      {publisher.publishAudio ? (
                        <MicrophoneIcon />
                      ) : (
                        <MicrophoneMuteIcon />
                      )}
                    </Button>
                    <Button
                      className={
                        publisher.publishVideo
                          ? classes.validIcon
                          : classes.inValidIcon
                      }
                      onClick={() =>
                        setPublisher({
                          ...publisher,
                          publishVideo: !publisher.publishVideo,
                        })
                      }
                    >
                      {!publisher.publishVideo ? (
                        <CameraOffIcon />
                      ) : (
                        <CameraIcon />
                      )}
                    </Button>
                    <Button
                      data-testid="hangupIcon"
                      className={classes.hangupIcon}
                      onClick={() => setShowConfirmation(true)}
                    >
                      <HangupIcon />
                    </Button>
                    <Box>
                      <Typography className={classes.controlsText}>
                        {scheduleData.topic?.length > 25
                          ? `${scheduleData.topic.slice(0, 25)}...`
                          : scheduleData.topic}
                      </Typography>
                      <Typography className={classes.controlsSubscriptText}>
                        {timer.minutes}:{timer.seconds} /{" "}
                        {scheduleData.duration}:00
                      </Typography>
                    </Box>
                  </Box>
                </Box>
              )}
              {showConfirmation && (
                <Box className={classes.controlPanel}>
                  <Box
                    className={classes.publisherControls}
                    justifyContent="space-between"
                  >
                    <Box>
                      <Typography className={classes.controlsText}>
                        {scheduleData.topic.length > 20
                          ? `${scheduleData.topic.slice(0, 20)}...`
                          : scheduleData.topic}
                      </Typography>
                      <Typography className={classes.controlsSubscriptText}>
                        {t("video-call-end-text-subscript")}
                      </Typography>
                    </Box>
                    <Box paddingRight="16px">
                      <Button
                        id="backBtn"
                        onClick={() => setShowConfirmation(false)}
                        className={formStyles.nevermindbtn}
                      >
                        {t("nevermind-button-text")}
                      </Button>
                      <Button
                        id="cancel"
                        onClick={goToDashboard}
                        className={formStyles.endcallbtn}
                      >
                        {t("end-call-button-text")}
                      </Button>
                    </Box>
                  </Box>
                </Box>
              )}
            </>
          )}
          {!sessionHelper && (
            <Box
              className={`${classes.noSessionText} ${classes.displayCaregiver}`}
            >
              {t("video-call-join-error")}
            </Box>
          )}
        </Box>
      )}
    </Box>
  )
}

export default VideoCall
