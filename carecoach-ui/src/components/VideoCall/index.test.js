import React from "react"
import { render, waitFor, screen } from "@testing-library/react"

import * as DashboardApiService from "../../services/Dashboard.service"
import VideoCall from "."

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "/call",
  }),
  useParams: () => ({ scheduleUuid: "abc-123-hdfhj-1234" }),
}))

jest.mock("opentok-react")

describe("VideoCall test", () => {
  beforeEach(() => {
    jest.spyOn(DashboardApiService, "GetOpenTokSession").mockResolvedValue(
      Promise.resolve({
        data: {
          sessionId: "test session",
          token: "test token",
        },
      })
    )
    jest
      .spyOn(DashboardApiService, "updateCarecoachVideoSchedule")
      .mockResolvedValue(Promise.resolve({}))
  })
  afterEach(() => {
    jest.resetAllMocks()
  })
  it("Verify component render", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<VideoCall />)
    })
    expect(wrapper).toBeDefined()
  })

  it("Show components unable to join if error while session creation", async () => {
    jest.spyOn(DashboardApiService, "GetOpenTokSession").mockResolvedValue({})
    await waitFor(() => {
      render(<VideoCall />)
    })
    expect(screen.getByText(/^video-call-join-error$/)).toBeDefined()
  })
})
