import React from "react"
import PropTypes from "prop-types"
import { Box, Button, Typography } from "@material-ui/core"
import { useTranslation } from "react-i18next"

import { ReactComponent as PlusIcon } from "../../Assets/CarePlans/plus-icon.svg"
import { ReactComponent as SearchIcon } from "../../Assets/CarePlans/search-icon.svg"
import DrawerTitle from "../CommonStyles/DrawerTitle"
import FormStyles from "../CommonStyles/FormikFormStyles"
import useStyles from "./styles.js"

const AddOptions = (props) => {
  const drawerTitle = DrawerTitle()
  const formStyles = FormStyles()
  const classes = useStyles()
  const { t } = useTranslation()

  return (
    <>
      <Typography
        id="title"
        component="div"
        className={drawerTitle.secondaryTitle}
      >
        {props.title}
      </Typography>
      <Typography
        id="subscript"
        component="div"
        className={drawerTitle.subscript}
      >
        {props.subscript}
      </Typography>
      {props.description && (
        <Typography
          id="description"
          component="div"
          className={drawerTitle.description}
        >
          {props.description}
        </Typography>
      )}
      <Box
        className={drawerTitle.noOptionsDiv}
        onClick={() => props.addFromExisting()}
      >
        <SearchIcon />
        <Typography className={drawerTitle.noOptionsText}>
          {props.searchOption1Text}
        </Typography>
      </Box>
      <Box className={classes.orDiv}>
        <Box className={classes.divider} width="49%"></Box>
        <Typography className={classes.orText} width="2%">
          {t("or-text")}
        </Typography>
        <Box className={classes.divider} width="49%"></Box>
      </Box>
      <Box
        className={drawerTitle.noOptionsDiv}
        onClick={() =>
          props.addNewTaskFromScratch && props.addNewTaskFromScratch()
        }
      >
        <PlusIcon />
        <Typography className={drawerTitle.noOptionsText}>
          {props.searchOption2Text}
        </Typography>
      </Box>
      <Box className={drawerTitle.goBackDiv}>
        <Button className={formStyles.cancelbtn} onClick={props.backArrowClick}>
          {props.goBackText}
        </Button>
      </Box>
    </>
  )
}

AddOptions.defaultProps = {
  title: null,
  subscript: null,
  description: null,
  addFromExisting: () => {
    // intentional
  },
  addNewTaskFromScratch: () => {
    // intentional
  },
}

AddOptions.propTypes = {
  title: PropTypes.string,
  subscript: PropTypes.string,
  description: PropTypes.string,
  addFromExisting: PropTypes.func,
  addNewTaskFromScratch: PropTypes.func,
}
export default AddOptions
