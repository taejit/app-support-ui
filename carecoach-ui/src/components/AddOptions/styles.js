import { makeStyles } from "@material-ui/core"

export default makeStyles((theme) => ({
  orDiv: {
    padding: "12px 36px 20px 36px",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  divider: {
    height: "2px",
    backgroundColor: theme.palette.cloud,
  },
  orText: {
    color: theme.palette.dove,
    textAlign: "center",
    fontSize: "12px",
    fontWeight: "bold",
    padding: "0px 12px",
  },
}))
