import React from "react"
import { fireEvent, render, waitFor } from "@testing-library/react"
// import renderer from "react-test-renderer"
import AddOptions from "./index"

describe("AddOptions component testing", () => {
  it("Should display AddOptions page", async () => {
    let wrapper
    await waitFor(async () => {
      wrapper = render(<AddOptions />)
    })
    const { queryByText } = wrapper
    expect(queryByText(/^subscript$/)).toBeDefined()
  })
  it("When description present", async () => {
    const { queryByText } = render(
      <AddOptions {...{ description: "somedata" }} />
    )
    expect(queryByText(/^subscript$/)).toBeDefined()
  })
  it("Callback method test", async () => {
    const data = {
      description: "somedata",
      searchOption1Text: "option1",
      searchOption2Text: "option2",
      addFromExisting: jest.fn(),
      addNewTaskFromScratch: jest.fn(),
    }
    jest.spyOn(data, "addFromExisting")
    jest.spyOn(data, "addNewTaskFromScratch")
    const { queryByText } = render(<AddOptions {...data} />)
    const option1 = queryByText(/^option1$/)
    expect(option1).toBeDefined()
    await waitFor(() => {
      fireEvent.click(option1)
    })
    expect(data.addFromExisting).toHaveBeenCalled()
    const option2 = queryByText(/^option2$/)
    await waitFor(() => {
      fireEvent.click(option2)
    })
    expect(data.addNewTaskFromScratch).toHaveBeenCalled()
  })
})
