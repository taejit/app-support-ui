import { createMuiTheme } from "@material-ui/core/styles"

const theme = createMuiTheme({
  typography: {
    fontFamily: `Montserrat, -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen'`,
  },
  palette: {
    backgroundColorApp: "#24254e",
    white: "#fff",
    whiteShade: "#cac5e2",
    darkBlue: "#24254e",
    lanternGold: "#fbca71",
    brightShade: "#908cb5",
    brightBlue: "#5196e1",
    brightValid: "#0cc9a2",
    cloud: "#e7ecee",
    nightShade: "#505176",
    morningGlory: "#3c75c8",
    rose: "#db687a",
    twilight: "#737684",
    twinight: "#666975",
    dove: "#bec3c5",
    valid: "#07a689",
    darkError: "#b83055",
    ghost: "#edf2f4",
    skyBlue: "#9eccff",
  },
  overrides: {
    MuiFormLabel: {
      filled: {
        fontWeight: 500,
      },
    },
    MuiButton: {
      root: {
        textTransform: "none",
      },
    },
    MuiInputLabel: {
      shrink: {
        fontWeight: 500,
      },
    },
    MuiInput: {
      underline: {
        "&:hover:not($disabled):before": {
          borderBottomColor: "#24254e",
          height: "2px",
        },
      },
    },
  },
})

export default theme
