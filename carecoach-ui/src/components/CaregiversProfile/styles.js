import { makeStyles } from "@material-ui/core"

export default makeStyles((theme) => ({
  profilecontainer: {
    backgroundColor: theme.palette.white,
  },
  table: {
    marginLeft: "56px",
    marginBottom: "56px",
  },
  subtitle: {
    marginBottom: "20px",
    marginLeft: "72px",
  },
  theadercell: {
    width: "40%",
  },
  tags: {
    fontSize: "12px",
    fontWeight: "600",
    lineHeight: "1.5",
    borderRadius: "4px",
    backgroundColor: theme.palette.cloud,
    padding: "7px",
    margin: "5px",
    color: theme.palette.nightShade,
  },
}))
