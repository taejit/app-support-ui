import React, { useEffect, useState, useContext } from "react"
import { useTranslation } from "react-i18next"
import clsx from "clsx"
import {
  Grid,
  Box,
  Typography,
  TableContainer,
  Table,
  TableBody,
} from "@material-ui/core"
import FormatPhoneNumber from "../../utils/FormatPhoneNumber"
import getPronounsDisplay from "../../utils/getPronounsDisplay"
import { getFormattedDate } from "../../utils/dateformatting"
import CaregiversContext from "../../Contexts/CaregiversProfileContext"
import CaregiversPageTitle from "../CommonStyles/CaregiversPageTitle"
import StyledTableCell from "../CommonStyles/StyledTableCell"
import StyledTableRow from "../CommonStyles/StyledTableRow"
import TableSubTitle from "../CommonStyles/TableSubTitle"
import useStyles from "./styles"

const CaregiversProfile = () => {
  const [profile] = useState(useContext(CaregiversContext).caregiverProfile)
  const [caregiverProfileData, setCaregiversProfileData] = useState([])
  const [careReceipient, setCareReceipient] = useState([])
  const [firstName, setFirstName] = useState(null)
  const { t } = useTranslation()

  const pageTitleClasses = CaregiversPageTitle()
  const pageSubTitle = TableSubTitle()
  const classes = useStyles()

  const getCommunicationPreferenceDisplay = (commPreference) => {
    const preference = commPreference || "calls"
    return `${t(
      "caregiver-profile-communicatore-prefer"
    )} ${preference.toLowerCase()}`
  }

  const setAllProfileData = () => {
    const caregiverData = Object.entries({
      [t("caregiver-profile-full-name")]: `${profile.firstName} ${
        profile.lastName || ""
      }`,
      [t("caregiver-profile-goes-by")]: `${profile.nickName || ""}`,
      [t("pronouns")]: getPronounsDisplay(profile.pronouns),
      [t("employername")]: profile.employerName,
      [t("email")]: profile.email,
      [t("mobilephone")]: FormatPhoneNumber(profile.phone),
      [t("location")]: profile.location,
      [t("caregiver-profile-caregiving-role")]: profile.role,
      [t("caregiver-profile-time-spent-in-role")]: profile.timeSpentInRole,
      [t(
        "caregiver-profile-communication-preference"
      )]: getCommunicationPreferenceDisplay(profile.preferredCommunication),
    })

    const carerecipient = profile.recipient
    const carereceipientsData = Object.entries({
      [t("caregiver-profile-full-name")]: `${carerecipient.firstName} ${
        carerecipient.lastName || ""
      }`,
      [t("caregiver-profile-goes-by")]: `${carerecipient.nickName || ""}`,
      [t("pronouns")]: getPronounsDisplay(carerecipient.pronouns),
      [t("caregiver-profile-carereceipient-dob")]: getFormattedDate(
        carerecipient.dateOfBirth
      ),
      [t(
        "caregiver-profile-carereceipient-relation"
      )]: carerecipient.relationship,
      [t(
        "caregiver-profile-carereceipient-living-arrangement"
      )]: carerecipient.livingArrangement,
      [t("location")]: carerecipient.location,
      [t(
        "caregiver-profile-carereceipient-conditions"
      )]: carerecipient.conditions?.map((condition) => {
        return (
          <Box component="span" key={`${condition}`} className={classes.tags}>
            {condition}
          </Box>
        )
      }),
    })
    setFirstName(profile.firstName)
    setCaregiversProfileData(caregiverData)
    setCareReceipient(carereceipientsData)
  }
  /* eslint react-hooks/exhaustive-deps: 0 */
  useEffect(() => {
    function getData() {
      setAllProfileData()
    }
    getData()
  }, [])

  const getTableRow = (row, index) => {
    return (
      <StyledTableRow key={index}>
        <StyledTableCell
          key={`th${index}`}
          component="th"
          scope="row"
          className={classes.theadercell}
        >
          {`${row[0]}`}
        </StyledTableCell>
        <StyledTableCell key={`td${index}`} align="left" id="name">
          {row[1]}
        </StyledTableCell>
      </StyledTableRow>
    )
  }

  return (
    <Grid container className={classes.profilecontainer} key="profiletop">
      <Grid item xs={11} key="profileheading">
        <Typography
          className={pageTitleClasses.pagetitle}
          variant="h6"
          id="profiletitle"
          component="div"
        >
          <span className="capitalize">
            {profile.nickName ? profile.nickName : profile.firstName}
          </span>
          {"'s "}
          {t("caregivers-title-profile")}
        </Typography>
      </Grid>
      <Grid item xs={11} key="caregiver">
        <Typography
          className={clsx(pageSubTitle.subtitle, classes.subtitle)}
          variant="h6"
          id="profilecaregivertabletitle"
          component="div"
        >
          {t("caregivers-about-the-caregiver")}
        </Typography>
        <TableContainer className={classes.table}>
          <Table aria-label="customized table">
            <TableBody>
              {caregiverProfileData.map((row, index) => {
                return getTableRow(row, index)
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
      <Grid item xs={11} key="carerecipients">
        <Typography
          className={clsx(pageSubTitle.subtitle, classes.subtitle)}
          variant="h6"
          id="profilecarereceipienttabletitle"
          component="div"
        >
          {t("caregivers-about-the-loved-one")}
        </Typography>
        <TableContainer className={classes.table}>
          <Table aria-label="customized table">
            <TableBody>
              {careReceipient.map((row, index) => {
                return getTableRow(row, index)
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Grid>
  )
}

export default CaregiversProfile
