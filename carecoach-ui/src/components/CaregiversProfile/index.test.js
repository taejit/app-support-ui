import React from "react"
import { render } from "@testing-library/react"
import CaregiversProfileContext from "../../Contexts/CaregiversProfileContext"
import CaregiversProfile from "."

describe("Caregivers Profile page test", () => {
  it("Page loading test", () => {
    const caregiverProfile = {
      uuid: "c1260a8b-28f3-4b94-88ff-9bdb3da94f37",
      firstName: "John",
      lastName: "Coach",
      nickName: "Charlie",
      email: "example@lantern.care",
      phone: "+16021234567",
      employerName: "ABC Inc",
      pronouns: "he/him",
      location: "Gilbert, Arizona",
      role: "Primary Caregiver",
      timeSpentInRole: "Less than 6 months",
      recipient: {
        uuid: "c1260a8b-28f3-4b94-88ff-9bdb3da94f37",
        firstName: "John",
        lastName: "Coach",
        nickName: "Charlie",
        pronouns: "He/him",
        dateOfBirth: "2020-12-31",
        relationship: "he/him",
        location: "Gilbert, Arizona",
        livingArrangement: "Lives indepedently",
        livingArrangementDescription: "Lives among the wolves",
        conditions: ["Aging", "Diabetes", "Dementia", "Other1", "Other2"],
      },
    }
    const { queryAllByText } = render(
      <CaregiversProfileContext.Provider value={{ caregiverProfile }}>
        <CaregiversProfile />
      </CaregiversProfileContext.Provider>
    )
    const phone = queryAllByText("(602) 123-4567")
    expect(phone).toHaveLength(1)
  })
})
