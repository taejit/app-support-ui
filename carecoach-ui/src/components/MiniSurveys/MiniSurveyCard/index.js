import React from "react"
import PropTypes from "prop-types"
import { Box, Typography } from "@material-ui/core"
import useStyles from "../styles"

const MiniSurveyCard = (props) => {
  const classes = useStyles()

  return (
    <Box className={classes.grouping}>
      <Box className={classes.linedot}></Box>
      <Box className={classes.groupline}></Box>
      <Box className={classes.surveyCard} onClick={props.onClick}>
        <Typography
          className={`${classes[props.scoreClass]} ${classes.indicator}`}
        ></Typography>
        <Typography className={classes.surveyTitle}>{props.title}</Typography>
        <Typography className={classes.surveyScore}>{props.score}</Typography>
      </Box>
    </Box>
  )
}

MiniSurveyCard.propTypes = {
  title: PropTypes.string,
  score: PropTypes.number,
  scoreClass: PropTypes.string,
}

export default MiniSurveyCard
