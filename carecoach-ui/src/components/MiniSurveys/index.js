import React, { useContext, useEffect, useState } from "react"
import { useTranslation } from "react-i18next"
import { Box, Typography, Grid, CardMedia, Card } from "@material-ui/core"

import moment from "moment"

import CaregiversContext from "../../Contexts/CaregiversProfileContext"
import { GetCaregiversMiniSurveys } from "../../services/Caregivers.service"
import Survey from "./MiniSurveyCard"
import useStyles from "./styles"
import Loading from "../Loading"
import BurdenScoreBg from "../../Assets/Caregivers/burden-score-bg.png"
import { ReactComponent as IndeterminateLogo } from "../../Assets/Caregivers/indeterminate.svg"
import { ReactComponent as NoDataLogo } from "../../Assets/Caregivers/surveys-no-data.svg"
import ProgressCheck from "components/ProgressCheck"

const MiniSurveys = () => {
  const { t } = useTranslation()
  const classes = useStyles()
  const [surveysList, setSurveysList] = useState(null)
  const [showLoading, setShowLoading] = useState(true)
  const [isScoreIndeterminate, setIsScoreIndeterminate] = useState(false)
  const { caregiverProfile } = useContext(CaregiversContext)
  const [progressQuestions, setProgQuestions] = useState(null)

  useEffect(() => {
    const getMiniSurveys = async () => {
      setShowLoading(true)
      setSurveysList(null)
      const surveysResp = await GetCaregiversMiniSurveys(caregiverProfile.uuid)
      setShowLoading(false)
      if (
        surveysResp &&
        surveysResp.status === 200 &&
        surveysResp.data.length
      ) {
        checkForIndeterminateCase(surveysResp.data)
        setSurveysList(surveysResp.data)
      }
    }
    getMiniSurveys()
  }, [caregiverProfile])

  const getScoreText = (score) => {
    if (score < 5) return "light"
    else if (score < 15) return "mild"
    else return "heavy"
  }

  const checkForIndeterminateCase = (surveys) => {
    let recentSurveyDate = moment(surveys[0].createdAt)
    setIsScoreIndeterminate(moment().diff(recentSurveyDate, "days") > 30)
  }

  const getSurveyDate = (date) => {
    return moment(date).format("MMMM Do, YYYY")
  }

  const getScoreHeight = (score) => {
    return `${(score * 100) / 30}%`
  }

  const getBurdenText = (type) => {
    if (isScoreIndeterminate) {
      return t(`indeterminate-burden-${type}`)
    }
    return t(`${getScoreText(surveysList[0].overAllScore)}-burden-${type}`)
  }

  const getScoreMeter = () => {
    if (isScoreIndeterminate) {
      return <IndeterminateLogo className={classes.indeterminateLogo} />
    }
    return (
      <>
        <Box className={classes.completeScore}></Box>
        <Box
          className={classes.scoreFiller}
          height={getScoreHeight(surveysList[0].overAllScore)}
        >
          <Box
            borderRadius="6px"
            className={
              classes[
                `${getScoreText(surveysList[0].overAllScore)}BurdenIndicator`
              ]
            }
            height="100%"
          ></Box>
        </Box>
      </>
    )
  }

  const handleSurveyProgress = (survey) => {
    setProgQuestions(survey)
  }

  const handleClose = () => {
    setProgQuestions(null)
  }

  if (showLoading) {
    return (
      <Box className={classes.loadingIcon}>
        <Loading />
      </Box>
    )
  }

  return (
    <Box padding="72px">
      {progressQuestions && (
        <ProgressCheck
          backArrowClick={handleClose}
          onClose={handleClose}
          {...progressQuestions}
        />
      )}
      <Box>
        <Typography className={classes.greetingHeader}>
          <span className="capitalize">
            {caregiverProfile.nickName
              ? caregiverProfile.nickName
              : caregiverProfile.firstName}
          </span>
          's {t("progress-text")}
        </Typography>
      </Box>
      {surveysList ? (
        <Box className={classes.surveyBlock}>
          <Box className={classes.surveyLeftblock}>
            <Typography className={classes.subHeader}>
              {t("checks-text")}
            </Typography>
            {surveysList.map((survey) => {
              return (
                <Survey
                  key={survey.createdAt}
                  className={classes.surveyCard}
                  title={getSurveyDate(survey.createdAt)}
                  score={survey.overAllScore}
                  scoreClass={`${getScoreText(
                    survey.overAllScore
                  )}BurdenIndicator`}
                  onClick={() => handleSurveyProgress(survey)}
                />
              )
            })}
          </Box>
          <Box className={classes.surveyRightblock}>
            <Typography className={classes.subHeader}>
              {t("last-score-text")}
            </Typography>
            <Card className={classes.quoteArea}>
              <CardMedia className={classes.quoteBg} image={BurdenScoreBg}>
                <Box className={classes.scoreComments}>{getScoreMeter()}</Box>
                <Box className={classes.scoreComments}>
                  <Box className={classes.burdenText}>
                    {getBurdenText("text")}
                  </Box>
                  <Box className={classes.burdenDescription}>
                    {getBurdenText("description")}
                  </Box>
                </Box>
                <Box className={classes.scorePlacement}>
                  {!isScoreIndeterminate && (
                    <Typography>
                      <span
                        className={
                          classes[
                            `${getScoreText(
                              surveysList[0].overAllScore
                            )}BurdenColor`
                          ]
                        }
                      >
                        <span className={classes.overallScore}>
                          {surveysList[0].overAllScore}
                        </span>
                      </span>{" "}
                      / 30
                    </Typography>
                  )}
                </Box>
              </CardMedia>
            </Card>
          </Box>
        </Box>
      ) : (
        <Box className={classes.noSurveysBox}>
          <NoDataLogo />
          <Typography className={classes.noSurveysDisplayText}>
            {t("mini-survey-no-data-text")}
          </Typography>
          <Typography className={classes.noSurveysDisplayDescription}>
            {t("mini-survey-no-data-description")}
          </Typography>
        </Box>
      )}
    </Box>
  )
}

export default MiniSurveys
