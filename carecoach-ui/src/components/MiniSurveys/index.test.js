import React from "react"
import { render, waitFor, fireEvent, screen } from "@testing-library/react"

import MiniSurveys from "."
import * as CaregiversApiService from "../../services/Caregivers.service"
import moment from "moment"

describe("MiniSurveys test", () => {
  beforeEach(() => {
    jest
      .spyOn(CaregiversApiService, "GetCaregiversMiniSurveys")
      .mockResolvedValue({
        status: 200,
        data: [
          {
            uuid: 1,
            overallScore: 13,
            createdAt: "2020-12-31T07:53:20.908Z",
            responses: [
              {
                question: "Sample mini survey question goes here.",
                answer: "Answer to the mini survey question",
                questionIdentifier: "Q_HEALTH_ID",
                answerIdentifier: "A_HEALTH_ID",
                score: 1,
              },
            ],
          },
          {
            uuid: 2,
            overallScore: 4,
            createdAt: "2021-01-03T07:53:20.908Z",
            responses: [
              {
                question: "Sample mini survey question goes here.",
                answer: "Answer to the mini survey question",
                questionIdentifier: "Q_HEALTH_ID",
                answerIdentifier: "A_HEALTH_ID",
                score: 1,
              },
            ],
          },
          {
            uuid: 3,
            overallScore: 24,
            createdAt: "2021-04-17T07:53:20.908Z",
            responses: [
              {
                question: "Sample mini survey question goes here.",
                answer: "Answer to the mini survey question",
                questionIdentifier: "Q_HEALTH_ID",
                answerIdentifier: "A_HEALTH_ID",
                score: 1,
              },
            ],
          },
        ],
      })
  })
  afterEach(() => {
    jest.resetAllMocks()
  })
  it("Verify component show all data", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<MiniSurveys />)
    })
    const { queryByText } = wrapper
    expect(queryByText(/^progress-text$/)).toBeDefined()
  })

  it("Verify component Check Progress check data", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<MiniSurveys />)
    })
    const { queryByText } = wrapper

    const fireEvId = queryByText(
      moment("2020-12-31T07:53:20.908Z").format("MMMM Do, YYYY")
    )
    await waitFor(() => {
      fireEvent.click(fireEvId)
    })
    expect(screen.getByText(/^general-form-go-back$/)).toBeDefined()

    const goback = screen.getByText(/^general-form-go-back$/)
    await waitFor(() => {
      fireEvent.click(goback)
    })
  })
})
