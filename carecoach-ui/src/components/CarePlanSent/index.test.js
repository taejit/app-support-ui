import React from "react"
import { render, waitFor, fireEvent } from "@testing-library/react"
import { createMemoryHistory } from "history"
import { Router } from "react-router-dom"
import CarePlanSent from "."

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "/dashboard/caregivers/:caregiversUuid/careplan",
  }),
}))

test("renders CarePlanSent page", async () => {
  const history = createMemoryHistory()
  history.push = jest.fn()
  let wrapper
  await waitFor(() => {
    wrapper = render(
      <Router history={history}>
        <CarePlanSent />
      </Router>
    )
  })
  const { container } = wrapper
  const linkElement = container.querySelector(".view-care-plan")
  expect(linkElement).toBeDefined()
  await waitFor(async () => {
    fireEvent.click(linkElement)
  })
  expect(history.push).toHaveBeenCalled()
})
