import "./index.scss"
import React from "react"
import { useTranslation } from "react-i18next"
import { useHistory } from "react-router-dom"
import { useParams } from "react-router-dom"

function CarePlanSent() {
  const { t } = useTranslation()
  const history = useHistory()
  const { caregiversUuid, caregiversName } = useParams()

  const viewCarePlan = () => {
    history.push(`/dashboard/caregivers/${caregiversUuid}/careplan`)
  }

  return (
    <div className="CarePlansent">
      <img
        alt="careplansent"
        className="careplansent-img"
        src="./images/careplan/careplansent.png"
      />
      <div>
        <span id="careplansenttext" className="careplansent-text">
          {t("care-plan-sent")}
        </span>

        <div className="careplansent-desc">
          <p id="careplansentmsg" className="careplansent-message">
            {"Success! " +
              caregiversName +
              " will now be able to view and interact with the Care Plan you’ve just created. You’ll be able to add more Milestones as " +
              caregiversName +
              "’s caregiving needs evolve."}
          </p>
        </div>
        <button
          id="viewCarePlan"
          type="button"
          className="view-care-plan"
          onClick={viewCarePlan}
          data-amplify-analytics-on="click"
          data-amplify-analytics-name="click"
          data-amplify-analytics-attrs="page:carePlanSent,button:viewCarePlan"
        >
          {t("view-care-plan")}
        </button>
      </div>
    </div>
  )
}

export default CarePlanSent
